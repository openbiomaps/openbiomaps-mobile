# OpenBioMaps Mobile Application

This is an OpenBioMaps client application for Android mobile devices.

Main features:

- Users can log in to OpenBioMaps servers / projects
- Users can download project-specific (custom) data upload forms
- Users can collect observation data without internet connection
- The collected data can be uploaded to the servers manually or automatically
- Images or other files can be attached to observation records
- Observations can connected as list of observation and NULL observation also can be created
- The recorded data can be view on maps
- The recorded data can be exported
- Custom observation forms can be created on OpenBioMaps servers, providing rich capabilities for form customization and functionality.


## Developer notes

### Redux persist version handling

If any new state is introduced to the persisted redux structure, a new version is needed to be published with the next release with the necessary migration function.

#### Resources

[Tutorial for Redux Persist version handling](https://blog.bam.tech/developer-news/redux-persist-how-it-works-and-how-to-change-the-structure-of-your-persisted-store)

[Offical documentation](https://github.com/rt2zz/redux-persist)


#### Redux persist versions

| App version            | Redux Persist version |
| ---------------------- | --------------------- |
| OBM_mobile-r4_v1.5     | -1 (default)          |
| OBM_mobile-r4_v1.6     | 0                     |
| OBM_mobile-r4_v1.8.18  | 1                     |

### Observation list

There are two different supported observation method: **normal** and **list** type recording. 
- **Normal recording**: The user initiate normal recording when opening a new form, fill the required fields then save it by clicking the tick icon on the footer. In this case a single measurement object is created.
- **List recording**: The user initiate list recording by clicking the 'playlist-add' button on the footer in a form. In this case the user can record and save multiple measurements that will be bounded by an id (*sessionId*) unique to the current list. The observation list (called as session on the frontend) related states are stored in the servers/databaseses/observations/session reducer.  
While the session reducer state is not empty a session is active, so every measurement saved in a form, where the session was started, will belong the that session. *SessionId* is stored in the measurements' meta object.  
The *SessionId* is sent as *observation_list_id* to the api in the  measurement's meta if exists.  
(**Session in the frontend == observation_list in the backend**)

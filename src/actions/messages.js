import {
  MESSAGES_FETCH_START,
  MESSAGES_FETCH_SUCCESS,
} from './types';
import client from '../config/apiClient';


const fetchStart = () => ({
  type: MESSAGES_FETCH_START,
});

const fetchSuccess = messages => ({
  type: MESSAGES_FETCH_SUCCESS,
  payload: messages,
});

const makeSingleLoadMessagePromise = async (db) => {
  try {
    const response = await client.getMessageCount(db);
    if (Object.values(response).some(val => val !== 0)) {
      return { 
        personalMessage: response['personal message'], 
        commentNotification: response['comment notification'],
        onlyEmail: response['only email'],
        systemMessage: response['system message'],
        databaseName: db.databaseName, 
        userName: db.userName,
        projectUrl: db.projectUrl,
      };
    }
    return null;
  } catch (error) {
    return null;
  }
};

export const loadMessages = databaseList => async (dispatch, getState) => {
  const { network: { status: netStatus }, messages: { fetching } } = getState();

  if (['none', 'unknown'].includes(netStatus) || fetching) return;

  dispatch(fetchStart());
  
  const loadPromises = databaseList.map(async db => makeSingleLoadMessagePromise(db));
    
  const results = await Promise.all(loadPromises);
  
  setTimeout(() => dispatch(fetchSuccess(results.filter(result => result))), 1000);
};

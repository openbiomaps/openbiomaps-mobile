import * as Sentry from '@sentry/react-native';
import { 
  USER_FETCH_START, 
  USER_FETCH_STOP,
  USER_FETCH_ERROR, 
  LOGIN_SUCCESS, 
  LOGOUT_SUCCESS,
  CLEAR_AUTH_ERROR,
  SERVER_FETCH_ERROR,
  CLEAR_SERVER_ERROR,
} from './types';
import client from '../config/apiClient';
import ClientLogger, { ClientLoggerUseCases } from '../api/ClientLogger';
import I18n from '../i18n';
import { ERROR } from '../Constants';
import { performInServerList } from './server';
import { purgeDatabases } from './database';
import { syncMeasurements } from './measurement';
import { updateShortcutObservations } from './shortcutObservations';
import { updateMeasurementsMeta, updateMeasurementMetaList } from './measurementsMeta';
import { syncTrackLogs } from './tracklog';
import { showAlertDialog } from '../components/AlertDialog';

const fetchStart = () => ({
  type: USER_FETCH_START,
});

const fetchUserError = error => ({
  type: USER_FETCH_ERROR,
  error,
});

const fetchServerError = (error = null) => ({
  type: SERVER_FETCH_ERROR,
  error,
});

const clearServerError = () => ({
  type: CLEAR_SERVER_ERROR,
});

export const initAuthFetch = serverId => (dispatch) => {
  dispatch(performInServerList(serverId, { type: USER_FETCH_STOP }));
};

export const logout = serverId => (dispatch) => {
  dispatch(performInServerList(serverId, { type: LOGOUT_SUCCESS }));
};


export const tokenRefresh = (serverId, { syncMeasurementsAfter = false, syncTrackLogsAfter = false } = {}) =>
  async (dispatch, getState) => {
    const { network: { status }, servers: { data: serverList } } = getState();
    const currentServer = serverList.find(server => server.id === serverId);

    if (!currentServer?.user?.refreshToken) {
      dispatch(performInServerList(serverId, fetchUserError(ERROR.INVALID_REFRESH_TOKEN)));
      return;
    }
    const { 
      id: currentServerId,
      url: currentServerUrl,
      user: { refreshToken }, 
    } = currentServer;
  

    const hasNet = status && status !== 'none' && status !== 'unknown';

    if (!hasNet) {
      showAlertDialog({
        title: I18n.t('no_connection'), 
        message: I18n.t('sync_requires_net'),
        positiveLabel: I18n.t('ok'),
      });
      return;
    }

    dispatch(performInServerList(serverId, fetchStart()));
    try {
      dispatch(clearServerError());
      ClientLogger.log(ClientLoggerUseCases.FORCE_REFRESH, { refreshToken, serverUrl: currentServerUrl });
      const authResult = await client.refresh({ refreshToken, serverUrl: currentServerUrl });
      ClientLogger.log(ClientLoggerUseCases.DISPATCH_NEW_AUTHOBJECT, authResult);
      dispatch(performInServerList(currentServerId, {
        type: LOGIN_SUCCESS,
        payload: {
          ...authResult,
        },
      }));
      
      if (syncTrackLogsAfter) {
        dispatch(syncTrackLogs(currentServerId, syncTrackLogsAfter, syncMeasurementsAfter)); 
      } else if (syncMeasurementsAfter) {
        dispatch(syncMeasurements(currentServerId, syncMeasurementsAfter));
      }
    } catch (error) {   
      if ([ERROR.NETWORK_ERROR, ERROR.REQUEST_TIMEOUT, ERROR.LATE_RESPONSE_TRIAL_CALL].includes(error.message)) {
        dispatch(fetchServerError(error.message === ERROR.REQUEST_TIMEOUT ? ERROR.NETWORK_ERROR : error.message));
        dispatch(performInServerList(serverId, fetchUserError(null))); // stop fetching user
        Sentry.captureMessage(`
          Force refresh from tokenRefresh:
          errorList: ${[ERROR.NETWORK_ERROR, ERROR.REQUEST_TIMEOUT, ERROR.LATE_RESPONSE_TRIAL_CALL]}
          currentServerUrl: ${currentServerUrl}
          serverId: ${serverId}
          serverList: ${serverList}
        `);
      } else if (error.message === ERROR.INVALID_REFRESH_TOKEN || error.message === ERROR.EXPIRED_REFRESH_TOKEN) {
        Sentry.captureMessage(`
          Force refresh from tokenRefresh:
          errorList: ${[ERROR.INVALID_REFRESH_TOKEN, ERROR.EXPIRED_REFRESH_TOKEN]}
          currentServerUrl: ${currentServerUrl}
          serverId: ${serverId}
          serverList: ${serverList}
        `);
        dispatch(clearServerError());
        dispatch(performInServerList(serverId, fetchUserError(ERROR.INVALID_REFRESH_TOKEN)));
        ClientLogger.log(ClientLoggerUseCases.USER_LOGGED_OUT, serverId);
        dispatch(logout(serverId));
      } else {
        Sentry.captureMessage(`
          Force refresh from tokenRefresh:
          errorList (other error): ${error.message}
          currentServerUrl: ${currentServerUrl}
          serverId: ${serverId}
          serverList: ${serverList}
        `);
        dispatch(fetchServerError(error.message));
        dispatch(performInServerList(serverId, fetchUserError(error.message)));
      }
    }
  };

export const login = (userName, password, serverId) => async (dispatch) => {
  dispatch(performInServerList(serverId, fetchStart()));

  try {
    const authResult = await client.auth({ userName, password });

    dispatch(performInServerList(serverId, {
      type: LOGIN_SUCCESS,
      payload: {
        userName,
        ...authResult,
      },
    }));
  } catch (error) {
    const message = error.message === 'invalid_grant' ? I18n.t('invalid_username_password') : I18n.t('login_error');
    dispatch(performInServerList(serverId, fetchUserError(message)));
  }
};

export const clearAuthError = serverId => (dispatch) => {
  dispatch(performInServerList(serverId, {
    type: CLEAR_AUTH_ERROR,
  }));
};

/**
 * Init certain redux states when a new user is logged in.
 * @param {string} serverId - id of the target server
 */
export const purgePersistedStates = serverId => (dispatch) => {
  dispatch(updateMeasurementsMeta(serverId));
  dispatch(purgeDatabases(serverId));
  dispatch(updateShortcutObservations(serverId));
};

export const updatePersistedMetaStates = measurementsMeta => (dispatch) => {
  dispatch(updateMeasurementMetaList(measurementsMeta));
};

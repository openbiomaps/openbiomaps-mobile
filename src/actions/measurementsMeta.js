import StateFinder from '../StateFinder';
import {
  DELETE_SYNCED_MEASUREMENT,
  DELETE_UNSYNCED_MEASUREMENT,
  DELETE_ALL_SYNCED_MEASUREMENT,
  MEASUREMENT_CREATE_SUCCESS,
  MEASUREMENT_UPLOAD_SUCCESS,
  REPAIR_SYNCED_MEASUREMENT,
  REPAIR_UNSYNCED_MEASUREMENT,
} from './types';
import { getAllMeasurements } from '../utils';

export const measurementCreateSuccess = () => ({
  type: MEASUREMENT_CREATE_SUCCESS,
});

export const measurementUploadSuccess = (value = 1) => ({
  type: MEASUREMENT_UPLOAD_SUCCESS,
  value,
});

export const deleteSyncedMeasurements = (value = 1) => ({
  type: DELETE_SYNCED_MEASUREMENT,
  value,
});

export const deleteUnsyncedMeasurements = (value = 1) => ({
  type: DELETE_UNSYNCED_MEASUREMENT,
  value,
});

export const deleteAllSyncedMeasurements = () => ({
  type: DELETE_ALL_SYNCED_MEASUREMENT,
});

export const repairSyncedMeasurements = value => ({
  type: REPAIR_SYNCED_MEASUREMENT,
  value,
});

export const repairUnSyncedMeasurements = value => ({
  type: REPAIR_UNSYNCED_MEASUREMENT,
  value,
});

export const updateMeasurementsMeta = serverId => (dispatch, getState) => {
  const { servers: { data: serverList } } = getState();
  const server = new StateFinder(serverList).getServer(serverId).value;

  let syncedDelete = 0;
  let unsyncedDelete = 0;

  if (server.databases && server.databases.data) {
    server.databases.data.forEach((database) => {
      if (database.observations && database.observations.data) {
        database.observations.data.forEach((observation) => {
          if (observation.measurements && observation.measurements.data) {
            observation.measurements.data.forEach((measurement) => {
              if (measurement.data) {
                if (measurement.isSynced && !measurement.errors) {
                  syncedDelete++;
                } else {
                  unsyncedDelete++;
                }
              }
            });
          }
        });
      }
    });
  }
  if (syncedDelete > 0) {
    dispatch(deleteSyncedMeasurements(syncedDelete));
  }
  if (unsyncedDelete > 0) {
    dispatch(deleteUnsyncedMeasurements(unsyncedDelete));
  }
};

/* Update the number of synced/unsynced observations for the homeScreen blue banner
 *
 * */
export const updateMeasurementMetaList = measurementMeta => (dispatch, getState) => {
  const {
    servers: { data: serverList }, 
  } = getState();
  const { synced, unsynced } = measurementMeta;

  // check and restore measurementsMeta
  const allMeasurements = getAllMeasurements(serverList, false);
  let eSynced = 0;
  let eUnsynced = 0;
  allMeasurements.forEach(({ 
    measurement, 
  }) => {
    if (measurement.isSynced) {
      eSynced++;
    } else {
      eUnsynced++;
    }
  });
  if (eSynced !== synced) {
    dispatch(repairSyncedMeasurements(eSynced));
  } 
  if (eUnsynced !== unsynced) {
    dispatch(repairUnSyncedMeasurements(eUnsynced));
  }
};

import {
  ADD_TO_MEASUREMENT_LIST,
  REMOVE_FROM_MEASUREMENT_LIST,
  REMOVE_MORE_FROM_MEASUREMENT_LIST,
  PERFORM_IN_MEASUREMENT_LIST,
  SELECT_MEASUREMENT,
  SET_SYNC_STATUS,
  SYNC_DATA_START,
  UPDATE_MEASUREMENT,
  PERFORM_ON_THE_WHOLE_MEASUREMENT_LIST,
} from './types';
import { performInObservationList } from './observation';
import { removeStickinesses } from './form';
import { updateMeasurementsMetaNum } from './session';
import client from '../config/apiClient';
import { getFilteredMeasurements, groupMeasurements, parseDataToWkt, setMeasurementMetaRecordsTrackLog } from '../utils';
import { removeAllUnusedFiles, removeUnusedAttachedFilesOfMeasurement } from '../utils/reduxUtils';
import {
  measurementCreateSuccess,
  measurementUploadSuccess,
  deleteSyncedMeasurements,
  deleteUnsyncedMeasurements,
  deleteAllSyncedMeasurements,
} from './measurementsMeta';
import { ERROR, PIN_SAVE_LOGIC } from '../Constants';

const assembleDataForApi = (measurement) => {
  if (!measurement.data) return {};

  // Filter measurement data to remove redundant id props form the measurement.data object
  // in order to support previous versions where redundant id was introduced.
  const { id, ...measurementData } = measurement.data;
  return measurementData;
};

const assembleMetaForApi = (measurement) => {
  const { id, data, meta: { sessionId, appVersion: app_version } } = measurement;

  // Handling common meta props { id, app_version, observation_list_id(?) }
  let apiMeta = { id, app_version };
  if (sessionId) apiMeta.observation_list_id = sessionId;

  /* DYNAMIC_CODE_UPDATE >>>
   * custom_meta_params */
  /* <<< DYNAMIC_CODE_UPDATE */

  // Handling other meta props separately for regular measurement and null records:
  // For regular measurements: { form_version, gps_accuracy(?), started_at, finished_at }
  // For null record: { observation_list_start, observation_list_end }
  if (data) {
    const { formVersion, accuracy, startedAt, finishedAt, observationListNullRecord } = measurement.meta;
    apiMeta = {
      ...apiMeta,
      form_version: formVersion,
      started_at: startedAt,
      finished_at: finishedAt,
    };
    // Accuracy is an optional property. Add to the meta only if exists.
    if (accuracy) apiMeta.gps_accuracy = accuracy;
    // observationListNullRecord is an optional property.
    if (observationListNullRecord) apiMeta.observation_list_null_record = observationListNullRecord;
  } else {
    const { startedAt, finishedAt, measurementsNum, sessionTrackLog, recordedSessionTrackLog, trackLogInfo } = measurement.meta;
    let m = measurementsNum;
    if (measurementsNum) {
      m = measurementsNum - 1; // kivonjuk a metarekord számát a számlálóból. Ha egy a számláló, akkor az a metarekord
    }

    apiMeta = {
      ...apiMeta,
      observation_list_start: startedAt,
      observation_list_end: finishedAt,
      observation_list_track_log:
        sessionTrackLog?.map(({ longitude, latitude, timeStamp }) => ({ point: parseDataToWkt({ longitude, latitude }, 'point'), time_stamp: timeStamp })),
      recorded_session_tracklog: recordedSessionTrackLog,
      tracklog_info: trackLogInfo,
    };

    if (m) apiMeta.measurements_num = m;
    else apiMeta.measurements_num = 0; // only meta record has been created
  }

  return apiMeta;
};

/**
 * Add measurement object to the list
 * @param {object} serverId      - id of the target server
 * @param {object} databaseId    - id of the target database
 * @param {object} observationId - id of the target observation
 * @param {object} measurement   - new measurement object
 */
export const createMeasurement = (
  serverId,
  databaseId,
  observationId,
  measurement = {},
) => (dispatch) => {
  dispatch(performInObservationList(serverId, databaseId, observationId, {
    type: ADD_TO_MEASUREMENT_LIST,
    measurement,
    date: new Date(),
  }));

  // Dispatch action to increment unsynced measurement 
  // number in the measurement meta redux state. (no sessions)
  if (measurement.data) {
    dispatch(measurementCreateSuccess());
  }
};

/**
 * Perform an action in the observation list
 * @param {number} serverId - id of the target server
 * @param {number} databaseId - id of the target database
 * @param {number} observationId - id of the target observation
 * @param {number} measurementId - id of the target measurement
 * @param {object} action - the action what we want to perform in the observation list
 */
export const performInMeasurementList = (
  serverId,
  databaseId,
  observationId,
  measurementId,
  action,
) => (dispatch) => {
  dispatch(performInObservationList(serverId, databaseId, observationId, {
    type: PERFORM_IN_MEASUREMENT_LIST,
    id: measurementId,
    action,
  }));
};

export const performOnTheWholeMeasurementList = (
  serverId,
  databaseId,
  observationId,
  measurementIdArray,
  action,
) => (dispatch) => {
  dispatch(performInObservationList(serverId, databaseId, observationId, {
    type: PERFORM_ON_THE_WHOLE_MEASUREMENT_LIST,
    id: measurementIdArray,
    action,
  }));
};

export const updateMeasurement = (
  serverId,
  databaseId,
  observationId,
  measurement,
) => (dispatch) => {
  dispatch(performInMeasurementList(serverId, databaseId, observationId, measurement.id, {
    type: UPDATE_MEASUREMENT,
    measurement,
  }));
};

export const selectMeasurement = (databaseListId, observationListId, measurementId) => (dispatch) =>
  // eslint-disable-next-line implicit-arrow-linebreak
  dispatch(performInObservationList(databaseListId, observationListId, {
    type: SELECT_MEASUREMENT,
    id: measurementId,
  }));

export const syncDataStart = (serverId, databaseId, observationId, measurementId) => (dispatch) =>
  // eslint-disable-next-line implicit-arrow-linebreak
  dispatch(performInMeasurementList(serverId, databaseId, observationId, measurementId, {
    type: SYNC_DATA_START,
  }));

export const syncDataSuccess = (
  serverId,
  databaseId,
  observationId,
  measurementId,
  measurementErrors,
) => (dispatch) => dispatch(performInMeasurementList(serverId, databaseId, observationId, measurementId, {
  type: SET_SYNC_STATUS,
  payload: {
    errors: measurementErrors,
    status: true,
  },
}));

export const syncDataError = (
  serverId,
  databaseId,
  observationId,
  measurementId,
  measurementErrors = null,
) => (dispatch) => dispatch(performInMeasurementList(serverId, databaseId, observationId, measurementId, {
  type: SET_SYNC_STATUS,
  payload: {
    errors: measurementErrors,
    status: false,
  },
}));

/**
 * Prepare the promise which will ran and sync one measurement
 * @param {object} server - Selected server node of the hierarchy tree
 * @param {object} database - Selected database node of the hierarchy tree
 * @param {object} observation - Selected observation node of the hiearchy tree
 * @param {object} measurement - Current measurement object
 */
const syncMeasurementPromise = async ({
  server,
  database,
  observation,
  measurement,
}) => {
  // console.log('syncelek');
  const { id: serverId, url: serverUrl, user: { accessToken, refreshToken, requestTime, expiresIn } } = server;
  const { id: databaseId, projectUrl } = database;
  const { id: observationId, form: { dataTypes } } = observation;

  const { id: measurementId } = measurement;
  const apiData = assembleDataForApi(measurement);
  const apiMeta = assembleMetaForApi(measurement);

  const imageFields = dataTypes
    .filter((field) => field.type === 'file_id')
    .map((field) => field.column);

  const imageCount = imageFields.reduce(
    (acc, curr) => acc + (apiData[curr] ? apiData[curr].length : 0),
    0,
  );

  let errors = null;
  let result = null;
  try {
    if (imageCount) {
      result = await client.filePush({
        dataTypes,
        projectUrl,
        formId: observationId,
        measurement: apiData,
        meta: apiMeta,
        authObject: {
          serverId,
          serverUrl,
          accessToken,
          refreshToken,
          expiresIn,
          requestTime,
        },
      });
    } else {
      result = await client.dataPush({
        dataTypes,
        projectUrl,
        formId: observationId,
        measurement: apiData,
        meta: apiMeta,
        authObject: {
          serverId,
          serverUrl,
          accessToken,
          refreshToken,
          expiresIn,
          requestTime,
        },
      });
    }
  } catch (error) {
    // console.log('error a syncmeasurementben:');
    // console.log(error);

    try {
      Object.entries(error).forEach(([key, value]) => {
        error[key] = value['1'];
      });
      errors = error;
    } catch (err) {
      // console.log('hiba a object entriesben');
      errors = error;
    }
    // console.log('végleges errorok');
    // console.log(errors);
  }

  // There is no error, but it is not confirmed that the data is uploaded
  if (!errors && (result === 'undefined' || result === null || typeof result.status === 'undefined' || result.status !== 'success')) {
    errors = {};
    if (result !== null && typeof result.error !== 'undefined') {
      errors.message = result.error;
    } else if (result !== null && typeof result.message !== 'undefined') {
      errors.message = result.message;
    } else {
      let j = ERROR.UNKNOWN_ERROR;
      try {
        j = JSON.parse(result);
      } catch (e) {
        //    something else...
      }
      errors.message = j;
    }
  }

  /*
    This is how a result object will look like
    resultObject = {
    syncSucceeded: bool,
    serverId: null,
    databaseId: null,
    observationId: null,
    measurementId: null,
    payload: null,
    shouldIncreaseMeta: true
  };
  */

  const shouldIncreaseMeta = measurement.data !== null && measurement.data !== undefined;

  if (errors) {
    if (errors.message === ERROR.REQUEST_TIMEOUT || errors.message === ERROR.NETWORK_ERROR) {
      // If the data couldn't be uploaded but still correct
      return { syncSucceeded: false, serverId, databaseId, observationId, measurementId };
    }
    let resp = errors.message;
    try {
      resp = JSON.parse(errors.message);
    } catch (e) {
      //    plain text error message, e.g DATA_ALREADY_UPLOADED, File upload error
    }

    if (errors.message === ERROR.DATA_ALREADY_UPLOADED) {
      // move record to synced as successful data
      return { syncSucceeded: true, serverId, databaseId, observationId, measurementId, shouldIncreaseMeta };
    }
    // some error happened, which indicates the form is incorrect or invalid user data etc.
    return { syncSucceeded: false, serverId, databaseId, observationId, measurementId, payload: resp };
  }
  // everything is fine 
  return { syncSucceeded: true, serverId, databaseId, observationId, measurementId, shouldIncreaseMeta };
};

// Sync measurement only if it isn't in an opened observation list
const haveToSyncronize = ({
  observation,
  measurement,
}) => ((observation.session?.id) ? observation.session.id !== measurement.meta.sessionId : true);

/**
 * Try to sync one single measurement data
 * @param {object} server - Selected server node of the hierarchy tree
 * @param {object} database - Selected database node of the hierarchy tree
 * @param {object} observation - Selected observation node of the hiearchy tree
 * @param {object} measurement - Current measurement object
 * @param {bool}   syncManually - Determine syncronization method
 */
export const syncMeasurement = (
  server,
  database,
  observation,
  measurement,
  syncManually,
) => async (dispatch, getState) => {
  const { network: { status }, settings: { isAutoSyncEnabled } } = getState();
  const { id: serverId } = server;
  const { id: databaseId } = database;
  const { id: observationId } = observation;

  const shouldSyncAutomatically = isAutoSyncEnabled && status === 'wifi';
  const shouldSyncManually = syncManually && status !== 'none' && status !== 'unknown';

  // console.log('distinct sync');
  if (
    (shouldSyncAutomatically || shouldSyncManually) && haveToSyncronize({ observation, measurement })
  ) {
    dispatch(syncDataStart(serverId, databaseId, observationId, measurement.id));

    try {
      const result = await syncMeasurementPromise({
        server,
        database,
        observation,
        measurement,
      });
      if (result.syncSucceeded) {
        dispatch(syncDataSuccess(result.serverId, result.databaseId, result.observationId, result.measurementId));
        if (result.shouldIncreaseMeta) {
          dispatch(measurementUploadSuccess());
        }
      } else {
        dispatch(syncDataError(result.serverId, result.databaseId, result.observationId, result.measurementId, result.payload));
      }
    } catch (e) {
      // console.log('error a syncMeasurementben', e);
    }
  }
};

const resetStickinesses = (allMeasurements) => (dispatch) => {
  const groupedMeasurements = groupMeasurements(allMeasurements);

  Object.entries(groupedMeasurements).forEach(([serverId, serverVal]) => {
    Object.entries(serverVal).forEach(([databaseId, databaseVal]) => {
      Object.entries(databaseVal).forEach(([observationId]) => {
        dispatch(removeStickinesses(serverId, databaseId, observationId));
      });
    });
  });
};

const arrangeAndDispatchForAll = (measurementList, action) => (dispatch) => {
  const groupedMeasurements = groupMeasurements(measurementList);

  Object.entries(groupedMeasurements).forEach(([serverId, serverVal]) => {
    Object.entries(serverVal).forEach(([databaseId, databaseVal]) => {
      Object.entries(databaseVal).forEach(([observationId, measurementIdArray]) => {
        dispatch(performOnTheWholeMeasurementList(serverId, databaseId, observationId, measurementIdArray, action));
      });
    });
  });
};

const startSyncForAll = (list) => arrangeAndDispatchForAll(list, {
  type: SYNC_DATA_START,
});

const syncSuccessForAll = (list) => (dispatch) => {
  if (list.length === 0) {
    return;
  }
  dispatch(arrangeAndDispatchForAll(list, {
    type: SET_SYNC_STATUS,
    payload: {
      errors: null,
      status: true,
    },
  }));
  dispatch(measurementUploadSuccess(list.filter((item) => item.shouldIncreaseMeta).length));
};

const syncFailedButNoErrorForAll = (list) => arrangeAndDispatchForAll(list, {
  type: SET_SYNC_STATUS,
  payload: {
    errors: null,
    status: false,
  },
});

const syncMeasurementList = async (measurements) => {
  const promises = measurements.map(async (measurement) => syncMeasurementPromise(measurement));
  try {
    const results = await Promise.all(promises);
    return results;
  } catch (e) {
    // console.log('error A PROMISE ALLOS synclistben');
    // console.log(e);
    throw new Error(ERROR.UNKNOWN_ERROR);
  }
};

/** 
 * 1. collect the syncable measurements, then call SyncDataStart together
 * 2. upload the data async, and collect the responses and wait for them, then the same actions are be called together
 * 
 * */
/**
 * Sync all measurements belonging to the specified server
 * @param {number} serverId - id of the target server 
 * @param {bool}   syncManually - Determine syncronization method
 */
export const syncMeasurements = (serverId, syncManually = false) => async (dispatch, getState) => {
  const {
    network: { status },
    servers: { data: serverList },
    settings: { isAutoSyncEnabled, pinSaveLogic },
    trackLog: { trackLogList },
  } = getState();
  const filteredServers = serverList.filter((server) => server.id === serverId);

  const shouldSyncAutomatically = isAutoSyncEnabled && status === 'wifi';
  const shouldSyncManually = syncManually && status !== 'none' && status !== 'unknown';

  if (shouldSyncAutomatically || shouldSyncManually) {
    const filteredMeasurements = getFilteredMeasurements(
      filteredServers,
      true,
      (measurement) => !measurement?.isSynced,
    ).filter(haveToSyncronize);

    const syncableMeasurements = setMeasurementMetaRecordsTrackLog(filteredMeasurements, trackLogList);

    dispatch(startSyncForAll(syncableMeasurements));

    if (pinSaveLogic === PIN_SAVE_LOGIC.SAVE_SYNC_DEL) {
      dispatch(resetStickinesses(syncableMeasurements));
    }

    let i;
    for (i = 0; i < syncableMeasurements.length; i += 100) {
      const batchOfMeasurements = syncableMeasurements.slice(i, Math.min(i + 100, syncableMeasurements.length));
      try {
        // eslint-disable-next-line no-await-in-loop
        const results = await syncMeasurementList(batchOfMeasurements);

        dispatch(syncSuccessForAll(results.filter((res) => res.syncSucceeded)));
        dispatch(syncFailedButNoErrorForAll(results.filter((res) => !res.syncSucceeded && !res.payload)));

        // measurements could have various errors, if it is the case, i wouldnt really group the list by them, instead fetch them one by one.
        // this is the old way, and could lead to more rendering and refreshing, but i think grouping them wouldnt be more efficient. 
        const invalidMeasurements = results.filter((res) => res.payload);
        // TODO must decide, whether the error payload is a form related error, or a server related error, then fetch server related errors into the server error.
        invalidMeasurements.forEach(({
          serverId,
          databaseId,
          observationId,
          measurementId,
          payload,
        }) => dispatch(syncDataError(serverId, databaseId, observationId, measurementId, payload)));
      } catch (e) {
        // console.log(e);
        dispatch(syncFailedButNoErrorForAll(batchOfMeasurements));
      }
    }
  }
};

export const removeFromMeasurementList = (measurementData) => (dispatch) => {
  const {
    server,
    database,
    observation,
    measurement,
  } = measurementData;

  const { isSynced, errors, meta } = measurement;

  // Update measurementsMeta redux state
  if (isSynced && !errors) {
    if (!meta.observationListMetaRecord) {
      dispatch(deleteSyncedMeasurements());
    }
  } else if (!meta.observationListMetaRecord) {
    dispatch(deleteUnsyncedMeasurements());
  }

  dispatch(performInObservationList(server.id, database.id, observation.id, {
    type: REMOVE_FROM_MEASUREMENT_LIST,
    id: measurement.id,
  }));

  // update measurementsNum if an unsynced measurement is being deleted from a session.
  if (!isSynced && meta.sessionId) {
    dispatch(updateMeasurementsMetaNum(server.id, database.id, observation.id, meta.sessionId));
  }

  removeUnusedAttachedFilesOfMeasurement(measurementData);
};

export const removeAllFromMeasurementList = () /* measurementData */ => async (dispatch, getState) => {
  const {
    servers: { data: serverList },
  } = getState();

  /* const {
    server,
    database,
  } = measurementData; */
  // further improvement - remove data only from a specified server or database 
  // const filteredServers = serverList.filter(server => server.id === serverId);

  const filteredServers = serverList;
  const measurementsToDelete = getFilteredMeasurements(
    filteredServers, true, (measurement) => measurement?.isSynced && !measurement?.errors,
  );

  const groupedMeasurements = groupMeasurements(measurementsToDelete);
  dispatch(deleteAllSyncedMeasurements());

  Object.entries(groupedMeasurements).forEach(([serverId, serverVal]) => {
    Object.entries(serverVal).forEach(([databaseId, databaseVal]) => {
      Object.entries(databaseVal).forEach(([observationId, measurementIdArray]) => {
        dispatch(performInObservationList(serverId, databaseId, observationId, {
          type: REMOVE_MORE_FROM_MEASUREMENT_LIST,
          id: measurementIdArray,
        }));
      });
    });
  });

  removeAllUnusedFiles(false);
};

import {
  ADD_TO_DATABASE_LIST,
  REMOVE_FROM_DATABASE_LIST,
  PERFORM_IN_DATABASE_LIST,
  SELECT_DATABASE,
  DATABASE_FETCH_START,
  DATABASE_FETCH_ERROR,
  DATABASE_FETCH_SUCCESS,
  RESET_DATABASE_SELECTION,
  PURGE_DATABASE_DATA,
  UPDATE_DATABASE,
} from './types';
import client from '../config/apiClient';
import StateFinder from '../StateFinder';
import { performInServerList } from './server';
import { getUserState, formatStringToGeoJson } from '../utils';

const fetchStart = () => ({
  type: DATABASE_FETCH_START,
});

const fetchError = (error) => ({
  type: DATABASE_FETCH_ERROR,
  error,
});

/**
 * Load databases of the selected server
 * @param {object} selectedServer - Target server object
 */
export const loadDatabases = (selectedServer) => async (dispatch, getState) => {
  const { network: { status } } = getState();
  const { id: serverId, url: serverUrl } = selectedServer;
  const { accessToken, refreshToken, expiresIn, requestTime } = getUserState(selectedServer);

  // Dispatch fetch start if network connection is available
  if (status !== 'none' && status !== 'unknown') dispatch(performInServerList(serverId, fetchStart()));

  try {
    const result = await client.getProjectList({ 
      authObject: {
        serverId,
        serverUrl,
        accessToken,
        refreshToken,
        expiresIn,
        requestTime,
      },
    });

    const databases = result.map(({
      project_hash,
      project_description,
      stage,
      game,
      project_url,
    }) => ({
      id: project_hash,
      name: project_description,
      description: stage,
      game,
      settingsUrl: `${project_url}?login`,
      projectUrl: project_url,
    }));

    dispatch(performInServerList(serverId, {
      type: DATABASE_FETCH_SUCCESS,
      payload: databases,
    }));
  } catch (error) {
    dispatch(performInServerList(serverId, fetchError(error.message)));
  }
};

/**
 * Add empty database object to the list
 * @param {object} payload - the properties of the new database
 */
export const addToDatabaseList = (payload = {}) => ({
  type: ADD_TO_DATABASE_LIST,
  payload,
});

/**
 * Remove item from the list
 * @param {number} databaseId - id of the deleted database
 */
export const removeFromDatabaseList = (databaseId) => ({
  type: REMOVE_FROM_DATABASE_LIST,
  id: databaseId,
});

/**
 * Perform an action in the database list
 * @param {number} serverId - id of the target server
 * @param {number} databaseId - id of the target database
 * @param {object} action - the action what we want to perform in the database list
 */
export const performInDatabaseList = (serverId, databaseId, action) => (dispatch) => {
  dispatch(performInServerList(serverId, {
    type: PERFORM_IN_DATABASE_LIST,
    id: databaseId,
    action,
  }));
};

export const fetchProjectVars = (serverId, databaseId) => async (dispatch, getState) => {
  const { data: serverList } = getState().servers;
  const servers = new StateFinder(serverList);
  const { id, url, user } = servers.getServer(serverId).value;
  const { accessToken, refreshToken, expiresIn, requestTime } = user;
  const db = servers.getDatabase(databaseId).value;
  try {
    const result = await client.getProjectVars({ 
      projectUrl: db.projectUrl,
      authObject: {
        id,
        url,
        accessToken,
        refreshToken,
        expiresIn,
        requestTime,
      },
    });
    const fileSize = result.allowed_file_size;
    dispatch(performInServerList(serverId, {
      type: UPDATE_DATABASE,
      id: databaseId,
      payload: {
        ...db,
        projectFileSize: fileSize || db.projectFileSize,
        permanentSamplePlots: formatStringToGeoJson(result.permanent_sample_plots),
      },
    }));
  } catch (error) {
    dispatch(performInDatabaseList(serverId, databaseId, fetchError(error.message)));
  }
};

export const selectDatabase = (serverId, databaseId) => (dispatch) => {
  dispatch(performInServerList(serverId, {
    type: SELECT_DATABASE,
    id: databaseId,
    date: new Date(),
  })); 
  dispatch(fetchProjectVars(serverId, databaseId));
};

export const resetDatabaseSelection = () => (dispatch, getState) => {
  const { data: serverList } = getState().servers;

  serverList.forEach((server) => {
    if (server.isSelected) {
      dispatch(performInServerList(server.id, {
        type: RESET_DATABASE_SELECTION,
      }));
    }
  });
};

export const purgeDatabases = (serverId) => (dispatch) => {
  dispatch(performInServerList(serverId, {
    type: PURGE_DATABASE_DATA,
  }));
};

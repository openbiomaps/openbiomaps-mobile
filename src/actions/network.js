import { SET_NETWORK_STATUS, SET_NETWORK_ERROR } from './types';

export const changeNetworkStatus = networkType => ({
  type: SET_NETWORK_STATUS,
  payload: networkType,
});

export const changeNetworkError = (networkError = null) => ({
  type: SET_NETWORK_ERROR,
  payload: networkError,
});

import {
  ADD_TO_SHORTCUT_OBSERVATION_LIST,
  REMOVE_FROM_SHORTCUT_OBSERVATION_LIST,
} from './types';


/**
 * Update the shortcutObservations redux state.
 * @param {string} serverId     - REQ: Target server id.
 * @param {string} databaseId   - OPT: Target database id.
 * @param {array}  observations - OPT: Array of observations belonging to the provided server/database.
 */
export const updateShortcutObservations = (serverId, databaseId = null, observations = null) => (dispatch, getState) => {
  const { shortcutObservations } = getState();

  shortcutObservations.forEach((shortcutObservation) => {
    if (
      shortcutObservation.server.id === serverId &&
      (databaseId ? shortcutObservation.database.id === databaseId : true) &&
      (observations ? !observations.some(observation => observation.id === shortcutObservation.observation.id) : true)
    ) {
      dispatch({
        type: REMOVE_FROM_SHORTCUT_OBSERVATION_LIST,
        serverId: shortcutObservation.server.id,
        databaseId: shortcutObservation.database.id,
        observationId: shortcutObservation.observation.id,
      });
    }
  });
};

export const removeShortcutObservation = (serverId, databaseId, observationId) => (dispatch) => {
  dispatch({
    type: REMOVE_FROM_SHORTCUT_OBSERVATION_LIST,
    serverId,
    databaseId,
    observationId,
  });
};

export const addToShortcutObservationList = (server, database, observation) => (dispatch) => {
  if (!server || !database || !observation) {
    return;
  }

  dispatch({
    type: ADD_TO_SHORTCUT_OBSERVATION_LIST,
    payload: {
      server: {
        id: server.serverId,
        name: server.serverName,
      },
      database: {
        id: database.databaseId,
        name: database.databaseName,
        game: database.databaseGame
      },
      observation: {
        id: observation.id,
        name: observation.name,
        lastSelected: observation.lastSelected,
      },
    },
  });
};

export const createShortcut = (server, database, observation) => (dispatch) => {
  dispatch(addToShortcutObservationList(server, database, observation));
};

export const removeShortcut = (serverId, databaseId, observationId) => (dispatch) => {
  dispatch(removeShortcutObservation(serverId, databaseId, observationId));
};

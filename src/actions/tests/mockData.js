export const allDeletable = [
  {
    isSynced: true,
    id: 0,
    data: {
      comment: 'Test',
      datum: '2018-06-28T14:52:10.939Z',
      id: 0,
    },
    syncing: false,
    errors: null,
  },
  {
    isSynced: true,
    id: 1,
    data: {
      comment: 'Test',
      datum: '2018-06-28T14:52:10.939Z',
      id: 1,
    },
    syncing: false,
    errors: null,
  },
  {
    isSynced: true,
    id: 2,
    data: {
      comment: 'Test',
      datum: '2018-06-28T14:52:10.939Z',
      id: 2,
    },
    syncing: false,
    errors: null,
  },
  {
    isSynced: true,
    id: 3,
    data: {
      comment: 'Test',
      datum: '2018-06-28T14:52:10.939Z',
      id: 3,
    },
    syncing: false,
    errors: null,
  },
  {
    isSynced: true,
    id: 4,
    data: {
      comment: 'Test',
      datum: '2018-06-28T14:52:10.939Z',
      id: 4,
    },
    syncing: false,
    errors: null,
  },
  {
    isSynced: true,
    id: 5,
    data: {
      comment: 'Test',
      datum: '2018-06-28T14:52:10.939Z',
      id: 5,
    },
    syncing: false,
    errors: null,
  },
  {
    isSynced: true,
    id: 6,
    data: {
      comment: 'Test',
      datum: '2018-06-28T14:52:10.939Z',
      id: 6,
    },
    syncing: false,
    errors: null,
  },
  {
    isSynced: true,
    id: 7,
    data: {
      comment: 'Test',
      datum: '2018-06-28T14:52:10.939Z',
      id: 7,
    },
    syncing: false,
    errors: null,
  },
];

export const allDeletableExpected = [
  {
    isSynced: true,
    id: 7,
    data: {
      comment: 'Test',
      datum: '2018-06-28T14:52:10.939Z',
      id: 7,
    },
    syncing: false,
    errors: null,
  },
  {
    isSynced: true,
    id: 6,
    data: {
      comment: 'Test',
      datum: '2018-06-28T14:52:10.939Z',
      id: 6,
    },
    syncing: false,
    errors: null,
  },
  {
    isSynced: true,
    id: 5,
    data: {
      comment: 'Test',
      datum: '2018-06-28T14:52:10.939Z',
      id: 5,
    },
    syncing: false,
    errors: null,
  },
  {
    isSynced: true,
    id: 4,
    data: {
      comment: 'Test',
      datum: '2018-06-28T14:52:10.939Z',
      id: 4,
    },
    syncing: false,
    errors: null,
  },
  {
    isSynced: true,
    id: 3,
    data: {
      comment: 'Test',
      datum: '2018-06-28T14:52:10.939Z',
      id: 3,
    },
    syncing: false,
    errors: null,
  },
];

export const deleteOnlyOne = [
  {
    isSynced: false,
    id: 0,
    data: {
      comment: 'Test',
      datum: '2018-06-28T14:52:10.939Z',
      id: 0,
    },
    syncing: false,
    errors: null,
  },
  {
    isSynced: false,
    id: 1,
    data: {
      comment: 'Test',
      datum: '2018-06-28T14:52:10.939Z',
      id: 1,
    },
    syncing: false,
    errors: null,
  },
  {
    isSynced: true,
    id: 2,
    data: {
      comment: 'Test',
      datum: '2018-06-28T14:52:10.939Z',
      id: 2,
    },
    syncing: false,
    errors: {
      comment: 'error',
    },
  },
  {
    isSynced: false,
    id: 3,
    data: {
      comment: 'Test',
      datum: '2018-06-28T14:52:10.939Z',
      id: 3,
    },
    syncing: false,
    errors: null,
  },
  {
    isSynced: true,
    id: 4,
    data: {
      comment: 'Test',
      datum: '2018-06-28T14:52:10.939Z',
      id: 4,
    },
    syncing: false,
    errors: {
      comment: 'error',
    },
  },
  {
    isSynced: false,
    id: 5,
    data: {
      comment: 'Test',
      datum: '2018-06-28T14:52:10.939Z',
      id: 5,
    },
    syncing: false,
    errors: null,
  },
  {
    isSynced: false,
    id: 6,
    data: {
      comment: 'Test',
      datum: '2018-06-28T14:52:10.939Z',
      id: 6,
    },
    syncing: false,
    errors: null,
  },
  {
    isSynced: true,
    id: 7,
    data: {
      comment: 'Test',
      datum: '2018-06-28T14:52:10.939Z',
      id: 7,
    },
    syncing: false,
    errors: null,
  },
];

export const deleteOnlyOneExpected = [
  {
    isSynced: false,
    id: 6,
    data: {
      comment: 'Test',
      datum: '2018-06-28T14:52:10.939Z',
      id: 6,
    },
    syncing: false,
    errors: null,
  },
  {
    isSynced: false,
    id: 5,
    data: {
      comment: 'Test',
      datum: '2018-06-28T14:52:10.939Z',
      id: 5,
    },
    syncing: false,
    errors: null,
  },
  {
    isSynced: true,
    id: 4,
    data: {
      comment: 'Test',
      datum: '2018-06-28T14:52:10.939Z',
      id: 4,
    },
    syncing: false,
    errors: {
      comment: 'error',
    },
  },
  {
    isSynced: false,
    id: 3,
    data: {
      comment: 'Test',
      datum: '2018-06-28T14:52:10.939Z',
      id: 3,
    },
    syncing: false,
    errors: null,
  },
  {
    isSynced: true,
    id: 2,
    data: {
      comment: 'Test',
      datum: '2018-06-28T14:52:10.939Z',
      id: 2,
    },
    syncing: false,
    errors: {
      comment: 'error',
    },
  },
  {
    isSynced: false,
    id: 1,
    data: {
      comment: 'Test',
      datum: '2018-06-28T14:52:10.939Z',
      id: 1,
    },
    syncing: false,
    errors: null,
  },
  {
    isSynced: false,
    id: 0,
    data: {
      comment: 'Test',
      datum: '2018-06-28T14:52:10.939Z',
      id: 0,
    },
    syncing: false,
    errors: null,
  },
];

export const mock = measurements => ({
  databases: [
    {
      id: 9,
      name: 'test',
      game: 'off',
      description: 'test database',
      isSelected: true,
      observations: [{
        id: 92,
        name: 'test',
        isSelected: true,
        measurements,
      }],
    },
  ],
});

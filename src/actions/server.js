import {
  SELECT_SERVER,
  SERVER_FETCH_SUCCESS,
  SERVER_FETCH_START,
  SERVER_FETCH_ERROR,
  PERFROM_IN_SERVER_LIST,
  RESET_SERVER_SELECTION,
} from './types';
import client from '../config/apiClient';

const fetchStart = () => ({
  type: SERVER_FETCH_START,
});

const fetchError = error => ({
  type: SERVER_FETCH_ERROR,
  error,
});

export const selectServer = serverId => ({
  type: SELECT_SERVER,
  id: serverId,
  date: new Date(),
});

export const loadServers = () => async (dispatch, getState) => {
  const { network: { status } } = getState();

  // Dispatch fetch start if network connection is available
  if (status !== 'none' && status !== 'unknown') dispatch(fetchStart());

  try {
    const result = await client.getServerList();
    const servers = result
      .filter(server => server.domain)
      .map(({ domain, institute, server }) => ({
        id: domain,
        url: domain,
        name: institute || domain + '-' + server,
      }));

    dispatch({
      type: SERVER_FETCH_SUCCESS,
      payload: servers,
    });
  } catch (error) {
    dispatch(fetchError(error.message));
  }
};

/**
 * Perform an action in the server list
 * @param {number} serverId - id of the target server
 * @param {object} action - the action what we want to perform in the server list
 */
export const performInServerList = (serverId, action) => ({
  type: PERFROM_IN_SERVER_LIST,
  id: serverId,
  action,
});

export const resetServerSelection = () => ({
  type: RESET_SERVER_SELECTION,
});

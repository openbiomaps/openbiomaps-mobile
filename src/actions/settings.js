import { CHANGE_SETTINGS_DATA } from './types';
import I18n from '../i18n';

export const setLanguage = () => (dispatch, getState) => {
  const { language } = getState().settings;
  I18n.locale = language.id;
};

export const changeSettingsData = (key, value) => (dispatch) => {
  dispatch({
    type: CHANGE_SETTINGS_DATA,
    key,
    value,
  });

  // If key === 'language' the display language should be changed
  if (key === 'language') {
    dispatch(setLanguage());
  }
};

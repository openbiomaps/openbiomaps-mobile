import {
  USER_FETCH_START,
  USER_FETCH_ERROR,
  USER_FETCH_STOP,
  LOGIN_SUCCESS,
  LOGOUT_SUCCESS,
  CLEAR_AUTH_ERROR,
} from '../actions/types';

const initialState = {
  fetching: false,
  error: null,
  accessToken: null,
  refreshToken: null,
  userName: null,
  expiresIn: null,
  requestTime: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case USER_FETCH_START:
      return {
        ...state,
        error: null,
        fetching: true,
      };
    case USER_FETCH_STOP:
      return {
        ...state,
        fetching: false,
      };
    case USER_FETCH_ERROR:
      return {
        ...state,
        error: action.error,
        fetching: false,
      };
    case LOGIN_SUCCESS:
      return {
        ...state,
        error: null,
        fetching: false,
        ...action.payload,
      };
    case LOGOUT_SUCCESS:
      return {
        ...state,
        accessToken: null,
        refreshToken: null,
      };
    case CLEAR_AUTH_ERROR:
      return {
        ...state,
        error: null,
      };
    default:
      return state;
  }
};

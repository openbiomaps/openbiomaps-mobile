import { BACKUP_ERROR, ADD_BACKUP, REMOVE_BACKUP, REMOVE_BACKUPS, LOAD_BACKUP_LIST } from '../actions/types';

const initialState = {
  backups: [],
  error: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case LOAD_BACKUP_LIST:
    case ADD_BACKUP:
      return {
        ...state,
        backups: state.backups.concat(action.payload),
      };
    case REMOVE_BACKUP: {
      const newArray = state.backups.filter((item) => item !== action.backupId);
      return {
        ...state,
        backups: newArray,
      };
    }
    case REMOVE_BACKUPS: {
      const newArray = state.backups.filter((item) => !action.backupIdArray.includes(item));
      return {
        ...state,
        backups: newArray,
      };
    }
    case BACKUP_ERROR:
      return {
        ...state,
        error: action.error,
      };
    default:
      return state;
  }
};

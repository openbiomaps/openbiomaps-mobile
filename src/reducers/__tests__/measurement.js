import measurementReducer from '../database/observation/measurement';
import { UPDATE_MEASUREMENT } from '../../actions/types';

describe('measurement reducer', () => {
  it('should handle UPDATE_MEASUREMENT', () => {
    const state = {
      id: 0,
      isSynced: true,
      syncing: false,
      data: {
        field: 'field',
      },
      errors: {
        field: 'field error',
      },
    };

    const action = {
      type: UPDATE_MEASUREMENT,
      payload: {
        field: 'modified field',
        newField: 'new field',
      },
    };

    expect(measurementReducer(state, action)).toEqual({
      id: 0,
      isSynced: false,
      syncing: false,
      data: {
        field: 'modified field',
        newField: 'new field',
      },
      errors: null,
    });
  });
});

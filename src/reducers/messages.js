import {
  MESSAGES_FETCH_START,
  MESSAGES_FETCH_SUCCESS,
} from '../actions/types';

const INITIAL_STATE = {
  messages: [],
  fetching: false,
  error: null,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case MESSAGES_FETCH_START:
      return {
        ...state,
        fetching: true,
      };
    case MESSAGES_FETCH_SUCCESS:
      return {
        messages: action.payload,
        fetching: false,
        error: null,
      };
    default:
      return state;
  }
};

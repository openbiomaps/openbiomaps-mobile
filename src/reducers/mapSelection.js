import { SELECT_DATABASE_FOR_MAP, REMOVE_SELECTED_DATABASE_FOR_MAP } from '../actions/types';

const initialState = {
  selectedDatabaseForMap: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SELECT_DATABASE_FOR_MAP:
      return { selectedDatabaseForMap: action.payload };
    case REMOVE_SELECTED_DATABASE_FOR_MAP:
      return initialState;
    default:
      return state;
  }
};

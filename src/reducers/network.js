import { SET_NETWORK_STATUS, SET_NETWORK_ERROR } from '../actions/types';

const VALID_NET_STATUSES = [
  'none',
  'wifi',
  'cellular',
  'unknown',
  // Android only
  'bluetooth',
  'ethernet',
  'wimax',
];

const initialState = {
  status: null,
  error: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_NETWORK_STATUS: {
      const { payload } = action;
      const status = VALID_NET_STATUSES.indexOf(payload) !== -1 ? payload : 'unknown';
      return {
        ...state,
        status,
      };
    }
    case SET_NETWORK_ERROR: {
      const { payload } = action;
      return {
        ...state,
        error: payload,
      };
    }
    default:
      return state;
  }
};

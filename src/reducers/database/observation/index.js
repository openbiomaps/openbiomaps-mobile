import { combineReducers } from 'redux';
import { MEASUREMENT_LIST_TYPE, OBSERVATION_LIST_TYPE } from '../../../actions/types';

import form from './form';
import measurement from './measurement';
import list from '../../list';
import game from './game';
import session from './session';
import { stateMapper } from '../../itemProperty';
import lastMeasurement from './lastMeasurement';

const measurementList = list(measurement, MEASUREMENT_LIST_TYPE);

const initialState = {
  id: null,
  name: null,
  isSelected: false,
  lastSelected: null,
  formVersion: null,
  hidden: false,
};

const observationReducer = combineReducers({
  ...stateMapper(initialState, OBSERVATION_LIST_TYPE),
  form,
  game,
  measurements: measurementList,
  session,
  lastMeasurement,
});

export default observationReducer;

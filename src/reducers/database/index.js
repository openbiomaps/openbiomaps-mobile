import { combineReducers } from 'redux';
import { OBSERVATION_LIST_TYPE, DATABASE_LIST_TYPE } from '../../actions/types';

import list from '../list';
import { stateMapper } from '../itemProperty';
import observation from './observation';
import toplist from './toplist';

const observationList = list(observation, OBSERVATION_LIST_TYPE);

const initialState = {
  id: null,
  name: null,
  game: 'off',
  description: null,
  isSelected: false,
  lastSelected: null,
  settingsUrl: null,
  projectUrl: null,
  hidden: null,
  projectFileSize: null,
  permanentSamplePlots: null,
};

const databaseReducer = combineReducers({
  ...stateMapper(initialState, DATABASE_LIST_TYPE),
  observations: observationList,
  toplist,
});

export default databaseReducer;

import { TOPLIST_FETCH_SUCCESS, TOPLIST_FETCH_START, TOPLIST_FETCH_ERROR } from '../../actions/types';

const INITIAL_STATE = {
  data: [],
  fetching: false,
  error: null,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case TOPLIST_FETCH_START:
      return {
        ...state,
        fetching: true,
        error: null,
      };
    case TOPLIST_FETCH_SUCCESS:
      return {
        ...state,
        fetching: false,
        error: null,
        data: action.payload,
      };
    case TOPLIST_FETCH_ERROR:
      return {
        ...state,
        fetching: false,
        error: action.error,
      };
    default:
      return state;
  }
};


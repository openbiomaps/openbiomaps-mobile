import { Component } from 'react';
import { NativeModules, Platform } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import * as actions from './actions';

/**
 * Component to check the used and remaining RAM
 */
class MemoryHandler extends Component {
  constructor() {
    super();
    this.getMemoryUsage = () => {};
  }
  
  componentDidMount() {
    if (Platform.OS === 'android') {
      this.getMemoryUsage = setInterval(async () => {
        NativeModules.MemoryHandler.getCurrentMemoryUsage((remainingMem, usedMem) =>
          // eslint-disable-next-line implicit-arrow-linebreak
          this.props.setMemoryUsage(usedMem / (remainingMem + usedMem), remainingMem));
      }, 10000);
    }
  }
      
  componentWillUnmount() {
    clearInterval(this.getMemoryUsage);
  }

  render() {
    return null;
  }
}

MemoryHandler.propTypes = {
  setMemoryUsage: PropTypes.func.isRequired,
};

const mapDispatchToProps = (dispatch) => {
  const { setMemoryUsage } = actions;
  return bindActionCreators({
    setMemoryUsage,
  }, dispatch);
};

export default connect(null, mapDispatchToProps)(MemoryHandler);

import React from 'react';
import { View, Text } from 'react-native';
import { createStackNavigator, HeaderBackButton } from 'react-navigation-stack';
import ServerListNavigator from '../ServerListNavigator';
import DatabaseListNavigator from '../DatabaseListNavigator';
import ObservationListNavigator from '../ObservationListNavigator';
import ObservationGameNavigator from '../ObservationGameNavigator';
import {
  headerStyle,
  TouchableIcon,
  headerElementStyle,
  roundedButton,
  Row,
} from '../../components';
import I18n from '../../i18n';
import {
  HomeScreen,
  LoginScreen,
  CreateMeasurementScreen,
  EditMeasurementScreen,
  ObservationMapScreen,
  PositionInputScreen,
  Camera,
} from '../../screens';
import SettingsScreen from '../../screens/SettingsScreen';
import ExportScreen from '../../screens/ExportScreen';
import NavigationHandler from '../../NavigationHandler';
import ObservationCardsNavigator from '../ObservationCardsNavigator';
import MainTrackLogScreen from '../../screens/MainTrackLogScreen';
import TrackLogListNavigator from '../TrackLogListNavigator';

export default createStackNavigator(
  {
    Home: {
      screen: HomeScreen,
      navigationOptions: () => ({
        headerShown: false,
      }),
    },
    Settings: {
      screen: SettingsScreen,
      navigationOptions: () => ({
        headerTitle: () => (
          <View>
            <Text
              style={headerElementStyle.title}
              numberOfLines={1}
              adjustsFontSizeToFit={true}
            >
              {I18n.t('settings')}
            </Text>
          </View>
        ),
        headerLeft: () => (
          <HeaderBackButton
            style={headerElementStyle.leftButton}
            tintColor="black"
            labelVisible={false}
            onPress={() => NavigationHandler.navigate('Home')}
          />
        ),
        headerStyle: headerStyle,
        headerTitleAlign: 'center',
        headerBackTitleVisible: false,
        headerTintColor: 'black',
      }),
    },
    ServerList: {
      screen: ServerListNavigator,
      navigationOptions: () => ({
        headerTitle: () => (
          <View>
            <Text
              style={headerElementStyle.title}
              numberOfLines={1}
              adjustsFontSizeToFit={true}
            >
              {I18n.t('servers')}
            </Text>
            <Text
              style={headerElementStyle.subTitle}
              numberOfLines={1}
              adjustsFontSizeToFit={true}
            >
              {I18n.t('choose_server')}
            </Text>
          </View>
        ),
        headerLeft: () => (
          <HeaderBackButton
            style={headerElementStyle.leftButton}
            tintColor="black"
            labelVisible={false}
            onPress={() => NavigationHandler.navigate('Home')}
          />
        ),
        headerStyle: headerStyle,
        headerTitleAlign: 'center',
        headerBackTitleVisible: false,
        headerTintColor: 'black',
      }),
    },
    Login: {
      screen: LoginScreen,
      navigationOptions: ({ navigation }) => ({
        headerTitle: () => (
          <View>
            <Text
              style={headerElementStyle.title}
              numberOfLines={1}
              adjustsFontSizeToFit={true}
            >
              {navigation.state.params
                ? `${navigation.state.params.title}`
                : ''}
            </Text>
            <Text
              style={headerElementStyle.subTitle}
              numberOfLines={1}
              adjustsFontSizeToFit={true}
            >
              {I18n.t('login')}
            </Text>
          </View>
        ),
        headerLeft: () => (
          <HeaderBackButton
            style={headerElementStyle.leftButton}
            tintColor="black"
            labelVisible={false}
            onPress={() => NavigationHandler.navigate('Home')}
          />
        ),
        headerStyle: headerStyle,
        headerTitleAlign: 'center',
        headerBackTitleVisible: false,
        headerTintColor: 'black',
      }),
    },
    DatabaseList: {
      screen: DatabaseListNavigator,
      navigationOptions: ({ navigation }) => ({
        headerTitle: () => (
          <View>
            <Text
              style={headerElementStyle.title}
              numberOfLines={1}
              adjustsFontSizeToFit={true}
            >
              {navigation.state.params
                ? `${navigation.state.params.title}`
                : ''}
            </Text>
            <Text
              style={headerElementStyle.subTitle}
              numberOfLines={1}
              adjustsFontSizeToFit={true}
            >
              {I18n.t('database_screen_title')}
            </Text>
          </View>
        ),
        headerLeft: () => (
          <HeaderBackButton
            style={headerElementStyle.leftButton}
            tintColor="black"
            labelVisible={false}
            onPress={() => NavigationHandler.navigate('Home')}
          />
        ),
        headerRight: () => (
          <TouchableIcon
            style={headerElementStyle.rightButton}
            name="more-vert"
            size={24}
            color="black"
            onPress={navigation.getParam('toggleMenu')}
          />
        ),
        headerStyle: headerStyle,
        headerTitleAlign: 'center',
        headerBackTitleVisible: false,
        headerTintColor: 'black',
      }),
    },
    ObservationList: {
      screen: ObservationListNavigator,
      navigationOptions: ({ navigation }) => ({
        headerTitle: () => (
          <View>
            <Text
              style={headerElementStyle.title}
              numberOfLines={1}
              adjustsFontSizeToFit={true}
            >
              {navigation.state.params
                ? `${navigation.state.params.title}`
                : ''}
            </Text>
            <Text
              style={headerElementStyle.subTitle}
              numberOfLines={1}
              adjustsFontSizeToFit={true}
            >
              {I18n.t('observation_screen_title')}
            </Text>
          </View>
        ),
        headerLeft: () => (
          <HeaderBackButton
            style={headerElementStyle.leftButton}
            tintColor="black"
            labelVisible={false}
            onPress={() => NavigationHandler.navigate('Home')}
          />
        ),
        headerRight: () => (
          <TouchableIcon
            style={headerElementStyle.rightButton}
            name="more-vert"
            size={24}
            color="black"
            onPress={navigation.getParam('toggleMenu')}
          />
        ),
        headerStyle: headerStyle,
        headerTitleAlign: 'left',
        headerBackTitleVisible: false,
        headerTintColor: 'black',
      }),
    },
    CreateMeasurement: {
      screen: CreateMeasurementScreen,
      navigationOptions: ({ navigation }) => ({
        headerTitle: () => (
          <View>
            <Text
              style={headerElementStyle.title}
              numberOfLines={1}
              adjustsFontSizeToFit={true}
            >
              {navigation.state.params
                ? `${navigation.state.params.title}`
                : ''}
            </Text>
            <Text
              style={headerElementStyle.subTitle}
              numberOfLines={1}
              adjustsFontSizeToFit={true}
            >
              {navigation.state.params
                ? `${navigation.state.params.description}`
                : ''}
            </Text>
          </View>
        ),
        headerLeft: () => (
          <HeaderBackButton
            style={headerElementStyle.leftButton}
            tintColor="black"
            labelVisible={false}
            onPress={navigation.getParam('onHeaderBackPress')}
          />
        ),
        headerRight: () =>
          navigation.state.params.pinned ? (
            <TouchableIcon
              style={headerElementStyle.rightButton}
              name="thumbtack"
              size={24}
              color="black"
              onPress={navigation.getParam('deleteShortcut')}
              iconType="FontAwesome5"
            />
          ) : (
            <TouchableIcon
              style={headerElementStyle.rightButton}
              name="pin"
              size={24}
              color="black"
              onPress={navigation.getParam('toggleShortcut')}
              iconType="SimpleLineIcon"
            />
          ),
        headerStyle: headerStyle,
        headerTitleAlign: 'left',
        headerBackTitleVisible: false,
        headerTintColor: 'black',
      }),
    },
    Camera: {
      screen: Camera,
      navigationOptions: {
        headerShown: false,
      },
    },
    EditMeasurement: {
      screen: EditMeasurementScreen,
      navigationOptions: ({ navigation }) => ({
        headerTitle: () => (
          <View>
            <Text
              style={headerElementStyle.title}
              numberOfLines={1}
              adjustsFontSizeToFit={true}
            >
              {navigation.state.params
                ? `${navigation.state.params.title}`
                : ''}
            </Text>
            <Text
              style={headerElementStyle.subTitle}
              numberOfLines={1}
              adjustsFontSizeToFit={true}
            >
              {navigation.state.params
                ? `${navigation.state.params.description}`
                : ''}
            </Text>
          </View>
        ),
        headerLeft: () => (
          <HeaderBackButton
            style={headerElementStyle.leftButton}
            tintColor="black"
            labelVisible={false}
            onPress={navigation.getParam('onHeaderBackPress')}
          />
        ),
        headerRight: () => (
          <View style={{ flex: 1, flexDirection: 'row' }}>
            {navigation.state.params.pinned ? (
              <TouchableIcon
                style={headerElementStyle.rightButton}
                name="thumbtack"
                size={24}
                color="black"
                onPress={navigation.getParam('deleteShortcut')}
                iconType="FontAwesome5"
              />
            ) : (
              <TouchableIcon
                style={headerElementStyle.rightButton}
                name="pin"
                size={24}
                color="black"
                onPress={navigation.getParam('toggleShortcut')}
                iconType="SimpleLineIcon"
              />
            )}
            {navigation.state.params.createNewForm == true && (
              <TouchableIcon
                style={headerElementStyle.rightButton}
                name="plus"
                size={24}
                color="black"
                onPress={navigation.getParam('onCreateNewForm')}
                iconType="FontAwesome5"
              />
            )}
          </View>
        ),
        headerStyle: headerStyle,
        headerTitleAlign: 'left',
        headerBackTitleVisible: false,
        headerTintColor: 'black',
      }),
    },
    ObservationCards: {
      screen: ObservationCardsNavigator,
      navigationOptions: ({ navigation }) => ({
        headerTitle: () => (
          <View>
            <Text
              style={[
                headerElementStyle.title,
                headerElementStyle.titlePadding,
              ]}
              numberOfLines={2}
              adjustsFontSizeToFit={true}
            >
              {I18n.t('measurements')}
            </Text>
          </View>
        ),
        headerLeft: () => (
          <HeaderBackButton
            style={headerElementStyle.leftButton}
            tintColor="black"
            labelVisible={false}
            onPress={() => NavigationHandler.navigate('Home')}
          />
        ),
        headerRight: () => (
          <TouchableIcon
            name="sync"
            size={24}
            color="black"
            style={[headerElementStyle.rightButton, roundedButton]}
            onPress={navigation.getParam('toggleSync')}
          />
        ),
        headerStyle: headerStyle,
        headerTitleAlign: 'center',
        headerBackTitleVisible: false,
        headerTintColor: 'black',
      }),
    },
    ObservationMap: {
      screen: ObservationMapScreen,
      navigationOptions: () => ({
        headerTitle: () => (
          <View>
            <Text
              style={headerElementStyle.title}
              numberOfLines={1}
              adjustsFontSizeToFit={true}
            >
              {I18n.t('map')}
            </Text>
          </View>
        ),
        headerLeft: () => (
          <HeaderBackButton
            style={headerElementStyle.leftButton}
            tintColor="black"
            labelVisible={false}
            onPress={() => NavigationHandler.navigate('Home')}
          />
        ),
        headerStyle: headerStyle,
        headerTitleAlign: 'center',
        headerBackTitleVisible: false,
        headerTintColor: 'black',
      }),
    },
    PositionInput: {
      screen: PositionInputScreen,
      navigationOptions: () => ({
        headerShown: false,
      }),
    },
    ObservationGame: {
      screen: ObservationGameNavigator,
      navigationOptions: ({ navigation }) => ({
        headerTitle: () => (
          <View>
            <Text
              style={headerElementStyle.title}
              numberOfLines={1}
              adjustsFontSizeToFit={true}
            >
              {navigation.state.params
                ? `${navigation.state.params.title}`
                : ''}
            </Text>
            <Text
              style={headerElementStyle.subTitle}
              numberOfLines={1}
              adjustsFontSizeToFit={true}
            >
              {I18n.t('observation_screen_title')}
            </Text>
          </View>
        ),
        headerLeft: () => (
          <HeaderBackButton
            style={headerElementStyle.leftButton}
            tintColor="black"
            labelVisible={false}
            onPress={() => NavigationHandler.navigate('Home')}
          />
        ),
        headerRight: () => (
          <TouchableIcon
            style={headerElementStyle.rightButton}
            name="more-vert"
            size={24}
            color="black"
            onPress={navigation.getParam('toggleMenu')}
          />
        ),
        headerStyle: headerStyle,
        headerTitleAlign: 'center',
        headerBackTitleVisible: false,
        headerTintColor: 'black',
      }),
    },
    Export: {
      screen: ExportScreen,
      navigationOptions: () => ({
        headerTitle: () => (
          <View>
            <Text
              style={headerElementStyle.title}
              numberOfLines={1}
              adjustsFontSizeToFit={true}
            >
              {I18n.t('export')}
            </Text>
          </View>
        ),
        headerLeft: () => (
          <HeaderBackButton
            style={headerElementStyle.leftButton}
            tintColor="black"
            labelVisible={false}
            onPress={() => NavigationHandler.navigate('Home')}
          />
        ),
        headerStyle: headerStyle,
        headerTitleAlign: 'center',
        headerBackTitleVisible: false,
        headerTintColor: 'black',
      }),
    },
    MainTrackLog: {
      screen: MainTrackLogScreen,
      navigationOptions: () => ({
        headerTitle: () => (
          <View>
            <Text
              style={headerElementStyle.title}
              numberOfLines={1}
              adjustsFontSizeToFit={true}
            >
              {I18n.t('tracklog')}
            </Text>
          </View>
        ),
        headerLeft: () => (
          <HeaderBackButton
            style={headerElementStyle.leftButton}
            tintColor="black"
            labelVisible={false}
            onPress={() => NavigationHandler.navigate('Home')}
          />
        ),
        headerStyle: headerStyle,
        headerTitleAlign: 'center',
        headerBackTitleVisible: false,
        headerTintColor: 'black',
      }),
    },
    TrackLogLists: {
      screen: TrackLogListNavigator,
      navigationOptions: ({ navigation }) => ({
        headerTitle: () => (
          <View>
            <Text
              style={[
                headerElementStyle.title,
                headerElementStyle.titlePadding,
              ]}
              numberOfLines={1}
              adjustsFontSizeToFit={true}
            >
              {I18n.t('tracklogs')}
            </Text>
          </View>
        ),
        headerLeft: () => (
          <HeaderBackButton
            style={headerElementStyle.leftButton}
            tintColor="black"
            labelVisible={false}
            onPress={() => NavigationHandler.navigate('Home')}
          />
        ),
        headerRight: () => (
          <TouchableIcon
            name="sync"
            size={24}
            color="black"
            style={[headerElementStyle.rightButton, roundedButton]}
            onPress={navigation.getParam('toggleTrackLogSync')}
          />
        ),
        headerStyle: headerStyle,
        headerTitleAlign: 'center',
        headerBackTitleVisible: false,
        headerTintColor: 'black',
      }),
    },
  },
  {
    initialRouteName: 'Home',
  },
);

/* eslint-disable react/prop-types */
import React from 'react';
import { Text } from 'react-native';
import { createMaterialTopTabNavigator } from 'react-navigation-tabs';
import { DatabaseListScreen, LastDatabasesScreen } from '../../screens';
import I18n from '../../i18n';
import { Colors } from '../../colors';

export default createMaterialTopTabNavigator(
  {
    LastDatabasesScreen: {
      screen: LastDatabasesScreen,
      navigationOptions: {
        tabBarLabel: ({ tintColor }) => (
          <Text style={{ color: tintColor }}>{I18n.t('latest_tab').toUpperCase()}</Text>
        ),
      },
    },
    DatabaseListScreen: {
      screen: DatabaseListScreen,
      navigationOptions: {
        tabBarLabel: ({ tintColor }) => (
          <Text style={{ color: tintColor }}>{I18n.t('all_tab').toUpperCase()}</Text>
        ),
      },
    },
  },
  {
    initialRouteName: 'DatabaseListScreen',
    tabBarOptions: {
      indicatorStyle: {
        backgroundColor: 'black',
      },
      activeTintColor: 'black',
      inactiveTintColor: 'rgba(0, 0, 0, 0.61)',
      style: {
        backgroundColor: Colors.mantisGreen,
      },
    },
  },
);

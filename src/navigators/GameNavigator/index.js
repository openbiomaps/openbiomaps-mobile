import React from 'react';
import { View, Text } from 'react-native';
import { createStackNavigator, HeaderBackButton } from 'react-navigation-stack';
import { headerStyle, titleStyle, headerTitleStyle } from '../../components';
import I18n from '../../i18n';
import { 
  QuizInformationScreen,
  QuizQuestionsScreen,
  QuizTaskScreen,
} from '../../screens';
import NavigationHandler from '../../NavigationHandler';

export default createStackNavigator(
  {
    QuizInformation: {
      screen: QuizInformationScreen,
      navigationOptions: ({ navigation }) => ({
        headerTitle:  () =>(
          <View style={[headerTitleStyle.container, { height: 56 }]}>
            <Text style={[headerTitleStyle.title, { marginBottom: 0 }]} numberOfLines={1}>{navigation.state.params ? `${navigation.state.params.title}` : ''}</Text>
          </View>
        ),
        headerLeft:  () => (
          <HeaderBackButton onPress={() => NavigationHandler.goBack()} />
        ),
        headerStyle: [headerStyle, { height: 56 }],
        headerTitleStyle: titleStyle,
      }),
    },
    QuizQuestions: {
      screen: QuizQuestionsScreen,
      navigationOptions: ({ navigation }) => ({
        headerTitle:  () =>(
          <View style={headerTitleStyle.container}>
            <Text style={headerTitleStyle.title} numberOfLines={1}>{navigation.state.params ? `${navigation.state.params.title}` : ''}</Text>
            <Text style={headerTitleStyle.description}>{I18n.t('questions_screen_title')}</Text>
          </View>
        ),
        headerStyle,
        headerTitleStyle: titleStyle,
      }),
    },
    QuizTask: {
      screen: QuizTaskScreen,
      navigationOptions: ({ navigation }) => ({
        headerTitle:  () =>(
          <View style={headerTitleStyle.container}>
            <Text style={headerTitleStyle.title} numberOfLines={1}>{navigation.state.params ? `${navigation.state.params.title}` : ''}</Text>
            <Text style={headerTitleStyle.description}>{I18n.t('task_screen_title')}</Text>
          </View>
        ),
        headerStyle,
        headerTitleStyle: titleStyle,
      }),
    },
  },
  {
    initialRouteName: 'QuizInformation',
  },
);

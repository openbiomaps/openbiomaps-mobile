/* eslint-disable react/prop-types */
import React from 'react';
import { Text } from 'react-native';
import { createMaterialTopTabNavigator } from 'react-navigation-tabs';
import I18n from '../../i18n';
import { Colors } from '../../colors';
import { SyncedTrackLogsScreen, UnsyncedTrackLogsScreen } from '../../screens';

export default createMaterialTopTabNavigator(
  {
    UnsyncedTrackLogsScreen: {
      screen: UnsyncedTrackLogsScreen,
      navigationOptions: {
        tabBarLabel: ({ tintColor }) => (
          <Text style={{ color: tintColor }}>{I18n.t('unsynced').toUpperCase()}</Text>
        ),
      },
    },
    SyncedTrackLogsScreen: {
      screen: SyncedTrackLogsScreen,
      navigationOptions: {
        tabBarLabel: ({ tintColor }) => (
          <Text style={{ color: tintColor }}>{I18n.t('synced').toUpperCase()}</Text>
        ),
      },
    },
  },
  {
    initialRouteName: 'UnsyncedTrackLogsScreen',
    tabBarOptions: {
      indicatorStyle: {
        backgroundColor: 'black',
      },
      activeTintColor: 'black',
      inactiveTintColor: 'rgba(0, 0, 0, 0.61)',
      style: {
        backgroundColor: Colors.mantisGreen,
      },
    },
  },
);

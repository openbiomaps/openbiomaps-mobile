import { useCallback, useEffect, useRef } from 'react';
import { Platform } from 'react-native';

import Geolocation, { PositionError } from 'react-native-geolocation-service';
import VIForegroundService from '@voximplant/react-native-foreground-service';
import { useDispatch } from 'react-redux';
import GPSState from 'react-native-gps-state';

import { setGpsError, setGpsPosition, stopGpsRecording } from './actions/gps';
import { useShallowEqualSelector } from './utils/useShallowEqualSelector';
import { hasLocationPermission, withBackgroundLocationPermissionAndroid } from './utils/permissonRequests';
import { usePrevious } from './utils/usePrevious';
import I18n from './i18n';

const LOCATION_CHANNEL_ID = 'locationChannel';
const LOCATION_FOREGROUND_SERVICE_ID = 420;

const GpsHandler = () => {
  const dispatch = useDispatch();

  const distanceFilter = useShallowEqualSelector((state) => state.settings.distanceFilter);
  const isRecording = useShallowEqualSelector((state) => state.gps.isRecording);
  const error = useShallowEqualSelector((state) => state.gps.error);

  const prevDistanceFilter = usePrevious(distanceFilter);

  const watchId = useRef(undefined);
  const isForegroundServiceRunning = useRef(false);
  const distanceFilterRef = useRef(distanceFilter);

  const startForegroundService = useCallback(async () => {
    if (isForegroundServiceRunning.current) {
      return;
    }

    try {
      if (Platform.Version >= 26) {
        await VIForegroundService.getInstance().createNotificationChannel({
          id: LOCATION_CHANNEL_ID,
          name: I18n.t('location_tracking_channel_name'),
          description: I18n.t('location_tracking_channel_description'),
          enableVibration: false,
        });
      }

      await VIForegroundService.getInstance().startService({
        channelId: LOCATION_CHANNEL_ID,
        id: LOCATION_FOREGROUND_SERVICE_ID,
        title: 'OpenBioMaps',
        text: I18n.t('tracking_location_updates'),
        icon: 'ic_launcher',
        killOnDestroy: true,
      });

      isForegroundServiceRunning.current = true;
    } catch (e) {
      console.log(e);
    }
  }, []);

  const stopForegroundService = useCallback(async () => {
    if (!isForegroundServiceRunning.current) {
      return;
    }

    try {
      await VIForegroundService.getInstance().stopService();
      isForegroundServiceRunning.current = false;
    } catch (e) {
      console.log(e);
    }
  }, []);

  const startWatchingPosition = useCallback(() => {
    if (watchId.current) {
      return;
    }

    watchId.current = Geolocation.watchPosition(
      ({ coords: { latitude, longitude, accuracy }, timestamp  }) => {
        dispatch(setGpsPosition({ latitude, longitude, accuracy, timestamp }));
      },
      ({ code, message }) => {
        dispatch(setGpsError({ code, message }));
      },
      {
        accuracy: {
          android: "high",
          ios: "bestForNavigation"
        },
        enableHighAccuracy: true,
        interval: 1000,
        fastestInterval: 1000,
        distanceFilter: distanceFilterRef.current,
        showsBackgroundLocationIndicator: true
      },
    );
  }, [dispatch]);

  const stopWatchingPosition = useCallback(() => {
    if (watchId.current === undefined) {
      return;
    }

    Geolocation.clearWatch(watchId.current);
    watchId.current = undefined;
  }, []);

  const startLocationTracking = useCallback(async () => {
    const hasPermission = await hasLocationPermission();

    if (!hasPermission) {
      dispatch(stopGpsRecording());
      dispatch(setGpsError({ code: PositionError.PERMISSION_DENIED, message: 'Location permission not granted.' }));
      return;
    }

    if (hasPermission) {
      if (Platform.OS === 'android') {
        if (Platform.Version > 30) {
          await withBackgroundLocationPermissionAndroid({
            onGranted: async () => {
              await startForegroundService();
            }
          })
        } else {
          await startForegroundService();
        }
      }
    }
    startWatchingPosition();
  }, [startForegroundService, startWatchingPosition, dispatch]);

  const stopLocationTracking = useCallback(async () => {
    stopWatchingPosition();

    if (Platform.OS === 'android') {
      await stopForegroundService();
    }
  }, [stopForegroundService, stopWatchingPosition]);

  useEffect(() => {
    if (isRecording) {
      startLocationTracking();
    } else {
      stopLocationTracking();
    }
  }, [startLocationTracking, stopLocationTracking, isRecording]);

  useEffect(() => {
    GPSState.addListener((gpsState) => {
      if (gpsState === GPSState.AUTHORIZED) {
        if (isRecording && error.code === PositionError.POSITION_UNAVAILABLE) {
          stopWatchingPosition();
          startWatchingPosition();
        }
      }
    });

    return () => {
      GPSState.removeListener();
    };
  }, [startWatchingPosition, stopWatchingPosition, error, isRecording]);

  useEffect(() => {
    distanceFilterRef.current = distanceFilter;
  }, [distanceFilter]);

  useEffect(() => {
    const isDistanceFilterChanged = prevDistanceFilter !== distanceFilter;

    if (isRecording && isDistanceFilterChanged) {
      stopWatchingPosition();
      startWatchingPosition();
    }
  }, [isRecording, distanceFilter, prevDistanceFilter, startWatchingPosition, stopWatchingPosition]);

  return null;
};

export default GpsHandler;
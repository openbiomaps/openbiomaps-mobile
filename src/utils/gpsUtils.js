import * as Sentry from '@sentry/react-native';
import moment from 'moment';
import I18n from '../i18n';

/**
 * Gets the center of an array of points. A point is a 2 length array of number
 * @param {array of point: array of number} line
 * @returns { latitude: number, longitude: number }
 */
const getLineCenter = (line) => {
  const sums = line.reduce(
    (acc, curr) => ({
      latitude: acc.latitude + curr[1],
      longitude: acc.longitude + curr[0],
    }),
    { latitude: 0, longitude: 0 },
  );
  return {
    latitude: sums.latitude / line.length,
    longitude: sums.longitude / line.length,
  };
};

/**
 * gets the center of an array of LineString
 * @param {array of line} polygon
 * @returns
 */
const getPolygonCenter = (polygon) => {
  const sums = polygon.reduce(
    (acc, curr) => {
      const curLineCenter = getLineCenter(curr);
      return {
        latitude: acc.latitude + curLineCenter.latitude,
        longitude: acc.longitude + curLineCenter.longitude,
      };
    },
    { latitude: 0, longitude: 0 },
  );
  return {
    latitude: sums.latitude / polygon.length,
    longitude: sums.longitude / polygon.length,
  };
};

/**
 * gets the center of an array of Polygon
 * @param {Array of poligon} multipolygon
 */
const getMultiPolygonCenter = (multipolygon) => {
  const sums = multipolygon.reduce(
    (acc, curr) => {
      const curPolygonCenter = getPolygonCenter(curr);
      return {
        latitude: acc.latitude + curPolygonCenter.latitude,
        longitude: acc.longitude + curPolygonCenter.longitude,
      };
    },
    { latitude: 0, longitude: 0 },
  );
  return {
    latitude: sums.latitude / multipolygon.length,
    longitude: sums.longitude / multipolygon.length,
  };
};

/**
 * gets the geoJson and calculates the place of the labels.
 * Possible types:
 * "Point", "MultiPoint", "LineString", "MultiLineString", "Polygon", "MultiPolygon", and "GeometryCollection"
 * @param {type: string, coordinates} geoJson
 * @returns { latitude: number, longitude: number} coordinate
 */
export const getCenterOfGeometry = ({ type, coordinates }) => {
  try {
    switch (type) {
      case 'Point':
        // array of lat lng coords: [lon, lat]
        return { latitude: coordinates[1], longitude: coordinates[0] };
      case 'LineString':
      case 'MultiPoint':
        return getLineCenter(coordinates);
      case 'Polygon':
      case 'MultiLineString':
        return getPolygonCenter(coordinates);
      case 'MultiPolygon':
        return getMultiPolygonCenter(coordinates);
      default:
        return null;
    }
  } catch (error) {
    Sentry.captureMessage(
      `Error while calculating center of geojson of type ${type}: ${error.message}`,
    );
    return null;
  }
};

export const getAverageByKey = (coordArray, key) => {
  const sum = coordArray.reduce((acc, curr) => acc + curr[key], 0);
  return sum / coordArray.length;
};

export const getCoordDeltaFromCoordArray = (coordArray, key) => {
  let min = coordArray[0][key];
  let max = coordArray[0][key];
  coordArray.forEach((coord) => {
    min = Math.min(min, coord[key]);
    max = Math.max(max, coord[key]);
  });
  return Math.abs(max - min);
};

export const calculateInitialRegion = (geometryValue) => {
  if (!Array.isArray(geometryValue)) {
    return {
      latitude: geometryValue.latitude,
      longitude: geometryValue.longitude,
      latitudeDelta: 0.0082,
      longitudeDelta: 0.0051,
    };
  }
  return {
    latitude: getAverageByKey(geometryValue, 'latitude'),
    longitude: getAverageByKey(geometryValue, 'longitude'),
    latitudeDelta: Math.max(
      0.0082,
      getCoordDeltaFromCoordArray(geometryValue, 'latitude'),
    ),
    longitudeDelta: Math.max(
      0.0051,
      getCoordDeltaFromCoordArray(geometryValue, 'longitude'),
    ),
  };
};

export const parseValueToString = (value, dataType) => {
  if (value !== I18n.t('no_field_data')) {
    switch (dataType.type) {
      case 'time':
        return moment(value).format('HH:mm');
      case 'datetime':
        return moment(value).format('YYYY.MM.DD HH:mm');
      case 'date':
        return moment(value).format('YYYY.MM.DD');
      case 'boolean':
        return value ? I18n.t('bool_true') : I18n.t('bool_false');
      case 'list': {
        const {
          list,
          filtering_list: { list_values = [], list_labels = [] } = {},
        } = dataType;

        /**
         * If the current list value is comming from a linked list filtering process, then
         * the value won't be in the dataType.list object, insted it can be reach (filter again)
         * from dataType.filtering_list.list_values and list_labels (labels is to display) array.
         */
        if (!Object.keys(list).includes(value)) {
          const index = list_values.findIndex((item) => item === value);
          const filteredValue = list_labels[index];
          return filteredValue;
        }
        return dataType.list[value];
      }
      case 'point':
        return `${value.latitude.toFixed(6)}, ${value.longitude.toFixed(6)}`;
      case 'line':
      case 'polygon':
        return dataType.type;
      case 'wkt': {
        const { wktValue, wktType } = value;
        return wktType === 'point' &&
          !(Array.isArray(wktValue) && wktValue.latitude && wktValue.longitude)
          ? `${wktValue.latitude.toFixed(6)}, ${wktValue.longitude.toFixed(6)}`
          : wktType;
      }
      case 'file_id':
        return value.toString();
      default:
        return value;
    }
  }
  return I18n.t('no_field_data');
};

export const parseDataToWkt = (data, type) => {
  if (type === 'point' && data.latitude && data.longitude) {
    return `POINT (${data.longitude} ${data.latitude})`;
  }

  if (type === 'line' && Array.isArray(data) && data.length > 0) {
    const newData = data;
    while (newData.length < 2) newData.push(data[data.length - 1]);
    return `LINESTRING (${newData
      .reduce((acc, curr) => `${acc}${curr.longitude} ${curr.latitude}, `, '')
      .slice(0, -2)})`;
  }

  if (type === 'polygon' && Array.isArray(data) && data.length > 0) {
    const newData = data;
    while (newData.length < 3) newData.push(data[data.length - 1]);
    const firstElement =
      data.length > 0 ? `${data[0].longitude} ${data[0].latitude}` : '';
    return `POLYGON ((${data.reduce(
      (acc, curr) => `${acc}${curr.longitude} ${curr.latitude}, `,
      '',
    )}${firstElement}))`;
  }

  return null;
};

export const isValidCoordinateArray = (coordinateArray, caseDescription) => {
  if (!coordinateArray) return false;
  if (coordinateArray.some((coordinate) => !coordinate)) {
    Sentry.captureMessage(
      `there is a null coordinate in an array: ${caseDescription}`,
    );
    return false;
  }
  return true;
};

/**
 * Formats the permanent sample plots coming from the server to a geojson formatted object, and calculates the center of the plot for the label.
 * @param { array of {
 *  id: string,
 *  name: string,
 *  geometry: string (geoJson stringified)
 * }} samplePlots
 * @returns { array of {
 *  id: number,
 *  name: string,
 *  geoJson: object,
 *  labelCoord: object
 * }}
 */
export const formatStringToGeoJson = (samplePlots) => {
  const formatted = samplePlots?.map(({ id, name, geometry }) => {
    let parsedGeometry = null;
    try {
      parsedGeometry = JSON.parse(geometry);
      return {
        id: parseInt(id, 10),
        name,
        geoJson: {
          type: 'FeatureCollection',
          features: [
            {
              type: 'Feature',
              properties: {},
              geometry: parsedGeometry,
            },
          ],
        },
        labelCoords: getCenterOfGeometry(parsedGeometry),
      };
    } catch (e) {
      Sentry.captureMessage(
        `error during sampleplot (id: ${id}) formatting: ${e.message}`,
      );
      return null;
    }
  });
  return formatted;
};

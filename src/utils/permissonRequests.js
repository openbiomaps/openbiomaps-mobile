import { Linking, PermissionsAndroid, Platform } from 'react-native';

import { FileSystem } from 'react-native-unimodules';
import Toast from 'react-native-simple-toast';
import I18n from '../i18n';
import { showAlertDialog } from '../components/AlertDialog';
import { ERROR } from '../Constants';
import Geolocation from 'react-native-geolocation-service';
import RNPermissions, { PERMISSIONS } from 'react-native-permissions';
import * as Sentry from '@sentry/react-native';

const { StorageAccessFramework } = FileSystem;

const DEFAULT_EXTERNAL_BACKUP_DIR =
  StorageAccessFramework.getUriForDirectoryInRoot('obm');

const isAndroid = Platform.OS === 'android';
const isIOS = Platform.OS === 'ios';

export const withBackgroundLocationPermissionAndroid = async ({
  onGranted,
}) => {
  const checkBackgroundLocation = await RNPermissions.check(
    PERMISSIONS.ANDROID.ACCESS_BACKGROUND_LOCATION,
  );

  if (checkBackgroundLocation == 'granted') {
    return onGranted();
  }

  showAlertDialog({
    title: I18n.t('background_location_permission_title'),
    message: I18n.t('background_location_permission_message'),
    onPositiveBtnPress: async () => {
      const backgroundLocationRequest = await RNPermissions.request(
        PERMISSIONS.ANDROID.ACCESS_BACKGROUND_LOCATION,
      );

      if (backgroundLocationRequest == 'granted') {
        return onGranted();
      }
    },
  });
};

export const hasLocationPermissionAndroidLatest = async () => {
  let accessFineLocation = await RNPermissions.check(
    PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION,
  );
  let accessCoarseLocation = await RNPermissions.check(
    PERMISSIONS.ANDROID.ACCESS_COARSE_LOCATION,
  );
  let accessBackgroundLocation = await RNPermissions.check(
    PERMISSIONS.ANDROID.ACCESS_BACKGROUND_LOCATION,
  );

  if (accessCoarseLocation == 'denied') {
    const requestedPermissions = await RNPermissions.requestMultiple([
      PERMISSIONS.ANDROID.ACCESS_COARSE_LOCATION,
      PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION,
    ]);
    accessCoarseLocation =
      requestedPermissions[PERMISSIONS.ANDROID.ACCESS_COARSE_LOCATION];
    accessFineLocation =
      requestedPermissions[PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION];
  }

  if (accessFineLocation == 'denied') {
    const requestedPermission = await RNPermissions.request(
      PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION,
    );
    accessFineLocation = requestedPermission;
  }

  if (
    accessCoarseLocation == 'granted' ||
    accessFineLocation == 'granted' ||
    accessBackgroundLocation == 'granted'
  ) {
    return true;
  }

  return false;
};

/**
 * @deprecated since 2.2.22
 */
export const hasLocationPermissionAndroid = async () => {
  const checkLocationPermission = await RNPermissions.checkMultiple([
    PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION,
    PERMISSIONS.ANDROID.ACCESS_COARSE_LOCATION,
    PERMISSIONS.ANDROID.ACCESS_BACKGROUND_LOCATION,
  ]);

  if (
    checkLocationPermission['android.permission.ACCESS_BACKGROUND_LOCATION'] ==
      RNPermissions.RESULTS.GRANTED &&
    checkLocationPermission['android.permission.ACCESS_COARSE_LOCATION'] ==
      RNPermissions.RESULTS.GRANTED &&
    checkLocationPermission['android.permission.ACCESS_FINE_LOCATION'] ==
      RNPermissions.RESULTS.GRANTED
  ) {
    return true;
  } else if (
    checkLocationPermission['android.permission.ACCESS_COARSE_LOCATION'] !=
      RNPermissions.RESULTS.GRANTED ||
    checkLocationPermission['android.permission.ACCESS_FINE_LOCATION'] !=
      RNPermissions.RESULTS.GRANTED
  ) {
    const requestLocationPermission = await RNPermissions.requestMultiple([
      PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION,
      PERMISSIONS.ANDROID.ACCESS_COARSE_LOCATION,
    ]);

    if (
      requestLocationPermission['android.permission.ACCESS_COARSE_LOCATION'] ==
        RNPermissions.RESULTS.GRANTED &&
      requestLocationPermission['android.permission.ACCESS_FINE_LOCATION'] ==
        RNPermissions.RESULTS.GRANTED
    ) {
      return true;
    }
  } else if (
    checkLocationPermission['android.permission.ACCESS_BACKGROUND_LOCATION'] ==
    RNPermissions.RESULTS.GRANTED
  ) {
    return true;
  }
};

export const hasLocationPermissionAndroidUpTo30 = async () => {
  const checkLocationPermission = await PermissionsAndroid.check(
    PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
  );

  if (checkLocationPermission) {
    return checkLocationPermission;
  }
  return await PermissionsAndroid.request(
    PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
  );
};

export const hasLocationPermissionIOS = async () => {
  const openSetting = () => {
    Linking.openSettings().catch(() => {
      Alert.alert('Unable to open settings');
    });
  };
  const status = await Geolocation.requestAuthorization('whenInUse');

  if (status === 'granted') {
    return true;
  }

  if (status === 'denied') {
    Alert.alert('Location permission denied');
  }

  if (status === 'disabled') {
    Alert.alert(
      `Turn on Location Services to allow "${appConfig.displayName}" to determine your location.`,
      '',
      [
        { text: 'Go to Settings', onPress: openSetting },
        { text: "Don't Use Location", onPress: () => {} },
      ],
    );
  }

  return false;
};

export const hasLocationPermission = async () => {
  if (isAndroid) {
    if (Platform.Version < 23) {
      return true;
    } else if (Platform.Version < 30) {
      return await hasLocationPermissionAndroidUpTo30();
    } else {
      return await hasLocationPermissionAndroidLatest();
    }
  }

  if (isIOS) {
    return await hasLocationPermissionIOS();
  }

  return false;
};

export const hasExternalStoragePermission = async (
  errorMessage = I18n.t('external_permission_denied'),
) => {
  if (Platform.OS === 'android') {
    if (Platform.Version < 33) {
      const hasPermission = await PermissionsAndroid.check(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
      );

      if (!hasPermission) {
        const astatus = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        );

        if (astatus === PermissionsAndroid.RESULTS.NEVER_ASK_AGAIN) {
          Linking.openSettings();
        }

        if (astatus !== PermissionsAndroid.RESULTS.GRANTED) {
          Toast.show(errorMessage, Toast.LONG);

          return false;
        }
        return true;
      }
    } else if (Platform.Version >= 33) {
      return true;
    }
  } else if (Platform.OS === 'ios') {
    return true;
  }
  return true;
};

export const hasCamerePermission = async (
  errorMessage = I18n.t('camera_permission_denied'),
) => {
  if (Platform.OS === 'android') {
    const hasCameraPermission = await PermissionsAndroid.check(
      PermissionsAndroid.PERMISSIONS.CAMERA,
    );

    if (!hasCameraPermission) {
      const astatus = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA,
      );
      if (astatus !== PermissionsAndroid.RESULTS.GRANTED) {
        Toast.show(errorMessage, Toast.LONG);

        return false;
      }
      return true;
    }
    return true;
  } else if (Platform.OS === 'ios') {
    const checkStatus = await check(PERMISSIONS.IOS.CAMERA);

    if (checkStatus !== 'granted') {
      const requestStatus = await request(PERMISSIONS.IOS.CAMERA);

      if (requestStatus !== 'granted') {
        Toast.show(errorMessage, Toast.LONG);

        return false;
      }
      return true;
    }
    return true;
  }
};

export const getExtDirUri = (extDirPath, name) =>
  `${extDirPath}${encodeURIComponent(`/${name}`)}`;

/**
 * Return the uri of the directory inside the set external root directory but creates it if not exists
 * @param {string} extRootDir The set external root directory
 * @param {string} dirName The name of the directory we should create
 * @returns
 */
export const getExternalDirUriAndCreateIfNotExists = async (
  extRootDir = DEFAULT_EXTERNAL_BACKUP_DIR,
  dirName,
) => {
  const newDirUri = `${extRootDir}/${dirName}`;
  try {
    const rootDirContent = await StorageAccessFramework.readDirectoryAsync(
      extRootDir,
    );
    const decodedContent = rootDirContent.map((item) =>
      decodeURIComponent(item),
    );
    const dir = decodedContent.find((item) => item === newDirUri);
    if (!dir) {
      return await StorageAccessFramework.makeDirectoryAsync(
        extRootDir,
        dirName,
      );
    }
    return dir;
  } catch (error) {
    throw new Error('Directory could not been created');
  }
};

const SAFRootDir =
  'content://com.android.externalstorage.documents/tree/primary/';

// eslint-disable-next-line no-async-promise-executor
export const requestExternalSAFDirPermission = (pathNameOrNull, type) =>
  new Promise(async (resolve, reject) => {
    if (Platform.OS == 'android') {
      const pathName = pathNameOrNull || DEFAULT_EXTERNAL_BACKUP_DIR;
      try {
        if (pathName === SAFRootDir) {
          Toast.show(I18n.t('root_dir_selected'), Toast.LONG);
          throw new Error(ERROR.ROOT_DIR_SET);
        }
        await StorageAccessFramework.readDirectoryAsync(pathName);
        resolve(pathName);
      } catch (error) {
        showAlertDialog({
          title: I18n.t('external_folder', { type }),
          message: `${I18n.t('select_external_folder_description', {
            type,
          })}\n\n${I18n.t('external_dir_warning')}`,
          positiveLabel: I18n.t('ok'),
          onPositiveBtnPress: async () => {
            try {
              const permissions =
                await StorageAccessFramework.requestDirectoryPermissionsAsync(
                  DEFAULT_EXTERNAL_BACKUP_DIR,
                );
              if (permissions.granted) {
                // Gets SAF URI from response
                const uri = permissions.directoryUri;
                if (uri === SAFRootDir) {
                  Toast.show(I18n.t('root_dir_selected'), Toast.LONG);
                  reject(new Error(ERROR.ROOT_DIR_SET));
                }
                resolve(uri);
              }
            } catch (error) {
              console.warn('permission error', { error });
              reject();
            }
          },
        });
      }
    }
  });

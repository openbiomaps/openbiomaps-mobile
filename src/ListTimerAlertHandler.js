import { Component } from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getSessions } from './utils';
import { showAlertDialog } from './components';
import I18n from './i18n';

const TIME_OF_LAST_ALERT_AFTER_OFFICIAL = 300000;

class ListTimerAlertHandler extends Component {
  constructor(props) {
    super(props);
    
    this.state = {
      alertCallIds: [],
    };
  }

  componentDidMount = () => {
    this.setListTimerAlerts();
  };

  shouldComponentUpdate = (nextProps) => {
    if (JSON.stringify(this.props.sessions) !== JSON.stringify(nextProps.sessions)) {
      return true;
    }

    return false;
  };

  componentDidUpdate = () => {
    this.setListTimerAlerts();
  }
  
  componentWillUnmount = () => {
    const { alertCallIds } = this.state;
    
    alertCallIds.forEach(({ firstAlert, secondAlert }) => {
      clearTimeout(firstAlert);
      clearTimeout(secondAlert);
    });
  }

  calculateAlertTimes = (mustFinishAt) => {
    let remainingTime = Math.min(moment().diff(mustFinishAt), 0);

    if (remainingTime >= 0) {
      return {
        firstTime: null,
        secondTime: 1,
      };
    }
    
    remainingTime = Math.abs(remainingTime);

    return {
      firstTime: remainingTime,
      secondTime: remainingTime + TIME_OF_LAST_ALERT_AFTER_OFFICIAL,
    };
  }

  makeAlert = ({ observationName }) => {
    showAlertDialog({
      title: I18n.t('default_alert_title'),
      message: I18n.t('list_time_expired_alert', { observationName }),
      positiveLabel: I18n.t('ok'),
    });
  }

  setListTimerAlerts = () => {
    const { alertCallIds } = this.state;
    const { sessions } = this.props;

    if (sessions.length === 0 && alertCallIds.length === 0) return;

    // if the session has been closed, (alertcallIds array has an element whom sessionid is not in the sessions state) clear its timeout and delete from the array
    const filteredAlertCallIds = alertCallIds.reduce((acc, curr) => {
      if (!sessions.find((session => session.sessionId === curr.sessionId))) {
        clearTimeout(curr.firstAlert);
        clearTimeout(curr.secondAlert);
      } else {
        acc.push(curr);
      }
      return acc;
    }, []);

    // if a session hasn't been scheduled, (sessions prop array has an element, which doesnt have a scheduled alert in the filteredAlertCallIds) add one to it.
    const newlyCreatedAlertCallIds = sessions.reduce((acc, curr) => {
      const alreadyScheduled = filteredAlertCallIds.find((callIdObject => callIdObject.sessionId === curr.sessionId));
      
      if (curr.mustFinishAt && !alreadyScheduled) {
        const { firstTime, secondTime } = this.calculateAlertTimes(curr.mustFinishAt);
        acc.push({
          sessionId: curr.sessionId,
          firstAlert: firstTime && setTimeout(() => this.makeAlert(curr), firstTime),
          secondAlert: setTimeout(() => this.makeAlert(curr), secondTime),
        });
      }
      return acc;
    }, []);

    const newAlertCallIds = [...newlyCreatedAlertCallIds, ...filteredAlertCallIds];

    if (JSON.stringify(newAlertCallIds) !== JSON.stringify(alertCallIds)) {
      this.setState({
        alertCallIds: newAlertCallIds,
      });
    }
  }
  
  render() {
    return null;
  }
}
  
ListTimerAlertHandler.propTypes = {
  sessions: PropTypes.array.isRequired,
};
  
const mapStateToProps = ({ servers: { data: serverList } }) => ({
  sessions: getSessions(serverList),
});

export default connect(mapStateToProps)(ListTimerAlertHandler);

import React, { Component } from 'react';
import { ScrollView, View } from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import HTMLView from 'react-native-htmlview';
import { footerStyle, TouchableIcon } from '../../components';
import styles from '../QuizInformationScreen/styles';
import { renderNode, getSelectedServer, getSelectedDatabase, getSelectedObservation } from '../../utils';
import NavigationHandler from '../../NavigationHandler';

class QuizTaskScreen extends Component {
  handleDonePress = () => {
    const { selectedObservation, selectedDatabase } = this.props;
    NavigationHandler.navigate('CreateMeasurement', { 
      title: selectedObservation.name,
      description: selectedDatabase.name,
    });
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <ScrollView contentContainerStyle={styles.container}>
          <HTMLView value={this.props.content} stylesheet={styles} renderNode={renderNode} />
        </ScrollView>
        <View style={footerStyle.container}>
          <TouchableIcon name="home" onPress={() => NavigationHandler.navigate('Home')} style={[footerStyle.leftButton, footerStyle.roundedShape]} />
          <View style={footerStyle.rightButtonContainer} >
            <TouchableIcon name="done" onPress={this.handleDonePress} style={[footerStyle.rightButton, footerStyle.roundedShape]} />
          </View>
        </View>
      </View>
    );
  }
}

QuizTaskScreen.propTypes = {
  content: PropTypes.string.isRequired,
  selectedDatabase: PropTypes.object.isRequired,
  selectedObservation: PropTypes.object.isRequired,
};

const mapStateToProps = ({ servers: { data: serverList } }) => {
  const selectedServer = getSelectedServer(serverList);
  const selectedDatabase = getSelectedDatabase(selectedServer);
  const selectedObservation = getSelectedObservation(selectedDatabase);
  const content = selectedObservation.game.taskDescription;
  
  return {
    selectedDatabase, 
    selectedObservation,
    content,
  };
};

export default connect(
  mapStateToProps,
  null,
)(QuizTaskScreen);

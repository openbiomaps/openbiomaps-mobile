import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableWithoutFeedback,
  ActivityIndicator,
  TouchableOpacity,
} from 'react-native';

import Toast from 'react-native-simple-toast';
import Icon from 'react-native-vector-icons/MaterialIcons';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import I18n from '../../i18n';
import {
  getSelectedServer,
  getSelectedDatabase,
  getSelectedObservation,
  calculateInitialRegion,
  isGeometryValueValid,
  getCurrentSessionTrackLog,
  hashStringToHexColor,
  getPermanentSamplePlots,
} from '../../utils';
import { TRACKLOG_ACCURACY_LIMIT, GEOM_EDIT_MODE, GPS_OBSERVER, GPS_ERROR, FALLBACK_POSITION, DELTAS } from '../../Constants';
import { footerStyle, Spinner, TouchableIcon, MapComponent } from '../../components';
import NavigationHandler from '../../NavigationHandler';
import ZoomScale from '../../components/ZoomScale';
import { Colors } from '../../colors';
import withGpsSubscription, { GpsDefaultPropTypes, GpsPropTypes } from '../../hocs/withGpsSubscription';

import styles from './styles';

class PositionInput extends Component {
  constructor(props) {
    super(props);

    this.state = {
      geometryValue: this.getInitialGeometryValue(),
      region: this.getInitialRegion(),
      rotation: null,
      buttonIndicator: false,
      showCurrentSessionTrackLog: true,
      geometryValueEditMode: null,
      fixedMarkerPosition: this.getPosition(),
    };

    this.animationInProgress = false;
  }

  componentDidUpdate(prevProps) {
    const { geometryValueEditMode, geometryValue } = this.state;
    const { type, isValueFixed, gps } = this.props;

    if (!geometryValue && !prevProps.gps.position && gps.position) {
      // eslint-disable-next-line react/no-did-update-set-state
      this.setState({ geometryValue: type === 'point' ? gps.position : [] });
    }

    if (gps.position && prevProps.gps.position !== gps.position) {
      if (!geometryValueEditMode && !isValueFixed && gps.position.accuracy <= TRACKLOG_ACCURACY_LIMIT) {
        // eslint-disable-next-line react/no-did-update-set-state
        this.setState(({ geometryValue: prevGeometryValue }) => ({
          fixedMarkerPosition: gps.position,
          geometryValue: type !== 'point' && Array.isArray(prevGeometryValue) ? [...prevGeometryValue, gps.position] : (prevGeometryValue || gps.position),
        }));
      }
    }

    if (prevProps.gps.error && !gps.error) {
      this.handleMyLocationPress();
    }
  }

  componentWillUnmount() {
    this.setCollectedGeometryValues();
  }

  isLoading = () => {
    const { gps } = this.props;
    const { geometryValue } = this.state;
    return !geometryValue && !gps.position && !gps.error;
  }

  getInitialGeometryValue = () => {
    const { type, valueFromParent, valueLocal, gps } = this.props;

    if (valueFromParent) return valueFromParent;
    if (type !== 'point') return valueLocal || [];
    return gps.position;
  }

  getInitialRegion = () => {
    const initialGeometryValue = this.getInitialGeometryValue();
    if (!initialGeometryValue || (Array.isArray(initialGeometryValue) && !initialGeometryValue.length)) {
      return null;
    }
    return calculateInitialRegion(initialGeometryValue);
  }

  getFallbackPosition = () => {
    const { permanentSamplePlots } = this.props;
    if (permanentSamplePlots?.length) {
      return permanentSamplePlots[0].labelCoords;
    }
    return FALLBACK_POSITION;
  }

  getPosition = () => {
    const { gps } = this.props;
    return !gps.error ? gps.position : this.getFallbackPosition();
  }

  getRegion = () => {
    const { region } = this.state;

    const position = this.getPosition();

    return region || { ...position, ...DELTAS };
  }

  setCollectedGeometryValues = () => {
    const { type, isValueFixed, saveCollectedGeometryValue } = this.props;
    const { geometryValue } = this.state;

    if (type === 'point' || isValueFixed) return;

    saveCollectedGeometryValue(geometryValue);
  }

  handleRegionChange = async (region) => {
    const response = await this.map.getCamera();
    this.setState({ region, rotation: response.heading });
  };

  handlePress = async (e) => {
    const { type } = this.props;
    const { geometryValueEditMode } = this.state;
    const { coordinate } = e.nativeEvent;
    if (type === 'point') {
      if (this.animationInProgress) return;
      this.animationInProgress = true;
      setTimeout(() => {
        this.setState({
          geometryValue: coordinate,
        }, () => { this.animationInProgress = false; });
      }, 300);
    } else if (geometryValueEditMode) {
      this.setState((prevState) => ({
        geometryValue: [...prevState.geometryValue, coordinate],
      }));
    } else {
      Toast.show(I18n.t('choose_edit_mode_first'), Toast.LONG);
    }
  }

  handleMyLocationPress = () => {
    this.setState({ buttonIndicator: true });

    const position = this.getPosition();
    this.map.animateCamera({ center: position });

    this.setState({
      fixedMarkerPosition: position,
      buttonIndicator: false,
    });
  }

  handleDonePress = () => {
    const { navKey, formColumn, generateSavedValue, type } = this.props;
    const { geometryValue } = this.state;

    if (!isGeometryValueValid({ type, geometryValue, toastNeeded: true, minSize: type === 'polygon' ? 3 : 2 })) return;

    NavigationHandler.setParams({
      positionInputCoordinates: { value: generateSavedValue(geometryValue), accuracy: 1, columnName: formColumn },
    }, navKey);

    // setTimeout is needed after upgrading react-navigation from v3 to v4
    // in order to avoid infinite state update loop on go back action after setting params.
    setTimeout(() => {
      NavigationHandler.goBack();
    }, 0);
  }

  handleBackPress = () => {
    NavigationHandler.goBack();
  }

  handleEditModeChange = (newMode) => {
    const { geometryValue } = this.state;

    this.setCollectedGeometryValues();

    if (newMode === GEOM_EDIT_MODE.USE_EXISTING && geometryValue) {
      this.setState({
        geometryValueEditMode: newMode,
      });
    } else {
      this.setState({
        geometryValueEditMode: newMode,
        geometryValue: [],
      });
    }
  }

  cutGeometryValueEnd = () => {
    this.setState(({ geometryValue }) => {
      if (Array.isArray(geometryValue) && geometryValue.length > 0) {
        return ({
          geometryValue: geometryValue.slice(0, -1),
        });
      }
      return null;
    });
  }

  generateHintText = (type, editMode) => {
    if (type === 'point') return I18n.t('position_input_hint_point_press');
    if (editMode) return I18n.t('position_input_hint_array_press', { type: I18n.t(type) });
    return I18n.t('position_input_array_mode_hint');
  }

  render() {
    const { mapEngine, currentSessionTrackLog, type, permanentSamplePlots, gps } = this.props;
    const {
      geometryValue,
      rotation,
      buttonIndicator,
      showCurrentSessionTrackLog,
      geometryValueEditMode,
      fixedMarkerPosition,
    } = this.state;

    const isLoading = this.isLoading();
    const region = this.getRegion();

    if (isLoading) return <Spinner />;

    return (
      <View style={styles.container}>
        <View style={styles.headerContainer}>
          <Text style={styles.title}>{I18n.t('position_input_hint_type', { type: I18n.t(type) })}</Text>
          <Text style={styles.modeChooserHint}>{this.generateHintText(type, geometryValueEditMode)}</Text>
        </View>
        <View style={styles.mapContainer}>

          <MapComponent
            provider={mapEngine.id}
            permanentSamplePlots={permanentSamplePlots}
            region={region}
            onRegionChange={this.handleRegionChange}
            mapRef={(ref) => { this.map = ref; }}
            onLongPress={this.handlePress}
            inputGeometryValue={geometryValue}
            position={fixedMarkerPosition}
            currentSessionTrackLog={showCurrentSessionTrackLog ? currentSessionTrackLog : null}
            type={type}
            editMode={geometryValueEditMode !== null}
            gpsError={gps.error}
          />
          {type !== 'point' && geometryValueEditMode === null
            && (
              <View style={[styles.editModeChooserContainer, gps.error !== null && { top: 10 }]}>
                <View style={styles.buttonContainer}>
                  {Object.entries(GEOM_EDIT_MODE).map(([, mode]) => (
                    <TouchableOpacity
                      style={styles.buttonStyle}
                      onPress={() => this.handleEditModeChange(mode)}
                      key={mode}
                    >
                      <Text style={styles.buttonTextStyle}>{I18n.t(`position_input_mode_${mode}`)}</Text>
                    </TouchableOpacity>
                  ))}
                </View>
              </View>
            )}
          {type !== 'point' && geometryValueEditMode !== null && geometryValue && geometryValue.length > 0
            && (
              <View style={[styles.editModeChooserContainer, gps.error !== null && { top: 10 }]}>
                <TouchableOpacity
                  style={styles.buttonStyle}
                  onPress={this.cutGeometryValueEnd}
                >
                  <Text style={styles.buttonTextStyle}>{I18n.t('delete_last_element')}</Text>
                </TouchableOpacity>
              </View>
            )}
          {gps.error && <Text style={styles.errorText}>{I18n.t(GPS_ERROR[gps.error.code])}</Text>}
          <View style={[styles.floatingContainer, (rotation !== 0 && { justifyContent: 'flex-end' })]}>
            {region && rotation === 0 && <ZoomScale region={region} rotation={rotation} />}
            {currentSessionTrackLog
              && (
                <TouchableIcon
                  name={showCurrentSessionTrackLog ? 'map-marker-path' : 'visibility'}
                  onPress={() => this.setState({ showCurrentSessionTrackLog: !showCurrentSessionTrackLog })}
                  style={styles.showTrackLogButton}
                  iconType={showCurrentSessionTrackLog ? 'MaterialCommunityIcon' : 'MaterialIcon'}
                  vectorIconStyle={showCurrentSessionTrackLog
                    ? { color: currentSessionTrackLog.sessionId ? hashStringToHexColor({ string: currentSessionTrackLog.sessionId, alpha: 'ff' }) : Colors.blue }
                    : null}
                />
              )}
            <TouchableWithoutFeedback onPress={this.handleMyLocationPress}>
              <View style={styles.myLocationButton}>
                {buttonIndicator
                  ? <ActivityIndicator style={styles.spinner} />
                  : <Icon name="my-location" size={24} style={styles.myLocationIcon} />}
              </View>
            </TouchableWithoutFeedback>
          </View>

        </View>
        <View style={styles.bottomContainer}>

          <View style={footerStyle.container}>
            <TouchableIcon
              name="close"
              onPress={this.handleBackPress}
              style={[footerStyle.roundedShape, { flex: 1 }]}
            />
            <TouchableIcon
              name="done"
              onPress={this.handleDonePress}
              style={[footerStyle.roundedShape, { flex: 1 }]}
            />
          </View>
        </View>

      </View>
    );
  }
}

PositionInput.propTypes = {
  navigation: PropTypes.shape({ getParam: PropTypes.func.isRequired }).isRequired,
  mapEngine: PropTypes.object.isRequired,
  type: PropTypes.string,
  isValueFixed: PropTypes.bool,
  navKey: PropTypes.string,
  formColumn: PropTypes.string,
  generateSavedValue: PropTypes.func,
  saveCollectedGeometryValue: PropTypes.func,
  valueFromParent: PropTypes.oneOfType([
    PropTypes.shape({
      longitude: PropTypes.number.isRequired,
      latitude: PropTypes.number.isRequired,
    }),
    PropTypes.arrayOf(PropTypes.shape({
      longitude: PropTypes.number.isRequired,
      latitude: PropTypes.number.isRequired,
    })),
  ]),
  valueLocal: PropTypes.oneOfType([
    PropTypes.shape({
      longitude: PropTypes.number.isRequired,
      latitude: PropTypes.number.isRequired,
    }),
    PropTypes.arrayOf(PropTypes.shape({
      longitude: PropTypes.number.isRequired,
      latitude: PropTypes.number.isRequired,
    })),
  ]),
  currentSessionTrackLog: PropTypes.object,
  permanentSamplePlots: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number,
    name: PropTypes.string,
    geoJson: PropTypes.object,
    labelCoords: PropTypes.shape({
      latitude: PropTypes.number,
      longitude: PropTypes.number,
    }),
  })),
  gps: GpsPropTypes,
};

PositionInput.defaultProps = {
  type: null,
  isValueFixed: false,
  navKey: null,
  formColumn: null,
  generateSavedValue: () => { },
  saveCollectedGeometryValue: () => { },
  valueFromParent: null,
  valueLocal: null,
  currentSessionTrackLog: null,
  permanentSamplePlots: [],
  gps: GpsDefaultPropTypes,
};

const mapStateToProps = (
  {
    servers: { data: serverList },
    settings: { mapEngine },
    trackLog: { currentSessionTrackLogs },
  },
  { navigation },
) => {
  const {
    server,
    database,
    observation,
    type,
    isValueFixed,
    key: navKey,
    column: formColumn,
    generateSavedValue,
    saveCollectedGeometryValue,
    valueFromParent,
    valueLocal,
  } = navigation.state.params;
  const selectedServer = server || getSelectedServer(serverList);
  const selectedDatabase = database || getSelectedDatabase(selectedServer);
  const selectedObservation = observation || getSelectedObservation(selectedDatabase);
  const permanentSamplePlots = getPermanentSamplePlots(selectedDatabase, selectedObservation);
  const currentSessionTrackLog = getCurrentSessionTrackLog(
    selectedObservation,
    currentSessionTrackLogs,
  );

  return {
    selectedServer,
    selectedDatabase,
    selectedObservation,
    currentSessionTrackLog,
    mapEngine,
    type,
    isValueFixed,
    navKey,
    formColumn,
    generateSavedValue,
    saveCollectedGeometryValue,
    valueFromParent,
    valueLocal,
    permanentSamplePlots,
  };
};

const getGpsSubscriptionOptions = ({ navigation }) => {
  const observation = navigation.getParam('observation');
  return { observer: `${GPS_OBSERVER.FORM}-${observation.id}` };
};

export default connect(mapStateToProps)(withGpsSubscription(
  PositionInput,
  getGpsSubscriptionOptions,
));

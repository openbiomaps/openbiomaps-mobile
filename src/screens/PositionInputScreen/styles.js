import { StyleSheet } from 'react-native';
import { Colors } from '../../colors';

const text = {
  fontSize: 14,
  lineHeight: 18,
  color: 'white',
  paddingHorizontal: 10,
};

export default StyleSheet.create({
  bottomContainer: {
    flexDirection: 'column',
    alignItems: 'flex-end',
  },
  myLocationIcon: {
    transform: [{ rotate: '-45deg' }],
    color: '#333',
  },
  myLocationButton: {
    backgroundColor: 'white',
    margin: 15,
    padding: 10,
    transform: [{ rotate: '45deg' }],
    borderRadius: 10,
    elevation: 2,
  },
  doneIcon: {
    transform: [{ rotate: '-45deg' }],
    color: 'white',
  },
  doneButton: {
    backgroundColor: '#434A54',
    margin: 15,
    padding: 10,
    transform: [{ rotate: '45deg' }],
    borderRadius: 10,
    elevation: 3,
  },
  container: {
    flex: 1,
    width: '100%',
    justifyContent: 'flex-end',
  },
  headerContainer: {
    backgroundColor: Colors.mantisGreen,
    paddingVertical: 5,
    height: 70,
  },
  editModeChooserContainer: {
    margin: 5,
    position: 'absolute',
    top: 0,
    right: 0,
    left: 0,
  },
  title: {
    ...text,
    textAlign: 'center',
    fontSize: 18,
  },
  modeChooserHint: {
    ...text,
    textAlign: 'center',
  },
  errorText: {
    ...text,
    textAlign: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.87)',
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
  },
  spinner: {
    marginVertical: 2,
  },
  floatingContainer: { 
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'space-between',
    width: '100%',
    paddingStart: 15,
  },
  mapContainer: { 
    flex: 1, 
    justifyContent: 'flex-end',
  },
  showTrackLogButton: {
    backgroundColor: 'white',
    margin: 15,
    padding: 10,
    borderRadius: 10,
    elevation: 2,
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    padding: 5,
    paddingHorizontal: 10,
  },
  buttonStyle: {
    flex: 1,
    borderWidth: 1.3,
    borderColor: 'black',
    borderRadius: 5,
    justifyContent: 'center',
    padding: 5,
    margin: 5,
    backgroundColor: Colors.mantisGreen,
  },
  buttonTextStyle: {
    textAlign: 'center',
    color: 'white',
  },
});

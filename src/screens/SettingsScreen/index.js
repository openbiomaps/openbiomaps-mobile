import React, { Component } from 'react';
import { ScrollView, KeyboardAvoidingView, Platform } from 'react-native';

import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as actions from '../../actions';
import I18n from '../../i18n';
import { APP_VERSION, LANG, MAP_ENGINE, PIN_SAVE_LOGIC } from '../../Constants';
import { sortAlphabeticallyByKey } from '../../utils';
import {
  getCurrentLanguage,
  removeAllUnusedFiles,
} from '../../utils/reduxUtils';
import {
  SettingsItem,
  SignalFooter,
  BackupSettingsItem,
} from '../../components';

// import DeviceInfo from 'react-native-device-info';
/* DYNAMIC_CODE_UPDATE >>>
 * imports_in_SettingsScreen */
/* <<< DYNAMIC_CODE_UPDATE */

class SettingsScreen extends Component {
  componentDidUpdate = (prevProps) => {
    const { language, navigation } = this.props;

    if (prevProps.language.id !== language.id) {
      // Force reload current component when the language is changed.
      this.forceUpdate();
      // Force reload navigaiton header when the language is changed.
      navigation.setParams();
    }
  };

  /* DYNAMIC_CODE_UPDATE >>>
   * consts_in_SettingsScreen_component */
  /* <<< DYNAMIC_CODE_UPDATE */

  render() {
    const {
      changeSettingsData,
      isBackupEnabled,
      isAutoSyncEnabled,
      isSuccessNotificationSound,
      mapEngine,
      pinSaveLogic,
      createBackup,
      trackLogUpdateInterval,
      distanceFilter,
    } = this.props;

    return (
      <KeyboardAvoidingView
        style={{ flex: 1 }}
        behavior={Platform.OS === 'ios' ? 'padding' : null}
        enabled
        keyboardVerticalOffset={Platform.select({ ios: 100, android: 550 })}
      >
        <ScrollView>
          <SettingsItem type="title" title={I18n.t('general')} />
          <SettingsItem
            type="picker"
            title={I18n.t('language')}
            data={sortAlphabeticallyByKey(LANG, 'value').map(
              ({ id, value }) => ({
                key: id,
                value,
              }),
            )}
            onValueChange={({ key: id, value }) =>
              changeSettingsData('language', { id, value })
            }
            value={getCurrentLanguage().value}
          />

          <SettingsItem type="title" title={I18n.t('backup')} />
          <SettingsItem
            type="switch"
            title={I18n.t('auto_backup')}
            description={I18n.t('auto_backup_description')}
            value={isBackupEnabled}
            onValueChange={(value) =>
              changeSettingsData('isBackupEnabled', value)
            }
          />
          <SettingsItem
            type="button"
            title={I18n.t('create_backup')}
            description={I18n.t('create_backup_description')}
            iconName="save"
            onPress={() => createBackup(I18n.t('successful_backup_creation'))}
          />
          <BackupSettingsItem title={I18n.t('restore_backup')} />

          <SettingsItem type="title" title={I18n.t('form')} />
          <SettingsItem
            type="picker"
            title={I18n.t('pin_status_save')}
            data={[
              {
                key: PIN_SAVE_LOGIC.DONT_SAVE,
                value: I18n.t(PIN_SAVE_LOGIC.DONT_SAVE),
              },
              { key: PIN_SAVE_LOGIC.SAVE, value: I18n.t(PIN_SAVE_LOGIC.SAVE) },
              {
                key: PIN_SAVE_LOGIC.SAVE_SYNC_DEL,
                value: I18n.t(PIN_SAVE_LOGIC.SAVE_SYNC_DEL),
              },
            ]}
            onValueChange={({ key }) => changeSettingsData('pinSaveLogic', key)}
            value={I18n.t(pinSaveLogic)}
          />

          <SettingsItem type="title" title={I18n.t('sync')} />
          <SettingsItem
            type="switch"
            title={I18n.t('auto_sync')}
            description={I18n.t('sync_with_wifi')}
            value={isAutoSyncEnabled}
            onValueChange={(value) =>
              changeSettingsData('isAutoSyncEnabled', value)
            }
          />

          <SettingsItem type="title" title={I18n.t('form')} />
          <SettingsItem
            type="switch"
            title={I18n.t('notification_sound_form_save')}
            value={isSuccessNotificationSound}
            onValueChange={(value) =>
              changeSettingsData('isSuccessNotificationSound', value)
            }
          />

          <SettingsItem type="title" title={I18n.t('map')} />
          <SettingsItem
            type="picker"
            title={I18n.t('map_engine')}
            data={sortAlphabeticallyByKey(MAP_ENGINE, 'value').map(
              ({ id, value }) => ({
                key: id,
                value,
              }),
            )}
            onValueChange={({ key: id, value }) =>
              changeSettingsData('mapEngine', { id, value })
            }
            value={mapEngine.value}
          />

          <SettingsItem type="title" title={I18n.t('gps')} />
          <SettingsItem
            type="numtext"
            title={I18n.t('distance_filter')}
            description={I18n.t('distance_filter_description')}
            value={distanceFilter}
            maxValue={500}
            minValue={0}
            alertTitle={I18n.t('performance_alert_title')}
            alertMessage={I18n.t('frequent_position_update_alert_message')}
            onValueChange={(value) =>
              changeSettingsData('distanceFilter', value)
            }
          />

          <SettingsItem type="title" title={I18n.t('tracklog')} />
          <SettingsItem
            type="numtext"
            title={I18n.t('tracklog_update_time')}
            description={I18n.t('tracklog_time_description')}
            value={trackLogUpdateInterval}
            maxValue={30}
            minValue={0}
            onValueChange={(value) =>
              changeSettingsData('trackLogUpdateInterval', value)
            }
            multiplierForStoring={1000}
          />

          <SettingsItem type="title" title={I18n.t('storage_settings')} />
          <SettingsItem
            type="button"
            title={I18n.t('delete_unused_files')}
            description={I18n.t('delete_unused_files_description')}
            iconName="delete"
            onPress={removeAllUnusedFiles}
          />

          <SettingsItem type="title" title={I18n.t('about')} />
          <SettingsItem
            type="info"
            title={I18n.t('version')}
            description={APP_VERSION}
          />
          {
            // ONLY_FOR_CUSTOM_APP
            this.custom_app_version !== undefined && (
              // eslint-disable-next-line no-undef
              <SettingsItem
                type="info"
                title="Custom app-version"
                description={CUSTOM_APP_VERSION}
              />
            )
          }
          {
            // ONLY_FOR_CUSTOM_APP
            this.device_uniq_id !== undefined && (
              // eslint-disable-next-line no-undef
              <SettingsItem
                type="info"
                title="Device Id"
                description={DeviceInfo.getUniqueId()}
              />
            )
          }
        </ScrollView>
        <SignalFooter
          connectionText={I18n.t('internet_connection')}
          noConnectionText={I18n.t('no_connection')}
        />
      </KeyboardAvoidingView>
    );
  }
}

SettingsScreen.propTypes = {
  changeSettingsData: PropTypes.func.isRequired,
  isBackupEnabled: PropTypes.bool.isRequired,
  isAutoSyncEnabled: PropTypes.bool.isRequired,
  isSuccessNotificationSound: PropTypes.bool.isRequired,
  language: PropTypes.object.isRequired,
  navigation: PropTypes.object.isRequired,
  mapEngine: PropTypes.object.isRequired,
  pinSaveLogic: PropTypes.string.isRequired,
  createBackup: PropTypes.func.isRequired,
  trackLogUpdateInterval: PropTypes.number.isRequired,
  distanceFilter: PropTypes.number.isRequired,
};

const mapStateToProps = ({ settings }) => {
  const {
    isBackupEnabled,
    isAutoSyncEnabled,
    isSuccessNotificationSound,
    language,
    mapEngine,
    pinSaveLogic,
    trackLogUpdateInterval,
    distanceFilter,
  } = settings;

  return {
    isBackupEnabled,
    isAutoSyncEnabled,
    isSuccessNotificationSound,
    language,
    mapEngine,
    pinSaveLogic,
    trackLogUpdateInterval,
    distanceFilter,
  };
};

const mapDispatchToProps = (dispatch) => {
  const { changeSettingsData, createBackup } = actions;

  return bindActionCreators(
    {
      changeSettingsData,
      createBackup,
    },
    dispatch,
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(SettingsScreen);

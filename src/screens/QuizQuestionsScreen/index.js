import React, { Component } from 'react';
import { ScrollView, Text, View } from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from '../../actions';
import { footerStyle, TouchableIcon } from '../../components';
import Question from './Question';
import I18n from '../../i18n';
import styles from './styles';
import { getSelectedServer, getSelectedDatabase, getSelectedObservation } from '../../utils';
import NavigationHandler from '../../NavigationHandler';

class QuizQuestionsScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      questions: props.questions.map(question => ({
        ...question,
        answers: question.answers.map(answer => ({
          ...answer,
          value: false,
        })),
      })),
      showError: false,
    };
  }

  handleDonePress = () => {
    const { serverId, databaseId, observationId, navigation, quizPassed } = this.props;
    const { state: { params } } = navigation;
    const { questions } = this.state;
    const passed = questions.reduce((acc, question) =>
      acc && question.answers.reduce((answerAcc, answer) =>
        answerAcc && (!answer.isRight === !answer.value), true), true);

    if (passed) {
      quizPassed(serverId, databaseId, observationId);
      NavigationHandler.navigate('QuizTask', { title: params.title });
    } else {
      this.setState({ showError: true });
    }
  }

  handleValueChange = (questionIndex, answer, value) => {
    this.setState((prevState) => {
      const questions = [...prevState.questions];
      const answers = questions[questionIndex].answers.map(answer => (
        questions[questionIndex].qtype === 'singleselect'
          ? { ...answer, value: false }
          : { ...answer }));

      answers[answer].value = value;
      questions[questionIndex].answers = answers;
      return {
        questions,
        showError: false,
      };
    });
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        {this.state.showError && <Text style={styles.error}>{I18n.t('quiz_error')}</Text>}
        <ScrollView contentContainerStyle={styles.container}>
          {this.state.questions.map((question, index) => (
            <Question
              {...question}
              key={question.caption}
              onValueChange={(answer, value) => this.handleValueChange(index, answer, value)}
              showErrors={this.state.showErrors}
            />
          ))}
        </ScrollView>
        <View style={footerStyle.container}>
          <TouchableIcon name="home" onPress={() => NavigationHandler.navigate('Home')} style={[footerStyle.leftButton, footerStyle.roundedShape]} />
          <View style={footerStyle.rightButtonContainer} >
            <TouchableIcon name="done" onPress={this.handleDonePress} style={[footerStyle.rightButton, footerStyle.roundedShape]} />
          </View>
        </View>
      </View>
    );
  }
}

QuizQuestionsScreen.propTypes = {
  serverId: PropTypes.string.isRequired,
  databaseId: PropTypes.string.isRequired,
  observationId: PropTypes.string.isRequired,
  navigation: PropTypes.shape({}).isRequired,
  quizPassed: PropTypes.func.isRequired,
  questions: PropTypes.arrayOf(PropTypes.shape({
    caption: PropTypes.string.isRequired,
    answers: PropTypes.arrayOf(PropTypes.shape({
      text: PropTypes.string.isRequired,
      isRight: PropTypes.bool.isRequired,
    })).isRequired,
    qtype: PropTypes.string.isRequired,
  })).isRequired,
};

const mapStateToProps = ({ servers: { data: serverList } }) => {
  const selectedServer = getSelectedServer(serverList);
  const selectedDatabase = getSelectedDatabase(selectedServer);
  const selectedObservation = getSelectedObservation(selectedDatabase);
  const { id, game: { questions } } = selectedObservation;

  return { 
    serverId: selectedServer.id,
    databaseId: selectedDatabase.id, 
    observationId: id, 
    questions,
  };
};

const mapDispatchToProps = (dispatch) => {
  const { quizPassed } = actions;

  return bindActionCreators(
    {
      quizPassed,
    },
    dispatch,
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(QuizQuestionsScreen);

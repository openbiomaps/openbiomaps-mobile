import React from 'react';
import { ScrollView, View } from 'react-native';
import PropTypes from 'prop-types';
import HTMLView from 'react-native-htmlview';
import Answer from './Answer';
import styleSheet from '../QuizInformationScreen/styles';
import styles from './styles';
import { renderNode } from '../../utils';

const Question = ({
  answers,
  caption,
  onValueChange,
}) => (
  <View style={styles.questionContainer}>
    <ScrollView style={styles.htmlContainer}>
      <HTMLView value={caption} stylesheet={styleSheet} renderNode={renderNode} />
    </ScrollView>
    {answers.map((answer, index) => (
      <Answer
        key={answer.text}
        value={answer.value}
        onValueChange={value => onValueChange(index, value)}
        text={answer.text}
      />
    ))}
  </View>
);

Question.propTypes = {
  answers: PropTypes.arrayOf(PropTypes.shape({
    text: PropTypes.string.isRequired,
    value: PropTypes.bool,
    isRight: PropTypes.bool.isRequired,
  })).isRequired,
  caption: PropTypes.string.isRequired,
  onValueChange: PropTypes.func.isRequired,
};

export default Question;

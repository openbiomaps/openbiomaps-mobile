import React, { Component } from 'react'; 
import { RNCamera } from 'react-native-camera';
import { TouchableOpacity, Alert, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

class Camera extends Component {
  constructor(props) {
    super(props);
    this.state = {
      photo: null,
      takingPic: false,
    };
  }
 
    componentDidMount = () => {
      this.setState({ photo: null });
    }

      takePicture = async () => {
        if (this.camera && !this.state.takingPic) {
          const options = {
            quality: 0.85,
            fixOrientation: true,
            forceUpOrientation: true,
            pauseAfterCapture: true,
          };
    
          this.setState({ takingPic: true });
    
          try {
            const data = await this.camera.takePictureAsync(options);
            this.setState({ photo: data });
          } catch (err) {
            Alert.alert('Error', `Failed to take picture: ${err.message || err}`);
            return;
          } finally {
            this.setState({ takingPic: false });
          }
        }
      };

      savePicture = () => {
        const { uri } = this.state.photo;
        const { addItem } = this.props.navigation.state.params;
        const fields = uri.split('/');
        const name = fields[fields.length - 1];
        addItem('image/jpeg', name, uri);
        this.props.navigation.goBack();
      }

      render() {
        return (
          <RNCamera
            ref={(ref) => {
              this.camera = ref;
            }}
            captureAudio={false}
            style={{ flex: 1 }}
            type={RNCamera.Constants.Type.back}
            androidCameraPermissionOptions={{
              title: 'Permission to use camera',
              message: 'We need your permission to use your camera',
              buttonPositive: 'Ok',
              buttonNegative: 'Cancel',
            }}
          >
            {!this.state.photo
              ? (
                <TouchableOpacity
                  activeOpacity={0.5}
                  style={styles.btnAlignment}
                  onPress={this.takePicture}
                >
                  <Icon name="camera" size={50} color="#fff" />
                </TouchableOpacity>
              ) : (
                <TouchableOpacity
                  activeOpacity={0.5}
                  style={styles.btnAlignment}
                  onPress={this.savePicture}
                >
                  <Icon name="check-circle" size={50} color="#fff" />
                </TouchableOpacity>
              )}
            
          </RNCamera>
        );
      }
}

const styles = StyleSheet.create({
  btnAlignment: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-end',
    alignItems: 'center',
    marginBottom: 20,
  },
});

export default Camera;

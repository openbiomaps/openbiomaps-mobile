import React, {Component} from 'react';
import {Platform, View} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import PropTypes from 'prop-types';
import {FileSystem} from 'react-native-unimodules';
import {zip} from 'react-native-zip-archive';
import {createGpxFromTrackLog, formatToFilename, getAllMeasurements, parseDataToWkt,} from '../../utils';
import {hasExternalStoragePermission, requestExternalSAFDirPermission,} from '../../utils/permissonRequests';
import I18n from '../../i18n';
import styles from './styles';
import {MenuItem, showAlertDialog} from '../../components';
import * as actions from '../../actions';
import * as Sentry from '@sentry/react-native';
import { cloneDeep } from 'lodash';

const {StorageAccessFramework} = FileSystem;
const OBM_TEMPORARY_CACHE_DIR = `${FileSystem.cacheDirectory}OBM`;

class ExportScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isExportButtonDisabled: false,
            isTracklogButtonDisabled: false,
            allFilesCopied: true,
        };
    }

    exportFailedAlert = (message) => {
        showAlertDialog({
            title: I18n.t('error_title_export'),
            message: I18n.t('error_text_export') + '\n' + message,
            positiveLabel: I18n.t('ok'),
        });
    };

    exportFinishedAlert = (zipPath, positivePress) => {
        const {allFilesCopied} = this.state;
        const errorInfo = allFilesCopied ? '' : I18n.t('error_file_copy');
        showAlertDialog({
            title: I18n.t('export_finished'),
            message: `${I18n.t('export_path')}: ${zipPath} \n${errorInfo}`,
            positiveLabel: I18n.t('ok'),
            onPositiveBtnPress: positivePress,
        });
    };

    removeDir = async (path) => {
        try {
            await FileSystem.deleteAsync(path, {idempotent: true});
        } catch (error) {
            console.log(error);
        }
    };

    /**
     * Creates a zip file from a directory given by zipPath. It must be a local uri (inside the apps private directory).
     * Removes the folder itself if removeOriginal is set to true (default)
     * @param {string} zipPath
     * @param {boolean} [removeOriginal]
     * @returns {Promise<boolean>} true if succeeded
     */
    zipDir = async (zipPath, removeOriginal = true) => {
        try {
            await zip(zipPath, `${zipPath}.zip`);
        } catch (error) {
            this.exportFailedAlert(error);
            return false;
        }
        if (removeOriginal) await this.removeDir(zipPath);
        return true;
    };

    getHeaderValue = (headerKey, obj) => {
        const foundValue = headerKey
            .replace(/\[([^\]]+)]/g, '.$1')
            .split('.')
            .reduce((data, param, _, arr) => {
                // if at any point the nested keys passed do not exist, splice the array so it doesnt keep reducing
                if (data[param] === undefined) {
                    arr.splice(1);
                }
                return data[param];
            }, obj);

        const newValue =
            typeof foundValue === 'object' ? JSON.stringify(foundValue) : foundValue;

        if (newValue === undefined) {
            return headerKey in obj ? obj[headerKey] : '';
        }
        return newValue;
    };

    jsons2arrays = (jsons, headers) => {
        const {headerKeys, headerLabels} = headers.reduce(
            (acc, headerItem) => {
                acc.headerKeys.push(headerItem.key);
                acc.headerLabels.push(headerItem.label);
                return acc;
            },
            {headerKeys: [], headerLabels: []},
        );
        const data = jsons.map((json) =>
            headerKeys.map((headerKey) => this.getHeaderValue(headerKey, json)),
        );
        return [headerLabels, ...data];
    };

    createCsvString = (formData, separator = '\t', enclosingCharacter = '') =>
        this.jsons2arrays(formData.data, formData.headers)
            .filter((e) => e)
            .map((row) =>
                row
                    .map(
                        (column) => `${enclosingCharacter}${column}${enclosingCharacter}`,
                    )
                    .join(separator),
            )
            .join('\n');

    /**
     * Writes the existing tracklogs to a directory given by a file uri
     * @async
     * @param {string} trackLogDirPath file uri of directory
     * @returns {Promise<number>} number of succeeded tracklogs written
     */
    writeTracklogWithFileUri = async (trackLogDirPath) => {
        const {trackLogList} = this.props;

        const promises = trackLogList.map(async (trackLog) => {
            if (trackLog.trackLogArray.length) {
                try {
                    const gpxTrackLog = createGpxFromTrackLog(trackLog);
                    const newTracklogFileUri = `${trackLogDirPath}/${formatToFilename(
                        trackLog.trackLogName,
                    )}.gpx`;
                    await FileSystem.writeAsStringAsync(newTracklogFileUri, gpxTrackLog, {
                        encoding: 'utf8',
                    });
                    return true;
                } catch (error) {
                    console.log('write tracklog error: ', {error});
                    return false;
                }
            }
            return false;
        });
        const promiseResponses = await Promise.all(promises);
        return promiseResponses.filter((success) => success).length;
    };

    writeFiles = async (formData, extDirUri) => {
        const promises = [];
        const tempDirPath = `${OBM_TEMPORARY_CACHE_DIR}/export`;
        const tracklogDirPath = `${tempDirPath}/tracklog`;
        const fileDirPath = `${tempDirPath}/files`;

        // cleaning temp dir path
        await FileSystem.deleteAsync(OBM_TEMPORARY_CACHE_DIR, {idempotent: true});

        // creating subfolders
        await FileSystem.makeDirectoryAsync(tracklogDirPath, {
            intermediates: true,
        });
        await FileSystem.makeDirectoryAsync(fileDirPath, {intermediates: true});

        // write tracklogs
        await this.writeTracklogWithFileUri(tracklogDirPath);

        // writing files and csvs
        formData.forEach((observation) => {
            const formName = observation.formName
                .replace(/([^a-z0-9 ]+)/gi, '-')
                .replace(/\s/g, '_');
            const pathToWrite = `${tempDirPath}/${observation.formId}_${formName}.csv`;
            observation.data.forEach((data) => {
                if (typeof data.obm_files_id !== 'undefined') {
                    data.obm_files_id.forEach((files) =>
                        promises.push(
                            FileSystem.copyAsync({
                                from: files.uri,
                                to: `${fileDirPath}/${files.uri.substr(
                                    files.uri.lastIndexOf('/') + 1,
                                )}`,
                            }),
                        ),
                    );
                }
            });
            // \uFEFF is a universal BOM for UTF-8
            promises.push(
                FileSystem.writeAsStringAsync(
                    pathToWrite,
                    `\uFEFF ${this.createCsvString(observation)}`,
                    {encoding: 'utf8'},
                ),
            );
        });

        try {
            await Promise.all(promises);
        } catch (error) {
            Sentry.captureException(new Error(`Export Screen writeFiles: ${error}`));
            console.log('error', {error});
            this.setState({allFilesCopied: false});
        }

        // zip the files directory, because subfolder zipping creates multiple subfolders in the main diretory
        const filesZipSucceeded = await this.zipDir(fileDirPath);
        if (!filesZipSucceeded) throw new Error('files zipping error');

        const tracklogSucceeded = await this.zipDir(tracklogDirPath);
        if (!tracklogSucceeded) throw new Error('tracklog zipping error');

        const allZipSucceeded = await this.zipDir(tempDirPath);
        if (!allZipSucceeded) throw new Error('final zip error');

        if (Platform.OS == 'android') {
            const zipFile = await FileSystem.readAsStringAsync(`${tempDirPath}.zip`, {
                encoding: FileSystem.EncodingType.Base64,
            });

            const safFile = await StorageAccessFramework.createFileAsync(
                extDirUri,
                `${this.getTodayDate()}.zip`,
                'application/zip',
            );

            await StorageAccessFramework.writeAsStringAsync(safFile, zipFile, {
                encoding: FileSystem.EncodingType.Base64,
            });
        } else if (Platform.OS == 'ios') {
            await FileSystem.copyAsync({
                from: `${tempDirPath}.zip`,
                to: FileSystem.documentDirectory + `${this.getTodayDate()}.zip`,
            });
        }

        await FileSystem.deleteAsync(`${tempDirPath}.zip`, {idempotent: true});
    };

    getTodayDate = () => {
        const today = new Date();
        const dd = String(today.getDate()).padStart(2, '0');
        const mm = String(today.getMonth() + 1).padStart(2, '0');
        const yyyy = today.getFullYear();
        const hours = today.getHours();
        const minutes = today.getMinutes();
        const seconds = today.getSeconds();
        const miliSeconds = today.getMilliseconds();
        return yyyy + mm + dd + '_' + hours + minutes + seconds + miliSeconds;
    };

    constructDataToWrite = () => {
        const {serverList} = this.props;
        const measurementsData = getAllMeasurements(serverList);
        const preparedData = [];

        Object(measurementsData).forEach((measurementData) => {
            const formId = measurementData.observation.id;
            if (!preparedData.some((observation) => observation.formId === formId)) {
                // create the header if not exists for an observation
                const headers = [];
                const [...dataTypes] = measurementData.observation.form.dataTypes;
                dataTypes.forEach((dataType) => {
                    const headerElement = {
                        label: dataType.column,
                        key: dataType.column,
                    };
                    headers.push(headerElement);
                });
                headers.push({label: 'meta', key: 'meta'});
                headers.push({label: 'synced', key: 'synced'});

                const formName = measurementData.observation.name;
                preparedData.push({formId, formName, headers, data: []});
            }

            preparedData.forEach((observation) => {
                if (observation.formId === formId) {
                    const data = cloneDeep(measurementData.measurement.data);
                    const meta = measurementData.measurement.meta;
                    const synced = measurementData.measurement.isSynced;
                    const dataTypes = measurementData.observation.form.dataTypes;

                    dataTypes.forEach(({column, type}) => {
                        if (
                            data !== undefined &&
                            ['point', 'line', 'polygon', 'wkt'].includes(type)
                        ) {
                            if (data[column] !== null) {
                                data[column] =
                                    type === 'wkt' && data[column].wktType
                                        ? parseDataToWkt(
                                            data[column].wktValue,
                                            data[column].wktType,
                                        )
                                        : parseDataToWkt(data[column], type);
                            }
                        }
                    });
                    observation.data.push({...data, meta, synced});
                }
            });
        });

        return preparedData;
    };

    writePermission = async () => {
        try {
            const granted = await hasExternalStoragePermission();
            if (granted) return true;
            showAlertDialog({
                title: I18n.t('error_title_file_writing_permission'),
                message: I18n.t('error_text_file_writing_permission'),
                positiveLabel: I18n.t('ok'),
            });
            return false;
        } catch (err) {
            console.warn(err);
        }
        return false;
    };

    startExport = async () => {
        const {externalExportDirectory, changeSettingsData} = this.props;
        this.setState({isExportButtonDisabled: true});
        try {
            if (Platform.OS == 'android') {
                const extExportDirUri = await requestExternalSAFDirPermission(
                    externalExportDirectory,
                    'Export',
                );

                if (extExportDirUri)
                    changeSettingsData('externalExportDirectory', extExportDirUri);

                const permission = await this.writePermission();

                if (permission && extExportDirUri) {
                    
                    const constructedData = this.constructDataToWrite();

                    await this.writeFiles(constructedData, extExportDirUri);
                    await FileSystem.deleteAsync(OBM_TEMPORARY_CACHE_DIR, {
                        idempotent: true,
                    });

                    this.exportFinishedAlert(
                        `${decodeURIComponent(extExportDirUri)}/${this.getTodayDate()}.zip`,
                    );
                } else {
                    throw new Error('Export Screen startExport: permission denied');
                }
            } else if (Platform.OS == 'ios') {
                const permission = await this.writePermission();
                if (permission) {
                    const constructedData = this.constructDataToWrite();
                    await this.writeFiles(constructedData, FileSystem.documentDirectory);
                    this.exportFinishedAlert(
                        I18n.t('iosBackupFolderPlace') + ' ' + `${this.getTodayDate()}.zip`,
                    );
                } else {
                    throw new Error('permission denied');
                }
            }
        } catch (error) {
            console.log('export failed: ', {error});
            this.exportFailedAlert(error);
        } finally {
            this.setState({isExportButtonDisabled: false});
        }
    };

    tracklogExport = async () => {
        const {
            externalTrackLogExportDirectory,
            changeSettingsData,
            trackLogList,
        } = this.props;
        this.setState({isTracklogButtonDisabled: true});
        try {
            if (Platform.OS == 'android') {
                const extTrackLogExportDirUri = await requestExternalSAFDirPermission(
                    externalTrackLogExportDirectory,
                    'Tracklog Export',
                );
                if (extTrackLogExportDirUri)
                    changeSettingsData(
                        'externalTrackLogExportDirectory',
                        extTrackLogExportDirUri,
                    );

                const permission = await this.writePermission();
                if (permission && extTrackLogExportDirUri) {
                    const tempDirPath = `${OBM_TEMPORARY_CACHE_DIR}/tracklogexport`;

                    // clean temporary cache dir and create an empty one
                    await FileSystem.deleteAsync(OBM_TEMPORARY_CACHE_DIR, {
                        idempotent: true,
                    });
                    await FileSystem.makeDirectoryAsync(tempDirPath, {
                        intermediates: true,
                    });

                    const succeededTracks = await this.writeTracklogWithFileUri(
                        tempDirPath,
                    );
                    if (!succeededTracks && trackLogList.length) {
                        throw new Error('No tracklogs could be exported');
                    }

                    const zipSucceded = await this.zipDir(tempDirPath);
                    if (!zipSucceded) {
                        throw new Error("couldn't create a zip file");
                    }

                    // Reading the created zip file from the local storage, where it has been created
                    const zipFile = await FileSystem.readAsStringAsync(
                        `${tempDirPath}.zip`,
                        {encoding: FileSystem.EncodingType.Base64},
                    );

                    // Writing the zip file to the specified external storage directory
                    const safFile = await StorageAccessFramework.createFileAsync(
                        extTrackLogExportDirUri,
                        `exported_tracklogs_${this.getTodayDate()}.zip`,
                        'application/zip',
                    );
                    await StorageAccessFramework.writeAsStringAsync(safFile, zipFile, {
                        encoding: FileSystem.EncodingType.Base64,
                    });

                    this.exportFinishedAlert(decodeURIComponent(safFile));
                }
            } else if (Platform.OS == 'ios') {
                const permission = await this.writePermission();

                if (permission) {
                    const tempDirPath = `${OBM_TEMPORARY_CACHE_DIR}/tracklogexport`;

                    await FileSystem.makeDirectoryAsync(tempDirPath, {
                        intermediates: true,
                    });

                    const succeededTracks = await this.writeTracklogWithFileUri(
                        tempDirPath,
                    );
                    if (!succeededTracks && trackLogList.length) {
                        throw new Error('No tracklogs could be exported');
                    }

                    const zipSucceded = await this.zipDir(tempDirPath);
                    if (!zipSucceded) {
                        throw new Error("couldn't create a zip file");
                    }

                    await FileSystem.copyAsync({
                        from: `${tempDirPath}.zip`,
                        to:
                            FileSystem.documentDirectory +
                            `exported_tracklogs_${this.getTodayDate()}.zip`,
                    });
                    this.exportFinishedAlert(
                        I18n.t('iosBackupFolderPlace') +
                        ' ' +
                        `exported_tracklogs_${this.getTodayDate()}.zip`,
                    );
                } else {
                    throw new Error('permission denied');
                }
            }
        } catch (error) {
            console.log('tracklog export error: ', {error});
            this.exportFailedAlert(error);
        } finally {
            if (Platform.OS == 'android') {
                // cleaning up in temp dir
                await FileSystem.deleteAsync(OBM_TEMPORARY_CACHE_DIR, {
                    idempotent: true,
                });
                this.setState({isTracklogButtonDisabled: false});
            } else if (Platform.OS == 'ios') {
                await FileSystem.deleteAsync(OBM_TEMPORARY_CACHE_DIR, {
                    idempotent: true,
                });
                this.setState({isTracklogButtonDisabled: false});
            }
        }
    };

    render() {
        const {isExportButtonDisabled, isTracklogButtonDisabled} = this.state;
        const {trackLogList} = this.props;
        return (
            <View style={styles.baseContainer}>
                <MenuItem
                    customContainerStyle={
                        isExportButtonDisabled ? styles.disabled : styles.enabled
                    }
                    numberOfLines={2}
                    disabled={isExportButtonDisabled}
                    onPress={() => this.startExport()}
                    title={`${I18n.t('start_export')}`}
                />
                <MenuItem
                    customContainerStyle={
                        isTracklogButtonDisabled || !trackLogList.length
                            ? styles.disabled
                            : styles.enabled
                    }
                    numberOfLines={2}
                    disabled={isTracklogButtonDisabled}
                    onPress={() => this.tracklogExport()}
                    title={`${I18n.t('tracklog_export')}`}
                />
            </View>
        );
    }
}

ExportScreen.propTypes = {
    serverList: PropTypes.array.isRequired,
    trackLogList: PropTypes.array.isRequired,
    externalTrackLogExportDirectory: PropTypes.string,
    externalExportDirectory: PropTypes.string,
    changeSettingsData: PropTypes.func.isRequired,
};

ExportScreen.defaultProps = {
    externalTrackLogExportDirectory: null,
    externalExportDirectory: null,
};

const mapStateToProps =
    () =>
        ({
             servers: {data: serverList},
             trackLog: {trackLogList},
             settings: {externalTrackLogExportDirectory, externalExportDirectory},
         }) => ({
            serverList,
            trackLogList,
            externalTrackLogExportDirectory,
            externalExportDirectory,
        });

const mapDispatchToProps = (dispatch) => {
    const {changeSettingsData} = actions;

    return bindActionCreators(
        {
            changeSettingsData,
        },
        dispatch,
    );
};

export default connect(mapStateToProps, mapDispatchToProps)(ExportScreen);

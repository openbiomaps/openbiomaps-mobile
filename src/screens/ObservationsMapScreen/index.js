import React, {Component} from 'react';
import {InteractionManager, Text, View,} from 'react-native';

import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import moment from 'moment';

import * as actions from '../../actions';
import I18n from '../../i18n';
import {footerStyle, ListModal, MapComponent, Picker, Spinner, TouchableIcon, ZoomScale} from '../../components';
import NavigationHandler from '../../NavigationHandler';
import {
    getFilteredTrackLogListBySelectedDatabase,
    getMeasurementsForMap,
    getNonEmptyDatabases,
    getPermanentSamplePlots,
    getSelectedDatabase,
    getSelectedObservation,
    getSelectedServer,
    getTrackLogById,
    measurementDataToArray,
} from '../../utils';
import {MapScreenSettingsModal} from '../../components/MapScreenSettingsModal';
import {DELTAS, FALLBACK_POSITION, GPS_ERROR, GPS_OBSERVER} from '../../Constants';
import withGpsSubscription, {GpsDefaultPropTypes, GpsPropTypes} from '../../hocs/withGpsSubscription';

import styles from './styles';

class ObservationMap extends Component {
    constructor(props) {
        super(props);

        const {trackLogToShowOnly, currentTrackLog} = props;

        this.state = {
            region: undefined,
            rotation: undefined,
            isMeasurementDetailModalOpen: false,
            selectedListModalCompatibleMeasurement: null,
            showTrackLog: true,
            showMeasurements: true,
            showSettings: false,
            showPernamentSamplePlots: false,
            showOnlyOneTrackLog: trackLogToShowOnly !== null || currentTrackLog !== null,
        };
    }

    componentDidMount() {
        InteractionManager.runAfterInteractions(() => {
            const {gps} = this.props;
            gps.subscribe();
        });
    }

    componentDidUpdate(prevProps) {
        const {gps} = this.props;

        if (prevProps.gps.error && !gps.error) {
            this.handleMyLocationPress();
        }
    }

    componentWillUnmount() {
        const {gps} = this.props;
        gps.unsubscribe();
    }

    handleRegionChange = async (region) => {
        const response = await this.map.getCamera();
        this.setState({region, rotation: response.heading});
    };

    handleMyLocationPress = async () => {
        const position = this.getPosition();
        this.map.animateCamera({center: position});
    };

    handleMeasurementPress = (measurement) => {
        this.setState({
            isMeasurementDetailModalOpen: true,
            selectedListModalCompatibleMeasurement: measurementDataToArray(measurement),
        });
    }

    handleDatabasePress = (databaseId) => {
        const {selectDatabaseForMap, selectedDatabaseForMap, databaseList} = this.props;
        if (databaseId !== selectedDatabaseForMap?.key) {
            const newDb = databaseList.find((db) => db.key === databaseId);
            if (newDb) {
                selectDatabaseForMap(newDb);
            }
        }
    }

    hideDetailsModal = () => {
        this.setState({
            isMeasurementDetailModalOpen: false,
        });
    }

    hideSettingsModal = () => {
        this.setState({
            showSettings: false,
        });
    }

    changeSettingsModalVisibility = () => {
        this.setState(({showSettings}) => ({
            showSettings: !showSettings,
        }));
    }

    changeTrackLogVisibility = () => {
        this.setState(({showTrackLog}) => ({
            showTrackLog: !showTrackLog,
        }));
    }

    changeMeasurementsVisibility = () => {
        this.setState(({showMeasurements}) => ({
            showMeasurements: !showMeasurements,
        }));
    }

    changePernamentSamplePlotsVisibility = () => {
        this.setState(({showPernamentSamplePlots}) => ({
            showPernamentSamplePlots: !showPernamentSamplePlots,
        }));
    }

    changeDateTimeFilter = (filterData, startTime, endTime) => {
        const {changeSettingsData} = this.props;
        changeSettingsData('dateFilterOnMapSettings', {filterData, startTime, endTime, setTime: new Date()});
    }

    shiftIntervalToCurrentTime = () => {
        const {filterData, startTime, endTime, setTime, changeSettingsData} = this.props;

        const diff = moment().diff(setTime);
        const newStart = new Date(new Date(startTime).getTime() + diff);
        const newEnd = new Date(new Date(endTime).getTime() + diff);

        changeSettingsData('dateFilterOnMapSettings', {
            filterData,
            startTime: newStart,
            endTime: newEnd,
            setTime: new Date()
        });
    }

    cancelShowOnlyOneTrackLog = () => {
        this.setState({
            showOnlyOneTrackLog: false,
        });
    }

    isLoading = () => {
        const {gps} = this.props;
        return !gps.position && !gps.error;
    }

    getPosition = () => {
        const {gps} = this.props;
        return !gps.error ? gps.position : FALLBACK_POSITION;
    }

    getRegion = () => {
        const {region} = this.state;
        const {trackLogToShowOnly} = this.props;

        const position = this.getPosition();

        const {trackLogArray = []} = trackLogToShowOnly || {};
        const trackLogPosition = trackLogArray.length > 0 ? trackLogArray[0] : null;

        return region || {...position, ...DELTAS, ...trackLogPosition};
    }

    render() {
        const {
            rotation,
            isMeasurementDetailModalOpen,
            selectedListModalCompatibleMeasurement,
            showSettings,
            showTrackLog,
            showMeasurements,
            showOnlyOneTrackLog,
            showPernamentSamplePlots,
        } = this.state;

        const {
            mapEngine,
            databaseList,
            selectedMeasurements,
            selectedDatabaseForMap,
            filterData,
            startTime,
            endTime,
            trackLogList,
            trackLogToShowOnly,
            currentTrackLog,
            permanentSamplePlots,
            gps,
        } = this.props;

        const isLoading = this.isLoading();
        const position = this.getPosition();
        const region = this.getRegion();

        if (isLoading) return <Spinner/>;

        return (
            <View style={styles.container}>
                <View style={styles.mapContainer}>
                    <MapComponent
                        mapRef={(ref) => {
                            this.map = ref;
                        }}
                        permanentSamplePlots={permanentSamplePlots}
                        provider={mapEngine.id}
                        region={region}
                        onRegionChange={this.handleRegionChange}
                        onMeasurementPress={this.handleMeasurementPress}
                        position={position}
                        lastMeasurements={(showMeasurements && selectedDatabaseForMap) ? selectedMeasurements : null}
                        showTrackLogs={showTrackLog}
                        showMeasurements={showMeasurements}
                        showPernamentSamplePlots={showPernamentSamplePlots}
                        trackLogList={showTrackLog ? trackLogList : []}
                        trackLogToShowOnly={showOnlyOneTrackLog ? trackLogToShowOnly : null}
                        currentTrackLog={showOnlyOneTrackLog ? currentTrackLog : null}
                        showOnlyOneTrackLog={showOnlyOneTrackLog}
                    />
                    <View style={[styles.floatingContainer, (rotation !== 0 && {justifyContent: 'flex-end'})]}>
                        {region && rotation === 0 && <ZoomScale region={region} rotation={rotation}/>}
                        {!showOnlyOneTrackLog
                            && (
                                <TouchableIcon
                                    name="visibility"
                                    onPress={() => this.setState({showSettings: !showSettings})}
                                    style={styles.showTrackLogButton}
                                />
                            )}
                        <TouchableIcon
                            name="my-location"
                            onPress={this.handleMyLocationPress}
                            style={styles.myLocationButton}
                        />
                    </View>
                </View>
                {!showOnlyOneTrackLog
                    && (
                        <View>
                            <ListModal
                                isOpen={isMeasurementDetailModalOpen}
                                listData={selectedListModalCompatibleMeasurement}
                                onPress={this.hideDetailsModal}
                            />
                            <MapScreenSettingsModal
                                isModalVisible={showSettings}
                                showTrackLog={showTrackLog}
                                changeTrackLogVisibility={this.changeTrackLogVisibility}
                                showMeasurements={showMeasurements}
                                changeMeasurementsVisibility={this.changeMeasurementsVisibility}
                                showPernamentSamplePlots={showPernamentSamplePlots}
                                changePernamentSamplePlotsVisibility={this.changePernamentSamplePlotsVisibility}
                                hideModal={this.hideSettingsModal}
                                changeFilter={this.changeDateTimeFilter}
                                filterByDate={filterData}
                                startTime={startTime || new Date().setDate(new Date().getDate() - 1)}
                                endTime={endTime || new Date()}
                                shiftIntervalToCurrentTime={this.shiftIntervalToCurrentTime}
                            />
                        </View>
                    )}
                <View style={styles.bottomContainer}>
                    <View style={footerStyle.container}>
                        <TouchableIcon
                            name="close"
                            onPress={() => NavigationHandler.goBack()}
                            style={[footerStyle.roundedShape, {paddingHorizontal: 10}]}
                        />
                        {!showOnlyOneTrackLog
                            && (
                                <Picker
                                    data={databaseList}
                                    value={(
                                            !databaseList.length && I18n.t('no_measurements'))
                                        || databaseList.find((db) => db.serverId === selectedDatabaseForMap?.serverId
                                            && db.key === selectedDatabaseForMap?.key)?.value
                                        || I18n.t('database_screen_title')}
                                    onItemSelect={this.handleDatabasePress}
                                    customPickerStyle={{
                                        baseContainer: [footerStyle.roundedShape, footerStyle.pickerContainer],
                                        labelContainer: {},
                                        text: {color: 'black'},
                                        textProps: {numberOfLines: 1, ellipsizeMode: 'tail'},
                                    }}
                                    touchableIconProps={{
                                        color: 'black',
                                        size: 30,
                                    }}
                                />
                            )}
                    </View>
                </View>
                <View style={styles.topContainer}>
                    {gps.error
                        && (
                            <View style={styles.errorContainer}>
                                <Text style={styles.errorText}>{I18n.t(GPS_ERROR[gps.error.code])}</Text>
                            </View>
                        )}
                    {showOnlyOneTrackLog
                        && (
                            <View style={styles.showOnlyOneContainer}>
                                <Text
                                    style={styles.showOnlyOneText}>{trackLogToShowOnly?.trackLogName ? `${I18n.t('showing_only_one_tracklog')}${trackLogToShowOnly?.trackLogName}` : I18n.t('current_tracklog')}</Text>
                                <TouchableIcon
                                    name="close"
                                    style={styles.roundedContainer}
                                    onPress={this.cancelShowOnlyOneTrackLog}
                                />
                            </View>
                        )}
                </View>

            </View>
        );
    }
}

ObservationMap.propTypes = {
    selectedMeasurements: PropTypes.arrayOf(PropTypes.object).isRequired,
    selectedDatabaseForMap: PropTypes.object,
    databaseList: PropTypes.arrayOf(PropTypes.object).isRequired,
    mapEngine: PropTypes.object.isRequired,
    selectDatabaseForMap: PropTypes.func.isRequired,
    filterData: PropTypes.bool.isRequired,
    startTime: PropTypes.oneOfType([
        PropTypes.instanceOf(Date),
        PropTypes.number,
        PropTypes.string,
    ]),
    endTime: PropTypes.oneOfType([
        PropTypes.instanceOf(Date),
        PropTypes.number,
        PropTypes.string,
    ]),
    setTime: PropTypes.oneOfType([
        PropTypes.instanceOf(Date),
        PropTypes.number,
        PropTypes.string,
    ]),
    changeSettingsData: PropTypes.func.isRequired,
    trackLogList: PropTypes.array.isRequired,
    trackLogToShowOnly: PropTypes.object,
    currentTrackLog: PropTypes.object,
    permanentSamplePlots: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number,
        name: PropTypes.string,
        geoJson: PropTypes.object,
        labelCoords: PropTypes.shape({
            latitude: PropTypes.number,
            longitude: PropTypes.number,
        }),
    })),
    gps: GpsPropTypes,
};

ObservationMap.defaultProps = {
    selectedDatabaseForMap: null,
    startTime: null,
    endTime: null,
    setTime: null,
    trackLogToShowOnly: null,
    currentTrackLog: null,
    permanentSamplePlots: [],
    gps: GpsDefaultPropTypes,
};

const mapStateToProps = ({
                             servers: {data: serverList},
                             settings: {mapEngine, dateFilterOnMapSettings: {filterData, startTime, endTime, setTime}},
                             mapSelection: {selectedDatabaseForMap},
                             trackLog: {trackLogList, currentTrackLog}
                         }, props) => {
    const selectedServer = getSelectedServer(serverList);
    const selectedDatabase = getSelectedDatabase(selectedServer);
    const selectedObservation = getSelectedObservation(selectedDatabase);
    const permanentSamplePlots = getPermanentSamplePlots(selectedDatabase, selectedObservation);

    return {
        selectedMeasurements: getMeasurementsForMap(
            serverList,
            selectedDatabaseForMap?.serverId,
            selectedDatabaseForMap?.key,
            null,
            filterData ? {startTime, endTime} : null,
        ),
        trackLogList: getFilteredTrackLogListBySelectedDatabase(
            trackLogList,
            selectedDatabaseForMap?.key,
            filterData ? {startTime, endTime} : null,
        ),
        databaseList: getNonEmptyDatabases(serverList),
        mapEngine,
        selectedDatabaseForMap,
        filterData,
        startTime,
        endTime,
        setTime,
        trackLogToShowOnly: getTrackLogById(trackLogList, props.navigation.state.params?.trackLogId),
        currentTrackLog: props.navigation.state.params?.showCurrentTrackLog ? currentTrackLog : null,
        permanentSamplePlots,
    };
};

const mapDispatchToProps = (dispatch) => {
    const {selectDatabaseForMap, changeSettingsData} = actions;

    return bindActionCreators(
        {
            selectDatabaseForMap,
            changeSettingsData,
        },
        dispatch,
    );
};

const getGpsSubscriptionOptions = () => ({observer: GPS_OBSERVER.MAP});

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(withGpsSubscription(ObservationMap, getGpsSubscriptionOptions));

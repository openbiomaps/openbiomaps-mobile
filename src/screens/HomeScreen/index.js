import React, { Component, Fragment } from 'react';
import { View, ScrollView, ImageBackground, Text } from 'react-native';

import PropTypes from 'prop-types';
import moment from 'moment';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as actions from '../../actions';
import I18n from '../../i18n';
import backgroundImage from '../../assets/login_background.jpg';
import { Colors } from '../../colors';
import NavigationHandler from '../../NavigationHandler';
import {
  getSelectedServer,
  getSelectedDatabase,
  getSelectedObservation,
  getSessions,
  getAllMeasurements,
  getPreviouslySelectedDatabases,
  getUserState,
} from '../../utils';
import client from '../../config/apiClient';
import {
  TouchableIcon,
  MenuItem,
  showAlertDialog,
  NotificationHandler,
  MessageView,
  GpsButton,
} from '../../components';
import { UNSYNCED_MEASUREMENTS_NUM_LIMIT, ERROR } from '../../Constants';
import {
  NotificationModel,
  NOTIFICATION_TYPES,
} from '../../models/NotificationModel';
import { store } from '../../config/setupStore';

import styles from './styles';

/* DYNAMIC_CODE_UPDATE >>>
 * imports_in_homeScreen */
/* <<< DYNAMIC_CODE_UPDATE */

class HomeScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      hasAuthError: false,
      sessions: [],
    };

    // References
    this.onNavigationFocus = null;
    this.notificationHandler = null;
  }

  componentDidMount = () => {
    const { getBackupList, navigation, loadObservations } = this.props;

    getBackupList();

    const {
      servers: { data: serverList },
    } = store.getState();
    const serverStore = getSelectedServer(serverList);
    const databaseStore = getSelectedDatabase(serverStore);

    loadObservations(serverStore, databaseStore);

    // Force update component when it is in focus to evoke render() for updateing changed props.
    // forceUpdate() calls render() by skipping shouldComponentUpdate()
    this.onNavigationFocus = navigation.addListener('willFocus', () => {
      this.loadMessages();
      this.forceUpdate();
    });
  };

  shouldComponentUpdate = (nextProps, nextState) => {
    const { hasAuthError, sessions } = this.state;
    const {
      trackLogRecordingInProgress,
      shortcutObservations,
      previouslySelectedDatabaseList,
      gpsError,
    } = this.props;

    if (gpsError !== nextProps.gpsError) {
      return true;
    }

    if (hasAuthError !== nextState.hasAuthError) {
      return true;
    }

    if (trackLogRecordingInProgress !== nextProps.trackLogRecordingInProgress) {
      return true;
    }

    // Array equality can be checked quicky by converting the arrays into JSON object.
    if (JSON.stringify(sessions) !== JSON.stringify(nextState.sessions)) {
      return true;
    }

    // Deleting a shortcut should rerender the screen in order to disappear
    if (
      JSON.stringify(shortcutObservations) !==
      JSON.stringify(nextProps.shortcutObservations)
    ) {
      return true;
    }

    if (
      JSON.stringify(previouslySelectedDatabaseList) !==
      JSON.stringify(nextProps.previouslySelectedDatabaseList)
    ) {
      return true;
    }

    // Do not render component when other props change to prevent rerender every time
    // when a deeply nested property changes in the hierarchy tree.
    return false;
  };

  componentDidUpdate = () => {
    this.handleAuthError();
    this.handleSessionNotification();
    this.handleMeasurementNumLimitNotification();
    this.handleOldMeasurementNotification();
  };

  componentWillUnmount = () => {
    this.onNavigationFocus.remove();
  };

  loadMessages = async () => {
    const { previouslySelectedDatabaseList, loadMessages } = this.props;

    if (previouslySelectedDatabaseList.length > 0)
      loadMessages(previouslySelectedDatabaseList);
  };

  handleAuthError = () => {
    const { serverList } = this.props;
    const { hasAuthError } = this.state;

    const notification = NotificationModel.build(NOTIFICATION_TYPES.AUTH_ERROR);

    this.setState(
      {
        hasAuthError: serverList.some(
          ({ user = {} }) => user.error === ERROR.INVALID_REFRESH_TOKEN,
        ),
      },
      () => {
        if (!this.notificationHandler) return;
        if (hasAuthError) {
          this.notificationHandler.showNotification(notification);
        } else {
          this.notificationHandler.hideNotification(notification);
        }
      },
    );
  };

  handleSessionNotification = () => {
    const { serverList } = this.props;
    const { sessions } = this.state;

    const notification = NotificationModel.build(NOTIFICATION_TYPES.SESSION);

    this.setState({ sessions: getSessions(serverList) }, () => {
      if (!this.notificationHandler) return;
      if (sessions.length) {
        this.notificationHandler.showNotification(notification);
      } else {
        this.notificationHandler.hideNotification(notification);
      }
    });
  };

  handleMeasurementNumLimitNotification = () => {
    const { unsynced } = this.props;
    const unsyncedMeasurementsNum = parseInt(unsynced, 10);
    const notification = NotificationModel.build(
      NOTIFICATION_TYPES.UNSYNCED_MEASUREMENTS,
    );

    if (!this.notificationHandler) return;
    if (unsyncedMeasurementsNum > UNSYNCED_MEASUREMENTS_NUM_LIMIT) {
      this.notificationHandler.showNotification(notification);
    } else {
      this.notificationHandler.hideNotification(notification);
    }
  };

  handleOldMeasurementNotification = () => {
    const { serverList } = this.props;

    const hasMeasurementOlderThanOneWeek = getAllMeasurements(
      serverList,
      false,
    ).some(
      ({ measurement }) =>
        !measurement.isSynced &&
        moment(moment()).diff(measurement.date, 'week') > 1,
    );
    const notification = NotificationModel.build(
      NOTIFICATION_TYPES.OLD_MEASUREMENTS,
    );

    if (!this.notificationHandler) return;
    if (hasMeasurementOlderThanOneWeek) {
      this.notificationHandler.showNotification(notification);
    } else {
      this.notificationHandler.hideNotification(notification);
    }
  };

  onCollectDataPress = () => {
    const { hasAuthError } = this.state;
    const {
      serverList,
      selectedServer: {
        name: serverName,
        url,
        user = {},
        databases: { data: databaseList = [] } = {},
      },
      selectedDatabase: {
        name: databaseName,
        game,
        observations: { data: observationList = [] } = {},
      },
    } = this.props;

    if (hasAuthError) {
      // Navigate to the LastServersScreen for reauthentication
      // if there is an auth error in any of the servers.
      NavigationHandler.navigate('LastServersScreen');
    } else if (user.accessToken) {
      // Set selected server url
      client.prefix = url;

      if (databaseName) {
        if (game === 'on') {
          NavigationHandler.navigate('ObservationGame', {
            title: databaseName,
          });
        } else if (
          observationList.some(
            (observation) =>
              observation.lastSelected && observation.form?.dataTypes?.length,
          )
        ) {
          NavigationHandler.navigate(
            'ObservationList',
            { title: databaseName },
            null,
            NavigationHandler.navigate('LastObservationsScreen'),
          );
        } else {
          NavigationHandler.navigate(
            'ObservationList',
            { title: databaseName },
            null,
            NavigationHandler.navigate('ObservationListScreen'),
          );
        }
      } else if (
        databaseList.some(
          (database) =>
            database.lastSelected && database.observations.data.length,
        )
      ) {
        NavigationHandler.navigate(
          'DatabaseList',
          { title: serverName },
          null,
          NavigationHandler.navigate('LastDatabasesScreen'),
        );
      } else {
        NavigationHandler.navigate(
          'DatabaseList',
          { title: serverName },
          null,
          NavigationHandler.navigate('DatabaseListScreen'),
        );
      }
    } else if (
      serverList.some((server) => server.user && server.user.requestTime)
    ) {
      NavigationHandler.navigate('LastServersScreen');
    } else {
      NavigationHandler.navigate('ServersScreen');
    }
  };

  getShortcutObservationSessionIcon = (shortcutObservation) => {
    const { sessions } = this.state;

    if (
      sessions.find(
        (item) => item.observationId === shortcutObservation.observation.id,
      )
    ) {
      return this.renderSessionIcon();
    }
    return null;
  };

  showDeleteDialog = (observation) => {
    const { removeShortcutObservation } = this.props;
    const {
      server: { id: serverId },
      database: { id: databaseId },
      observation: { id: observationId },
    } = observation;
    showAlertDialog({
      title: I18n.t('delete_shortcut_title'),
      message: I18n.t('delete_shortcut_message'),
      showCancelButton: true,
      positiveLabel: I18n.t('ok'),
      negativeLabel: I18n.t('cancel'),
      onPositiveBtnPress: () =>
        removeShortcutObservation(serverId, databaseId, observationId),
    });
  };

  getDeleteShortcutIcon = (observation) => (
    <TouchableIcon
      name="close"
      size={20}
      color="white"
      style={styles.deleteIconContainer}
      vectorIconStyle={{ margin: 3 }}
      onPress={() => {
        this.showDeleteDialog(observation);
      }}
    />
  );

  selectShortcutObservation = (shortcutObservation) => () => {
    const { server, database, observation } = shortcutObservation;
    const {
      selectServer,
      selectDatabase,
      selectObservation,
      resetServerSelection,
      resetDatabaseSelection,
      shortcutObservations,
    } = this.props;

    // Reset server and database selection before determining the new path
    // Avoid confusing navigation based on selected entries in the hierarchy tree.
    resetServerSelection();
    resetDatabaseSelection();

    // Determine path of the selected observation form in the hierarchy tree
    selectServer(server.id);
    selectDatabase(server.id, database.id, database.game);
    selectObservation(server.id, database.id, observation.id);
    // Set selected server url
    client.prefix = server.url;

    const shortcutObservationIds = [];
    const {
      servers: { data: serverList },
    } = store.getState();

    const serverStore = getSelectedServer(serverList);
    const databaseStore = getSelectedDatabase(serverStore);

    shortcutObservations.map((shortcut) =>
      shortcutObservationIds.push(shortcut['observation']['id']),
    );

    NavigationHandler.navigate('CreateMeasurement', {
      title: observation.name,
      description: database.name,
      measurementData: {
        server: serverStore,
        database: databaseStore,
        observation,
      },
      pinned: shortcutObservationIds.includes(observation.id),
    });
  };

  renderSessionIcon = () => (
    <TouchableIcon
      name="playlist-play"
      size={20}
      color={Colors.blue}
      style={styles.iconContainer}
      vectorIconStyle={{ margin: 3 }}
    />
  );

  renderErrorIcon = () => (
    <TouchableIcon
      name="error"
      size={30}
      color={Colors.red}
      style={styles.iconContainer}
      vectorIconStyle={{ margin: -3 }}
    />
  );

  getCustomTrackLogInfoColor = (inProgress, error) => {
    if (error) return { backgroundColor: Colors.brickRed };
    if (inProgress) return { backgroundColor: Colors.orange };
    return null;
  };

  render() {
    const { hasAuthError } = this.state;
    const {
      shortcutObservations,
      synced,
      unsynced,
      previouslySelectedDatabaseList,
      trackLogRecordingInProgress,
      gpsError,
    } = this.props;
    /* DYNAMIC_CODE_UPDATE >>>
     * consts_in_HomeScreen_render */
    /* <<< DYNAMIC_CODE_UPDATE */
    // Home menu parameters
    const displayMenu = {
      menu_shortcut: true,
      menu_collectData: true,
      menu_collectedData: true,
      menu_map: true,
      menu_settings: true,
      menu_export: true,
      menu_trackLog: true,
      menu_type1_style: { backgroundColor: Colors.asparagusGreen },
      menu_type2_style: {},
      header: {
        display: false,
      },
      footer: {
        display: false,
      },
    };

    /* DYNAMIC_CODE_UPDATE >>>
     * menu_config */
    /* <<< DYNAMIC_CODE_UPDATE */
    // Home menu parameters

    return (
      <ImageBackground
        source={backgroundImage}
        style={styles.background}
        resizeMode="cover"
      >
        <GpsButton />
        <NotificationHandler
          ref={(ref) => {
            this.notificationHandler = ref;
          }}
        />

        <ScrollView
          style={styles.baseContainer}
          contentContainerStyle={styles.contentContainer}
        >
          {previouslySelectedDatabaseList.length > 0 && (
            <MessageView
              previouslySelectedDatabaseList={previouslySelectedDatabaseList}
            />
          )}
          {displayMenu.header.display && (
            <MenuItem
              title={displayMenu.header.title}
              description={displayMenu.header.description}
              customContainerStyle={displayMenu.header.style}
              onPress={displayMenu.header.action}
              numberOfLines={null}
              icon={() => displayMenu.header.icon}
            />
          )}
          {/* Display the shortcutted forms */}
          {displayMenu.menu_shortcut &&
            (shortcutObservations.length > 0 ? (
              shortcutObservations.map((shortcut) => (
                <MenuItem
                  key={shortcut.observation.id}
                  title={shortcut.observation.name}
                  description={shortcut.database.name}
                  customContainerStyle={displayMenu.menu_type1_style}
                  onPress={this.selectShortcutObservation(shortcut)}
                  numberOfLines={null}
                  icon={() => this.getShortcutObservationSessionIcon(shortcut)}
                  deleteIcon={() => this.getDeleteShortcutIcon(shortcut)}
                />
              ))
            ) : (
              <MenuItem
                title={I18n.t('no_shortcut_observations')}
                description={I18n.t('create_shortcut_suggestion')}
                customContainerStyle={displayMenu.menu_type1_style}
                disabled
                numberOfLines={null}
                onPress={() => {}}
              />
            ))}

          {/* Other menu items */}
          {displayMenu.menu_collectData && (
            <MenuItem
              title={I18n.t('collect_data')}
              customContainerStyle={displayMenu.menu_type2_style}
              onPress={this.onCollectDataPress}
              icon={() => hasAuthError && this.renderErrorIcon()}
            />
          )}
          {displayMenu.menu_collectedData && (
            <MenuItem
              title={I18n.t('collected_data')}
              customContainerStyle={displayMenu.menu_type2_style}
              labelIconName="folder-open-o"
              onPress={() => NavigationHandler.navigate('ObservationCards')}
              infoComponent={() => (
                <>
                  <TouchableIcon
                    name="cloud-off"
                    size={16}
                    color="white"
                    label={unsynced > 0 ? unsynced.toString() : '0'} // Letagadjuk a listás metarekordokat
                    labelStyle={styles.infoText}
                    disabled
                  />
                  <TouchableIcon
                    iconType="MaterialCommunityIcon"
                    name="cloud-upload-outline"
                    size={16}
                    color="white"
                    label={synced > 0 ? synced.toString() : '0'} // Letagadjuk a listás metarekordokat
                    labelStyle={styles.infoText}
                    disabled
                  />
                </>
              )}
            />
          )}
          <View style={styles.bottomContainer}>
            {displayMenu.menu_map && (
              <MenuItem
                title={I18n.t('map')}
                customContainerStyle={displayMenu.menu_type2_style}
                onPress={() => NavigationHandler.navigate('ObservationMap')}
              />
            )}
            {displayMenu.menu_trackLog && (
              <MenuItem
                title={I18n.t('tracklog')}
                customContainerStyle={displayMenu.menu_type2_style}
                onPress={() => NavigationHandler.navigate('MainTrackLog')}
                infoComponent={() => (
                  <>
                    <Text style={styles.centeredInfoText} numberOfLines={1}>
                      {trackLogRecordingInProgress
                        ? I18n.t('tracklog_recording_in_progress')
                        : I18n.t('tracklog_not_recording')}
                    </Text>
                  </>
                )}
                customInfoContainerStyle={this.getCustomTrackLogInfoColor(
                  trackLogRecordingInProgress,
                  gpsError,
                )}
              />
            )}
          </View>
          <View style={styles.bottomContainer}>
            {displayMenu.menu_settings && (
              <MenuItem
                title={I18n.t('settings')}
                customContainerStyle={displayMenu.menu_type2_style}
                onPress={() => NavigationHandler.navigate('Settings')}
              />
            )}
            {displayMenu.menu_export && (
              <MenuItem
                title={I18n.t('export')}
                customContainerStyle={displayMenu.menu_type2_style}
                onPress={() => NavigationHandler.navigate('Export')}
              />
            )}
          </View>
          {displayMenu.footer.display && (
            <MenuItem
              title={displayMenu.footer.title}
              description={displayMenu.footer.description}
              customContainerStyle={displayMenu.footer.style}
              onPress={displayMenu.footer.action}
              numberOfLines={null}
              icon={() => displayMenu.footer.icon}
            />
          )}
        </ScrollView>
      </ImageBackground>
    );
  }
}

HomeScreen.propTypes = {
  shortcutObservations: PropTypes.array.isRequired,
  selectServer: PropTypes.func.isRequired,
  selectDatabase: PropTypes.func.isRequired,
  selectObservation: PropTypes.func.isRequired,
  navigation: PropTypes.object.isRequired,
  getBackupList: PropTypes.func.isRequired,
  selectedDatabase: PropTypes.object.isRequired,
  selectedServer: PropTypes.object.isRequired,
  serverList: PropTypes.array.isRequired,
  resetServerSelection: PropTypes.func.isRequired,
  resetDatabaseSelection: PropTypes.func.isRequired,
  synced: PropTypes.number.isRequired,
  unsynced: PropTypes.number.isRequired,
  removeShortcutObservation: PropTypes.func.isRequired,
  previouslySelectedDatabaseList: PropTypes.array.isRequired,
  loadMessages: PropTypes.func.isRequired,
  trackLogRecordingInProgress: PropTypes.bool.isRequired,
  gpsError: PropTypes.shape({
    code: PropTypes.number.isRequired,
    message: PropTypes.string.isRequired,
  }),
};

HomeScreen.defaultProps = {
  gpsError: null,
};

const mapStateToProps = ({
  servers: { data: serverList },
  shortcutObservations,
  measurementsMeta,
  trackLog,
  gps,
}) => {
  const selectedServer = getSelectedServer(serverList);
  const selectedDatabase = getSelectedDatabase(selectedServer);
  const selectedObservation = getSelectedObservation(selectedDatabase);
  const { synced, unsynced } = measurementsMeta;
  const previouslySelectedDatabaseList =
    getPreviouslySelectedDatabases(serverList);
  const { recordingInProgress: trackLogRecordingInProgress } = trackLog;
  const { error: gpsError } = gps;
  return {
    serverList,
    shortcutObservations,
    selectedServer,
    selectedDatabase,
    selectedObservation,
    synced,
    unsynced,
    previouslySelectedDatabaseList,
    trackLogRecordingInProgress,
    gpsError: Object.values(gpsError).every((item) => item) ? gpsError : null,
  };
};

const mapDispatchToProps = (dispatch) => {
  const {
    /* DYNAMIC_CODE_UPDATE >>>
     * consts_mapDispatchToProps */
    /* <<< DYNAMIC_CODE_UPDATE */
    selectServer,
    selectDatabase,
    selectObservation,
    getBackupList,
    resetServerSelection,
    resetDatabaseSelection,
    removeShortcutObservation,
    loadMessages,
    loadObservations,
  } = actions;

  return bindActionCreators(
    {
      /* DYNAMIC_CODE_UPDATE >>>
       * bind_actions */
      /* <<< DYNAMIC_CODE_UPDATE */
      selectServer,
      selectDatabase,
      selectObservation,
      getBackupList,
      resetServerSelection,
      resetDatabaseSelection,
      removeShortcutObservation,
      loadMessages,
      loadObservations,
    },
    dispatch,
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);

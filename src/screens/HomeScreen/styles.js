import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  baseContainer: {
    flex: 1,
  },
  contentContainer: {
    flexGrow: 1,
    paddingBottom: 16,
    paddingHorizontal: 8,
  },
  bottomContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  background: {
    width: '100%',
    height: '100%',
  },
  infoText: {
    fontSize: 13,
    color: 'white',
    marginHorizontal: 4,
  },
  centeredInfoText: {
    flex: 1,
    fontSize: 13,
    color: 'white',
    textAlign: 'center',
    marginVertical: 1,
  },
  iconContainer: {
    position: 'absolute',
    right: 20,
    backgroundColor: 'white',
    borderRadius: 30,
  },
  deleteIconContainer: {
    position: 'absolute',
    left: 10,
    top: 10,
    backgroundColor: 'transparent',
    borderRadius: 10,
    borderColor: 'white',
    borderWidth: 2,
  },
});

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from '../../actions';
import ObservationList from '../../components/ObservationList';
import { getObservationState, getSelectedServer, getSelectedDatabase } from '../../utils';

const mapStateToProps = ({ servers: { data: serverList } }) => {
  const selectedServer = getSelectedServer(serverList);
  const selectedDatabase = getSelectedDatabase(selectedServer);
  const { observationList, fetching, error } = getObservationState(selectedDatabase);

  return {
    selectedServer,
    selectedDatabase,
    observationList: observationList
      .filter(observation => observation.lastSelected && observation.form?.dataTypes?.length)
      .sort((a, b) => new Date(b.lastSelected) - new Date(a.lastSelected)),
    optionNeeded: true,
    fetching,
    error,
  };
};

const mapDispatchToProps = (dispatch) => {
  const { loadObservations, selectObservation, createShortcut } = actions;

  return bindActionCreators(
    {
      loadObservations,
      selectObservation,
      createShortcut,
    },
    dispatch,
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ObservationList);

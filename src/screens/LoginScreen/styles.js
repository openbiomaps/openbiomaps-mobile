import { StyleSheet } from 'react-native';
import { Colors } from '../../colors';

export default StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingHorizontal: 30,
  },
  keyboardAvoidingContainer: {
    flex: 1,
    width: '100%',
    marginBottom: 30,
  },
  inputFieldContainer: {
    marginTop: 40,
    width: '100%'
  },
  passwordButtonIcon: {
    position: 'absolute',
    right: 0,
    bottom: 15,
    justifyContent: 'center',
    alignItems: 'flex-end',
    paddingRight: 15,
  },
  loginButton: {
    backgroundColor: Colors.mantisGreen,
    padding: 10,
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  loginButtonContainer: {
    marginTop: 30
  },
  buttonText: {
    fontWeight: 'bold',
    color: Colors.white,
    fontSize: 20
  },
  errorContainer: {
    marginTop: 30
  },
  errorText: {
    fontSize: 16,
    color: Colors.red,
    textAlign: 'center',
    fontWeight: 'bold'
  },
});

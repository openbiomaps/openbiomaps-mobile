import React, { Component } from 'react';
import { View, InteractionManager, ActivityIndicator, Text } from 'react-native';

import { activateKeepAwake, deactivateKeepAwake } from '@sayem314/react-native-keep-awake';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { throttle, isEmpty } from 'lodash';

import * as actions from '../../actions';
import NavigationHandler from '../../NavigationHandler';
import I18n from '../../i18n';
import { NotificationModel, NOTIFICATION_TYPES } from '../../models/NotificationModel';
import { ERROR } from '../../Constants';
import { 
  footerStyle, 
  TouchableIcon, 
  Spinner, 
  NotificationHandler,
  showAlertDialog,
  TrackLogList,
} from '../../components';

class UnsyncedTrackLogsScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      didFinishInitialAnimation: false,
    };
    this.notificationHandler = null;
    this.throttledToggleSync = throttle(this.toggleTrackLogSync, 2000);
  }

  componentDidMount = () => {
    activateKeepAwake();
    const { navigation } = this.props;
    navigation.dangerouslyGetParent()?.setParams({
      toggleTrackLogSync: this.throttledToggleSync,
    });

    // Delay initial fetching/rendering in order to improve navigaiton performance.
    InteractionManager.runAfterInteractions(() => {
      setTimeout(() => {
        this.setState({ didFinishInitialAnimation: true });
      }, 500);
    });
  }

  componentDidUpdate = () => {
    if (this.notificationHandler) {
      this.handleAuthError();
      this.handleSyncProgressNotification();
      this.handleNetWorkError();
      this.handleUserFetchingNotification();
    }
  };

  componentWillUnmount = () => {
    deactivateKeepAwake();
  }

  handleAuthError = () => {
    const { showAuthError } = this.props;

    if (showAuthError) {
      const notification = NotificationModel.build(NOTIFICATION_TYPES.AUTH_ERROR, this.renderLoginNavButton());
      this.notificationHandler.showNotification(notification); 
    } else {
      this.notificationHandler.hideNotification(NotificationModel.build(NOTIFICATION_TYPES.AUTH_ERROR));
    }
  }

  handleUserFetchingNotification = () => {
    const { fetchingUsers } = this.props;

    if (fetchingUsers.length) {
      const notification = NotificationModel.build(NOTIFICATION_TYPES.COMMUNICATING_WITH_THE_SERVER, this.renderSpinner());
      this.notificationHandler.showNotification(notification);
    } else {
      this.notificationHandler.hideNotification(NotificationModel.build(NOTIFICATION_TYPES.COMMUNICATING_WITH_THE_SERVER));
    }
  }

  handleNetWorkError = () => {
    const { networkError, serverError } = this.props;
    if (networkError || serverError) {
      const unknownErrors = {};
      let networkNotification = null;
      switch (networkError) {
        case ERROR.LATE_RESPONSE_TRIAL_CALL:
          networkNotification = NotificationModel.build(NOTIFICATION_TYPES.LATE_RESPONSE_TRIAL_CALL);
          break;
        case ERROR.NETWORK_ERROR:
          networkNotification = NotificationModel.build(NOTIFICATION_TYPES.NETWORK_ERROR);
          break;
        default: { 
          if (networkError) unknownErrors[networkError] = true;
        }
      }

      let serverNotification = null;
      switch (serverError) {
        case ERROR.LATE_RESPONSE_TRIAL_CALL:
          serverNotification = NotificationModel.build(NOTIFICATION_TYPES.LATE_RESPONSE_TRIAL_CALL);
          break;
        case ERROR.NETWORK_ERROR:
          serverNotification = NotificationModel.build(NOTIFICATION_TYPES.NETWORK_ERROR);
          break;
        default:
          if (serverError) unknownErrors[serverError] = true;
      }
      
      if (networkNotification) this.notificationHandler.showNotification(networkNotification);
      if (serverNotification && serverNotification.id !== networkNotification?.id) {
        this.notificationHandler.showNotification(serverNotification);
      }
      
      if (!isEmpty(unknownErrors)) {
        this.notificationHandler.showNotification(NotificationModel.build(
          NOTIFICATION_TYPES.UNKNOWN_ERROR,
          (
            <View>
              {Object.keys(unknownErrors).map((message) => (<Text key={message} style={{ textAlign: 'center' }}>{message}</Text>))}
            </View>),
        ));
      }
    } else {
      this.notificationHandler.hideNotification(NotificationModel.build(NOTIFICATION_TYPES.NETWORK_ERROR));
      this.notificationHandler.hideNotification(NotificationModel.build(NOTIFICATION_TYPES.LATE_RESPONSE_TRIAL_CALL));
      this.notificationHandler.hideNotification(NotificationModel.build(NOTIFICATION_TYPES.UNKNOWN_ERROR));
    }
  }

  handleSyncProgressNotification = () => {
    const { syncing } = this.props;

    if (syncing) {
      const notification = NotificationModel.build(NOTIFICATION_TYPES.SYNCING_IN_PROGRESS, this.renderSpinner(syncing));
      this.notificationHandler.showNotification(notification);
    } else {
      this.notificationHandler.hideNotification(NotificationModel.build(NOTIFICATION_TYPES.SYNCING_IN_PROGRESS));
    }
  }

  onDeletePress = (trackLogId, sessionInfo) => {
    const { deleteUnsyncedTrackLog } = this.props;

    showAlertDialog({
      title: I18n.t('delete_tracklog_dialog_title'), 
      message: I18n.t('delete_dialog_message'),
      showCancelButton: true, 
      positiveLabel: I18n.t('bool_true'),
      negativeLabel: I18n.t('bool_false'),
      onPositiveBtnPress: () => deleteUnsyncedTrackLog(trackLogId, sessionInfo),
    });
  }

  onViewMapPress = (trackLogId) => {
    NavigationHandler.navigate('ObservationMap', {
      trackLogId,
    });
  }

  onDownloadPress = (trackLogId) => {
    const { exportTrackLog } = this.props;
    exportTrackLog(trackLogId);
  }

  toggleTrackLogSync = () => {
    const { serverList, tokenRefresh, status, trackLogList } = this.props;
    if (['none', 'unknown'].includes(status)) {
      showAlertDialog({
        title: I18n.t('no_connection'), 
        message: I18n.t('sync_requires_net'),
        positiveLabel: I18n.t('ok'),
      });
    } else {
      if (!trackLogList.length) {
        showAlertDialog({ message: I18n.t('no_data_to_sync') });
        return;
      }
      let syncStarted = false; 
      serverList.forEach((server) => {
        if (trackLogList.some(({ selectedDatabases }) => selectedDatabases.some(({ serverId }) => serverId === server.id))) {
          syncStarted = true;
          tokenRefresh(server.id, { syncTrackLogsAfter: true });
        }
      });
      if (!syncStarted) {
        this.notificationHandler.showNotification(NotificationModel.build(
          NOTIFICATION_TYPES.NO_SERVER_ASSIGNED_TO_TRACKLOG_ERROR,
          null,
          3000,
        ));
      }
    }
  };

  navigateToServerScreen = () => {
    NavigationHandler.replace('ServerList');
  }

  renderLoginNavButton = () => (
    <TouchableIcon 
      name="account-circle"
      label={I18n.t('login')}
      onPress={this.navigateToServerScreen}
      labelStyle={{ color: 'white' }}
      color="white"
      style={[footerStyle.singleButton, footerStyle.roundedShape, { padding: 0 }]}
    />
  );

  renderSpinner = (num = 0) => (
    <View style={{ flexDirection: 'row' }}>
      {num !== 0 && <Text style={{ color: 'white' }}>{I18n.t('syncing_with_num_of_left', { num })}</Text>}
      <ActivityIndicator size="small" color="white" style={{ marginStart: 3 }} />
    </View>
  );  

  render() {
    const { didFinishInitialAnimation } = this.state;
    const { trackLogList } = this.props;

    if (!didFinishInitialAnimation) return <Spinner />;

    return (
      <View style={{ flex: 1 }}>
        <NotificationHandler ref={(ref) => { this.notificationHandler = ref; }} />
        <View style={{ flex: 1 }}>
          <TrackLogList 
            trackLogList={trackLogList} 
            onDeletePress={this.onDeletePress}
            onViewMapPress={this.onViewMapPress}
            onDownloadPress={this.onDownloadPress}
          />
        </View>
      </View>
    );
  }
}

UnsyncedTrackLogsScreen.propTypes = {
  trackLogList: PropTypes.arrayOf(PropTypes.object).isRequired,
  navigation: PropTypes.object.isRequired,
  deleteUnsyncedTrackLog: PropTypes.func.isRequired,
  tokenRefresh: PropTypes.func.isRequired,
  status: PropTypes.string.isRequired,
  showAuthError: PropTypes.bool,
  networkError: PropTypes.string,
  serverError: PropTypes.string,
  syncing: PropTypes.number,
  serverList: PropTypes.array.isRequired,
  exportTrackLog: PropTypes.func.isRequired,
  fetchingUsers: PropTypes.array.isRequired,
};

UnsyncedTrackLogsScreen.defaultProps = {
  showAuthError: false,
  networkError: null,
  serverError: null,
  syncing: 0,
};

const mapStateToProps = ({ 
  servers: { data: serverList, error: serverError },
  network: { status, error: networkError },
  trackLog: { trackLogList } }) => {
  const syncing = trackLogList.filter(({ syncing }) => syncing).length;
  const unsyncedTrackLogList = trackLogList
    .filter(({ selectedDatabases }) => selectedDatabases.length === 0 || selectedDatabases.some(({ isSynced }) => !isSynced));
  // Show auth error only when there are unsynced measurements
  // belonging to a server with auth error.
  let showAuthError = false;
  const fetchingUsers = [];
  serverList.forEach((serverItem) => {
    const { user = {}, serverUrl } = serverItem;
    if (
      !showAuthError
      && user.error === ERROR.INVALID_REFRESH_TOKEN && unsyncedTrackLogList.length
    ) {
      showAuthError = true;
    }
    if (user.fetching) fetchingUsers.push({ serverUrl, userName: user.userName });
  });

  return {
    trackLogList: unsyncedTrackLogList,
    serverList,
    status,
    showAuthError,
    networkError,
    serverError,
    syncing,
    fetchingUsers,
  };
};

const mapDispatchToProps = (dispatch) => {
  const { tokenRefresh, deleteUnsyncedTrackLog, exportTrackLog } = actions;

  return bindActionCreators(
    {
      tokenRefresh,      
      deleteUnsyncedTrackLog,
      exportTrackLog,
    },
    dispatch,
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(UnsyncedTrackLogsScreen);

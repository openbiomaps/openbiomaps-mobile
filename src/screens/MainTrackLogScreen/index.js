import React, { Component } from 'react';
import { View, Text, ScrollView } from 'react-native';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';

import * as actions from '../../actions';
import I18n from '../../i18n';
import { MenuItem, TouchableIcon } from '../../components';
import { Colors } from '../../colors';
import NavigationHandler from '../../NavigationHandler';
import { GPS_ERROR, GPS_OBSERVER } from '../../Constants';
import withGpsSubscription, { GpsDefaultPropTypes, GpsPropTypes } from '../../hocs/withGpsSubscription';

import styles from './styles';

class MainTrackLogScreen extends Component {
  switchTrackLog = () => {
    const {
      trackLogRecordingInProgress,
      startTrackLogRecording,
      stopTrackLogRecording,
      gps,
    } = this.props;

    if (trackLogRecordingInProgress) { 
      stopTrackLogRecording();
      gps.unsubscribe();
    } else {
      gps.subscribe();
      startTrackLogRecording();
    }
  };

  navigateToTrackLogListScreen = () => {
    NavigationHandler.navigate('TrackLogLists');
  }

  viewCurrentTrackLogOnMap = () => {
    NavigationHandler.navigate('ObservationMap', {
      showCurrentTrackLog: true,
    });
  }

  renderProgressIcon = () => {
    const { trackLogRecordingInProgress } = this.props;

    if (!trackLogRecordingInProgress) return null;

    return (
      <TouchableIcon
        name="explore" 
        size={30} 
        color={Colors.blue} 
        style={styles.iconContainer}
        vectorIconStyle={{ margin: -3 }}
      />
    );
  }

  render() {
    const {
      trackLogRecordingInProgress,
      currentTrackLog,
      gps,
    } = this.props;

    const databaseName = `${currentTrackLog?.selectedDatabases?.reduce((acc, database) => (`${acc}\t-${database.serverName} (${database.databaseName})\n`), '')}`;
    
    return (
      <ScrollView 
        style={styles.baseContainer}
        contentConatinerStyle={styles.contentContainer}
      >
        <MenuItem
          title={trackLogRecordingInProgress ? I18n.t('tracklog_switched_on') : I18n.t('tracklog_switched_off')}
          description={trackLogRecordingInProgress ? I18n.t('push_to_turn_off') : I18n.t('push_to_turn_on')}
          onPress={this.switchTrackLog}
          icon={this.renderProgressIcon}
          customContainerStyle={styles.trackLogSwitchButton}
          numberOfLines={3}
        />
        <MenuItem 
          title={I18n.t('recorded_tracklogs')}
          onPress={this.navigateToTrackLogListScreen}
          customContainerStyle={styles.trackLogSwitchButton}
          numberOfLines={3}
        />
        { gps.error
          && (
          <View style={styles.trackLogInfoContainer}>
            <Text style={styles.trackLogErrorText}>{I18n.t(GPS_ERROR[gps.error.code])}</Text>
          </View>
          )}
        { currentTrackLog
          && (
          <View style={styles.trackLogInfoContainer}>
            <Text style={styles.trackLogInfoText}>{I18n.t('current_tracklog_length', { length: currentTrackLog?.trackLogArray?.length })}</Text>
            
            { currentTrackLog.selectedDatabases?.length !== 0
              ? (
                <View>
                  <Text style={styles.trackLogInfoDatabaseText}>{I18n.t('current_tracklog_servers')}</Text>
                  <Text style={styles.trackLogInfoDatabaseText}>
                    {databaseName}
                  </Text>
                </View>
                )
              : <Text style={styles.trackLogWarningText}>{I18n.t('current_tracklog_no_server')}</Text>}
            { currentTrackLog?.trackLogArray.length !== 0 && (
            <TouchableIcon
              style={styles.roundedIcon}
              name="map"
              size={24}
              width={30}
              onPress={this.viewCurrentTrackLogOnMap}
              label={I18n.t('view_on_map')}
            />
            )}
          </View>
          )}
      </ScrollView>
    );
  }
}
  
MainTrackLogScreen.propTypes = {
  trackLogRecordingInProgress: PropTypes.bool.isRequired,
  currentTrackLog: PropTypes.object,
  startTrackLogRecording: PropTypes.func.isRequired,
  stopTrackLogRecording: PropTypes.func.isRequired,
  gps: GpsPropTypes,
};

MainTrackLogScreen.defaultProps = {
  currentTrackLog: null,
  gps: GpsDefaultPropTypes,
};  
  
const mapStateToProps = () => ({ 
  trackLog: { recordingInProgress, currentTrackLog }, 
}) => ({
  trackLogRecordingInProgress: recordingInProgress,
  currentTrackLog,
});
  
const mapDispatchToProps = (dispatch) => {
  const {
    startTrackLogRecording,
    stopTrackLogRecording,
  } = actions;

  return bindActionCreators(
    {
      startTrackLogRecording,
      stopTrackLogRecording,
    },
    dispatch,
  );
};

const getGpsSubscriptionOptions = () => ({ observer: GPS_OBSERVER.TRACKLOG, shouldReceivePositionUpdates: false });
  
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withGpsSubscription(MainTrackLogScreen, getGpsSubscriptionOptions));

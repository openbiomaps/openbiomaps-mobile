
 
import { StyleSheet } from 'react-native';
import { Colors } from '../../colors';

export default StyleSheet.create({
  baseContainer: {
    flex: 1,
  },
  contentContainer: {
    flexGrow: 1,
    justifyContent: 'flex-start',
    flexDirection: 'column',
    paddingHorizontal: 8,
  },
  trackLogSwitchButton: {
    flex: 0,
  },
  trackLogInfoContainer: {
    borderColor: Colors.mantisGreen,
    borderWidth: 5,
    borderRadius: 15,
    margin: 8,
    padding: 15,
  },
  trackLogInfoText: {
    fontSize: 18,
    textAlign: 'center',
    margin: 3,
  },
  trackLogInfoDatabaseText: {
    fontSize: 18,
    textAlign: 'left',
    marginTop: 5,
  },
  trackLogWarningText: {
    fontSize: 18,
    color: Colors.brickRed,
    textAlign: 'center',
  },
  trackLogErrorText: {
    fontSize: 18,
    color: Colors.brickRed,
    textAlign: 'center',
  },
  iconContainer: {
    position: 'absolute',
    right: 20,
    backgroundColor: 'white',
    borderRadius: 30,
  },
  roundedIcon: {
    borderWidth: 1,
    borderColor: 'black',
    borderRadius: 3,
    paddingHorizontal: 20,
    paddingVertical: 7,
    marginStart: 7,
    marginTop: 5,
    justifyContent: 'center',
  },
});

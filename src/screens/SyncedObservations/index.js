import React, { Component } from 'react';
import { View, InteractionManager } from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from '../../actions';
import { getFilteredMeasurements } from '../../utils';
import I18n from '../../i18n';
import NavigationHandler from '../../NavigationHandler';
import { 
  footerStyle,
  TouchableIcon,
  showAlertDialog,
  Spinner,
  ObservationOneLineList,
} from '../../components';
import { Colors } from '../../colors';

class SyncedObservationsScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      didFinishInitialAnimation: false,
    };
  }

  componentDidMount = () => {
    // Delay initial fetching/rendering in order to improve navigaiton performance.
    InteractionManager.runAfterInteractions(() => {
      setTimeout(() => {
        this.setState({ didFinishInitialAnimation: true });
      }, 500);
    });
  }

  onDeletePress = (measurementData) => {
    showAlertDialog({
      title: I18n.t('delete_dialog_title'), 
      message: I18n.t('delete_dialog_message'),
      showCancelButton: true, 
      positiveLabel: I18n.t('bool_true'),
      negativeLabel: I18n.t('bool_false'),
      onPositiveBtnPress: () => this.props.removeFromMeasurementList(measurementData),
    });
  }

  onDeleteAllPress = (/* measurementData */) => {
    showAlertDialog({
      title: I18n.t('delete_all_synced_dialog_title'), 
      message: I18n.t('delete_dialog_message'),
      showCancelButton: true, 
      positiveLabel: I18n.t('bool_true'),
      negativeLabel: I18n.t('bool_false'),
      onPositiveBtnPress: () => this.props.removeAllFromMeasurementList({ server: '*', database: '*' }),
    });
  }

  onSchemaPress = (measurementData) => {
    NavigationHandler.replace('CreateMeasurement', {
      title: measurementData.observation.name,
      description: measurementData.database.name,
      measurementData,
    });
  }

  render() {
    const { didFinishInitialAnimation } = this.state;
    const { allMeasurements } = this.props;

    if (!didFinishInitialAnimation) return <Spinner />;

    return (
      <View style={{ flex: 1 }}>
        <View style={{ flex: 1 }}>
          <ObservationOneLineList 
            measurements={allMeasurements} 
            type="synced"
            onDeletePress={this.onDeletePress}
            onSchemaPress={this.onSchemaPress}
          />
        </View>
        <View style={footerStyle.narrowContainer}>
          <TouchableIcon
            name="delete"
            label={I18n.t('delete_all')}
            labelStyle={footerStyle.deleteButtonLabel}
            onPress={this.onDeleteAllPress}
            size={22}
            color={Colors.darkRed}
            style={[footerStyle.singleButton, footerStyle.roundedShape]}
          />
        </View>
      </View>
    );
  }
}

SyncedObservationsScreen.propTypes = {
  allMeasurements: PropTypes.arrayOf(PropTypes.object).isRequired,
  removeFromMeasurementList: PropTypes.func.isRequired,
  removeAllFromMeasurementList: PropTypes.func.isRequired,
};

const mapStateToProps = ({ servers: { data: serverList } }) => {
  // Filter out null records that contain only session related info.
  const allMeasurements = getFilteredMeasurements(serverList, false, measurement => measurement?.isSynced);

  return {
    allMeasurements,
  };
};

const mapDispatchToProps = (dispatch) => {
  const { removeFromMeasurementList, removeAllFromMeasurementList } = actions;

  return bindActionCreators(
    {
      removeFromMeasurementList,
      removeAllFromMeasurementList,
    },
    dispatch,
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SyncedObservationsScreen);

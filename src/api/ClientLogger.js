import { FileLogger } from 'react-native-file-logger';
import { FileSystem } from 'react-native-unimodules';
import { decodeQueryString } from '../utils';

const { StorageAccessFramework } = FileSystem;

export const ClientLoggerUseCases = {
  ACCESS_EXPIRED_CALL_REFRESH_LOGIC: 'Access token has expired. Calling refreshLogic...', // authobject
  REFRESHING_IN_REFRESHLOGIC: 'Refreshing tokens with refreshtoken ', // authobject
  DISPATCH_NEW_AUTHOBJECT: 'Dispatching the new tokens', // authObject
  FORCE_REFRESH: ' A force refresh is being called in user actions', // refreshToken, serverUrl
  NETWORK_HANDLER_REFRESH: 'The network handler detected that the access token has expired, force refreshing...', // default
  INVALID_ACCESS_TOKEN: 'Access token is invalid, retrying with refresh', // default
  UNEXPECTED_OAUTH_ERROR: 'An unexpected oauth error happened. message: ', // default
  OTHER_ERROR_IN_MAIN_INTERCEPTOR: 'Other error happened in the main interceptor: ', // default
  USER_LOGGED_IN: 'A user logged in: ', // authObject, userName
  USER_LOGGED_OUT: 'A user logged out:', // default
  REFRESH_TOKEN_ERROR: 'The refresh call failed. ', // errorMessage: e, refreshtoken, serverUrl 
  NEW_REQUEST: 'a new request is in progress: ', // config object
};

export const SelectableClientLoggers = {
  CONSOLE: 'CONSOLE',
  FILE: 'FILE', // TODO file logger currently not available
  NONE: 'NONE',
};

class ClientLogger {
  constructor() {
    this._selectedLogger = SelectableClientLoggers.NONE;
  }

  _formatDate = (time) => {
    const t = time ? new Date(time) : new Date();
    return `${t.getMonth() + 1}. ${t.getDate()}. ${t.getHours()}:${t.getMinutes()}:${t.getSeconds()}`;
  };

  _authObjectToString = ({ accessToken, refreshToken, expiresIn, requestTime }) => `accessToken: ${accessToken} expires at: ${this._formatDate(requestTime + expiresIn)} refreshToken: ${refreshToken}`;

  _getCallFromConfig = (configData) => configData
    && `scope: ${configData.scope} ${configData.access_token
      ? `accesstoken: ${configData.access_token}`
      : `userName: ${configData.username}`}`;

  _logWithSelectedLogger = async (string) => {
    switch (this._selectedLogger) {
      case SelectableClientLoggers.CONSOLE:
        console.log(`${this._formatDate()}\t${string}`);
        return;
      case SelectableClientLoggers.FILE:
        FileLogger.info(string);
        return;
      case SelectableClientLoggers.NONE:
        return;
      default:
        console.log(string);
    }
  };

  _initFileLogger = () => {
    FileLogger.configure({
      captureConsole: false,
      // logsDirectory: StorageAccessFramework.getUriForDirectoryInRoot('obm'),
      logsDirectory: FileSystem.getContentUriAsync('obm')
    });
  };

  setLogMode = (selectableLogger) => {
    // eslint-disable-next-line no-undef
    if (!__DEV__) return;
    switch (selectableLogger) {
      case SelectableClientLoggers.FILE:
        this._selectedLogger = selectableLogger;
        this._initFileLogger();
        return;
      case SelectableClientLoggers.CONSOLE:
        this._selectedLogger = selectableLogger;
        return;
      default:
        this._selectedLogger = SelectableClientLoggers.NONE;
    }
  }

  log = (useCase, payload = null) => {
    if (this._selectedLogger === SelectableClientLoggers.NONE) return;

    switch (useCase) {
      case ClientLoggerUseCases.REFRESHING_IN_REFRESHLOGIC:
      case ClientLoggerUseCases.ACCESS_EXPIRED_CALL_REFRESH_LOGIC:
      case ClientLoggerUseCases.DISPATCH_NEW_AUTHOBJECT:
        this._logWithSelectedLogger(`${useCase}\t ${this._authObjectToString(payload)}\n`);
        return;
      case ClientLoggerUseCases.FORCE_REFRESH:
        this._logWithSelectedLogger(`${useCase}\t refreshToken: ${payload.refreshToken}\t serverUrl: ${payload.serverUrl}\n`);
        return;
      case ClientLoggerUseCases.USER_LOGGED_IN:
        this._logWithSelectedLogger(`${useCase}\t username: ${payload.userName}\t ${this._authObjectToString(payload)}\n`);
        return;
      case ClientLoggerUseCases.REFRESH_TOKEN_ERROR:
        this._logWithSelectedLogger(`${useCase}\t${payload.e}\t\
        refreshToken: ${payload.refreshToken}\t serverUrl: ${payload.serverUrl}\n`);
        return;
      case ClientLoggerUseCases.NEW_REQUEST:
        this._logWithSelectedLogger(`${useCase}\t ${this._getCallFromConfig(decodeQueryString(payload))}`);
        return;
      default: {
        if (payload) {
          this._logWithSelectedLogger(`${useCase}\t${payload}\n`);
        } else {
          this._logWithSelectedLogger(`${useCase}`);
        }
      }
    }
  };
}

export default new ClientLogger();

import { store } from '../config/setupStore';
import { syncMeasurements, performInServerList } from '../actions';
import {
  LOGIN_SUCCESS,
  SET_NETWORK_ERROR,
} from '../actions/types';

export const dispatchNewTokens = (serverId, authObject) => {
  store.dispatch(performInServerList(serverId, {
    type: LOGIN_SUCCESS,
    payload: {
      ...authObject,
    },
  }));
  
  store.dispatch(syncMeasurements(serverId));
};

export const dispatchNetworkError = async error =>
  store.dispatch({
    type: SET_NETWORK_ERROR,
    payload: error,
  });

export const dispatchClearNetworkError = async () =>
  store.dispatch({
    type: SET_NETWORK_ERROR,
    payload: null,
  });

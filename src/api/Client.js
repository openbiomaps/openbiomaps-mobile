import moment from 'moment';
import { parseDataToWkt, queryString } from '../utils';
import { getCurrentLanguage } from '../utils/reduxUtils';
import { ERROR } from '../Constants';
import {
  createMainAxiosInstance,
  createRefreshAxiosInterceptor,
} from './ClientAxiosHelper';
import ClientLogger, { ClientLoggerUseCases } from './ClientLogger';

const calculateExpiresIn = (t) => (t - 60) * 1000;

/**
 * {@link API docs: http://openbiomaps.org/documents/api.html}
 * @example <caption>Usage of the Client class</caption>
 */
export default class Client {
  constructor({ apiPrefix }) {
    this.apiPrefix = apiPrefix;
    this.apiVersion = 'v2.4';

    // create axios default interceptor
    this.axiosInstance = createMainAxiosInstance(this);

    // create axios refresher interceptor
    this.refresherInstance = createRefreshAxiosInterceptor();
  }

  set prefix(prefix) {
    this.apiPrefix = prefix;
  }

  /**
   * Authenticates the client.
   *
   * @example curl -u mobile:123 http://openbiomaps.org/oauth/token.php -d "grant_type=password&username=email@email.com&password=****&scope=get_form_data+get_form_list+put_data"
   * @returns {void}
   */
  async auth({ userName, password }) {
    /* DYNAMIC_CODE_UPDATE >>>
     * auth */
    /* <<< DYNAMIC_CODE_UPDATE */
    const path = 'oauth/token.php';
    const scope =
      'get_form_data get_form_list get_project_list put_data get_trainings get_training_questions training_results training_toplist request_time get_message_count tracklog get_project_vars';

    try {
      // '+' triggers the valueOf() method -> give the current timestamps in miliseconds
      const requestTime = +new Date();
      const res = await this.axiosInstance.post(
        `${this.apiPrefix}/${path}`,
        queryString({
          grant_type: 'password',
          username: userName.toLowerCase(), // auto lowerCase email as user name
          password,
          scope,
        }),
        {
          headers: {
            Authorization: 'Basic bW9iaWxlOjEyMw==',
            'Accept-Language': getCurrentLanguage().id,
          },
        },
      );

      const authObject = {
        accessToken: res.access_token,
        expiresIn: calculateExpiresIn(res.expires_in),
        refreshToken: res.refresh_token,
        requestTime,
      };
      ClientLogger.log(ClientLoggerUseCases.USER_LOGGED_IN, {
        ...authObject,
        userName,
      });

      return authObject;
    } catch (error) {
      throw new Error(error);
    }
  }

  /**
   * Refresh the client auth info.
   */
  refresh = async ({ refreshToken, serverUrl }) => {
    const path = `${serverUrl}/oauth/token.php`;

    try {
      const trialRequestTime = Math.floor(+new Date() / 1000);
      await this.refresherInstance.post(
        `${serverUrl}/${this.apiVersion}/pds.php`,
        queryString({
          scope: 'request_time',
          value: trialRequestTime,
        }),
      );

      // if empty refresh token ...

      const requestTime = +new Date();
      const res = await this.refresherInstance.post(
        path,
        queryString({
          grant_type: 'refresh_token',
          refresh_token: refreshToken,
        }),
        {
          headers: {
            Authorization: 'Basic bW9iaWxlOjEyMw==',
            'Accept-Language': getCurrentLanguage().id,
          },
        },
      );

      const authObject = {
        accessToken: res.access_token,
        expiresIn: calculateExpiresIn(res.expires_in),
        refreshToken: res.refresh_token,
        requestTime,
      };

      return authObject;
    } catch (e) {
      ClientLogger.log(ClientLoggerUseCases.REFRESH_TOKEN_ERROR, {
        e,
        refreshToken,
        serverUrl,
      });
      throw new Error(e);
    }
  };

  /**
   * List the servers
   */
  getServerList = async () => {
    /* DYNAMIC_CODE_UPDATE >>>
     * get_server_list */
    /* <<< DYNAMIC_CODE_UPDATE */

    const path =
      'https://openbiomaps.org/projects/openbiomaps_network/?query={"available":"up"}&orderby=institute&output=json';

    try {
      const response = await this.axiosInstance.get(`${path}`, {
        headers: {
          'Accept-Language': getCurrentLanguage().id,
        },
      });

      return response;
    } catch (error) {
      throw new Error(error);
    }
  };

  /**
   * Lists the forms.
   *
   * @example curl -v http://openbiomaps.org/projects/checkitout/pds.php -d "access_token=asdf1234&scope=get_form_list&value=NULL&table=checkitout"
   * @returns {array} list of the forms
   */
  getFormList = async ({ projectUrl, authObject, noRetry = false }) => {
    /* DYNAMIC_CODE_UPDATE >>>
     * get_form_list */
    /* <<< DYNAMIC_CODE_UPDATE */

    if (!authObject.accessToken) {
      throw new Error(ERROR.ACCESS_TOKEN_MISSING);
    }

    const path = 'pds.php';

    try {
      const res = await this.axiosInstance.post(
        `${projectUrl}/${this.apiVersion}/${path}`,
        queryString({
          access_token: authObject.accessToken,
          scope: 'get_form_list',
        }),
        {
          headers: {
            'Accept-Language': getCurrentLanguage().id,
          },
          authObject,
        },
      );

      return res.data;
    } catch (e) {
      if (e === ERROR.INVALID_ACCESS_TOKEN && !noRetry) {
        return this.getFormList({
          projectUrl,
          authObject: { ...authObject, expiresIn: 0 },
          noRetry: true,
        });
      }
      throw new Error(e);
    }
  };

  /**
   * Gets a form's data by id.
   *
   * @example curl -v http://openbiomaps.org/projects/checkitout/pds.php -d "access_token=abcd1234&scope=get_form_data&value=128&table=checkitout"
   * @returns {array} list of the item definitions
   */
  getFormData = async ({
    projectName,
    projectUrl,
    id,
    authObject,
    noRetry = false,
  }) => {
    if (!authObject.accessToken) {
      throw new Error(ERROR.ACCESS_TOKEN_MISSING);
    }

    const path = 'pds.php';

    try {
      const res = await this.axiosInstance.post(
        `${projectUrl}/${this.apiVersion}/${path}`,
        queryString({
          access_token: authObject.accessToken,
          scope: 'get_form_data',
          value: id,
          project: projectName,
        }),
        {
          headers: {
            'Accept-Language': getCurrentLanguage().id,
          },
          authObject,
        },
      );

      return res.data;
    } catch (e) {
      if (e === ERROR.INVALID_ACCESS_TOKEN && !noRetry) {
        return this.getFormData({
          projectName,
          projectUrl,
          id,
          authObject: { ...authObject, expiresIn: 0 },
          noRetry: true,
        });
      }
      throw new Error(e);
    }
  };

  /**
   * Gets the list of available database projects on a server.
   *
   * @example curl -v http://openbiomaps.org/projects/checkitout/pds.php -d "access_token=abcd1234&scope=get_project_list&value=NULL&table=checkitout"
   * @returns {array} database projects
   */
  async getProjectList({ authObject, noRetry = false }) {
    /* DYNAMIC_CODE_UPDATE >>>
     * get_project_list */
    /* <<< DYNAMIC_CODE_UPDATE */

    if (!authObject.accessToken) {
      throw new Error(ERROR.ACCESS_TOKEN_MISSING);
    }

    const path = 'pds.php';

    try {
      const res = await this.axiosInstance.post(
        `${this.apiPrefix}/${this.apiVersion}/${path}`,
        queryString({
          access_token: authObject.accessToken,
          scope: 'get_project_list',
          accessible: 1,
        }),
        {
          headers: {
            'Accept-Language': getCurrentLanguage().id,
          },
          authObject,
        },
      );

      const projects = res.data.map((item) => {
        if (item.training === 't') {
          return { ...item, game: 'on' };
        }
        return { ...item };
      });

      return projects;
    } catch (e) {
      if (e === ERROR.INVALID_ACCESS_TOKEN && !noRetry) {
        return this.getProjectList({
          authObject: { ...authObject, expiresIn: 0 },
          noRetry: true,
        });
      }
      throw new Error(e);
    }
  }

  /**
   * Egyébként nem hibás, de két dolog hiányzik hozzá, hogy működjön.
   * Az egyik, hogy a get_project_vars scope-t engedélyezni kell az authetnikációkor a mobil-appban, a másik pedig,
   * hogy a projektekben sincs engedélyezve ahol a múltkor frissített pds fut (amelyik frissítésnél megszüntettem több
   * authentikáció nélküli lekérdezést a login-error issuek kapcsán). Ezt most elérhetővé tettem authentikációval, de a
   * szerver frissítések után lesz csak elérhető, de egyelőre nem kell használni az appban. - Miki
   */
  /**
   * Gets the variables associated with the server e.g. login URL for the web login flow.
   * Does not require auth.
   *
   * @example curl -v http://openbiomaps.org/projects/checkitout/pds.php -d "scope=get_project_vars"
   * @returns {array} database projects
   */
  async getProjectVars({ projectUrl, authObject, noRetry = false }) {
    const path = 'pds.php';

    try {
      const res = await this.axiosInstance.post(
        `${projectUrl}/${this.apiVersion}/${path}`,
        queryString({
          access_token: authObject.accessToken,
          scope: 'get_project_vars',
        }),
        {
          headers: {
            'Accept-Language': getCurrentLanguage().id,
          },
          authObject,
        },
      );
      return res.data;
    } catch (e) {
      if (e === ERROR.INVALID_ACCESS_TOKEN && !noRetry) {
        return this.getProjectVars({
          projectUrl,
          authObject: { ...authObject, expiresIn: 0 },
          noRetry: true,
        });
      }
      throw new Error(e);
    }
  }

  async uploadTrackLog({ projectUrl, tracklog, authObject }) {
    if (!authObject.accessToken) {
      throw new Error(ERROR.ACCESS_TOKEN_MISSING);
    }

    const path = 'pds.php';
    const query = queryString({
      access_token: authObject.accessToken,
      scope: 'tracklog',
      value: JSON.stringify(tracklog),
    });

    try {
      const res = await this.axiosInstance.post(
        `${projectUrl}/${this.apiVersion}/${path}`,
        query,
        {
          headers: {
            'Accept-Language': getCurrentLanguage().id,
          },
          authObject,
          timeout: 20000,
        },
      );

      return res;
    } catch (error) {
      throw new Error(error);
    }
  }

  // refreshing the token first, so basically no way an access token can be invalid.
  // even if access token vanishes during sync, error will be shown and the next refresh will be good
  /**
   * Save the new measurement.
   *
   * @example curl -v http://openbiomaps.org/projects/checkitout/pds.php -d "access_token=abcd1234&scope=get_project_list&value=NULL&table=checkitout"
   * @returns {array} database projects
   */
  async dataPush({
    dataTypes,
    projectUrl,
    formId,
    measurement,
    meta,
    authObject,
  }) {
    if (!authObject.accessToken) {
      throw new Error(ERROR.ACCESS_TOKEN_MISSING);
    }

    const path = 'pds.php';

    const header = Object.keys(measurement);

    const measurementData = { ...measurement };

    header.forEach((key) => {
      const resType = dataTypes.find((type) => type.column === key);

      measurementData[key] = this.typeConverter(resType, key, measurement);
    });

    const query = queryString({
      access_token: authObject.accessToken,
      form_id: formId,
      scope: 'put_data',
      header: JSON.stringify(header),
      data: JSON.stringify([measurementData]),
      metadata: JSON.stringify(meta),
    });

    try {
      const res = await this.axiosInstance.post(
        `${projectUrl}/${this.apiVersion}/${path}`,
        query,
        {
          headers: {
            'Accept-Language': getCurrentLanguage().id,
          },
          authObject,
          timeout: 20000,
        },
      );

      return res;
    } catch (error) {
      throw new Error(error);
    }
  }

  typeConverter = (dataTypes, key, measurement) => {
    switch (dataTypes && dataTypes.type) {
      case 'point':
      case 'line':
      case 'polygon':
        return parseDataToWkt(measurement[key], dataTypes.type);
      case 'wkt': {
        return parseDataToWkt(
          measurement[key].wktValue,
          measurement[key].wktType,
        );
      }
      case 'datetime':
        return moment(measurement[key]).format('YYYY-MM-DD HH:mm:ss');
      case 'date':
        return moment(measurement[key]).format('YYYY-MM-DD');
      case 'time':
        return moment(measurement[key]).format('HH:mm:ss');
      case 'autocompletelist':
        return measurement[key].toString();
      default:
        return measurement[key];
    }
  };

  // same as datapush
  /**
   * Save the new measurement.
   *
   * @example curl -v http://openbiomaps.org/projects/checkitout/pds.php -d "access_token=abcd1234&scope=get_project_list&value=NULL&table=checkitout"
   * @returns {array} database projects
   */
  async filePush({
    dataTypes,
    projectUrl,
    formId,
    measurement,
    meta,
    authObject,
  }) {
    if (!authObject.accessToken) {
      throw new Error(ERROR.ACCESS_TOKEN_MISSING);
    }

    const path = 'pds.php';

    const header = Object.keys(measurement);

    const imageFields = dataTypes
      .filter((field) => field.type === 'file_id')
      .map((field) => field.column);
    const images = imageFields.reduce(
      (acc, curr) => (measurement[curr] ? [...acc, ...measurement[curr]] : acc),
      [],
    );
    const files = images.map((image, index) => `file${index}`);

    const resMeasurement = { ...measurement };

    header.forEach((key) => {
      const resType = dataTypes.find((type) => type.column === key);
      resMeasurement[key] = this.typeConverter(resType, key, measurement);
    });

    const batch = [
      {
        data: [resMeasurement],
        attached_files: files.join(','),
      },
    ];

    const formData = new FormData();
    formData.append('access_token', authObject.accessToken);
    formData.append('form_id', formId);
    formData.append('scope', 'put_data');
    formData.append('header', JSON.stringify(header));
    formData.append('batch', JSON.stringify(batch));
    formData.append('metadata', JSON.stringify(meta));
    images.forEach((file, index) => {
      formData.append(`file${index}`, {
        uri: file.uri,
        type: file.type,
        name: file.fileName,
      });
    });

    try {
      const res = await this.axiosInstance.post(
        `${projectUrl}/${this.apiVersion}/${path}`,
        formData,
        {
          authObject,
          timeout: 20000,
        },
      );

      return res;
    } catch (error) {
      throw new Error(error);
    }
  }

  /**
   * Gets a project's trainings.
   *
   * @example curl -F ‘scope=get_trainings’ -F ‘access_token=abcd1234 -F ‘project=dinpi’ http://localhost/biomaps/pds.php
   * @returns {array} list of the item definitions
   */
  getTrainings = async ({
    projectName,
    projectUrl,
    authObject,
    noRetry = false,
  }) => {
    if (!authObject.accessToken) {
      throw new Error(ERROR.ACCESS_TOKEN_MISSING);
    }

    const path = 'pds.php';

    try {
      const res = await this.axiosInstance.post(
        `${projectUrl}/${this.apiVersion}/${path}`,
        queryString({
          access_token: authObject.accessToken,
          scope: 'get_trainings',
          project: projectName,
        }),
        {
          headers: {
            'Accept-Language': getCurrentLanguage().id,
          },
          authObject,
        },
      );

      return res.data;
    } catch (e) {
      if (e === ERROR.INVALID_ACCESS_TOKEN && !noRetry) {
        return this.getTrainings({
          projectName,
          projectUrl,
          authObject: { ...authObject, expiresIn: 0 },
          noRetry: true,
        });
      }
      throw new Error(e);
    }
  };

  /**
   * Gets the questions and answers for a given training.
   *
   * @example curl -F ‘scope=get_training_questions’ -F ‘access_token=abcd1234 -F ‘project=dinpi’ http://dinpi.openbiomaps.org/biomaps/pds.php -F 'value=1'
   * @returns {array} list of the item definitions
   */
  getTrainingQuestions = async ({
    projectName,
    trainingId,
    projectUrl,
    authObject,
    noRetry = false,
  }) => {
    if (!authObject.accessToken) {
      throw new Error(ERROR.ACCESS_TOKEN_MISSING);
    }

    const path = 'pds.php';

    try {
      const res = await this.axiosInstance.post(
        `${projectUrl}/${this.apiVersion}/${path}`,
        queryString({
          access_token: authObject.accessToken,
          scope: 'get_training_questions',
          project: projectName,
          value: trainingId,
        }),
        {
          headers: {
            'Accept-Language': getCurrentLanguage().id,
          },
          authObject,
        },
      );

      return res.data;
    } catch (e) {
      if (e === ERROR.INVALID_ACCESS_TOKEN && !noRetry) {
        return this.getTrainingQuestions({
          projectName,
          trainingId,
          projectUrl,
          authObject: { ...authObject, expiresIn: 0 },
          noRetry: true,
        });
      }
      throw new Error(e);
    }
  };

  /**
   * Gets the questions and answers for a given training.
   *
   * @example curl -F ‘scope=training_results’ -F ‘access_token=abcd1234 -F ‘project=dinpi’ http://dinpi.openbiomaps.org/biomaps/pds.php -F 'value=1'
   * @returns {array} list of the results
   */
  trainingResults = async ({
    projectName,
    projectUrl,
    authObject,
    noRetry = false,
  }) => {
    if (!authObject.accessToken) {
      throw new Error(ERROR.ACCESS_TOKEN_MISSING);
    }

    const path = 'pds.php';

    try {
      const res = await this.axiosInstance.post(
        `${projectUrl}/${this.apiVersion}/${path}`,
        queryString({
          access_token: authObject.accessToken,
          scope: 'training_results',
          project: projectName,
        }),
        {
          headers: {
            'Accept-Language': getCurrentLanguage().id,
          },
          authObject,
        },
      );

      return res.data;
    } catch (e) {
      if (e === ERROR.INVALID_ACCESS_TOKEN && !noRetry) {
        return this.trainingResults({
          projectUrl,
          authObject: { ...authObject, expiresIn: 0 },
          noRetry: true,
        });
      }
      throw new Error(e);
    }
  };

  // curl -F ‘scope=training_toplist’ -F ‘access_token=5ac3...’ -F ‘project=dinpi’ http://localhost/biomaps/pds.php
  trainingToplist = async ({
    projectName,
    projectUrl,
    authObject,
    noRetry = false,
  }) => {
    if (!authObject.accessToken) {
      throw new Error(ERROR.ACCESS_TOKEN_MISSING);
    }

    const path = 'pds.php';

    try {
      const res = await this.axiosInstance.post(
        `${projectUrl}/${this.apiVersion}/${path}`,
        queryString({
          access_token: authObject.accessToken,
          scope: 'training_toplist',
          project: projectName,
        }),
        {
          headers: {
            'Accept-Language': getCurrentLanguage().id,
          },
          authObject,
        },
      );

      return res.data;
    } catch (e) {
      if (e === ERROR.INVALID_ACCESS_TOKEN && !noRetry) {
        return this.trainingTopList({
          projectName,
          projectUrl,
          authObject: { ...authObject, expiresIn: 0 },
          noRetry: true,
        });
      }
      throw new Error(e);
    }
  };

  getMessageCount = async ({ projectUrl, authObject, noRetry = false }) => {
    if (!authObject.accessToken) {
      throw new Error(ERROR.ACCESS_TOKEN_MISSING);
    }

    const path = 'pds.php';

    try {
      const res = await this.axiosInstance.post(
        `${projectUrl}/${this.apiVersion}/${path}`,
        queryString({
          access_token: authObject.accessToken,
          scope: 'get_message_count',
        }),
        {
          headers: {
            'Accept-Language': getCurrentLanguage().id,
          },
          authObject,
        },
      );
      return res.data;
    } catch (e) {
      if (e === ERROR.INVALID_ACCESS_TOKEN && !noRetry) {
        return this.getMessageCount({
          projectUrl,
          authObject: { ...authObject, expiresIn: 0 },
          noRetry: true,
        });
      }
      throw new Error(e);
    }
  };
}

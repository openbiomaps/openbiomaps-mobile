import { useCallback, useEffect, useMemo } from 'react';

import { useDispatch } from 'react-redux';

import { getSessions } from './utils';
import { usePrevious } from './utils/usePrevious';
import { useShallowEqualSelector } from './utils/useShallowEqualSelector';
import { useGpsSubscription } from './utils/useGpsSubscription';
import { useTrackLogPositionUpdate } from './utils/useTrackLogPositionUpdate';
import { addPointToSessionTrackLogs, handleEndedSessions, handleNewSessions } from './actions';
import { GPS_OBSERVER } from './Constants';

const getObserver = (observationId) => {
  if (!observationId) return null;
  return `${GPS_OBSERVER.SESSION_TRACKLOG}-${observationId}`;
};

const SessionTrackLogHandler = () => {
  const dispatch = useDispatch();
  
  const servers = useShallowEqualSelector((state) => state.servers.data);
  const position = useShallowEqualSelector((state) => state.gps.position);
  const observers = useShallowEqualSelector((state) => state.gps.observers);
  
  const currSessions = useMemo(() => getSessions(servers, true), [servers]);

  const prevSessions = usePrevious(currSessions) || [];

  const addedSession = useMemo(() => {
    if (!prevSessions || !currSessions) return null;
    return currSessions.filter(({ sessionId: newId }) => !prevSessions.some(({ sessionId: oldId }) => oldId === newId))[0];
  }, [prevSessions, currSessions]);
  
  const endedSession = useMemo(() => {
    if (!prevSessions || !currSessions) return null;
    return prevSessions.filter(({ sessionId: oldId }) => !currSessions.some(({ sessionId: newId }) => oldId === newId))[0];
  }, [prevSessions, currSessions]);

  const isObservingGps = useMemo(() => {
    if (!currSessions.length || !observers.length) return false;
    const observationIds = currSessions.map((item) => item.observationId);
    return observationIds.every((item) => observers.includes(getObserver(item)));
  }, [currSessions, observers]);

  const validPosition = useMemo(() => {
    if (!isObservingGps || !Object.values(position).every((item) => item)) return null;
    return position;
  }, [isObservingGps, position]);

  const { subscribe, unsubscribe } = useGpsSubscription({ 
    observer: getObserver(addedSession?.observationId) || getObserver(endedSession?.observationId), 
  });

  const handleSessionTracklogRecording = useCallback(() => {
    if (addedSession) {
      dispatch(handleNewSessions([addedSession]));
      subscribe();
    }

    if (endedSession) {
      dispatch(handleEndedSessions([endedSession]));
      unsubscribe();
    }
  }, [addedSession, endedSession, subscribe, unsubscribe, dispatch]);

  useTrackLogPositionUpdate(addPointToSessionTrackLogs, validPosition);

  useEffect(() => {
    handleSessionTracklogRecording();
  }, [handleSessionTracklogRecording]);

  return null;
};

export default SessionTrackLogHandler;

import React from 'react';
import { Modal, View, TouchableWithoutFeedback, Dimensions } from 'react-native';
import PropTypes from 'prop-types';
import MeasurementInfo from '../MeasurementInfo';
import styles from './styles';

const getMaxHeight = () => Dimensions.get('window').height * 0.85;

const MeasurementInfoPopup = props => (
  <Modal
    transparent
    visible={props.visible}
    onRequestClose={props.onClose}
  >
    <View style={styles.container}>
      <TouchableWithoutFeedback onPress={props.onClose}>
        <View style={styles.closeArea} />
      </TouchableWithoutFeedback>
      <View style={[styles.popup, { maxHeight: getMaxHeight() }]}>
        <MeasurementInfo {...props} />
      </View>
    </View>
  </Modal>
);

MeasurementInfoPopup.propTypes = {
  visible: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
};

export default MeasurementInfoPopup;

import React, { Component } from "react";
import { Text, View } from 'react-native';
import PropTypes from 'prop-types';
import * as Animatable from 'react-native-animatable';
import styles from '../Notification/styles';
import * as Sentry from '@sentry/react-native';
import SoundPlayer from 'react-native-sound-player'
import { showAlertDialog } from '../AlertDialog';
import I18n from '../../i18n';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

class PeriodicNotificationTimer extends Component {

    _notificationTimer;
    _timer;

    constructor(props) {
        super(props);
        this.state = {
            countDownTime: this.props.countDownTime * 60,
            leftCountDownTime: null,
            time: null,
            alertShow: false,
        };

        clearInterval(this._notificationTimer);
        clearInterval(this._timer);

        this._timer = setInterval(this.calculateTime, 1000);
        this._notificationTimer = setInterval(this.calculateCountDownTime, 1000);

    }

    componentDidMount = () => {
        this.calculateCountDownTime();
        this.calculateTime();
    }

    componentWillUnmount = () => {
        clearInterval(this._notificationTimer);
        clearInterval(this._timer);
    }

    componentDidUpdate(previousProps, previousState) {
        if (previousProps.showPeriodicNotificationCountDownTimer !== this.props.showPeriodicNotificationCountDownTimer) {
            this.setState({
                leftCountDownTime: this.props.countDownTime * 60,
                countDownTime: this.props.countDownTime * 60
            });
        }
    }

    calculateCountDownTime = () => {
        this.setState({
            leftCountDownTime: this.state.countDownTime,
            countDownTime: Math.max(0, this.state.countDownTime - 1)
        });

        if (this.state.countDownTime == 0) {
            if (!this.state.alertShow) {
                this.setState({
                    alertShow: true
                })
                showAlertDialog({
                    title: I18n.t('periodical_notification_message'),
                    positiveLabel: I18n.t('ok'),
                    onPositiveBtnPress: () => {
                        this.setState({
                            alertShow: false
                        })
                    }
                });
            }

            try {
                SoundPlayer.playSoundFile('periodic_notification', 'wav');
            } catch (e) {
                Sentry.captureMessage(`Periodic Notification Sound Error: ${e}`);
            }

            this.setState({
                countDownTime: this.props.countDownTime * 60,
            })
        }
    }


    getTimeLeft = () => {
        const { leftCountDownTime } = this.state;
        return parseInt(leftCountDownTime / (60 * 60), 10) % 24 + 'h :' + parseInt(leftCountDownTime / 60, 10) % 60 + 'm :' + leftCountDownTime % 60 + 's'
    };

    _generateTimeFormat = diff => `${diff.getUTCHours()}h : ${diff.getUTCMinutes()}m : ${diff.getUTCSeconds()}s`;

    calculateTime = () => {
        const { startTimer } = this.props;

        const diff = new Date().getTime() - new Date(startTimer).getTime();
        const dateDiff = new Date(diff);
        this.setState({
            time: this._generateTimeFormat(dateDiff),
        });
    }

    render = () => {
        const { time } = this.state;

        return (
            <Animatable.View
                style={styles.baseContainerStyle}
                animation="bounceIn"
                useNativeDriver>
                <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'center', paddingRight: 40 }}>
                        <MaterialCommunityIcons name={"timer-outline"} size={16} style={{ paddingRight: 10, paddingTop: 2, color: 'white' }} />
                        <Text style={{ color: 'white', fontSize: 16 }}>{this.getTimeLeft()}</Text>
                    </View>

                    <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                        <MaterialCommunityIcons name={"clock-time-three-outline"} size={16} style={{ paddingRight: 10, paddingTop: 2, color: 'white' }} />
                        <Text style={{ color: 'white', fontSize: 16 }}>{time}</Text>
                    </View>
                </View>
            </Animatable.View>
        )
    }
}

PeriodicNotificationTimer.prototype = {
    showPeriodicNotificationTimer: PropTypes.bool,
    showPeriodicNotificationCountDownTimer: PropTypes.bool,
    countDownTime: PropTypes.number,
    startTimer: PropTypes.number
}

export default PeriodicNotificationTimer;
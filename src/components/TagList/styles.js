import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  baseContainer: { 
    flexDirection: 'row', 
    flexWrap: 'wrap',
  },
});

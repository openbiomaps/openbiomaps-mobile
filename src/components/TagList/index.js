import React from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';
import Tag from '../Tag';
import styles from './styles';

const TagList = ({ data, onItemPress, style }) => (
  <View style={[styles.baseContainer, style]}>
    { data.map((item, index) => (
      <Tag label={item} key={index.toString()} onPress={onItemPress} />
    ))}
  </View>
);

TagList.propTypes = {
  data: PropTypes.array.isRequired,
  onItemPress: PropTypes.func,
  style: PropTypes.object,
};

TagList.defaultProps = {
  onItemPress: () => {},
  style: {},
};

export default TagList;

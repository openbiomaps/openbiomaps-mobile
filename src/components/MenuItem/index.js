import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import { Colors } from '../../colors';
import { TouchableIcon } from '../TouchableIcon';
import styles from './styles';

const MenuItem = ({
  title,
  description,
  customContainerStyle,
  customInfoContainerStyle,
  onPress,
  infoComponent,
  numberOfLines,
  icon,
  labelIconName,
  deleteIcon,
  disabled,
}) => (
  <TouchableOpacity
    activeOpacity={0.7}
    style={[styles.baseContainer, customContainerStyle]}
    onPress={onPress}
    disabled={disabled}
  >
    <View style={styles.row}>
      {labelIconName &&
        <TouchableIcon
          name={labelIconName}
          iconType="FontAwesome"
          size={30}
          color={Colors.white}
          style={styles.iconContainer}
          onPress={onPress}
        />
      }
      <Text style={styles.title} numberOfLines={numberOfLines} adjustsFontSizeToFit={true}>
        {title}
      </Text>
    </View>
    {description && <Text style={styles.text}>{description}</Text>}
    {icon && icon()}
    {deleteIcon && deleteIcon()}
    <View style={[styles.infoContainer, customInfoContainerStyle]}>{infoComponent()}</View>
  </TouchableOpacity>
);

MenuItem.propTypes = {
  title: PropTypes.string.isRequired,
  onPress: PropTypes.func.isRequired,
  description: PropTypes.string,
  customContainerStyle: PropTypes.object,
  customInfoContainerStyle: PropTypes.object,
  infoComponent: PropTypes.func,
  numberOfLines: PropTypes.number,
  icon: PropTypes.func,
  labelIcon: PropTypes.string,
  deleteIcon: PropTypes.func,
  disabled: PropTypes.bool,
};

MenuItem.defaultProps = {
  description: null,
  customContainerStyle: {},
  customInfoContainerStyle: {},
  infoComponent: () => { },
  numberOfLines: 1,
  icon: () => { },
  labelIconName: null,
  deleteIcon: null,
  disabled: false,
};

export default MenuItem;

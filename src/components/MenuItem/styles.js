import { StyleSheet } from 'react-native';
import { Colors } from '../../colors';

const defaultText = {
  fontSize: 18,
  color: 'white',
  fontWeight: '600',
  textAlign: 'center',
};

export default StyleSheet.create({
  baseContainer: {
    padding: 20,
    backgroundColor: Colors.mantisGreen,
    marginTop: 16,
    flex: 1,
    marginHorizontal: 8,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    overflow: 'hidden',
  },
  infoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    flexDirection: 'row',
    paddingHorizontal: 8,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: Colors.blue,
  },
  iconContainer: {
    marginRight: 8,
  },
  title: {
    ...defaultText,
    fontSize: 28,
  },
  text: {
    ...defaultText,
    marginTop: 8,
  },
  row: {
    justifyContent: 'center',
    flexDirection: 'row'
  }
});

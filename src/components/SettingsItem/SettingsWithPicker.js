import React, { Component, Fragment } from 'react';
import { TouchableOpacity, Text } from 'react-native';
import PropTypes from 'prop-types';
import { SettingsWithPickerStyle as styles } from './styles';
import PickerModal from '../PickerModal';

class SettingsWithPicker extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showModal: false,
    };
  }

  handleRowPress = () => {
    const { data, onValueChange } = this.props;

    if (data.length) {
      this.setState({ showModal: true });
    } else {
      onValueChange({
        key: 'error',
        value: 'no_data',
      });
    }
  };

  render() {
    const { title, value, data, onValueChange } = this.props;
    const { showModal } = this.state;

    return (
      <Fragment>
        <TouchableOpacity
          style={styles.baseContainer}
          onPress={this.handleRowPress}
          activeOpacity={1}
        >
          <Text style={styles.title}>{title}</Text>
          {value && <Text style={styles.value}>{value}</Text>}
        </TouchableOpacity>

        <PickerModal
          modalTitle={title}
          selectedValue={value}
          showModal={showModal}
          onRequestClose={() => this.setState({ showModal: !showModal })}
          data={data}
          onItemSelect={item => onValueChange(item)}
        />
      </Fragment>
    );
  }
}

SettingsWithPicker.propTypes = {
  title: PropTypes.string.isRequired,
  data: PropTypes.arrayOf(PropTypes.object).isRequired,
  onValueChange: PropTypes.func.isRequired,
  value: PropTypes.string,
};

SettingsWithPicker.defaultProps = {
  value: null,
};

export default SettingsWithPicker;

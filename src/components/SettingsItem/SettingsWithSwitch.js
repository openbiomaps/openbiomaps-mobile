import React from 'react';
import { TouchableOpacity, Text, Switch, View } from 'react-native';
import PropTypes from 'prop-types';
import { SettingsWithSwitchStyle as styles } from './styles';

const SettingsWithSwitch = ({ title, value, description, onValueChange }) => (
  <TouchableOpacity 
    style={styles.baseContainer} 
    onPress={() => onValueChange(!value)}
    activeOpacity={1}
  >
    <View style={styles.titleContainer}>
      <Text style={styles.title}>{title}</Text>
      {description && <Text style={styles.description}>{description}</Text>}
    </View>
    <Switch 
      value={value} 
      onValueChange={() => onValueChange(!value)} 
      style={styles.switchContainer}
    />
  </TouchableOpacity>
);

SettingsWithSwitch.propTypes = {
  title: PropTypes.string.isRequired,
  value: PropTypes.bool.isRequired,
  onValueChange: PropTypes.func.isRequired,
  description: PropTypes.string,
};

SettingsWithSwitch.defaultProps = {
  description: null,
};

export default SettingsWithSwitch;

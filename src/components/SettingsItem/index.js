import React from 'react';
import PropTypes from 'prop-types';
import SettingsWithPicker from './SettingsWithPicker';
import SettingsWithSwitch from './SettingsWithSwitch';
import SettingsTitle from './SettingsTitle';
import SettingsInfo from './SettingsInfo';
import SettingsWithNumText from './SettingsWithNumText';
import SettingsWithButton from './SettingsWithButton';

const SettingsItem = ({ type, ...restProps }) => {
  switch (type) {
    case 'picker':
      return <SettingsWithPicker {...restProps} />;
    case 'switch':
      return <SettingsWithSwitch {...restProps} />;
    case 'title':
      return <SettingsTitle {...restProps} />;
    case 'info':
      return <SettingsInfo {...restProps} />;
    case 'numtext':
      return <SettingsWithNumText {...restProps} />;
    case 'button':
      return <SettingsWithButton {...restProps} />;
    default:
      return null;
  }
};

SettingsItem.propTypes = {
  type: PropTypes.string.isRequired,
};

export default SettingsItem;

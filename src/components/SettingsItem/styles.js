import { StyleSheet } from 'react-native';
import { Colors } from '../../colors';

// Local general styles
const SettingsItemStyle = {
  baseContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 14,
    paddingHorizontal: 16,
    borderBottomWidth: 0.5,
    borderBottomColor: Colors.darkGray,
  },
  text: {
    fontSize: 16,
    color: 'black',
  },
};

export const SettingsWithPickerStyle = StyleSheet.create({
  baseContainer: {
    ...SettingsItemStyle.baseContainer,
    flexDirection: 'column',
    alignItems: 'flex-start',
  },
  title: {
    ...SettingsItemStyle.text,
  },
  value: {
    ...SettingsItemStyle.text,
    fontSize: 14,
    color: Colors.gray,
  },
});

export const SettingsWithSwitchStyle = StyleSheet.create({
  baseContainer: {
    ...SettingsItemStyle.baseContainer,
  },
  titleContainer: {
    flex: 1,
  },
  title: {
    ...SettingsItemStyle.text,
  },
  description: {
    ...SettingsItemStyle.text,
    fontSize: 14,
    color: Colors.gray,
  },
  switchContainer: {
    marginLeft: 'auto',
  },
});

export const SettingsWithButtonStyle = StyleSheet.create({
  baseContainer: {
    ...SettingsItemStyle.baseContainer,
  },
  titleContainer: {
    flex: 1,
  },
  title: {
    ...SettingsItemStyle.text,
  },
  description: {
    ...SettingsItemStyle.text,
    fontSize: 14,
    color: Colors.gray,
  },
  buttonStyle: {
    borderWidth: 1,
    borderRadius: 5,
    justifyContent: 'center',
    marginHorizontal: 10,
    padding: 3,
    width: '30%',
  },
});

export const SettingsWithNumTextStyle = StyleSheet.create({
  baseContainer: {
    ...SettingsItemStyle.baseContainer,
  },
  titleContainer: {
    flex: 1,
  },
  title: {
    ...SettingsItemStyle.text,
  },
  description: {
    ...SettingsItemStyle.text,
    fontSize: 14,
    color: Colors.gray,
  },
  switchContainer: {
    marginLeft: 'auto',
  },
});

export const SettingsTitleStyle = StyleSheet.create({
  baseContainer: {
    ...SettingsItemStyle.baseContainer,
    borderBottomWidth: 0,
    paddingBottom: 0,
    alignItems: 'flex-end',
  },
  title: {
    ...SettingsItemStyle.text,
    color: Colors.mantisGreen,
  },
});

export const SettingsInfoStyle = StyleSheet.create({
  baseContainer: {
    ...SettingsItemStyle.baseContainer,
    flexDirection: 'column',
    alignItems: 'flex-start',
  },
  title: {
    ...SettingsItemStyle.text,
  },
  description: {
    ...SettingsItemStyle.text,
    fontSize: 14,
    color: Colors.gray,
  },
});


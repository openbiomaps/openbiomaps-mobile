import React from 'react';
import { View, Text } from 'react-native';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { footerStyle } from '../Footer/styles';

const getIconName = ({ netStatus }) => {
  if (netStatus !== 'none' && netStatus !== 'unknown') return 'cloud';
  return 'cloud-off';
};

const getFooterText = ({ netStatus, connectionText, noConnectionText }) => {
  if (netStatus !== 'none' && netStatus !== 'unknown') return connectionText;
  return noConnectionText;
};

const SignalFooter = props => (
  <View style={footerStyle.wrapContentContainer}>
    <Icon name={getIconName(props)} size={24} color="white" style={{ marginEnd: 10 }} />
    <Text style={footerStyle.footerText}>{getFooterText(props)}</Text>
  </View>
);

const mapStateToProps = ({ network: { status = 'none' } }) => ({
  netStatus: status,
});

export default connect(mapStateToProps)(SignalFooter);

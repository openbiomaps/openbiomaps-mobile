import React from 'react';
import { View } from 'react-native';

import PropTypes from 'prop-types';

import styles from './styles';

const Row = ({ children, style }) => (
  <View style={[styles.baseContainer, style]}>
    {children}
  </View>
);

Row.propTypes = {
  children: PropTypes.node.isRequired,
  style: PropTypes.object,
};

Row.defaultProps = {
  style: {},
};

export default Row;

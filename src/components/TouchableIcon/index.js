import React from 'react';
import { TouchableOpacity, Text } from 'react-native';
import PropTypes from 'prop-types';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import SimpleLineIcon from 'react-native-vector-icons/SimpleLineIcons';
import styles from './styles';

const getIcon = (iconType, iconProps) => {
  switch (iconType) {
    case 'MaterialCommunityIcon':
      return <MaterialCommunityIcon {...iconProps} />;
    case 'MaterialIcon':
      return <MaterialIcon {...iconProps} />;
    case 'FontAwesome':
      return <FontAwesome {...iconProps} />;
    case 'FontAwesome5':
      return <FontAwesome5 {...iconProps} />;
    case 'SimpleLineIcon':
      return <SimpleLineIcon {...iconProps}/>;  
    default:
      return <MaterialIcon {...iconProps} />;
  }
};

const TouchableIcon = ({ 
  iconType,
  name, 
  size, 
  color, 
  onPress, 
  style, 
  label, 
  labelStyle,
  vectorIconStyle,
  disabled,
  ...touchableProps
}) => (
  <TouchableOpacity onPress={onPress} style={[styles.baseContainer, style, { borderColor: color }]} disabled={disabled} {...touchableProps}>
    {getIcon(iconType, { name, size, color, style: vectorIconStyle })}
    {label && <Text style={[styles.label, labelStyle]}>{label}</Text>}
  </TouchableOpacity>
);

TouchableIcon.propTypes = {
  name: PropTypes.string.isRequired,
  onPress: PropTypes.func,
  size: PropTypes.number,
  color: PropTypes.string,
  style: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.object,
    PropTypes.array,
  ]),
  iconType: PropTypes.string,
  label: PropTypes.string,
  labelStyle: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.object,
    PropTypes.array,
  ]),
  vectorIconStyle: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.object,
    PropTypes.array,
  ]),
  disabled: PropTypes.bool,
};

TouchableIcon.defaultProps = {
  onPress: () => {},
  size: 30,
  color: 'black',
  style: null,
  iconType: 'MaterialIcon',
  label: null,
  labelStyle: null,
  vectorIconStyle: null,
  disabled: false,
};

export { TouchableIcon };

import { StyleSheet } from 'react-native';
import { SCREEN_WIDTH, SCREEN_HEIGHT } from '../../dimens';

export const styles = StyleSheet.create({
  baseContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  labelContainer: {
    flex: 1,
  },
  backdropContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.6)',
  },
  itemContainer: {
    paddingHorizontal: 24,
    paddingVertical: 10,
  },
  list: {
    maxHeight: SCREEN_HEIGHT * 0.8,
    width: SCREEN_WIDTH * 0.9,
    flexGrow: 0,
    backgroundColor: 'white',
    borderRadius: 5,
  },
  text: {
    fontSize: 16,
  },
});

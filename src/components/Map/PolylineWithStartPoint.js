import React from 'react';
import PropTypes from 'prop-types';
import { Text, View } from 'react-native';
import { Marker, Polyline } from 'react-native-maps';
import styles from './styles';

const PolylineWithStartPoint = ({
  coordinates,
  strokeColor,
}) => (
  <View>
    <Polyline
      coordinates={coordinates}
      strokeColor={strokeColor}
      strokeWidth={4}
    />
    {coordinates.length &&
      <Marker
        coordinate={coordinates[0]}
        anchor={{ x: 0.5, y: 0.5 }}
      >
        <View style={styles.startPoint}>
          <Text>S</Text>
        </View>
      </Marker>
    }
  </View>
);

PolylineWithStartPoint.propTypes = {
  coordinates: PropTypes.array.isRequired,
  strokeColor: PropTypes.string,
};

PolylineWithStartPoint.defaultProps = {
  strokeColor: 'white',
};

export default PolylineWithStartPoint;

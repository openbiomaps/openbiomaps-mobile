import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Text, View } from 'react-native';
import MapView, {
  Geojson,
  MAP_TYPES,
  Marker,
  Polygon,
  Polyline,
  UrlTile,
} from 'react-native-maps';
import Icon from 'react-native-vector-icons/MaterialIcons';
import styles from './styles';
import { hashStringToHexColor, isValidCoordinateArray } from '../../utils';
import { Colors } from '../../colors';
import PolylineWithStartPoint from './PolylineWithStartPoint';

const MapComponent = ({
  provider = 'google',
  permanentSamplePlots,
  lastMeasurements,
  showTrackLogs,
  showMeasurements,
  showPernamentSamplePlots,
  region,
  onRegionChange,
  onMeasurementPress,
  position,
  mapRef,
  onLongPress,
  inputGeometryValue,
  inputRef,
  liteMode,
  style,
  trackLogList,
  trackLogToShowOnly,
  type,
  showOnlyOneTrackLog,
  currentTrackLog,
  currentSessionTrackLog,
  editMode,
  gpsError,
}) => {
  const isInputGeometryValueArray =
    !!inputGeometryValue && Array.isArray(inputGeometryValue);
  const isInputGeometryValuePoint =
    !!inputGeometryValue && !Array.isArray(inputGeometryValue);
  const showInputGeometryValueAsArray =
    isInputGeometryValueArray && inputGeometryValue.length > 1;
  const showInputGeometryValueStartPoint =
    isInputGeometryValueArray &&
    inputGeometryValue.length === 1 &&
    !!inputGeometryValue[0].latitude &&
    !!inputGeometryValue[0].longitude;
  const showTrackLogList =
    showTrackLogs &&
    (!showOnlyOneTrackLog || trackLogToShowOnly === null) &&
    currentTrackLog === null &&
    trackLogList.length !== 0;
  const showMeasurementPoints =
    showMeasurements &&
    lastMeasurements &&
    (!showOnlyOneTrackLog || trackLogToShowOnly === null) &&
    currentTrackLog === null;
  return (
    <MapView
      ref={mapRef}
      provider={provider === 'google' ? 'google' : null}
      mapType={provider === 'google' ? MAP_TYPES.HYBRID : MAP_TYPES.NONE}
      initialRegion={region}
      onRegionChangeComplete={onRegionChange}
      style={[StyleSheet.absoluteFillObject, style]}
      pitchEnabled={false}
      onLongPress={onLongPress}
      liteMode={liteMode}
    >
      {provider === 'osm' && (
        <UrlTile urlTemplate="http://obm.bnpi.hu:83/tile/{z}/{x}/{y}.png" />
      )}
      {!gpsError && !!position && (
        <Marker coordinate={position} anchor={{ x: 0.5, y: 0.7 }}>
          <Icon name="add" size={60} color="white" />
        </Marker>
      )}
      {!gpsError &&
        showMeasurementPoints &&
        lastMeasurements.map(
          (measurement) =>
            !measurement.isMetaRecord &&
            !!measurement.coordinate && (
              <Marker
                coordinate={measurement.coordinate}
                onPress={
                  onMeasurementPress && (() => onMeasurementPress(measurement))
                }
                key={measurement.id}
                anchor={{ x: 0.5, y: 0.5 }}
              >
                <View style={styles.circle} />
              </Marker>
            ),
        )}
      {!gpsError &&
        showTrackLogList &&
        trackLogList.map(({ trackLogArray, trackLogId, sessionId }) => (
          <Polyline
            coordinates={trackLogArray}
            key={trackLogId}
            strokeColor={
              sessionId
                ? hashStringToHexColor({ string: sessionId, alpha: 'ff' })
                : 'white'
            }
            strokeWidth={4}
          />
        ))}
      {showOnlyOneTrackLog &&
        isValidCoordinateArray(
          trackLogToShowOnly?.trackLogArray,
          'trackLogToShowOnly',
        ) && (
          <PolylineWithStartPoint
            coordinates={trackLogToShowOnly.trackLogArray}
            key={trackLogToShowOnly.trackLogId}
            strokeColor={
              trackLogToShowOnly.sessionId
                ? hashStringToHexColor({
                    string: trackLogToShowOnly.sessionId,
                    alpha: 'ff',
                  })
                : 'white'
            }
          />
        )}
      {type === 'line' &&
        showInputGeometryValueAsArray &&
        isValidCoordinateArray(
          inputGeometryValue,
          'inputGeometryValueLine',
        ) && (
          <Polyline
            coordinates={inputGeometryValue}
            strokeWidth={4}
            strokeColor="white"
          />
        )}
      {type === 'polygon' &&
        showInputGeometryValueAsArray &&
        isValidCoordinateArray(
          inputGeometryValue,
          'inputGeometryValuePolygon',
        ) && (
          <Polygon
            coordinates={inputGeometryValue}
            strokeWidth={4}
            strokeColor="white"
          />
        )}
      {editMode && showInputGeometryValueStartPoint && (
        <Marker
          coordinate={inputGeometryValue[0]}
          key={`${inputGeometryValue[0].latitude}${inputGeometryValue[0].longitude}`}
          anchor={{ x: 0.5, y: 0.5 }}
        >
          <View style={styles.circle} />
        </Marker>
      )}
      {!gpsError &&
        currentTrackLog?.trackLogArray.length !== 0 &&
        showOnlyOneTrackLog &&
        isValidCoordinateArray(
          currentTrackLog?.trackLogArray,
          'currentTrackLog',
        ) && (
          <PolylineWithStartPoint
            coordinates={currentTrackLog.trackLogArray}
            key="currentTrackLog"
            strokeColor={Colors.orange}
          />
        )}
      {!gpsError &&
        isValidCoordinateArray(
          currentSessionTrackLog?.trackLogArray,
          'currentSessionTrackLog',
        ) && (
          <PolylineWithStartPoint
            coordinates={currentSessionTrackLog.trackLogArray.filter(
              (coord) => coord,
            )}
            key="currentSessionTrackLog"
            strokeColor={
              currentSessionTrackLog.sessionId
                ? hashStringToHexColor({
                    string: currentSessionTrackLog.sessionId,
                    alpha: 'ff',
                  })
                : Colors.blue
            }
          />
        )}
      {showPernamentSamplePlots &&
        !!permanentSamplePlots?.length &&
        permanentSamplePlots.map(
          (plot) =>
            !!plot.geoJson && (
              <Geojson
                geojson={plot.geoJson}
                key={plot.id}
                fillColor="rgba(255,0,0,0.3)"
                strokeColor="rgba(255,0,0,0.5)"
                strokeWidth={4}
              />
            ),
        )}
      {!gpsError &&
        showPernamentSamplePlots &&
        !!permanentSamplePlots?.length &&
        permanentSamplePlots.map(
          (plot) =>
            !!plot.labelCoords && (
              <Marker
                coordinate={{
                  latitude: plot.labelCoords.latitude,
                  longitude: plot.labelCoords.longitude,
                }}
                key={`label-${plot.id}`}
              >
                <Text style={styles.labelText}>{plot.name}</Text>
              </Marker>
            ),
        )}
      {!gpsError && type === 'point' && isInputGeometryValuePoint && (
        <Marker
          coordinate={inputGeometryValue}
          anchor={{ x: 0.5, y: 0.9 }}
          ref={inputRef}
        >
          <View style={styles.marker}>
            <Icon name="place" size={40} color="white" />
          </View>
        </Marker>
      )}
    </MapView>
  );
};

MapComponent.propTypes = {
  provider: PropTypes.string.isRequired,
  permanentSamplePlots: PropTypes.arrayOf(PropTypes.shape({})),
  lastMeasurements: PropTypes.arrayOf(PropTypes.shape({})),
  region: PropTypes.shape({
    longitudeDelta: PropTypes.number,
  }),
  onRegionChange: PropTypes.func,
  onMeasurementPress: PropTypes.func,
  position: PropTypes.shape({
    longitude: PropTypes.number.isRequired,
    latitude: PropTypes.number.isRequired,
  }),
  mapRef: PropTypes.func,
  onLongPress: PropTypes.func,
  inputGeometryValue: PropTypes.oneOfType([
    PropTypes.shape({
      longitude: PropTypes.number.isRequired,
      latitude: PropTypes.number.isRequired,
    }),
    PropTypes.arrayOf(
      PropTypes.shape({
        longitude: PropTypes.number.isRequired,
        latitude: PropTypes.number.isRequired,
      }),
    ),
  ]),
  inputRef: PropTypes.func,
  liteMode: PropTypes.bool,
  style: PropTypes.shape({}),
  showTrackLogs: PropTypes.bool,
  showMeasurements: PropTypes.bool,
  trackLogList: PropTypes.array,
  trackLogToShowOnly: PropTypes.shape({
    trackLogArray: PropTypes.arrayOf(
      PropTypes.shape({
        longitude: PropTypes.number.isRequired,
        latitude: PropTypes.number.isRequired,
      }),
    ),
    trackLogId: PropTypes.string,
    sessionId: PropTypes.string,
  }),
  type: PropTypes.string,
  showOnlyOneTrackLog: PropTypes.bool,
  showPernamentSamplePlots: PropTypes.bool,
  currentTrackLog: PropTypes.shape({
    trackLogArray: PropTypes.arrayOf(
      PropTypes.shape({
        longitude: PropTypes.number.isRequired,
        latitude: PropTypes.number.isRequired,
      }),
    ),
  }),
  currentSessionTrackLog: PropTypes.shape({
    trackLogArray: PropTypes.arrayOf(
      PropTypes.shape({
        longitude: PropTypes.number.isRequired,
        latitude: PropTypes.number.isRequired,
      }),
    ),
    sessionId: PropTypes.string,
  }),
  editMode: PropTypes.bool,
};

MapComponent.defaultProps = {
  permanentSamplePlots: null,
  onRegionChange: null,
  onMeasurementPress: null,
  position: null,
  lastMeasurements: null,
  liteMode: false,
  style: null,
  mapRef: null,
  onLongPress: null,
  inputGeometryValue: [],
  inputRef: null,
  region: null,
  showTrackLogs: false,
  showMeasurements: true,
  trackLogToShowOnly: null,
  type: 'point',
  currentTrackLog: null,
  trackLogList: null,
  showOnlyOneTrackLog: false,
  showPernamentSamplePlots: true,
  currentSessionTrackLog: null,
  editMode: false,
};

export default MapComponent;

import React from 'react';
import * as Animatable from 'react-native-animatable';
import PropTypes from 'prop-types';
import { Text } from 'react-native';
import { Colors } from '../../colors';
import styles from './styles';

const Notification = ({ notification, animation, backgroundColor, notificationRef, component }) => (
  <Animatable.View
    ref={(ref) => { if (notificationRef) notificationRef(ref); }}
    style={[styles.baseContainerStyle, backgroundColor && { backgroundColor }]}
    animation={!notificationRef ? animation || 'bounceIn' : null}
    useNativeDriver
  >
    <Text style={styles.textStyle}>{notification}</Text>
    {component}
  </Animatable.View>
);

Notification.propTypes = {
  notification: PropTypes.string.isRequired,
  animation: PropTypes.string,
  backgroundColor: PropTypes.string,
  notificationRef: PropTypes.func,
  component: PropTypes.object,
};

Notification.defaultProps = {
  animation: 'bounceIn',
  backgroundColor: Colors.orange,
  notificationRef: null,
  component: null,
};

export default Notification;

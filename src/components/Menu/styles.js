import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,.3)',
    justifyContent: 'flex-end',
  },
  closeArea: {
    flex: 1,
  },
  menuContainer: {
    backgroundColor: 'white',
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    paddingBottom:20
  },
  header: {
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderBottomColor: '#e5e5e5',
    borderBottomWidth: 1,
  },
  nameText: {
    color: 'black',
    fontSize: 20,
  },
  row: {
    flexDirection: 'row',
    paddingVertical: 5,
    marginVertical: 8,
    paddingHorizontal: 20,
    alignItems: 'center',
  },
  menuItemText: {
    marginHorizontal: 20,
    fontSize: 15,
  },
});

import React, { Component } from 'react';
import { View, Text, Modal, TouchableWithoutFeedback, Linking, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from '../../actions';
import I18n from '../../i18n';
import styles from './styles';
import { getSelectedServer, getSelectedDatabase, getUserState } from '../../utils';
import { TouchableIcon } from '../TouchableIcon';
import { Colors } from '../../colors';
import NavigationHandler from '../../NavigationHandler';
import { showAlertDialog } from '../AlertDialog';

class Menu extends Component {
  onSelectServerPress = () => {
    const { onClose, resetServerSelection, resetDatabaseSelection } = this.props;

    onClose();

    // The reset order is crucial! Always the database selection should be reset first!
    resetDatabaseSelection();
    resetServerSelection();
    NavigationHandler.navigate('LastServersScreen');
  }

  onSelectDatabasePress = () => {
    const { server, onClose, resetDatabaseSelection } = this.props;
    
    onClose();
    resetDatabaseSelection();
    NavigationHandler.navigate(
      'DatabaseList',
      { title: server.name },
      null,
      NavigationHandler.navigate('LastDatabasesScreen'),
    );
  }

  onOnlineSettingsPress = () => {
    const { onClose, database: { settingsUrl } } = this.props;

    onClose();
    Linking.canOpenURL(settingsUrl).then((supported) => {
      if (supported) {
        Linking.openURL(settingsUrl);
      } else {
        console.log(`Don't know how to open URI: ${settingsUrl}`);
      }
    });
  }

  onLogoutPress = (server) => {
    this.props.onClose();

    if (this.hasUnsyncedMeasurements(server)) {
      showAlertDialog({
        title: I18n.t('logout_dialog_title'),
        message: I18n.t('logout_dialog_message'),
        showCancelButton: true,
        positiveLabel: I18n.t('bool_true'),
        negativeLabel: I18n.t('bool_false'),
        onPositiveBtnPress: () => this.logout(server),
      });
    } else {
      this.logout(server);
    }
  }

  logout = (server) => {
    const { resetDatabaseSelection, resetServerSelection, logout } = this.props;

    // The reset order is crucial! Always the database selection should be reset first!
    resetDatabaseSelection();
    resetServerSelection();
    logout(server.id);

    NavigationHandler.navigate('Home');
    showAlertDialog({
      title: I18n.t('logout_success'),
      message: I18n.t('logout_success_from', { server: server.name }),
    });
  }

  hasUnsyncedMeasurements = (server) => {
    let hasUnsyncedMeasurements = false;

    if (server.databases && server.databases.data.length) {
      server.databases.data.forEach((database) => {
        if (database.observations && database.observations.data.length && !hasUnsyncedMeasurements) {
          database.observations.data.forEach((observation) => {
            if (observation.measurements && observation.measurements.data.length && !hasUnsyncedMeasurements) {
              observation.measurements.data.forEach((measurement) => {
                if (!measurement.isSynced || measurement.errors) {
                  hasUnsyncedMeasurements = true;
                }
              });
            }
          });
        }
      });
    }

    return hasUnsyncedMeasurements;
  }

  render() {
    const {
      server,
      visible,
      onClose,
      userName,
      showProjectBtn,
    } = this.props;

    return (
      <Modal 
        animationType="fade" 
        presentationStyle="overFullScreen" 
        transparent 
        visible={visible} 
        onRequestClose={onClose}
      >
        <View style={styles.container}>
          <TouchableWithoutFeedback onPress={onClose}>
            <View style={styles.closeArea} />
          </TouchableWithoutFeedback>
          <View style={styles.menuContainer}>
            <View style={styles.header}>
              <Text style={styles.nameText}>{server.name}</Text>
              <Text>{userName}</Text>
            </View>
            <TouchableOpacity style={styles.row} onPress={this.onSelectServerPress}>
              <TouchableIcon iconType="MaterialCommunityIcon" name="server" size={20} color={Colors.darkGray} disabled />
              <Text style={styles.menuItemText}>{I18n.t('change_server')}</Text>
            </TouchableOpacity>

            {showProjectBtn && (
              <TouchableOpacity style={styles.row} onPress={this.onSelectDatabasePress}>
                <TouchableIcon name="format-list-numbered" size={20} color={Colors.darkGray} disabled />
                <Text style={styles.menuItemText}>{I18n.t('change_project')}</Text>
              </TouchableOpacity>
            )}
            {showProjectBtn && (
              <TouchableOpacity style={styles.row} onPress={this.onOnlineSettingsPress}>
                <TouchableIcon name="settings" size={20} color={Colors.darkGray} disabled />
                <Text style={styles.menuItemText}>{I18n.t('settings_online_profile')}</Text>
              </TouchableOpacity>
            )}

            <TouchableOpacity style={styles.row} onPress={() => this.onLogoutPress(server)}>
              <TouchableIcon iconType="MaterialCommunityIcon" name="logout" size={20} color={Colors.darkGray} disabled />
              <Text style={styles.menuItemText}>{I18n.t('logout')}</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  }
}

Menu.propTypes = {
  server: PropTypes.object.isRequired,
  database: PropTypes.object,
  userName: PropTypes.string,
  visible: PropTypes.bool,
  onClose: PropTypes.func.isRequired,
  resetServerSelection: PropTypes.func.isRequired,
  resetDatabaseSelection: PropTypes.func.isRequired,
  logout: PropTypes.func.isRequired,
  showProjectBtn: PropTypes.bool,
};

Menu.defaultProps = {
  database: {},
  userName: '',
  visible: false,
  showProjectBtn: false,
};

const mapStateToProps = ({ servers: { data: serverList } }) => {
  const selectedServer = getSelectedServer(serverList);
  const { userName } = getUserState(selectedServer);
  const selectedDatabase = getSelectedDatabase(selectedServer);

  return ({
    server: selectedServer,
    database: selectedDatabase,
    userName,
  });
};

const mapDispatchToProps = (dispatch) => {
  const { resetServerSelection, resetDatabaseSelection, logout } = actions;

  return bindActionCreators(
    {
      resetServerSelection,
      resetDatabaseSelection,
      logout,
    },
    dispatch,
  );
};

export default connect(
  mapStateToProps, 
  mapDispatchToProps,
)(Menu);

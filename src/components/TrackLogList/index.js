import React from 'react';
import { View, FlatList, Text } from 'react-native';

import PropTypes from 'prop-types';

import I18n from '../../i18n';

import TrackLogListItem from './TrackLogListItem';
import styles from './styles';

const keyExtractor = (item) => String(`${item.trackLogId}`);

const showEmptyList = (text) => (
  <View style={styles.emptyListContainer}>
    <Text>{text}</Text>
  </View>
);

const TrackLogList = ({ trackLogList, type, onDeletePress, onViewMapPress, onDownloadPress }) => {
  if (type === 'unsynced' && !trackLogList.length) return showEmptyList(I18n.t('no_unsynced_data'));
  if (type === 'synced' && !trackLogList.length) return showEmptyList(I18n.t('no_synced_data'));

  return (
    <FlatList
      keyExtractor={keyExtractor}
      data={trackLogList}
      initialNumToRender={20}
      windowSize={20}
      renderItem={({ item: { trackLogId, trackLogName, selectedDatabases, sessionId, trackLogArray } }) => (
        <TrackLogListItem
          trackLogId={trackLogId}
          trackLogName={trackLogName} 
          selectedDatabases={selectedDatabases}
          trackLogLength={trackLogArray.length}
          sessionId={sessionId}
          onDeletePress={onDeletePress}
          onDownloadPress={onDownloadPress}
          onViewMapPress={onViewMapPress}
        />
      )}
    />
  );
};

TrackLogList.propTypes = {
  trackLogList: PropTypes.arrayOf(PropTypes.object).isRequired,
  type: PropTypes.oneOf(['synced', 'unsynced']),
  onDeletePress: PropTypes.func.isRequired,
  onViewMapPress: PropTypes.func.isRequired,
  onDownloadPress: PropTypes.func.isRequired,
};

TrackLogList.defaultProps = {
  type: 'unsynced',
};

export default TrackLogList;

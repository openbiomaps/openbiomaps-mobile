import { StyleSheet } from 'react-native';
import { Colors } from '../../colors';

export default StyleSheet.create({
  blockerContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    width: '50%',
    height: '10%',
    backgroundColor: Colors.mantisGreen,  
  },
  blockerText: {
    padding: 10,
    marginBottom: 20,
    textAlign: 'center',
    fontSize: 20, 
    color: 'black',
  },
});

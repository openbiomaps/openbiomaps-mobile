import React, { Fragment } from 'react';
import { Text, View } from 'react-native';
import PropTypes from 'prop-types';
import moment from 'moment';
import I18n from '../../i18n';
import styles from './styles';
import { TouchableIcon } from '../TouchableIcon';
import { parseValueToString } from '../../utils';

// eslint-disable-next-line arrow-body-style
const arrangeBoldYellow = (results) => {
  const pieces = [];
  const dateFirstConcatenatedString = (
    <Text style={{ fontStyle: 'italic' }}>{`${
      results.find((result) => result.id === 'createdAt').value
    } - `}</Text>
  );
  pieces.push(
    results
      .reduce(
        (acc, attr) =>
          attr.id !== 'createdAt' ? `${acc}${attr.value || '*'}, ` : `${acc}`,
        '',
      )
      .slice(0, -2),
  );
  return (
    <Text>
      <Text>{dateFirstConcatenatedString}</Text>
      <Text style={{ fontWeight: 'bold' }}>{pieces}</Text>
    </Text>
  );
};

const getData = (dataTypes, values, boldYellow) => {
  const result = [];
  if (values.isNullRecord) {
    return [
      {
        name: I18n.t('date'),
        id: 'createdAt',
        value: moment(values.createdAt).format('YYYY.MM.DD HH:mm'),
        type: 'datetime',
      },
    ];
  }

  const date = {
    name: I18n.t('date'),
    id: 'createdAt',
    value: moment(values.createdAt).format('YYYY.MM.DD HH:mm'),
    type: 'datetime',
  };
  result.push(date);

  if (boldYellow.length) {
    boldYellow.forEach((column) => {
      const value =
        (Array.isArray(values[column])
          ? values[column].length
          : values[column]) || '-';
      const data = dataTypes.find((dataType) => dataType.column === column);

      // Handle cases when columns provided in the boldyellow
      // are not available in the datatypes.
      if (data) {
        const parsedValue = parseValueToString(value, data);
        result.push({
          name: data.shortName,
          id: data.column,
          value: parsedValue,
          type: data.type,
        });
      }
    });
  } else {
    const data = dataTypes[0];
    const value = values[data.column] || I18n.t('no_field_data');
    const parsedValue = parseValueToString(value, data);
    result.push({
      name: data.shortName,
      id: data.column,
      value: parsedValue,
      type: data.type,
    });
  }

  return result;
};

const MeasurementInfo = ({
  dataTypes,
  values,
  databaseName,
  observationName,
  boldYellow,
  editable,
  numInd,
  onEditPress,
  onDeletePress,
  onSchemaPress,
  onIncreaseNumInd,
  onDecreaseNumInd,
}) => (
  <Fragment>
    <View style={styles.topContainer}>
      <View style={styles.textVContainer}>
        <View>
          {databaseName !== '' && (
            <Fragment>
              <Text
                adjustsFontSizeToFit={true}
                style={styles.title}
              >{`${databaseName} ${
                values.isNullRecord ? `(${I18n.t('null_record')})` : ''
              }`}</Text>
            </Fragment>
          )}
          {observationName !== '' && (
            <Fragment>
              <Text style={{ marginTop: -10 }}>Form: {observationName}</Text>
            </Fragment>
          )}
          {databaseName === '' && values.isNullRecord && (
            <Fragment>
              <Text>{`${
                values.isNullRecord ? `(${I18n.t('null_record')})` : ''
              }`}</Text>
            </Fragment>
          )}
          <Text style={styles.details}>
            {arrangeBoldYellow(getData(dataTypes, values, boldYellow))}
          </Text>
          <View style={styles.buttonContainer}>
            {editable && numInd !== '' && values[numInd] !== undefined && (
              <Fragment>
                <TouchableIcon
                  style={styles.icon}
                  name="add"
                  size={22}
                  onPress={onIncreaseNumInd}
                />
                <TouchableIcon
                  style={styles.icon}
                  name="remove"
                  size={22}
                  onPress={onDecreaseNumInd}
                  color={values[numInd] > 0 ? 'black' : 'grey'}
                  disabled={values[numInd] <= 0}
                />
              </Fragment>
            )}
            {!values.isNullRecord && (
              <Fragment>
                {editable && (
                  <TouchableIcon
                    style={styles.icon}
                    name="edit"
                    size={22}
                    width={30}
                    onPress={onEditPress}
                  />
                )}
                <TouchableIcon
                  style={styles.icon}
                  name="content-copy"
                  size={22}
                  onPress={onSchemaPress}
                />
              </Fragment>
            )}
            <TouchableIcon
              style={styles.icon}
              name="delete"
              size={22}
              onPress={onDeletePress}
            />
          </View>
        </View>
      </View>
    </View>
  </Fragment>
);

MeasurementInfo.propTypes = {
  dataTypes: PropTypes.array.isRequired,
  values: PropTypes.object.isRequired,
  databaseName: PropTypes.string.isRequired,
  observationName: PropTypes.string,
  boldYellow: PropTypes.array.isRequired,
  editable: PropTypes.bool,
  numInd: PropTypes.string,
  onEditPress: PropTypes.func,
  onDeletePress: PropTypes.func.isRequired,
  onSchemaPress: PropTypes.func.isRequired,
  onIncreaseNumInd: PropTypes.func,
  onDecreaseNumInd: PropTypes.func,
};

MeasurementInfo.defaultProps = {
  editable: false,
  numInd: null,
  onEditPress: () => {},
  onIncreaseNumInd: () => {},
  onDecreaseNumInd: () => {},
};

export default MeasurementInfo;

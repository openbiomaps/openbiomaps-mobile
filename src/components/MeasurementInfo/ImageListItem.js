import React from 'react';
import { View, Image } from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';

const ImageListItem = ({ uri }) => (
  <View style={styles.imageContainer}>
    <Image style={styles.image} source={{ uri }} />
  </View>
);

ImageListItem.propTypes = {
  uri: PropTypes.string.isRequired,
};

export default ImageListItem;

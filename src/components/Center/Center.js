import React from 'react';
import { View } from 'react-native';

import PropTypes from 'prop-types';

import styles from './styles';

const Center = ({ children, style, flexDirection }) => (
  <View style={[styles.baseContainer, { flexDirection }, style]}>
    {children}
  </View>
);

Center.propTypes = {
  children: PropTypes.node.isRequired,
  style: PropTypes.object,
  flexDirection: PropTypes.oneOf(['column', 'row']),
};

Center.defaultProps = {
  style: {},
  flexDirection: 'column',
};

export default Center;

import React from 'react';
import { View, TouchableOpacity, Text } from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';

const ListAsButtons = ({ data, value, onItemSelect }) => (
  <View style={styles.baseContainer}>
    {data.map((item, index) => (
      <TouchableOpacity
        style={[
          styles.item,
          value === item.value && styles.selectedItem,
          index > 1 && styles.padding,
        ]}
        onPress={() => onItemSelect(item.key.toString())}
        key={item.key}
      >
        <Text style={styles.text} numberOfLines={1}>{item.value}</Text>
      </TouchableOpacity>
    ))}
  </View>
);

ListAsButtons.propTypes = {
  data: PropTypes.arrayOf(PropTypes.object).isRequired,
  onItemSelect: PropTypes.func.isRequired,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};

ListAsButtons.defaultProps = {
  value: null,
};

export { ListAsButtons };

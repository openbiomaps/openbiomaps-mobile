import { StyleSheet } from 'react-native';
import { SCREEN_WIDTH } from '../../dimens';
import { Colors } from '../../colors';

export default StyleSheet.create({
  baseContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    flex: 1,
    justifyContent: 'space-evenly',
  },
  item: {
    width: SCREEN_WIDTH * 0.45,
    height: 50,
    backgroundColor: Colors.silver,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    paddingHorizontal: 12,
  },
  selectedItem: {
    backgroundColor: Colors.mantisGreen,
  },
  padding: {
    marginTop: 8,
  },
  text: {
    color: 'white',
    fontSize: 16,  
    fontWeight: '600',
  },
});

import React from 'react';
import MaterialTextInput from 'react-native-material-textinput';
import { Colors } from '../../colors';

const styleProps = {
  marginBottom: 0,
  underlineHeight: 2,
  labelActiveColor: Colors.mantisGreen,
  underlineActiveColor: Colors.mantisGreen,
  paddingLeft: 15,
  paddingRight: 15,
  paddingTop: 25,
  labelColor: '#222',
  fontSize: 18,
  labelActiveTop: -28,
};

const TextInput = props => <MaterialTextInput {...styleProps} {...props} />;

export { TextInput };


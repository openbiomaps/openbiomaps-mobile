import * as Sentry from '@sentry/react-native';
import React, { Component } from 'react';
import { View, BackHandler, Text } from 'react-native';
import PropTypes from 'prop-types';
import { Spinner } from '../Spinner';
import EmptyList from '../EmptyList';
import { SelectorList } from '../SelectorList';
import NavigationHandler from '../../NavigationHandler';
import Menu from '../Menu';
import NotificationHandler from '../NotificationHandler';
import {
  NotificationModel,
  NOTIFICATION_TYPES,
} from '../../models/NotificationModel';
import { ERROR } from '../../Constants';
import { connect } from 'react-redux';

class ObservationList extends Component {
  // eslint-disable-next-line
  _willFocusSubscription;

  _willBlurSubscription;

  _didParentFocusSubscription;

  _willParentBlurSubscription;

  constructor(props) {
    super(props);

    this.state = {
      showMenu: false,
      initLoading: true,
    };
    this.notificationHandler = null;
    this.backPressListener = null;
  }

  componentDidMount = () => {
    const { navigation } = this.props;
    const {
      state: { routeName },
    } = navigation;
    const parentNavigator = navigation.dangerouslyGetParent();

    if (parentNavigator) {
      parentNavigator.setParams({
        toggleMenu: this.toggleMenu,
      });
      // Fetch data only when parent navigator gets in focus.
      this._didParentFocusSubscription = parentNavigator.addListener(
        'didFocus',
        () => {
          if (
            routeName === 'ObservationListScreen' ||
            routeName === 'ObservationSelectScreen'
          )
            this.loadObservations();
          this.setState({ initLoading: false });
        },
      );

      this._willParentBlurSubscription = parentNavigator.addListener(
        'willBlur',
        () => {
          this.setState({ initLoading: true });
        },
      );
    } else {
      Sentry.captureMessage(
        'ParentNavigator is null in ObservationList. Should not be null',
      );
    }

    // Force update component when it is in focus to evoke render() for updateing changed props.
    // forceUpdate() calls render() by skipping shouldComponentUpdate()
    this._willFocusSubscription = navigation.addListener('willFocus', () => {
      this.forceUpdate();
      this.backPressListener = BackHandler.addEventListener(
        'hardwareBackPress',
        this.onAndroidBackPress,
      );
    });

    this._willBlurSubscription = navigation.addListener('willBlur', () =>
      this.backPressListener?.remove(),
    );
  };

  shouldComponentUpdate = (nextProps, nextState) => {
    const { fetching, error } = this.props;
    const { showMenu, initLoading } = this.state;

    // Render component only when the feching state changes to prevent rerender every time
    // when a deeply nested property changes in the hierarchy tree.
    if (fetching !== nextProps.fetching) {
      return true;
    }

    // Rerender component when showMenu state changes
    if (showMenu !== nextState.showMenu) {
      return true;
    }

    // Rerender component when initLoading state changes
    if (initLoading !== nextState.initLoading) {
      return true;
    }

    if (error !== nextProps.error) {
      return true;
    }

    return false;
  };

  componentDidUpdate = () => {
    if (this.notificationHandler) {
      const { error } = this.props;
      if (error) {
        if (error === ERROR.INVALID_REFRESH_TOKEN) {
          this.notificationHandler.showNotification(
            NotificationModel.build(
              NOTIFICATION_TYPES.LOST_COMMUNICATION_WITH_THE_SERVER,
            ),
          );
        } else {
          this.notificationHandler.showNotification(
            NotificationModel.build(
              NOTIFICATION_TYPES.UNKNOWN_ERROR,
              <Text style={{ textAlign: 'center' }}>{error}</Text>,
            ),
          );
        }
      } else {
        this.notificationHandler.hideNotification(
          NotificationModel.build(
            NOTIFICATION_TYPES.LOST_COMMUNICATION_WITH_THE_SERVER,
          ),
        );
        this.notificationHandler.hideNotification(
          NotificationModel.build(NOTIFICATION_TYPES.UNKNOWN_ERROR),
        );
      }
    }
  };

  componentWillUnmount() {
    if (this._willFocusSubscription) this._willFocusSubscription.remove();
    if (this._willBlurSubscription) this._willBlurSubscription.remove();
    if (this._didParentFocusSubscription)
      this._didParentFocusSubscription.remove();
    if (this._willParentBlurSubscription)
      this._willParentBlurSubscription.remove();
  }

  onAndroidBackPress = () => {
    NavigationHandler.navigate('Home');
    return true;
  };

  toggleMenu = () => {
    this.setState((prevState) => ({ showMenu: !prevState.showMenu }));
  };

  selectObservation = (observation) => {
    const {
      selectObservation,
      selectedServer: { id: serverId },
      selectedDatabase: { id: databaseId, name: databaseName },
      shortcutObservations,
    } = this.props;

    selectObservation(serverId, databaseId, observation.id);

    const shortcutObservationIds = [];

    shortcutObservations.map((shortcut) =>
      shortcutObservationIds.push(shortcut['observation']['id']),
    );

    if (observation.game && !observation.game.passedQuiz) {
      NavigationHandler.navigate('QuizInformation', {
        title: observation.game.title,
      });
    } else {
      NavigationHandler.navigate('CreateMeasurement', {
        title: observation.name,
        description: databaseName,
        pinned: shortcutObservationIds.includes(observation.id),
      });
    }
  };

  createShortCut = (observation) => {
    const {
      createShortcut,
      selectedServer: { id: serverId, name: serverName },
      selectedDatabase: {
        id: databaseId,
        name: databaseName,
        game: databaseGame,
      },
    } = this.props;

    createShortcut(
      { serverId, serverName },
      { databaseId, databaseName, databaseGame },
      observation,
    );
  };

  loadObservations = () => {
    const { loadObservations, selectedServer, selectedDatabase } = this.props;
    loadObservations(selectedServer, selectedDatabase);
  };

  renderContent = () => {
    const { observationList, fetching, optionNeeded } = this.props;
    const { initLoading } = this.state;

    if (fetching || initLoading) return <Spinner />;
    if (!observationList.length)
      return <EmptyList onRefreshPress={this.loadObservations} />;

    return (
      <View>
        <NotificationHandler
          ref={(ref) => {
            this.notificationHandler = ref;
          }}
        />
        <SelectorList
          data={observationList}
          onPress={this.selectObservation}
          isOptionNeeded={optionNeeded}
          onOptionPositivePress={this.createShortCut}
        />
      </View>
    );
  };

  render() {
    const { navigation } = this.props;
    const { showMenu } = this.state;
    return (
      <View style={{ flex: 1 }}>
        {this.renderContent()}
        <Menu
          navigation={navigation}
          visible={showMenu}
          onClose={this.toggleMenu}
          showProjectBtn
        />
      </View>
    );
  }
}

ObservationList.propTypes = {
  loadObservations: PropTypes.func.isRequired,
  selectObservation: PropTypes.func.isRequired,
  createShortcut: PropTypes.func,
  observationList: PropTypes.array.isRequired,
  navigation: PropTypes.object.isRequired,
  selectedServer: PropTypes.object.isRequired,
  selectedDatabase: PropTypes.object.isRequired,
  fetching: PropTypes.bool,
  optionNeeded: PropTypes.bool,
  error: PropTypes.string,
};

ObservationList.defaultProps = {
  fetching: false,
  optionNeeded: false,
  error: null,
  createShortcut: () => {},
};

const mapStateToProps = ({ shortcutObservations }) => {
  return {
    shortcutObservations,
  };
};

export default connect(mapStateToProps)(ObservationList);

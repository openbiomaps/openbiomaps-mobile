import * as Sentry from '@sentry/react-native';
import React, { Component } from 'react';
import { View, BackHandler } from 'react-native';
import PropTypes from 'prop-types';
import { Spinner } from '../Spinner';
import EmptyList from '../EmptyList';
import { SelectorList } from '../SelectorList';
import NavigationHandler from '../../NavigationHandler';
import Menu from '../Menu';

class DatabaseList extends Component {
  // eslint-disable-next-line
  _willFocusSubscription;

  _willBlurSubscription;

  _didParentFocusSubscription;

  _willParentBlurSubscription;

  constructor(props) {
    super(props);

    this.state = {
      showMenu: false,
      initLoading: true,
    };

    this.backPressListener = null;
  }

  componentDidMount = () => {
    const { navigation } = this.props;
    const { state: { routeName } } = navigation;
    const parentNavigator = navigation.dangerouslyGetParent();

    if (parentNavigator) {
      parentNavigator.setParams({
        toggleMenu: this.toggleMenu,
      });
  
      // Fetch data only when parent navigator gets in focus.
      this._didParentFocusSubscription = parentNavigator.addListener('didFocus', () => {
        if (routeName === 'DatabaseListScreen') this.loadDatabases();
        this.setState({ initLoading: false });
      });
  
      this._willParentBlurSubscription = parentNavigator.addListener('willBlur', () => {
        this.setState({ initLoading: true });
      });
    } else {
      Sentry.captureMessage('ParentNavigator is null in DatabaseList. Should not be null');
    }
    
    // Force update component when it is in focus to evoke render() for updateing changed props.
    // forceUpdate() calls render() by skipping shouldComponentUpdate()
    this._willFocusSubscription = navigation.addListener('willFocus', () => {
      this.forceUpdate();
      this.backPressListener = BackHandler.addEventListener('hardwareBackPress', this.onAndroidBackPress);
    });

    this._willBlurSubscription = navigation.addListener('willBlur', () => this.backPressListener?.remove());
  };

  shouldComponentUpdate = (nextProps, nextState) => {
    const { fetching } = this.props;
    const { showMenu, initLoading } = this.state;

    // Render component only when the feching state changes to prevent rerender every time
    // when a deeply nested property changes in the hierarchy tree.
    if (fetching !== nextProps.fetching) {
      return true;
    }

    // Rerender component when showMenu state changes
    if (showMenu !== nextState.showMenu) {
      return true;
    }

    // Rerender component when initLoading state changes
    if (initLoading !== nextState.initLoading) {
      return true;
    }

    return false;
  };

  componentWillUnmount() {
    if (this._willFocusSubscription) this._willFocusSubscription.remove();
    if (this._willBlurSubscription) this._willBlurSubscription.remove();
    if (this._didParentFocusSubscription) this._didParentFocusSubscription.remove();
    if (this._willParentBlurSubscription) this._willParentBlurSubscription.remove();
  }

  onAndroidBackPress = () => {
    NavigationHandler.navigate('Home');
    return true;
  };

  toggleMenu = () => {
    this.setState((prevState) => ({ showMenu: !prevState.showMenu }));
  }

  selectDatabase = (database) => {
    const { observations = {} } = database;
    const { data: observationList = [] } = observations;
    const { selectDatabase, selectedServer } = this.props;

    selectDatabase(selectedServer.id, database.id);

    if (database.game === 'on') {
      NavigationHandler.navigate('ObservationGame', { title: database.name });
    } else if (observationList.some((observation) => observation.lastSelected && observation.form?.dataTypes?.length)) {
      NavigationHandler.navigate(
        'ObservationList',
        { title: database.name },
        null,
        NavigationHandler.navigate('LastObservationsScreen'),
      );
    } else {
      NavigationHandler.navigate(
        'ObservationList',
        { title: database.name },
        null,
        NavigationHandler.navigate('ObservationListScreen'),
      );
    }
  };

  loadDatabases = () => {
    const { loadDatabases, selectedServer } = this.props;
    loadDatabases(selectedServer);
  }

  renderContent = () => {
    const { databaseList, fetching } = this.props;
    const { initLoading } = this.state;

    if (fetching || initLoading) return <Spinner />;
    if (!databaseList.length) return <EmptyList onRefreshPress={this.loadDatabases} />;

    return <SelectorList data={databaseList} onPress={this.selectDatabase} />;
  }

  render() {
    const { navigation } = this.props;
    const { showMenu } = this.state;
    return (
      <View style={{ flex: 1 }}>
        {this.renderContent()}
        <Menu
          navigation={navigation}
          visible={showMenu}
          onClose={this.toggleMenu}
        />
      </View>
    );
  }
}

DatabaseList.propTypes = {
  loadDatabases: PropTypes.func.isRequired,
  databaseList: PropTypes.array.isRequired,
  selectDatabase: PropTypes.func.isRequired,
  navigation: PropTypes.object.isRequired,
  selectedServer: PropTypes.object.isRequired,
  fetching: PropTypes.bool,
};

DatabaseList.defaultProps = {
  fetching: false,
};

export default DatabaseList;

import React, { Component, Fragment } from 'react';
import { TouchableOpacity, Text, Platform } from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import moment from 'moment';
import * as actions from '../../actions';
import { SettingsWithPickerStyle as styles } from '../SettingsItem/styles';
import PickerModal from '../PickerModal';
import { showAlertDialog } from '../AlertDialog';
import I18n from '../../i18n';

class BackupSettingsItem extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showModal: false,
    };
  }

  handleRowPress = () => {
    const { backups } = this.props;

    if (backups.length) {
      this.setState({ showModal: true });
    } else {
      this.handleItemSelect({
        key: 'error',
        value: 'no_data',
      });
    }
  };

  handleItemSelect = ({ key, value }) => {
    const { restoreBackup, backups, pickAndRestoreExternalBackup } = this.props;

    if (key === 'error' && value === 'no_data') {
      showAlertDialog({
        title: I18n.t('restore_backup'),
        message: I18n.t('no_backup'),
        showCancelButton: true,
        onPositiveBtnPress: pickAndRestoreExternalBackup,
        positiveLabel: I18n.t('search_backup_manually'),
      });
    } else if (key === 'import') {
      showAlertDialog({
        title: I18n.t('choose_backup_from_filesystem_title'),
        message: I18n.t('choose_backup_from_filesystem_message'),
        positiveLabel: I18n.t('choose_backup_from_filesystem_positive'),
        onPositiveBtnPress: pickAndRestoreExternalBackup,
        showCancelButton: true,
      });
    } else {
      const backupId = backups[key];
      showAlertDialog({
        title: I18n.t('backup_confirm_title'),
        message: value,
        positiveLabel: I18n.t('restore'),
        onPositiveBtnPress: () => restoreBackup(backupId),
        showCancelButton: true,
      });
    }
  };

  importExternalBackup = {
    key: 'import',
    value: I18n.t('choose_backup_from_filesystem'),
  };

  render() {
    const { title, value, backups } = this.props;
    const { showModal } = this.state;

    return (
      <>
        <TouchableOpacity
          style={styles.baseContainer}
          onPress={this.handleRowPress}
          activeOpacity={1}
        >
          <Text style={styles.title}>{title}</Text>
          {value && <Text style={styles.value}>{value}</Text>}
        </TouchableOpacity>

        <PickerModal
          showModal={showModal}
          onRequestClose={() => this.setState({ showModal: !showModal })}
          data={[...backups.map((item, index) => ({
            key: index,
            value: `Backup ${index + 1} - ${moment.unix(item).format('YYYY-MM-DD, HH:mm')}`,
          })), this.importExternalBackup]}
          onItemSelect={(item) => this.handleItemSelect(item)}
        />
      </>
    );
  }
}

BackupSettingsItem.propTypes = {
  title: PropTypes.string.isRequired,
  backups: PropTypes.array.isRequired,
  value: PropTypes.string,
  restoreBackup: PropTypes.func.isRequired,
  pickAndRestoreExternalBackup: PropTypes.func.isRequired,
};

BackupSettingsItem.defaultProps = {
  value: null,
};

const mapStateToProps = ({ settings: { isBackupEnabled }, backup: { backups } }) => ({
  isBackupEnabled,
  backups,
});

const mapDispatchToProps = (dispatch) => {
  const { restoreBackup, pickAndRestoreExternalBackup } = actions;

  return bindActionCreators(
    {
      restoreBackup,
      pickAndRestoreExternalBackup,
    },
    dispatch,
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(BackupSettingsItem);

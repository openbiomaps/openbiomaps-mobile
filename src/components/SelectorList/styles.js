import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  listItem: {
    flexDirection: 'row',
    paddingHorizontal: 25,
    paddingVertical: 20,
    alignItems: 'center',
    borderBottomColor: '#e5e5e5',
    borderBottomWidth: 1,
    justifyContent: 'space-between',
  },
  mainText: {
    fontSize: 16,
    color: '#212121',
  },
  subText: {
    fontSize: 14,
    color: '#888',
  },
  icon: {
    marginLeft: 5,
    flex: 0,
  },
  iconContainer: {
    flex: 0,
    flexDirection: 'row',
  },
});

import React from 'react';
import { FlatList } from 'react-native';
import PropTypes from 'prop-types';
import ListItem from './ListItem';

const keyExtractor = item => String(item.id);

function SelectorList({ data, onPress, isOptionNeeded, onOptionPositivePress }) {
  return (
    <FlatList
      data={data}
      renderItem={({ item }) => 
      (
        <ListItem item={item} onPress={onPress} isOptionNeeded={isOptionNeeded} onOptionPositivePress={onOptionPositivePress} />
      )}
      keyExtractor={keyExtractor}
    />
  );
}

SelectorList.propTypes = {
  data: PropTypes.array.isRequired,
  onPress: PropTypes.func.isRequired,
  isOptionNeeded: PropTypes.bool,
  onOptionPositivePress: PropTypes.func,
};

SelectorList.defaultProps = {
  isOptionNeeded: false,
  onOptionPositivePress: () => {},
};

export { SelectorList };


import React from 'react';
import { TouchableOpacity, View, Text } from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';
import { TouchableIcon } from '../TouchableIcon';
import { Colors } from '../../colors';
import { showAlertDialog } from '../../components/AlertDialog';
import I18n from '../../i18n';

const showShortCutOption = (item, onOptionPositivePress) => {
  showAlertDialog({
    title: I18n.t('shortcut_to_home_screen_title'),
    message: I18n.t('shortcut_to_home_screen_message'),
    showCancelButton: true,
    positiveLabel: I18n.t('create_shortcut'),
    negativeLabel: I18n.t('cancel'),
    onPositiveBtnPress: () => { onOptionPositivePress(item); },
  });
};

const ListItem = ({ item, onPress, isOptionNeeded, onOptionPositivePress }) => (
  <TouchableOpacity
    style={[styles.listItem, item.disabled && { opacity: 0.5 }]}
    disabled={item.disabled} 
    onPress={() => onPress(item)}
  >
    <View>
      <Text style={styles.mainText}>{item.name}</Text>
      {item.description && (
      <Text style={styles.subText} ellipsizeMode="tail" numberOfLines={2}>
        {item.description}
      </Text>
        )}
    </View>
    <View style={styles.iconContainer}>
      {item.error && (
      <TouchableIcon 
        name="error"
        size={24}
        color={Colors.red}
        style={styles.icon}
      />
    )}
      {item.session && (
      <TouchableIcon 
        name="playlist-play"
        size={24}
        color={Colors.blue}
        style={styles.icon}
      />
    )}
      { isOptionNeeded &&
      <TouchableIcon 
        name="more-horiz"
        size={24}
        color={Colors.darkGray}
        style={styles.icon}
        onPress={() => { showShortCutOption(item, onOptionPositivePress); }}
      />
    }
    </View>
  </TouchableOpacity>
);

ListItem.propTypes = {
  item: PropTypes.object.isRequired,
  onPress: PropTypes.func.isRequired,
  isOptionNeeded: PropTypes.bool.isRequired,
  onOptionPositivePress: PropTypes.func.isRequired,
};

export default ListItem;

import { StyleSheet } from 'react-native';
import { Colors } from '../../colors';

export const styles = StyleSheet.create({

  errorContainer: {
    backgroundColor: Colors.opacFlamingo,
    flex: 1,
    marginHorizontal: 15,
    borderColor: Colors.brickRed,
    borderWidth: 3,
    borderRadius: 5,
    padding: 10,
  },
  errorText: {
    color: 'white',
    textAlign: 'left',
  },
});

import React, { Component, Fragment } from 'react';
import {
  View,
  Text,
  Modal,
  TouchableOpacity,
  FlatList,
  SafeAreaView,
} from 'react-native';
import PropTypes from 'prop-types';
import ListItem from './AutoCompleteListItem';
import { AutoComplete as styles } from './styles';
import { TouchableIcon } from '../TouchableIcon';
import { Colors } from '../../colors';
import TagList from '../TagList';

class MultiSelectList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
    };
  }

  getItems = (data, lastPickedData) => {
    const items = [];

    data.forEach((itemObject) => {
      const [key, value] = Object.entries(itemObject)[0];
      if (itemObject && key) {
        if (
          lastPickedData.some(
            (lastPickedItem) =>
              lastPickedItem &&
              lastPickedItem.key === key &&
              lastPickedItem.value === value,
          )
        ) {
          items.unshift({ key, value });
        } else {
          items.push({ key, value });
        }
      }
    });
    return items;
  };

  getValues = () => {
    if (
      this.props.value.length > 0 &&
      typeof this.props.value[0] === 'object'
    ) {
      return this.props.value.reduce((acc, curr) => {
        if (!curr) return acc;
        if (acc === '') return acc + curr.value;
        return `${acc}, ${curr.value}`;
      }, '');
    }
    return this.props.value.reduce((acc, curr) => {
      if (!curr) return acc;
      if (acc === '') return acc + curr;
      return `${acc}, ${curr}`;
    }, '');
  };

  handlePress = () => {
    this.toggleModal();
  };

  handleAddItem = (value) => {
    const valueArray = [...this.props.value];
    if (typeof value === 'object') {
      if (!valueArray.some((item) => item.value === value.value))
        valueArray.push(value);
      this.props.onValueChange(valueArray);
    } else if (!valueArray.includes(value)) {
      valueArray.push(value);
      this.props.onValueChange(valueArray);
    }
  };

  handleRemoveItem = (value) => {
    const valueArray = [...this.props.value];
    const filteredArray = valueArray.filter((item) => {
      if (typeof value === 'object') {
        return item.value !== value.value;
      }
      return item !== value;
    });
    this.props.onValueChange(filteredArray);
  };

  toggleModal = () => {
    this.setState({ visible: !this.state.visible });
  };

  render() {
    const { value, error, obligatory, data, lastPickedData } = this.props;
    const { visible } = this.state;

    return (
      <Fragment>
        <TouchableOpacity
          onPress={this.handlePress}
          style={[
            styles.container,
            obligatory && styles.obligatory,
            error && styles.error,
          ]}
          activeOpacity={1}
        >
          <Text style={styles.text}>{this.getValues()}</Text>
          <TouchableIcon
            name="arrow-drop-down"
            color={Colors.darkGray}
            size={24}
            disabled
          />
        </TouchableOpacity>

        {visible && (
          <Modal
            presentationStyle="pageSheet"
            visible={visible}
            onRequestClose={this.toggleModal}
            animationType="slide"
            transparent={false}
          >
            <View style={styles.header}>
              <TagList
                data={value}
                onItemPress={this.handleRemoveItem}
                style={styles.tagList}
              />
              <TouchableOpacity
                onPress={this.toggleModal}
                style={[styles.button, styles.closeButton]}
              >
                <TouchableIcon
                  name="close"
                  color={Colors.brickRed}
                  size={20}
                  onPress={this.toggleModal}
                />
              </TouchableOpacity>
            </View>

            <FlatList
              keyboardShouldPersistTaps="handled"
              data={this.getItems(
                data,
                lastPickedData.filter((item) => item),
              )}
              style={styles.scrollView}
              keyExtractor={(item) => item.key}
              renderItem={({ item }) => (
                <ListItem
                  key={item.key}
                  value={item.value}
                  onPress={() => {
                    this.handleAddItem(item);
                  }}
                />
              )}
            />
          </Modal>
        )}
      </Fragment>
    );
  }
}

MultiSelectList.propTypes = {
  data: PropTypes.oneOfType([PropTypes.object, PropTypes.array]).isRequired,
  value: PropTypes.array,
  onValueChange: PropTypes.func.isRequired,
  error: PropTypes.string,
  obligatory: PropTypes.bool,
  lastPickedData: PropTypes.array,
};

MultiSelectList.defaultProps = {
  value: [],
  error: null,
  obligatory: false,
  lastPickedData: [],
};

export default MultiSelectList;

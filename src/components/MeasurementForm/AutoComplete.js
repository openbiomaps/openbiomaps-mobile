import React, { Component } from 'react';
import {
  View,
  Text,
  Modal,
  TextInput,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import PropTypes from 'prop-types';
import ListItem from './AutoCompleteListItem';
import { AutoComplete as styles } from './styles';
import { TouchableIcon } from '../TouchableIcon';
import { Colors } from '../../colors';
import { removeAccents } from '../../utils';
import { UnfoldingList as unfoldingListStyle } from './styles';
import I18n from '../../i18n';
import { showAlertDialog } from '../AlertDialog';

class AutoComplete extends Component {
  constructor(props) {
    super(props);
    this.state = { visible: false, searchValue: '' };
  }

  handlePress = () => {
    this.toggleModal(false);
  };

  handleModalShow = () => {
    this.textInput.focus();
  };

  addItemToList = (value, defaultList) => {
    if (defaultList === 0) {
      defaultList = [];
    }
    const list = [...defaultList];

    const selectedElement = list.find((findValue) => findValue.item === value);
    const selecetedElementIndex = list.findIndex(
      (findValue) => findValue.item === value,
    );

    if (selectedElement) {
      const selectedElementCount = selectedElement.count;
      list[selecetedElementIndex] = {
        item: value,
        count: selectedElementCount + 1,
      };
    } else {
      list.push({ item: value, count: 1 });
    }

    return list;
  };

  handleItemPress = (value) => {
    if (this.props.unfoldingList) {
      const savedSelectedList = this.addItemToList(value, this.props.value);
      this.props.onValueChange(savedSelectedList);
    } else {
      this.props.onValueChange(value);
    }

    this.toggleModal(true);
  };

  handleChangeText = (textValue) => {
    this.props.onValueChange(textValue);
  };

  toggleModal = (close) => {
    if (!close) {
      if (this.state.searchValue) {
        this.props.onValueChange(this.state.searchValue);
      }
    }
    this.setState((prevState) => ({
      visible: !prevState.visible,
      searchValue: '',
    }));
  };

  handleDeletePress = () => {
    this.setState({ searchValue: '' });
    this.props.onValueChange(); // calling this with empty attr. resets the values to undefined, and deafult values will be shown
  };

  handleFilterList() {
    const { data } = this.props;
    const { searchValue } = this.state;
    const valueWithoutAccents = removeAccents(searchValue);

    return valueWithoutAccents.length > 0
      ? data
          .filter(
            (item) =>
              item &&
              removeAccents(item)
                .toLowerCase()
                .startsWith(valueWithoutAccents.toLowerCase()),
          )
          .slice(0, 10)
          .concat(
            data
              .filter(
                (item) =>
                  item &&
                  removeAccents(item)
                    .toLowerCase()
                    .includes(valueWithoutAccents.toLowerCase()),
              )
              .slice(0, 20),
          )
          .filter((v, i, a) => a.indexOf(v) === i)
      : [];
  }

  getLastPickedData() {
    const { lastPickedData, unfoldingList } = this.props;
    if (unfoldingList) {
      const lastData = [];
      Object.values(lastPickedData).forEach((element) => {
        if (!lastData.includes(element.item)) {
          lastData.push(element.item);
        }
      });
      return lastData;
    } else {
      return lastPickedData;
    }
  }

  getValue() {
    const { value, unfoldingList } = this.props;
    if (unfoldingList) {
      return I18n.t('unfolding_list_please_choose');
    } else {
      return value || '-';
    }
  }

  handleAddCount(item) {
    const savedSelectedList = this.addItemToList(item.item, this.props.value);

    this.props.onValueChange(savedSelectedList);
  }

  handleRemoveCount(item) {
    const savedSelectedList = this.props.value;

    const selectedElementSaved = savedSelectedList.find(
      (findValue) => findValue.item === item.item,
    );

    if (selectedElementSaved) {
      const selecetedElementIndexSaved = savedSelectedList.findIndex(
        (findValue) => findValue.item === item.item,
      );

      const selectedElementCountSaved = selectedElementSaved.count;

      if (selectedElementCountSaved == 1) {
        showAlertDialog({
          title: I18n.t('unfolding_list_delete_title', {
            itemName: this.findByKey(this.props.value, item.item).item,
          }),
          message: I18n.t('unfolding_list_delete_message'),
          showCancelButton: true,
          positiveLabel: I18n.t('bool_true'),
          negativeLabel: I18n.t('bool_false'),
          onPositiveBtnPress: () => {
            savedSelectedList.splice(selecetedElementIndexSaved, 1);
            this.props.onValueChange(savedSelectedList);
          },
        });
      } else {
        savedSelectedList[selecetedElementIndexSaved] = {
          item: item.item,
          count: selectedElementCountSaved - 1,
        };
        this.props.onValueChange(savedSelectedList);
      }
    }
  }

  findByKey = (arr, currValue) => {
    return arr.find((findValue) => findValue.item === currValue);
  };

  render() {
    const { error, obligatory, shortName } = this.props;
    const { searchValue } = this.state;

    return (
      <View>
        <TouchableOpacity
          onPress={this.handlePress}
          style={[
            styles.container,
            obligatory && styles.obligatory,
            error && styles.error,
          ]}
          activeOpacity={1}
        >
          <Text style={styles.text}>{this.getValue()}</Text>
          <TouchableIcon
            name="search"
            color={Colors.darkGray}
            size={24}
            disabled
          />
        </TouchableOpacity>
        {this.props.unfoldingList &&
          this.props.value !== 0 &&
          this.props.value && (
            <View style={unfoldingListStyle.container}>
              <FlatList
                contentInset={{ top: 0, bottom: 20, left: 0, right: 0 }}
                data={this.props.value}
                keyExtractor={(item) => item.item}
                removeClippedSubviews={false}
                renderItem={({ item }) => (
                  <View style={unfoldingListStyle.containerWithButton}>
                    <TouchableIcon
                      style={unfoldingListStyle.rowButton}
                      onPress={() => this.handleRemoveCount(item)}
                      name="remove"
                      color={Colors.darkGray}
                      size={30}
                    />
                    <View style={unfoldingListStyle.valueContainer}>
                      <Text>
                        {item.item} ({item.count})
                      </Text>
                    </View>
                    <TouchableIcon
                      style={unfoldingListStyle.rowButton}
                      onPress={() => this.handleAddCount(item)}
                      name="add"
                      color={Colors.darkGray}
                      size={30}
                    />
                  </View>
                )}
              />
            </View>
          )}

        <Modal
          presentationStyle="pageSheet"
          visible={this.state.visible}
          onRequestClose={() => this.toggleModal(true)}
          animationType="slide"
          transparent={false}
          onShow={this.handleModalShow}
        >
          <View
            style={{
              paddingHorizontal: 15,
              paddingTop: 10,
              elevation: 3,
              backgroundColor: '#FAFAFA',
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}
          >
            <Text style={{ fontWeight: 'bold', fontSize: 20 }}>
              {shortName}
            </Text>
            <TouchableOpacity
              onPress={() => this.toggleModal(true)}
              style={[styles.button, styles.closeButton]}
            >
              <TouchableIcon
                name="close"
                color={Colors.brickRed}
                size={20}
                onPress={() => this.toggleModal(true)}
              />
            </TouchableOpacity>
          </View>
          <View style={styles.header}>
            <TextInput
              ref={(input) => {
                this.textInput = input;
              }}
              value={searchValue}
              onChangeText={(searchValue) => this.setState({ searchValue })}
              blurOnSubmit
              onSubmitEditing={() => this.toggleModal(false)}
              style={styles.text}
            />
            {searchValue !== '' && (
              <TouchableIcon
                name="backspace"
                color={Colors.darkGray}
                size={20}
                style={[styles.button, styles.backspaceButton]}
                onPress={this.handleDeletePress}
              />
            )}
          </View>
          <FlatList
            keyboardShouldPersistTaps="handled"
            data={
              searchValue === ''
                ? this.getLastPickedData()
                : this.handleFilterList()
            }
            keyExtractor={(item) => item}
            renderItem={({ item }) => (
              <ListItem
                key={item}
                value={item}
                onPress={() => {
                  this.setState(
                    {
                      searchValue: '',
                    },
                    () => this.handleItemPress(item),
                  );
                }}
              />
            )}
          />
        </Modal>
      </View>
    );
  }
}

AutoComplete.propTypes = {
  shortName: PropTypes.string,
  data: PropTypes.array.isRequired,
  lastPickedData: PropTypes.array,
  value: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.string,
    PropTypes.number,
  ]),
  onValueChange: PropTypes.func.isRequired,
  error: PropTypes.string,
  obligatory: PropTypes.bool,
  unfoldingList: PropTypes.bool,
};

AutoComplete.defaultProps = {
  value: [] | '',
  error: null,
  obligatory: false,
  lastPickedData: [],
};

export default AutoComplete;

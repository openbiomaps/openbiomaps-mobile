import React, { Component, Fragment } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';

import PropTypes from 'prop-types';
import Clipboard from '@react-native-community/clipboard';
import * as Animatable from 'react-native-animatable';
import { withNavigation } from 'react-navigation';

import NavigationHandler from '../../NavigationHandler';
import { Colors } from '../../colors';
import I18n from '../../i18n';
import { TouchableIcon } from '../TouchableIcon';
import { showAlertDialog } from '../AlertDialog';
import { isGeometryValueValid } from '../../utils';
import { GPS_OBSERVER, POSITION_ACCURACY_LIMIT } from '../../Constants';
import withGpsSubscription, { GpsDefaultPropTypes, GpsPropTypes } from '../../hocs/withGpsSubscription';

import { PositionInput as styles } from './styles';

class PositionInput extends Component {
  constructor(props) {
    super(props);

    const { initValue, gps } = props;

    this.state = {
      pinPressed: false,
      valueLocal: initValue(gps.position),
    };
  }

  componentDidMount = async () => {
    this.setNavigationCallbacks();
  };

  componentDidUpdate(prevProps) {
    const { gps, valueFromParent, handleIncomingCoordinates } = this.props;
    const { valueLocal } = this.state;

    if (gps.position && prevProps.gps.position !== gps.position) {
      // eslint-disable-next-line react/no-did-update-set-state
      this.setState({
        valueLocal: handleIncomingCoordinates(valueLocal, gps.position),
      });
    }

    // Set pinPressed true when position data is available (inital setting from sticky value)
    // Reset pin state when a new measurement is initiated from the dialog
    if (
      (!prevProps.valueFromParent && valueFromParent)
      || (prevProps.valueFromParent && !valueFromParent)
    ) {
      // eslint-disable-next-line react/no-did-update-set-state
      this.setState({ pinPressed: valueFromParent !== null });
    }
  }

  componentWillUnmount() {
    this.removeNavigationCallbacks();
  }

  handleDataComingFromPositionInputScreen = (geometryValue) => {
    this.setState({
      valueLocal: geometryValue,
    });
  };

  onMapPress = () => {
    const { valueLocal, pinPressed } = this.state;
    const {
      type,
      column,
      valueFromParent,
      server,
      database,
      observation,
      generateSavedValue,
      navigation: {
        state: { key },
      },
    } = this.props;

    NavigationHandler.navigate('PositionInput', {
      column,
      valueFromParent,
      valueLocal,
      key,
      server,
      database,
      observation,
      type,
      isValueFixed: pinPressed,
      saveCollectedGeometryValue: this.handleDataComingFromPositionInputScreen,
      generateSavedValue,
    });
  };

  // TODO: only points can be copyed to clipboard right now, need wkt string converting
  onCopyPress = () => {
    const { valueLocal } = this.state; 
    const { valueFromParent, type, gps } = this.props;

    const position = valueFromParent || (gps.position?.accuracy < POSITION_ACCURACY_LIMIT && valueLocal);
    if (type === 'point' && position) {
      Clipboard.setString(
        `${position.latitude.toFixed(6)}, ${position.longitude.toFixed(6)}`,
      );
      showAlertDialog({
        title: I18n.t('copy'),
        message: I18n.t('clipboard_message'),
      });
    }
  };

  onPinPress = () => {
    const {
      type,
      column,
      initValue,
      generateSavedValue,
      gps: { isObserving, position, error, subscribe: subscribeToGps },
      navigation: { state: { key } },
    } = this.props;

    if (isObserving) {
      if (error) return;
      
      this.setState((prevState) => {
        const { pinPressed: prevPinPressed, valueLocal } = prevState;

        if (!prevPinPressed) {
          if (
            isGeometryValueValid({
              type,
              geometryValue: valueLocal,
            })
          ) {
            NavigationHandler.setParams(
              {
                positionInputCoordinates: {
                  value: generateSavedValue(valueLocal),
                  accuracy: position.accuracy,
                  columnName: column,
                },
              },
              key,
            );
            return { pinPressed: true, valueLocal: initValue(position) };
          }
          return null;
        }
        NavigationHandler.setParams(
          {
            positionInputCoordinates: {
              value: null,
              accuracy: null,
              columnName: column,
            },
          },
          key,
        );
        return { valueLocal: initValue(position) };
      });
    } else {
      subscribeToGps();
    }
  };

  getAccuracyText = () => {
    const { pinPressed } = this.state;
    const { 
      gps: {
        isObserving,
        position,
        error,
      },
    } = this.props;

    if (pinPressed) {
      return (
        <Text style={[styles.text, { color: Colors.mantisGreen }]}>
          {`[${I18n.t('fixed_position')}]`}
        </Text>
      );
    }

    if (!isObserving || error) {
      return (
        <Text style={[styles.text, { color: Colors.flamingo }]}>
          {`[${I18n.t('no_gps')}]`}
        </Text>
      );
    }

    if (!position?.accuracy) {
      return (
        <Text style={[styles.text, { color: Colors.flamingo }]}>
          {`[${I18n.t('uncertain_location')}]`}
        </Text>
      );
    }
    
    if (position?.accuracy > POSITION_ACCURACY_LIMIT) {
      return (
        <Text style={[styles.text, { color: Colors.flamingo }]}>
          {`[~ ${position?.accuracy.toFixed()} m]`}
        </Text>
      );
    }

    return (<Text style={styles.text}>{`[${position?.accuracy.toFixed(2)} m]`}</Text>);
  };

  getGPSIconName = () => {
    const { pinPressed } = this.state;
    const { 
      gps: {
        isObserving,
        error,
      },
    } = this.props;

    if (pinPressed) return 'place';
    if (isObserving) {
      if (error) return 'gps-off';
      return 'gps-fixed';
    }
    return 'gps-off';
  };

  getGPSIconAnimation = () => {
    const { pinPressed } = this.state;
    const { 
      gps: {
        isObserving,
        error,
      },
    } = this.props;

    if (isObserving) {
      if (pinPressed || error) return undefined;
      return 'fadeIn';
    }
    return undefined;
  };

  getCurrentGeometryValue = () => {
    const { valueLocal } = this.state;
    const { type, generateSavedValue, gps: { position } } = this.props;

    if (isGeometryValueValid({ type, geometryValue: valueLocal })) {
      if (type === 'point') {
        return position?.accuracy < POSITION_ACCURACY_LIMIT
          ? generateSavedValue(valueLocal)
          : null;
      }
      return generateSavedValue(valueLocal);
    }
    return null;
  };

  getCurrentAccuracy = () => {
    const { gps: { position } } = this.props;
    return position?.accuracy < POSITION_ACCURACY_LIMIT ? position?.accuracy : null;
  };

  clearCurrentGeometryValue = () => {
    const { type, initValue } = this.props;

    if (type === 'point') return;

    this.setState({
      valueLocal: initValue(),
    });
  };

  setNavigationCallbacks = () => {
    const {
      navigation: {
        state: { key },
      },
      column,
    } = this.props;

    // These functions ared called when the done button is pressed
    // on the create/edit measurement screen.
    // since multiple PositionInputComponent can occur, every field has to set a unique callback
    NavigationHandler.setParams(
      {
        [`getCurrentGeometryValue${column}`]: this.getCurrentGeometryValue,
        [`getCurrentAccuracy${column}`]: this.getCurrentAccuracy,
        [`clearCurrentGeometryValue${column}`]: this.clearCurrentGeometryValue,
      },
      key,
    );
  };

  removeNavigationCallbacks = () => {
    const {
      navigation: {
        state: { key },
      },
      column,
    } = this.props;

    NavigationHandler.setParams(
      {
        [`getCurrentGeometryValue${column}`]: () => {},
        [`getCurrentAccuracy${column}`]: () => {},
        [`clearCurrentGeometryValue${column}`]: () => {},
      },
      key,
    );
  };

  renderButtons = () => {
    const { pinPressed } = this.state;
    const { autoGeometry } = this.props;

    if (!autoGeometry) {
      return (
        <>
          <TouchableIcon
            style={styles.buttonContainer}
            onPress={this.onMapPress}
            name="map"
            size={30}
            color={Colors.darkGray}
          />
          <Animatable.View
            animation={this.getGPSIconAnimation()}
            iterationCount="infinite"
            direction="alternate"
            duration={500}
            useNativeDriver
          >
            <TouchableIcon
              style={styles.buttonContainer}
              onPress={this.onPinPress}
              name={this.getGPSIconName()}
              size={30}
              color={pinPressed ? Colors.mantisGreen : Colors.silver}
            />
          </Animatable.View>
        </>
      );
    }

    return null;
  };

  render() {
    const { pinPressed, valueLocal } = this.state;
    const { error, obligatory, generateInfoText, valueFromParent, gps: { position } } = this.props;

    const valueGeometry = valueFromParent || (position?.accuracy < POSITION_ACCURACY_LIMIT && valueLocal);

    const baseContainerStyle = [
      styles.field,
      obligatory && styles.obligatory,
      error && styles.error,
      pinPressed && styles.pressed,
    ];

    return (
      <View style={styles.container}>
        <View style={baseContainerStyle}>
          <TouchableOpacity onLongPress={this.onCopyPress}>
            <Text style={styles.text} numberOfLines={1}>
              {generateInfoText(valueGeometry)}
            </Text>
          </TouchableOpacity>
        </View>

        {this.renderButtons()}

        <View style={styles.updateIntervalContainer}>
          {this.getAccuracyText()}
        </View>
      </View>
    );
  }
}

PositionInput.propTypes = {
  valueFromParent: PropTypes.oneOfType([
    PropTypes.shape({
      longitude: PropTypes.number.isRequired,
      latitude: PropTypes.number.isRequired,
    }),
    PropTypes.arrayOf(PropTypes.shape({
      longitude: PropTypes.number.isRequired,
      latitude: PropTypes.number.isRequired,
    })),
  ]),
  type: PropTypes.string.isRequired,
  column: PropTypes.string.isRequired,
  navigation: PropTypes.object.isRequired, // Comming from withNavigation HOC
  error: PropTypes.string,
  obligatory: PropTypes.bool,
  server: PropTypes.object.isRequired,
  database: PropTypes.object.isRequired,
  observation: PropTypes.object.isRequired,
  autoGeometry: PropTypes.bool.isRequired,
  initValue: PropTypes.func.isRequired,
  handleIncomingCoordinates: PropTypes.func.isRequired,
  generateInfoText: PropTypes.func.isRequired,
  generateSavedValue: PropTypes.func.isRequired,
  gps: GpsPropTypes,
};

PositionInput.defaultProps = {
  valueFromParent: null,
  error: null,
  obligatory: false,
  gps: GpsDefaultPropTypes,
};

const getGpsSubscriptionOptions = ({ observation }) => ({ observer: `${GPS_OBSERVER.FORM}-${observation.id}` });

export default withNavigation(withGpsSubscription(
  PositionInput, 
  getGpsSubscriptionOptions,
));

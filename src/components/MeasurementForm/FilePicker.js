import React, { Component } from 'react';
import {
  View,
  Alert,
  ActivityIndicator,
  FlatList,
} from 'react-native';
import Toast from 'react-native-simple-toast';
import PropTypes from 'prop-types';
import { Button } from 'react-native-vector-icons/MaterialIcons';
import * as ImagePicker from 'react-native-image-crop-picker';
import DocumentPicker from 'react-native-document-picker';
import I18n from '../../i18n';
import ListItem from './FilePickerListItem';
import { FilePicker as styles } from './styles';
import { Colors } from '../../colors';
import { showAlertDialog } from '../AlertDialog';
import { ERROR } from '../../Constants';
import { isValidFilename } from '../../utils';
import { hasCamerePermission } from '../../utils/permissonRequests';
import { removeUnusedAttachedFiles } from '../../utils/reduxUtils';
import { deleteFile, moveToAttachedFiles } from '../../utils/fileSystemUtils';
import NavigationHandler from '../../NavigationHandler';

class FilePickerComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      indicator: false,
    };
  }

  componentWillUnmount() {
    const { items } = this.props;
    removeUnusedAttachedFiles(items.map((file) => file.uri));
  }

  /**
   * Copies the selected file to a local storage, with unique filename.
   * @param {*} type 
   * @param {*} fileName 
   * @param {*} uri 
   * @returns 
   */
  addItem = async (type, fileName, uri) => {
    if (isValidFilename(fileName)) {
      const { ok, data } = await moveToAttachedFiles(fileName, uri);
      if (!ok) {
        deleteFile(uri);
        return;
      }
      const { newFileName, newUri } = data;
      const { items, onChange } = this.props;
      const id = Math.max(0, ...items.map((item) => item.id)) + 1;
      const newItems = [{
        id,
        uri: newUri,
        type,
        fileName: newFileName,
      }, ...items];
      onChange(newItems);
    } else {
      showAlertDialog({
        title: I18n.t('error_title_filename'),
        message: I18n.t('error_text_filename'),
        positiveLabel: I18n.t('ok'),
      });
    }
  }

  removeItem = (id) => {
    const { items, onChange } = this.props;
    const newItems = items.reduce((acc, cur) => {
      if (cur.id === id) {
        // deleting picked file from local storage
        if (cur.uri) removeUnusedAttachedFiles([cur.uri]);
      } else {
        acc.push(cur);
      }
      return acc;
    }, []);
    onChange(newItems);
  }

  showIndicator = () => {
    this.setState({ indicator: true });
  }

  hideIndicator = () => {
    this.setState({ indicator: false });
  }

  handleAddFilePress = async () => {
    const { maximumSize } = this.props;
    try {
      const [res] = await DocumentPicker.pick({ copyTo: 'cachesDirectory' });
      const path = `file://${res.fileCopyUri}`;
      if (res.size < maximumSize || res.type.split('/')[0] === 'image') {
        this.addItem(res.type, res.name, path);
      } else {
        // delete copied temporary file
        deleteFile(path);
        showAlertDialog({
          title: I18n.t('error_title_max_file'),
          message: I18n.t('error_text_max_file'),
          positiveLabel: I18n.t('ok'),
        });
      }
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        // User cancelled the picker, exit any dialogs or menus and move on
      } else if (err === ERROR.PERMISSION_DENIED) {
        Toast.show(I18n.t('external_permission_denied'), Toast.LONG);
      } else if (err.message?.includes(ERROR.WRONG_CONTENT_URI_FILE_PATH_ERROR)) {
        Toast.show(I18n.t('wrong_content_uri'), Toast.LONG);
      }
    }
  }

  handleAddItemCamera = async () => {
    this.showIndicator();
    try {
      const hasPermission = await hasCamerePermission();
      if (!hasPermission) {
        this.hideIndicator();
        return;
      }
      if (hasPermission) {
        ImagePicker.launchCamera(
          { mediaType: 'photo' },
          (response) => {
            //TODO
            // {"errorCode": "camera_unavailable"}
            // console.warn(response);
            // response.errorCode
            if (response.didCancel !== true) {
              response.assets.forEach((response) => {
                if (response.error) {
                  this.hideIndicator();
                  Alert.alert(response.error);
                } else {
                  this.hideIndicator();
                  this.addItem(response.type, response.fileName, response.uri);
                }
              });
            } else {
              this.hideIndicator();
            }
          },
        );
      }
    } catch (err) {
      console.warn(err);
    }
  }

  handleAddItemLibrary = () => {
    this.showIndicator();
    ImagePicker.openPicker(
      { mediaType: 'photo' },
    ).then((response) => {
      if (response.didCancel !== true) {
        if (response.error) {
          this.hideIndicator();
          Alert.alert(response.error);
        } else {
          this.hideIndicator();
          const fields = response.path.split('/');
          const name = fields[fields.length - 1];
          this.addItem(response.mime, name, response.path);
        }
      } else {
        this.hideIndicator();
      }
    });
  }

  render() {
    const { error, obligatory, maximumSize, items } = this.props;
    const { indicator } = this.state;
    return (
      <View style={[styles.container, obligatory && styles.obligatory, error && styles.error]}>
        {indicator && <ActivityIndicator size="large" color={Colors.mantisGreen} />}

        <FlatList
          data={items}
          keyExtractor={(item) => String(item.id)}
          renderItem={({ item: { id, uri, fileName, type } }) => (
            <ListItem id={id} uri={uri} fileName={fileName} type={type} onRemove={this.removeItem} />
          )}
        />
        <View style={styles.buttonContainer}>
          <View style={styles.button}>
            <Button
              name="add-a-photo"
              backgroundColor="#2A9B44"
              onPress={() => NavigationHandler.navigate('Camera', { addItem: this.addItem })}
            />
          </View>
          <View style={styles.button}>
            <Button
              name="photo-library"
              backgroundColor="#2A9B44"
              onPress={this.handleAddItemLibrary}
            />
          </View>
          {maximumSize && (
            <View style={styles.button}>
              <Button
                name="attach-file"
                backgroundColor="#2A9B44"
                onPress={this.handleAddFilePress}
              />
            </View>
          )}
        </View>
      </View>
    );
  }
}

FilePickerComponent.propTypes = {
  items: PropTypes.array,
  onChange: PropTypes.func.isRequired,
  error: PropTypes.string,
  obligatory: PropTypes.bool,
  maximumSize: PropTypes.number,
};

FilePickerComponent.defaultProps = {
  items: [],
  error: null,
  obligatory: false,
  maximumSize: null,
};

export default FilePickerComponent;

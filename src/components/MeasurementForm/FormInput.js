import React, { Fragment } from 'react';
import { View, TextInput } from 'react-native';
import PropTypes from 'prop-types';
import { FormInput as styles } from './styles';
import { TouchableIcon } from '../TouchableIcon';
import { Colors } from '../../colors';

/**
 * Remove the last digit if the absolute value is higher than 9.
 * Otherwise override the original value with zero.
 * @param {string} value 
 * @param {function} onChangeText
 */
const onRemoveLongPress = (value, onChangeText) => {
  if (value && Math.abs(parseInt(value, 10)) > 9) {
    onChangeText(value.slice(0, -1));
  } else {
    onChangeText('0');
  }
};

/**
 * Concat zero at the end of input value if it isn't zero.
 * Otherwise override the original value with 1.
 * @param {string} value 
 * @param {onChangeText} onChangeText 
 */
const onAddLongPress = (value, onChangeText) => {
  if (value && value !== '0') {
    onChangeText(`${value}0`);
  } else {
    onChangeText('1');
  }
};

const FormInput = ({
  value,
  onChangeText,
  keyboardType,
  error,
  editable,
  obligatory,
  ...textInputProps
}) => (
  <View style={styles.container}>
    <TextInput
      value={value}
      onChangeText={onChangeText}
      style={[styles.field, obligatory && styles.obligatory, error && styles.error]}
      keyboardType={keyboardType}
      editable={editable}
      {...textInputProps}
    />
    {keyboardType === 'numeric' && (
      <Fragment>
        <TouchableIcon
          style={styles.buttonContainer}
          onPress={() => onChangeText((value ? parseInt(value, 10) - 1 : 0).toString())}
          onLongPress={() => onRemoveLongPress(value, onChangeText)}
          name="remove"
          color={editable ? Colors.darkGray : 'grey'}
          size={30}
          disabled={!editable}
        />
        <TouchableIcon
          style={styles.buttonContainer}
          onPress={() => onChangeText((value ? parseInt(value, 10) + 1 : 1).toString())}
          onLongPress={() => onAddLongPress(value, onChangeText)}
          name="add"
          color={editable ? Colors.darkGray : 'grey'}
          size={30}
          disabled={!editable}
        />
      </Fragment>
    )}
  </View>
);

FormInput.propTypes = {
  onChangeText: PropTypes.func.isRequired,
  value: PropTypes.string,
  keyboardType: PropTypes.string,
  error: PropTypes.string,
  editable: PropTypes.bool,
  obligatory: PropTypes.bool,
};

FormInput.defaultProps = {
  value: null,
  keyboardType: 'default',
  error: null,
  editable: true,
  obligatory: false,
};

export default FormInput;

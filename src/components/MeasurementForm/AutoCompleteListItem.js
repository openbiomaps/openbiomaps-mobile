import React from 'react';
import { TouchableOpacity, Text } from 'react-native';
import PropTypes from 'prop-types';
import { AutoComplete as styles } from './styles';

const ListItem = ({ onPress, value }) => (
  <TouchableOpacity style={styles.listItem} onPress={onPress}>
    <Text>{value}</Text>
  </TouchableOpacity>
);

ListItem.propTypes = {
  value: PropTypes.string.isRequired,
  onPress: PropTypes.func.isRequired,
};

export default ListItem;

import React, { Component } from 'react';
import { 
  View, 
  Text,
  TouchableOpacity,
} from 'react-native';

import PropTypes from 'prop-types';

import I18n from '../../i18n';
import { WKT_TYPES } from '../../Constants';
import { isGeometryValueValid } from '../../utils';

import PositionInput from './PositionInput';
import { GeometryTypeChooser as styles } from './styles';

class GeometryTypeChooser extends Component {
  constructor(props) {
    super(props);

    const { type } = props;

    this.state = {
      wktType: (type === 'wkt') ? 'point' : type,
    };
  }

  static getDerivedStateFromProps = (props, state) => {
    if (props.type !== 'wkt') return null;
    if (props.valueFromParentType && props.valueFromParentType !== state.wktType) {
      return ({ wktType: props.valueFromParentType });
    }
    return null;
  }

  generateSavedValue = (value) => {
    const { type } = this.props;
    const { wktType } = this.state;

    if (type !== 'wkt') return value;
    return {
      wktType,
      wktValue: value,
    };
  }

  handleIncomingCoordinates = (value, coord) => {
    const { wktType } = this.state;

    switch (wktType) {
      case 'point':
        return coord;
      case 'line':
      case 'polygon': {
        if (Array.isArray(value)) return [...value, coord];
        return [coord];
      }
      default:
        return null;
    }
  }

  generateInfoText = (value) => {
    const { wktType } = this.state;
    
    switch (wktType) {
      case 'point':
        return value && !Array.isArray(value) ? `${value.latitude.toFixed(6)}, ${value.longitude.toFixed(6)}` : '-';
      case 'line':
      case 'polygon': {
        if (!isGeometryValueValid({ type: wktType, geometryValue: value, minSize: wktType === 'line' ? 2 : 3 })) {
          return (
            <Text style={styles.warnTextStyle}>
              {`${I18n.t('wkt_array_length', { wktType: I18n.t(wktType), length: value?.length || 0 })} ( < ${wktType === 'line' ? '2' : '3'} )`}
            </Text>
          );
        }
        return I18n.t('wkt_array_length', { wktType: I18n.t(wktType), length: value?.length || 0 });
      }
      default:
        return '-';
    } 
  }

  initValue = (defaultValue = null) => {
    const { wktType } = this.state;

    switch (wktType) {
      case 'line':
      case 'polygon': {
        if (defaultValue && Array.isArray(defaultValue)) return defaultValue;
        return [];
      }
      default: 
        return defaultValue;
    }
  }

  render() {
    const { wktType } = this.state;
    const { type, valueFromParentType } = this.props;

    const fixedType = type === 'wkt' ? valueFromParentType || wktType : type;
    
    return (
      <View>
        <PositionInput 
          // eslint-disable-next-line react/jsx-props-no-spreading
          {...this.props}
          type={fixedType}
          initValue={this.initValue}
          handleIncomingCoordinates={this.handleIncomingCoordinates}
          generateInfoText={this.generateInfoText}
          generateSavedValue={this.generateSavedValue}
        />
        { type === 'wkt' && valueFromParentType === null 
        && (
        <View style={styles.buttonContainer}>
          { WKT_TYPES.map((myType) => (
            <TouchableOpacity 
              style={[styles.buttonStyle, fixedType === myType && styles.selectedButtonStyle]} 
              onPress={() => this.setState({ wktType: myType })}
              key={myType}
            >
              <Text style={styles.buttonTextStyle}>{I18n.t(myType)}</Text>
            </TouchableOpacity>
          ))}
        </View>
        )}
        { (type !== 'wkt' || (type === 'wkt' && valueFromParentType !== null))
          && <Text style={styles.simpleTextStyle}>{I18n.t('wkt_type', { wktType: I18n.t(wktType) })}</Text>}
      </View>
    );
  }
}

GeometryTypeChooser.propTypes = {
  type: PropTypes.string.isRequired,
  valueFromParent: PropTypes.oneOfType([
    PropTypes.shape({
      longitude: PropTypes.number.isRequired,
      latitude: PropTypes.number.isRequired,
    }),
    PropTypes.arrayOf(PropTypes.shape({
      longitude: PropTypes.number.isRequired,
      latitude: PropTypes.number.isRequired,
    })),
  ]),
  valueFromParentType: PropTypes.string,
};

GeometryTypeChooser.defaultProps = {
  valueFromParent: null,
  valueFromParentType: null,
};

export default GeometryTypeChooser;

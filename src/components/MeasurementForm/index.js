import React, { Component } from 'react';
import { View } from 'react-native';

import PropTypes from 'prop-types';
import 'react-native-get-random-values';
import { v4 as uuidv4 } from 'uuid';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { debounce } from 'lodash';
import { FileSystem } from 'react-native-unimodules';

import * as actions from '../../actions';
import I18n from '../../i18n';
import { cleanObject, isEmpty } from '../../utils';
import NavigationHandler from '../../NavigationHandler';
import { Colors } from '../../colors';
import Notification from '../Notification';
import { APP_VERSION, REQUEST_ERROR, WKT_TYPES } from '../../Constants';
import { showAlertDialog } from '../AlertDialog';

import Form from './Form';
import { FormContainer as styles } from './styles';

class MeasurementForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      values: {},
      stickinesses: props.savedStickinesses || {},
      errors: null,
      startedAt: null,
      accuracy: null,
    };

    const { createBackup } = this.props;
    this.debouncedCreateBackup = debounce(createBackup, 3000);
  }

  static getDerivedStateFromProps = (props, state) => {
    // Setting positionInputCoordinates coming from PositionInputScreen
    const positionInputCoordinates = props.navigation.getParam(
      'positionInputCoordinates',
      {},
    );

    if (isEmpty(positionInputCoordinates)) return null;

    const { value, accuracy, columnName } = positionInputCoordinates;

    const stickinesses = MeasurementForm.setStickyValueBackToFalse(
      state,
      columnName,
      value,
    );

    props.navigation.setParams({
      positionInputCoordinates: {},
    });

    return {
      values: {
        ...state.values,
        [columnName]: value,
      },
      stickinesses,
      accuracy,
    };
  };

  /**
   * Set sticky back to false if the value changes.
   */
  static setStickyValueBackToFalse = (state, key, value) => {
    const stickinesses = { ...state.stickinesses };
    if (state.stickinesses[key] && state.values[key] !== value) {
      if (state.values[key] !== '') {
        stickinesses[key] = true;
      } else {
        stickinesses[key] = false;
      }
    }
    return stickinesses;
  };

  componentDidMount() {
    this.initFormValues(this.props);
    const { navigation, formDataChangeListener } = this.props;
    navigation.setParams({
      handleSavePress: this.handleDonePress,
      toggleShortcut: this.toggleShortcut,
      deleteShortcut: this.deleteShortcut,
      onCreateNewForm: this.onCreateNewForm,
    });

    // Set formDataChangeListener back to false whenever a new MeasurementForm
    // instance is crated, to avoid unexpected exit modal appearance in the parent screen.
    formDataChangeListener(false);
  }

  componentDidUpdate(prevProps) {
    const { lastMeasurement } = this.props;
    // Init form values after successfull form upload.
    if (prevProps.lastMeasurement.id !== lastMeasurement.id) {
      this.initFormValues(this.props);
    }
  }

  componentWillUnmount() {
    const {
      server: { id: serverId },
      database: { id: databaseId },
      observation: { id: observationId },
      saveStickinesses,
      isEditing,
    } = this.props;
    const { stickinesses } = this.state;

    if (!isEditing)
      saveStickinesses(serverId, databaseId, observationId, stickinesses);
  }

  toggleShortcut = () => {
    showAlertDialog({
      title: I18n.t('shortcut_to_home_screen_title'),
      message: I18n.t('shortcut_to_home_screen_message'),
      showCancelButton: true,
      positiveLabel: I18n.t('create_shortcut'),
      negativeLabel: I18n.t('cancel'),
      onPositiveBtnPress: () => {
        this.createShortcut();
      },
    });
  };

  deleteShortcut = () => {
    showAlertDialog({
      title: I18n.t('delete_shortcut_title'),
      message: I18n.t('delete_shortcut_message'),
      showCancelButton: true,
      positiveLabel: I18n.t('ok'),
      negativeLabel: I18n.t('cancel'),
      onPositiveBtnPress: () => {
        this.removeShortcut();
      },
    });
  };

  //TODO új szöveg a dialogra
  onCreateNewForm = () => {
    const { navigation } = this.props;
    if (this.state.isModified) {
      showAlertDialog({
        title: I18n.t('have_unsaved_changes'),
        // message: I18n.t('do_you_want_to_exit'),
        message: 'Biztos új űralpot szeretnél kezdeni?',
        showCancelButton: true,
        positiveLabel: I18n.t('bool_true'),
        negativeLabel: I18n.t('bool_false'),
        onPositiveBtnPress: () =>
          NavigationHandler.replace('CreateMeasurement', {
            title: navigation.state.params.title,
            description: navigation.state.params.description,
            pinned: navigation.state.params.pinned,
          }),
      });
    } else {
      NavigationHandler.replace('CreateMeasurement', {
        title: navigation.state.params.title,
        description: navigation.state.params.description,
        pinned: navigation.state.params.pinned,
      });
    }
  };

  createShortcut = async () => {
    const {
      createShortcut,
      server: { id: serverId, name: serverName },
      database: { id: databaseId, name: databaseName, game: databaseGame },
      observation: { id, name, lastSelected },
      navigation,
    } = this.props;
    createShortcut(
      { serverId, serverName },
      { databaseId, databaseName, databaseGame },
      { id, name, lastSelected },
    );

    navigation.setParams({ pinned: true });
  };

  removeShortcut = async () => {
    const {
      removeShortcut,
      server: { id: serverId },
      database: { id: databaseId },
      observation: { id },
      navigation,
    } = this.props;
    removeShortcut(serverId, databaseId, id);

    navigation.setParams({ pinned: false });
  };

  checkFilesExists = async (columnName) => {
    const { values } = this.props;
    if (!values[columnName] || values[columnName].length === 0) {
      return;
    }

    const files = values[columnName];
    const checkedFiles = await Promise.all(
      files.map(async (file) => {
        try {
          const { exists } = await FileSystem.getInfoAsync(file.uri);
          if (!exists) {
            return {
              ...file,
              uri: null,
            };
          }
        } catch (error) {
          return {
            ...file,
            uri: null,
          };
        }
        return file;
      }),
    );

    values[columnName] = checkedFiles;
    this.setState({
      values,
    });
  };

  initFormValues = (props) => {
    const { stickinesses } = this.state;
    const {
      form,
      lastMeasurement,
      errors,
      meta = {},
      navigation,
      isEditing,
      values: valuesBeingCopied,
    } = props;
    const values = {};
    const copiedValues = { ...valuesBeingCopied };
    // Initialize form values
    if (form) {
      // Get first element of list types for displaying initial value on spinner
      form
        .filter((item) => item.type === 'list')
        .forEach((item) => {
          const { list } = item;
          if (list[0] && Object.keys(list[0])[0] !== '') {
            [values[item.column]] = [
              // #248 hibajegy miatt lett kiszedve még tesztelésre szorul, hogy minden működik-e
              Object.keys(list[0])[0],
            ];
          }
          if (item.list_definition?.multiselect) {
            values[item.column] =
              !values[item.column] || values[item.column] === ''
                ? []
                : [
                    {
                      key: Object.keys(list[0])[0],
                      value: Object.values(list[0])[0],
                    },
                  ];
          }
        });

      // Set current date/time value to date/time fields initially. (when copying, a new date should be set)
      form
        .filter(
          (item) =>
            item.type === 'datetime' ||
            item.type === 'date' ||
            item.type === 'time',
        )
        .forEach((item) => {
          if (!isEditing) {
            delete copiedValues[item.column];
          }
        });

      // Set default values for certain item types.
      form.forEach((item) => {
        // Error messages related to spcified columns
        if (
          errors !== null &&
          errors !== '' &&
          typeof errors === 'object' &&
          item.column in errors
        ) {
          if (typeof errors[item.column] === 'object') {
            if (typeof errors[item.column]['1'] !== 'undefined') {
              errors[item.column] = errors[item.column]['1'];
            }
          }
        }
        if (item.type === 'file_id') {
          this.checkFilesExists(item.column);
        }
        if (item.defaultValue) {
          switch (item.type) {
            case 'boolean':
              values[item.column] = item.defaultValue === 'true';
              break;
            case 'list': {
              const { list, defaultValue } = item;
              if (list.some((item) => Object.keys(item)[0] === defaultValue)) {
                values[item.column] = defaultValue;
              } else if (list[0]) {
                [values[item.column]] = [
                  {
                    key: Object.keys(list[0])[0],
                    value: Object.values(list[0])[0],
                  },
                ];
              }
              break;
            }
            default:
              values[item.column] = item.defaultValue;
              break;
          }
        }
      });

      // Set stcikiness according to the previously used values (lastMeasurement),
      // or according to the default values comming from the API.
      if (!isEmpty(stickinesses)) {
        form.forEach(({ column, type }) => {
          if (type === 'boolean' && stickinesses[column]) {
            // Set default form values based on stickiness
            values[column] = lastMeasurement[column];
          } else if (stickinesses[column] && lastMeasurement[column]) {
            // Set default form values based on stickiness
            values[column] = lastMeasurement[column];
          }
        });
      } else {
        form.forEach((item) => {
          stickinesses[item.column] =
            item.apiParams && item.apiParams.sticky === 'on';
        });
      }

      // clear geometry fields' local state with the help of their navigation callback
      form.forEach(({ type, column }) => {
        if (WKT_TYPES.includes(type) || type === 'wkt') {
          const clearCurrentGeometryValue = navigation.getParam(
            `clearCurrentGeometryValue${column}`,
            () => {},
          );
          clearCurrentGeometryValue();
        }
      });

      this.setState({
        values: {
          ...values,
          ...copiedValues,
        },
        stickinesses,
        errors,
        startedAt: +new Date(),
        accuracy: meta.accuracy,
      });
    }
  };

  collectFieldsToReset = (item, form) => {
    const { column: key } = item;
    let fields = [];
    const filterSelector = form.find((item) =>
      item.filtering_list?.filter_selector.includes(key),
    );
    if (filterSelector) {
      fields.push(filterSelector.column);
      const newFields = this.collectFieldsToReset(filterSelector, form);
      fields = [...new Set([...fields, ...newFields])];
    }
    return fields;
  };

  handleValueChange = (key, value, item) => {
    const { formDataChangeListener, form } = this.props;

    // if(item.type === 'date') {
    //   this.setState((prevState) => {
    //     const values = { ...prevState.values, [key]: value };
    //     const fieldsToReset = this.collectFieldsToReset(item, form);
    //     fieldsToReset.forEach((field) => {
    //       values[field] = undefined;
    //     });

    //     // Init list values when the same item is selected at the second time.
    //     // Only when list is displayed as buttons.
    //     if (item.apiParams.list_elements_as_buttons === 'on') {
    //       if (prevState.values[key] === value) {
    //         values[key] = '';
    //       }
    //     }
    //     const stickinesses = MeasurementForm.setStickyValueBackToFalse(prevState, key, value);
    //     return ({ values, stickinesses, showErrorBar: false });
    //   }, formDataChangeListener(false));
    // }else {
    this.setState((prevState) => {
      const values = { ...prevState.values, [key]: value };
      const fieldsToReset = this.collectFieldsToReset(item, form);
      fieldsToReset.forEach((field) => {
        values[field] = undefined;
      });

      // Init list values when the same item is selected at the second time.
      // Only when list is displayed as buttons.
      if (item.apiParams.list_elements_as_buttons === 'on') {
        if (prevState.values[key] === value) {
          values[key] = '';
        }
      }
      const stickinesses = MeasurementForm.setStickyValueBackToFalse(
        prevState,
        key,
        value,
      );
      return { values, stickinesses, showErrorBar: false };
    }, formDataChangeListener(true));
    // }
  };

  handleDonePress = async (isNullRecord, metaRecordSessionId) => {
    const {
      id,
      server,
      database,
      observation,
      syncMeasurement,
      onDonePress,
      navigation,
      isEditing,
      onSaveFailed,
    } = this.props;
    const {
      startedAt,
      values: stateValues,
      accuracy: stateAccuracy,
    } = this.state;

    const values = { ...stateValues };
    let accuracy = stateAccuracy;
    observation.form.dataTypes.forEach(({ type, column, apiParams }) => {
      if (
        (type === 'time' || type === 'date' || type === 'datetime') &&
        !values[column] &&
        !isEditing
      ) {
        values[column] = new Date();
      }
      if (WKT_TYPES.includes(type) || type === 'wkt') {
        if (
          (apiParams?.auto_geometry === 'on' && !isEditing) ||
          !values[column]
        ) {
          const getCurrentGeometryValue = navigation.getParam(
            `getCurrentGeometryValue${column}`,
            () => {},
          );
          const getCurrentAccuracy = navigation.getParam(
            `getCurrentAccuracy${column}`,
            () => {},
          );
          const currentPosition = getCurrentGeometryValue();
          const currentAccuracy = getCurrentAccuracy();

          if (currentPosition) values[column] = currentPosition;
          if (currentAccuracy && accuracy > currentAccuracy) {
            accuracy = currentPosition;
          }
        }
      }
    });

    const isValidFormFields = await this.validateFields(values); // null rekordnál is kellenek a kötelező mező ellenörzések!

    if (isValidFormFields) {
      // Generate new uuid when a new measurement is created.
      // Use id - comming form props - when an existing measurement is edited.
      const uuid = id || uuidv4();

      const meta = {
        appVersion: APP_VERSION,
        // difference in hours between the time on the local computer and Universal Coordinated Time (UTC)
        timezone: new Date().getTimezoneOffset() / 60,
      };

      // while editing, startedAt and finishedAt cannot change from original
      if (!isEditing) {
        meta.startedAt = startedAt;
        meta.finishedAt = +new Date();
      }

      // Add formVersion if not null. While editing, formVersion is undefined and updating to it would delete the formVersion
      if (observation.formVersion) meta.formVersion = observation.formVersion;

      // Add accuracy to the meta object if accuracy is not null
      if (accuracy) meta.accuracy = accuracy;

      // Add sessionId to the meta object if a measurement session is in progress
      if (!isEditing && observation.session)
        meta.sessionId = observation.session.id;
      else if (isNullRecord && metaRecordSessionId)
        meta.sessionId = metaRecordSessionId;

      // Set observationListNullRecord to the meta object if the measurement is a null record
      if (isNullRecord) meta.observationListNullRecord = true;
      else meta.observationListNullRecord = false;

      // Itt rakjuk össze a felvett rekord elemeit: egyedi azonosító, mezők adatai, metaadatok
      const measurement = { id: uuid, data: cleanObject(values), meta };

      onDonePress(measurement);
      await syncMeasurement(server, database, observation, measurement);
      this.debouncedCreateBackup();

      if (observation.game) {
        NavigationHandler.navigate('TaskCompleted');
      }
    } else {
      onSaveFailed();
    }
  };

  handlePinPress = (column) => {
    const { formDataChangeListener } = this.props;
    this.setState(
      (prevState) => ({
        stickinesses: {
          ...prevState.stickinesses,
          [column]: !prevState.stickinesses[column],
        },
      }),
      formDataChangeListener(true),
    );
  };

  validateFields = async (values) => {
    let isValid = true;

    const { form } = this.props;
    const errors = {};

    const minMax = form.filter((item) => item.control === 'minmax');
    minMax.forEach((item) => {
      const { column, count, type } = item;
      if (count !== '{}') {
        const [, minValue, maxValue] =
          /{(-?\d+(?:\.\d+)*),(-?\d+(?:\.\d+)*)}/.exec(count);
        const value = values[column];
        if (
          value &&
          type === 'text' &&
          (value.length < minValue || value.length > maxValue)
        ) {
          errors[column] = I18n.t('error_text_out_of_range', {
            min: minValue,
            max: maxValue,
          });
          isValid = false;
        }
        if (
          value &&
          type === 'numeric' &&
          (Number(value) < minValue || Number(value) > maxValue)
        ) {
          errors[column] = I18n.t('error_number_out_of_range', {
            min: minValue,
            max: maxValue,
          });
          isValid = false;
        }
      }
    });

    form
      .filter((item) => item.type === 'numeric')
      .forEach((item) => {
        const value = values[item.column];
        if (value && isNaN(value)) {
          errors[item.column] = I18n.t('error_nan');
          isValid = false;
        }
      });

    const required = form.filter(
      (item) => item.obl === '1' || item.obl === '3',
    );
    required.forEach(({ column, type }) => {
      const value = values[column];
      if (
        typeof value === 'undefined' ||
        value === '' ||
        value === null ||
        (type === 'file_id' && value.length === 0)
      ) {
        errors[column] = I18n.t('error_required');
        isValid = false;
      }
    });

    const attachedFilesColumns = form.filter((item) => item.type === 'file_id');
    await Promise.all(
      attachedFilesColumns.map(async (item) => {
        if (values[item.column]) {
          await Promise.all(
            values[item.column].map(async (file) => {
              try {
                const { exists } = await FileSystem.getInfoAsync(file.uri);
                if (!exists) {
                  isValid = false;
                  errors[item.column] = I18n.t('missing_file');
                }
              } catch (error) {
                isValid = false;
                errors[item.column] = I18n.t('missing_file');
              }
            }),
          );
        }
      }),
    );

    this.setState({ errors, showErrorBar: !isValid });
    return isValid;
  };

  renderErrorBar = () => {
    const { errors = {}, observation } = this.props;
    const { showErrorBar } = this.state;

    if (observation.hidden) return null;

    if (showErrorBar) {
      // ?
      return (
        <Notification
          notification={I18n.t('invalid_fields')}
          backgroundColor={Colors.flamingo}
        />
      );
    }
    if (errors && errors.errorMessage) {
      // actions/form.js error states
      return (
        <Notification
          notification={errors.errorMessage}
          backgroundColor={Colors.flamingo}
        />
      );
    }
    if (errors && Object.keys(errors).length && typeof errors !== 'string') {
      // normal error states?
      return (
        <Notification
          notification={I18n.t('invalid_fields')}
          backgroundColor={Colors.flamingo}
        />
      );
    }
    if (errors && typeof errors === 'string') {
      // It is happenning on unexpected general errors
      if (errors === REQUEST_ERROR.WRONG_FILE_URI) {
        return (
          <Notification
            notification={I18n.t('wrong_file_uri')}
            backgroundColor={Colors.flamingo}
          />
        );
      }
      return (
        <Notification notification={errors} backgroundColor={Colors.flamingo} />
      );
    }
    return null;
  };

  render() {
    const {
      form,
      getFormRef,
      server,
      database,
      observation,
      showObservationListCommonFields,
      isEditing,
      savedStickinesses,
      lastPickedValues,
    } = this.props;
    const { values, errors, stickinesses } = this.state;
    return (
      <View style={styles.container}>
        {this.renderErrorBar()}
        <Form
          form={form}
          values={values}
          errors={errors || {}}
          onValueChange={this.handleValueChange}
          stickinesses={stickinesses}
          savedStickinesses={savedStickinesses}
          onPinPress={this.handlePinPress}
          getFormRef={(ref) => getFormRef && getFormRef(ref)}
          server={server}
          database={database}
          observation={observation}
          showObservationListCommonFields={showObservationListCommonFields}
          isEditing={isEditing}
          lastPickedValues={lastPickedValues}
        />
      </View>
    );
  }
}

MeasurementForm.propTypes = {
  id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  server: PropTypes.object.isRequired,
  database: PropTypes.object.isRequired,
  observation: PropTypes.object.isRequired,
  form: PropTypes.array.isRequired,
  lastMeasurement: PropTypes.object,
  errors: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
  syncMeasurement: PropTypes.func.isRequired,
  createBackup: PropTypes.func.isRequired,
  onDonePress: PropTypes.func.isRequired,
  navigation: PropTypes.object.isRequired,
  formDataChangeListener: PropTypes.func.isRequired,
  getFormRef: PropTypes.func,
  showObservationListCommonFields: PropTypes.bool,
  isEditing: PropTypes.bool,
  saveStickinesses: PropTypes.func.isRequired,
  savedStickinesses: PropTypes.object,
  lastPickedValues: PropTypes.object,
  values: PropTypes.object,
  onSaveFailed: PropTypes.func,
  createShortcut: PropTypes.func.isRequired,
  removeShortcut: PropTypes.func.isRequired,
};

MeasurementForm.defaultProps = {
  id: null,
  lastMeasurement: {},
  errors: {},
  getFormRef: null,
  showObservationListCommonFields: false,
  isEditing: false,
  savedStickinesses: {},
  lastPickedValues: {},
  values: {},
  onSaveFailed: () => {},
};

const mapDispatchToProps = (dispatch) => {
  const {
    syncMeasurement,
    createBackup,
    saveStickinesses,
    createShortcut,
    removeShortcut,
  } = actions;

  return bindActionCreators(
    {
      syncMeasurement,
      createBackup,
      saveStickinesses,
      createShortcut,
      removeShortcut,
    },
    dispatch,
  );
};

export default connect(null, mapDispatchToProps)(MeasurementForm);

import { StyleSheet } from 'react-native';
import { Colors } from '../../colors';

export const CRings = StyleSheet.create({
  container: {
    width: '100%',
  },
  row: {
    flexDirection: 'row',
    marginLeft: 15,
    borderBottomColor: '#e5e5e5',
    borderBottomWidth: 1,
    paddingVertical: 5,
    paddingRight: 20,
    paddingLeft: 0,
    alignItems: 'center',
    width: '100%',
  },
  text: {
    flex: 1,
  },
});

export const DatePicker = StyleSheet.create({
  container: {
    height: 50,
    flex: 1,
    flexDirection: 'row',
    marginHorizontal: 15,
    alignItems: 'center',
  },
  field: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    padding: 12,
    backgroundColor: Colors.dirtyWhite,
    borderRadius: 5,
    marginRight: 4,
    borderColor: Colors.mantisGreen,
    borderWidth: 3,
  },
  text: {
    fontSize: 16,
  },
  buttonContainer: {
    marginLeft: 8,
  },
  error: {
    backgroundColor: Colors.opacFlamingo,
    borderColor: Colors.flamingo,
  },
  obligatory: {
    borderColor: Colors.brickRed,
  },
});

export const GeometryTypeChooser = StyleSheet.create({
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    padding: 5,
    paddingHorizontal: 10,
  },
  buttonStyle: {
    flex: 1,
    borderWidth: 1.3,
    borderColor: 'black',
    borderRadius: 5,
    justifyContent: 'center',
    padding: 5,
    margin: 5,
    backgroundColor: Colors.dirtyWhite,
  },
  buttonTextStyle: {
    textAlign: 'center',
  },
  simpleTextStyle: {
    paddingStart: 20,
  },
  warnTextStyle: {
    color: Colors.brickRed,
  },
  selectedButtonStyle: {
    borderColor: Colors.mantisGreen,
    backgroundColor: Colors.mantisGreen,
  },
});

export const PositionInput = StyleSheet.create({
  container: {
    height: 50,
    flex: 1,
    flexDirection: 'row',
    marginHorizontal: 15,
    alignItems: 'center',
  },
  field: {
    flex: 1,
    padding: 12,
    backgroundColor: Colors.dirtyWhite,
    borderRadius: 5,
    marginRight: 4,
    borderColor: Colors.mantisGreen,
    borderWidth: 3,
  },
  pressed: {
    backgroundColor: Colors.opacMantisGreen,
  },
  text: {
    fontSize: 16,
  },
  buttonContainer: {
    marginLeft: 8,
  },
  error: {
    backgroundColor: Colors.opacFlamingo,
    borderColor: Colors.flamingo,
  },
  obligatory: {
    borderColor: Colors.brickRed,
  },
  updateIntervalContainer: {
    position: 'absolute',
    top: -35,
    right: 0,
    height: 25,
    justifyContent: 'flex-start',
    flexDirection: 'row',
  },
});

export const FormInput = StyleSheet.create({
  container: {
    height: 50,
    flex: 1,
    flexDirection: 'row',
    marginHorizontal: 15,
    alignItems: 'stretch',
  },
  field: {
    flex: 2,
    padding: 12,
    borderRadius: 5,
    borderColor: Colors.mantisGreen,
    borderWidth: 3,
    fontSize: 16,
  },
  buttonContainer: {
    flex: 1,
    marginLeft: 8,
    borderWidth: 1.3,
    borderColor: 'black',
    borderRadius: 5,
    justifyContent: 'center',
  },
  error: {
    backgroundColor: Colors.opacFlamingo,
    borderColor: Colors.flamingo,
  },
  obligatory: {
    borderColor: Colors.brickRed,
  },
});

export const FilePicker = StyleSheet.create({
  container: {
    backgroundColor: Colors.dirtyWhite,
    padding: 12,
    marginHorizontal: 15,
    borderRadius: 5,
    borderColor: Colors.mantisGreen,
    borderWidth: 3,
  },
  button: {
    marginLeft: 10,
  },
  buttonContainer: {
    marginTop: 15,
    flexDirection: 'row',
    alignSelf: 'flex-end',
  },
  error: {
    backgroundColor: Colors.opacFlamingo,
    borderColor: Colors.flamingo,
  },
  obligatory: {
    borderColor: Colors.brickRed,
  },
});

export const FilePickerListItem = StyleSheet.create({
  container: {
    height: 200,
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
    marginTop: 3,
  },
  placeHolderContainer: {
    height: 100,
    borderColor: Colors.flamingo,
    borderWidth: 1,
    borderRadius: 10,
    padding: 5,
    backgroundColor: Colors.gray,
    marginTop: 3,
  },
  fileContainer: {
    height: 100,
    borderWidth: 1,
    borderRadius: 10,
    padding: 5,
    backgroundColor: Colors.mantisGreen,
    marginTop: 3,
  },
  removeButton: {
    position: 'absolute',
    bottom: 10,
    right: 10,
    padding: 5,
    borderRadius: 5,
    backgroundColor: 'rgba(255,255,255,.4)',
  },
  removeText: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#F4FAFE',
  },
  image: {
    borderRadius: 10,
    width: '100%',
    height: '100%',
  },
});

export const MeasurementForm = StyleSheet.create({
  container: {
    paddingBottom: 45,
    width: '100%',
  },
});

export const MeasurementFormListItem = StyleSheet.create({
  row: {
    marginHorizontal: 20,
    marginVertical: 6,
    flexDirection: 'row',
    alignItems: 'center',
  },
  textInput: {
    marginHorizontal: 20,
  },
  stickyButton: {
    padding: 5,
  },
  rotated: {
    transform: [{ rotate: '-45deg' }],
  },
  text: {
    fontSize: 16,
  },
  error: {
    fontSize: 16,
    color: '#f44242',
  },
  errorContainer: {
    marginHorizontal: 20,
    marginTop: 6,
  },
  requiredContainer: {
    height: 12,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  requiredText: {
    fontSize: 12,
    fontWeight: 'bold',
  },
  hidden: {
    display: 'none',
  },
});

export const List = StyleSheet.create({
  container: {
    minHeight: 50,
    backgroundColor: Colors.dirtyWhite,
    padding: 10,
    marginHorizontal: 15,
    borderRadius: 5,
    borderColor: Colors.mantisGreen,
    borderWidth: 3,
  },
  error: {
    backgroundColor: Colors.opacFlamingo,
    borderColor: Colors.flamingo,
  },
  obligatory: {
    borderColor: Colors.brickRed,
  },
});

export const AutoComplete = StyleSheet.create({
  container: {
    minHeight: 50,
    backgroundColor: Colors.dirtyWhite,
    padding: 10,
    marginHorizontal: 15,
    borderRadius: 5,
    borderColor: Colors.mantisGreen,
    borderWidth: 3,
    flexDirection: 'row',
    alignItems: 'center',
  },
  header: {
    padding: 15,
    elevation: 3,
    backgroundColor: '#FAFAFA',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  listItem: {
    borderBottomColor: 'lightgrey',
    borderBottomWidth: 1,
    padding: 15,
    marginLeft: 10,
  },
  multiButton: {
    justifyContent: 'center',
    margin: 5,
    flex: 1,
  },
  button: {
    marginHorizontal: 5,
    justifyContent: 'center',
  },
  backspaceButton: {
    marginRight: 8,
  },
  closeButton: {
    padding: 8,
    color: Colors.brickRed,
    borderRadius: 5,
    backgroundColor: Colors.opacBrickRed,
  },
  tagList: {
    flex: 14,
  },
  roundedButton: {
    flex: 0,
    borderWidth: 1.3,
    borderColor: Colors.gray,
    borderRadius: 5,
    justifyContent: 'center',
    paddingHorizontal: 6,
    marginStart: 10,
  },
  text: {
    fontSize: 16,
    flex: 1,
  },
  error: {
    backgroundColor: Colors.opacFlamingo,
    borderColor: Colors.flamingo,
  },
  obligatory: {
    borderColor: Colors.brickRed,
  },
  scrollView: {
    marginTop: 8,
  },
});

export const Boolean = StyleSheet.create({
  container: {
    marginHorizontal: 20,
    marginVertical: 6,
    borderColor: Colors.mantisGreen,
    borderWidth: 3,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 3,
  },
  icon: {
    marginRight: 15,
  },
  error: {
    borderColor: Colors.flamingo,
    borderWidth: 3,
  },
});

export const FormContainer = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  timerStyle: {
    fontSize: 14,
    color: 'black',
    textAlign: 'center',
  },
});

export const UnfoldingList = StyleSheet.create({
  container: {
    marginVertical: 10,
    marginHorizontal: 15,
  },
  containerWithButton: {
    minHeight: 50,
    flex: 1,
    flexDirection: 'row',
  },
  rowButton: {
    marginHorizontal: 5,
    borderWidth: 1.3,
    borderColor: 'black',
    borderRadius: 5,
    marginVertical: 5,
  },
  valueContainer: {
    backgroundColor: Colors.dirtyWhite,
    padding: 10,
    flexGrow: 1,
    marginVertical: 5,
    borderRadius: 5,
    borderColor: Colors.mantisGreen,
    borderWidth: 3,
    flexDirection: 'row',
    alignItems: 'center',
  },
});

import React, { useEffect, useState } from 'react';
import {
  View,
  TouchableWithoutFeedback,
  Text,
  TouchableOpacity,
} from 'react-native';
import * as Animatable from 'react-native-animatable';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import PropTypes from 'prop-types';
import moment from 'moment';
import { DatePicker as styles } from './styles';
import { Colors } from '../../colors';
import { TouchableIcon } from '..';

const DateTimePicker = ({
  value,
  onValueChange,
  dateOnly,
  timeOnly,
  error,
  obligatory,
  edit = false,
}) => {
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
  const [isRecording, setIsRecording] = useState(!value);

  const setInit = () => {
    if (edit) {
      setIsRecording(false);
    } else {
      if (dateOnly) {
        setIsRecording(false);
        onValueChange(new Date());
      } else if (!dateOnly) {
        setIsRecording(!value);
      }
    }
  };

  useEffect(() => {
    setInit();
  }, []);

  const showDatePicker = () => {
    setDatePickerVisibility(true);
    setIsRecording(false);
  };

  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };

  const handleConfirm = (date) => {
    hideDatePicker();
    onValueChange(date);
  };

  const getFormattedDateTime = (value, dateOnly, timeOnly) => {
    if (!value) {
      return '-';
    }
    const date = moment(value).format('YYYY.MM.DD');
    const time = moment(value).format('HH:mm:ss');
    if (dateOnly) {
      return date;
    }
    if (timeOnly) {
      return time;
    }
    return `${date} ${time}`;
  };

  const getIconMeta = (dateOnly, timeOnly) => {
    if (dateOnly) return { iconName: 'event' };
    if (timeOnly) return { iconName: 'access-time' };
    return { iconName: 'calendar-clock', iconType: 'MaterialCommunityIcon' };
  };

  const { iconName, iconType } = getIconMeta(dateOnly, timeOnly);

  return (
    <View style={styles.container}>
      <TouchableWithoutFeedback onPress={showDatePicker}>
        <View
          style={[
            styles.field,
            obligatory && styles.obligatory,
            error && styles.error,
          ]}
        >
          {isRecording ? (
            <Animatable.View
              animation={isRecording ? 'fadeIn' : undefined}
              iterationCount="infinite"
              direction="alternate"
              duration={500}
              useNativeDriver
            >
              <TouchableIcon
                style={styles.buttonContainer}
                name="update"
                size={22}
                color={Colors.mantisGreen}
                onPress={() => {
                  setIsRecording(false);
                  onValueChange(new Date());
                }}
              />
            </Animatable.View>
          ) : (
            <Text style={styles.text}>
              {getFormattedDateTime(value, dateOnly, timeOnly)}
            </Text>
          )}
        </View>
      </TouchableWithoutFeedback>
      <TouchableIcon
        style={styles.buttonContainer}
        onPress={() => {
          setIsRecording(false);
          onValueChange(new Date());
        }}
        iconType={iconType}
        name={iconName}
        size={30}
        color={Colors.darkGray}
      />
      {/* TODO locale */}
      <DateTimePickerModal
        isVisible={isDatePickerVisible}
        mode={dateOnly ? 'date' : timeOnly ? 'time' : 'datetime'}
        is24Hour
        locale="hu_HU"
        onConfirm={handleConfirm}
        onCancel={hideDatePicker}
      />
    </View>
  );
};

DateTimePicker.propTypes = {
  value: PropTypes.oneOfType([
    PropTypes.instanceOf(Date),
    PropTypes.string,
    PropTypes.number,
  ]),
  onValueChange: PropTypes.func.isRequired,
  dateOnly: PropTypes.bool,
  timeOnly: PropTypes.bool,
  error: PropTypes.string,
  obligatory: PropTypes.bool,
};

DateTimePicker.defaultProps = {
  value: null,
  dateOnly: false,
  timeOnly: false,
  error: null,
  obligatory: false,
};

export default DateTimePicker;

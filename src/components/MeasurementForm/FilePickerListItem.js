import React from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import PropTypes from 'prop-types';
import I18n from '../../i18n';
import { FilePickerListItem as styles } from './styles';

const renderFileComponent = (uri, fileName, type) => {
  if (uri) {
    if (type.split('/')[0] === 'image') {
      return (<Image style={styles.image} source={{ uri }} />);
    }
    return (<Text>{`${fileName}`}</Text>);
  }
  return (<Text>{`${fileName} ${I18n.t('missing')}`}</Text>);
};

const isImage = (uri, type) => uri && type.split('/')[0] === 'image';

const itemStyle = (uri, type) => {
  switch (true) {
    case isImage(uri, type): return styles.container;
    case uri !== null: return styles.fileContainer;
    default: return styles.placeHolderContainer;
  }
};

const FilePickerListItem = ({
  id,
  uri,
  fileName,
  type,
  onRemove,
}) => (
  <View style={itemStyle(uri, type)}>
    {renderFileComponent(uri, fileName, type)}
    <TouchableOpacity
      style={styles.removeButton}
      onPress={() => onRemove(id)}
    >
      <Text style={styles.removeText}>{I18n.t('remove')}</Text>
    </TouchableOpacity>
  </View>
);

FilePickerListItem.propTypes = {
  id: PropTypes.number.isRequired,
  uri: PropTypes.string,
  type: PropTypes.string.isRequired,
  fileName: PropTypes.string.isRequired,
  onRemove: PropTypes.func.isRequired,
};

FilePickerListItem.defaultProps = {
  uri: null,
};

export default FilePickerListItem;

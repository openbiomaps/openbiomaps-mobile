import React, { Component, Fragment } from 'react';
import {
  View,
  Text,
  Modal,
  TextInput,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import PropTypes from 'prop-types';
import ListItem from './AutoCompleteListItem';
import { AutoComplete as styles } from './styles';
import { TouchableIcon } from '../TouchableIcon';
import { Colors } from '../../colors';
import TagList from '../TagList';
import { removeAccents } from '../../utils';

class AutoCompleteList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      searchValue: '',
    };
  }

  getValues = () => {
    return this.props.value.reduce((acc, curr) => {
      if (acc === '') return acc + curr;
      return `${acc}, ${curr}`;
    }, '');
  };

  handlePress = () => {
    this.toggleModal(false);
  };

  handleModalShow = () => {
    this.textInput.focus();
  };

  handleAddItem = (value) => {
    const valueArray = [...this.props.value];
    if (!valueArray.includes(value)) valueArray.push(value);
    this.props.onValueChange(valueArray);
  };

  handleRemoveItem = (value) => {
    const valueArray = [...this.props.value];
    const filteredArray = valueArray.filter((item) => item !== value);
    this.props.onValueChange(filteredArray);
  };

  handleChangeText = (textValue) => {
    const valueArray = [...this.props.value];
    if (!valueArray.includes(textValue)) valueArray.push(textValue);
    this.props.onValueChange(valueArray);
  };

  toggleModal = (close) => {
    if (!close) {
      if (this.state.searchValue) {
        this.handleAddItem(this.state.searchValue);
      }
    }
    this.setState({ visible: !this.state.visible, searchValue: '' });
  };

  handleDeletePress = () => {
    this.setState({ searchValue: '' });
    this.props.onValueChange(); // calling this with empty attr. resets the values to undefined, and deafult values will be shown
  };

  render() {
    const { value, error, obligatory, data, lastPickedData, shortName } =
      this.props;
    const { searchValue } = this.state;
    const searchValueWithoutAccents = removeAccents(searchValue);
    const list =
      searchValueWithoutAccents.length > 0
        ? data
            .filter(
              (item) =>
                item &&
                removeAccents(item)
                  .toLowerCase()
                  .startsWith(searchValueWithoutAccents.toLowerCase()),
            )
            .slice(0, 10)
            .concat(
              data
                .filter(
                  (item) =>
                    item &&
                    removeAccents(item)
                      .toLowerCase()
                      .includes(searchValueWithoutAccents.toLowerCase()),
                )
                .slice(0, 20),
            )
            .filter((v, i, a) => a.indexOf(v) === i)
        : [];

    return (
      <Fragment>
        <TouchableOpacity
          onPress={this.handlePress}
          style={[
            styles.container,
            obligatory && styles.obligatory,
            error && styles.error,
          ]}
          activeOpacity={1}
        >
          <Text style={styles.text}>{this.getValues()}</Text>
          <TouchableIcon
            name="search"
            color={Colors.darkGray}
            size={24}
            disabled
          />
        </TouchableOpacity>

        <Modal
          presentationStyle="pageSheet"
          visible={this.state.visible}
          onRequestClose={() => this.toggleModal(true)}
          animationType="slide"
          transparent={false}
          onShow={this.handleModalShow}
        >
          <View
            style={{
              paddingHorizontal: 15,
              paddingTop: 10,
              elevation: 3,
              backgroundColor: '#FAFAFA',
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}
          >
            <Text style={{ fontWeight: 'bold', fontSize: 20 }}>
              {shortName}
            </Text>
            <TouchableOpacity
              onPress={() => this.toggleModal(true)}
              style={[styles.button, styles.closeButton]}
            >
              <TouchableIcon
                name="close"
                color={Colors.brickRed}
                size={20}
                onPress={() => this.toggleModal(true)}
              />
            </TouchableOpacity>
          </View>
          <View style={styles.header}>
            <TextInput
              ref={(input) => {
                this.textInput = input;
              }}
              value={searchValue}
              onChangeText={(searchValue) => this.setState({ searchValue })}
              blurOnSubmit
              onSubmitEditing={() => this.toggleModal(false)}
              style={styles.text}
            />
            {searchValue !== '' && (
              <TouchableIcon
                name="backspace"
                color={Colors.darkGray}
                size={20}
                style={[styles.button, styles.backspaceButton]}
                onPress={this.handleDeletePress}
              />
            )}
          </View>

          <TagList
            style={{ marginVertical: 10, marginHorizontal: 15 }}
            data={value}
            onItemPress={this.handleRemoveItem}
          />

          <FlatList
            keyboardShouldPersistTaps="handled"
            data={searchValue === '' ? lastPickedData : list}
            style={styles.scrollView}
            keyExtractor={(item) => item}
            renderItem={({ item }) => (
              <ListItem
                key={item}
                value={item}
                onPress={() => {
                  this.setState(
                    {
                      searchValue: '',
                    },
                    () => this.handleAddItem(item),
                  );
                }}
              />
            )}
          />
        </Modal>
      </Fragment>
    );
  }
}

AutoCompleteList.propTypes = {
  shortName: PropTypes.string,
  data: PropTypes.array.isRequired,
  value: PropTypes.array,
  onValueChange: PropTypes.func.isRequired,
  error: PropTypes.string,
  obligatory: PropTypes.bool,
  lastPickedData: PropTypes.array,
};

AutoCompleteList.defaultProps = {
  value: [],
  error: null,
  obligatory: false,
  lastPickedData: [],
};

export default AutoCompleteList;

import { StyleSheet, Dimensions } from 'react-native';

export default StyleSheet.create({
  baseContainer: {
    
    justifyContent: 'flex-end',
    flexDirection: 'column',
    marginBottom: 27,
    alignItems: 'center',
  },
  baseLine: {
    width: Dimensions.get('window').width * 0.5,
    height: Dimensions.get('window').width * 0.05,
    borderColor: 'white',
    borderLeftWidth: Dimensions.get('window').height * 0.005,
    borderRightWidth: Dimensions.get('window').height * 0.005,
    borderBottomWidth: Dimensions.get('window').height * 0.005,
  },
  label: {
    fontSize: 20,
    color: 'white',
  },
});

import React from 'react';
import { View, Text } from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';

const haversineDistance = ({ latitude: lat, longitude: lon1, longitudeDelta }, rotation) => {
  const earthRadiusInMeters = 6371e3;

  const lon2 = lon1 + (longitudeDelta / 2); 
  const latRad = lat * (Math.PI / 180);
  const deltaLongRad = (lon2 - lon1) * (Math.PI / 180);

  const squareOfHalfChordLength = Math.cos(latRad) * Math.cos(latRad) *
          Math.sin(deltaLongRad / 2) * Math.sin(deltaLongRad / 2);
  const angularDistanceRad = 2 * Math.atan2(Math.sqrt(squareOfHalfChordLength), Math.sqrt(1 - squareOfHalfChordLength));

  const distanceInMeters = earthRadiusInMeters * angularDistanceRad; // in metres, but with considering no rotation. (with 0 rotation, the half of the screen)

  if (rotation === 0) return distanceInMeters >= 1000 ? `≈${(distanceInMeters / 1000).toFixed(2)} km` : `≈${distanceInMeters.toFixed(2)} m`;
  return 'inaccurate';
};

const ZoomScale = ({ region, rotation }) => (
  <View style={styles.baseContainer} >
    <Text style={styles.label}>{haversineDistance(region, rotation)}</Text>
    <View style={styles.baseLine} />
  </View>
);
  
ZoomScale.propTypes = {
  region: PropTypes.object,
  rotation: PropTypes.number,
};

ZoomScale.defaultProps = {
  region: null,
  rotation: 0,
};
  
export default ZoomScale;

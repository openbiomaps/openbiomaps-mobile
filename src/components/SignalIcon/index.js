import React from 'react';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { footerStyle } from '../Footer/styles';

const getIconName = ({ netStatus }) => {
  if (netStatus !== 'none' && netStatus !== 'unknown') return 'cloud';
  return 'cloud-off';
};

const SignalIcon = props =>
  <Icon name={getIconName(props)} size={24} color="white" style={footerStyle.leftButton} />;

const mapStateToProps = ({ network }) => ({
  netStatus: network.status ? network.status : 'none',
});

export default connect(mapStateToProps)(SignalIcon);

import React, { Component } from 'react';
import { View, TouchableOpacity, Text, ActivityIndicator, Linking, AppState } from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from '../../actions';
import { styles } from './styles';
import { TouchableIcon } from '../TouchableIcon';
import I18n from '../../i18n';

class MessageView extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showDetail: false,
      currentAppState: AppState.currentState,
    };
    this.appStateChangeListener = null;
  }

  componentDidMount = () => {
    this.appStateChangeListener = AppState.addEventListener('change', this._handleAppStateChange);
  }

  componentWillUnmount = () => {
    this.appStateChangeListener?.remove();
  }

  _handleAppStateChange = (nextAppState) => {
    const { currentAppState } = this.state;
    if (
      currentAppState.match(/inactive|background/)
      && nextAppState === 'active'
    ) {
      this.setState({
        currentAppState: nextAppState,
      });
      this.loadMessages();
    } else if (currentAppState.match(/active/)
      && nextAppState === 'background'
    ) {
      this.setState({
        currentAppState: nextAppState,
        showDetail: false,
      });
    }
  };

  loadMessages = () => {
    const {
      previouslySelectedDatabaseList,
      netStatus,
      loadMessages,
    } = this.props;
    if (['none', 'unknown'].includes(netStatus)) return;

    loadMessages(previouslySelectedDatabaseList);
  }

  onExpandChange = () => {
    this.setState((state) => ({ showDetail: !state.showDetail }));
  }

  _goToProject = async (url) => {
    const messagesUrl = `${url}/profile/messages/`;
    const rightUrl = await Linking.canOpenURL(messagesUrl);

    if (rightUrl) {
      await Linking.openURL(messagesUrl);
    }
  }

  _generateMessageText = (label, count) => (count > 0 ? `${I18n.t(label)}: ${count}` : '');

  renderDetails = () => {
    const { previouslySelectedDatabaseList, messages } = this.props;

    if (previouslySelectedDatabaseList.length > 0) {
      return (
        <View>
          {messages.map(({ databaseName, userName, personalMessage, commentNotification, systemMessage, onlyEmail, projectUrl }) => (
            <TouchableOpacity onPress={() => this._goToProject(projectUrl)} key={databaseName} style={styles.detailItemContainer}>
              <Text style={styles.textStyle}>
                {`${databaseName} (${userName.split('@')[0]}):\n`
                  + `${this._generateMessageText('personal_messages', personalMessage)}  `
                  + `${this._generateMessageText('comment_notifications', commentNotification)}  `
                  + `${this._generateMessageText('system_messages', systemMessage)}  `
                  + `${this._generateMessageText('only_email', onlyEmail)}`}
              </Text>
            </TouchableOpacity>
          ))}
        </View>
      );
    }
    return null;
  }

  render() {
    const { showDetail } = this.state;
    const { fetching, messages } = this.props;

    if (fetching) {
      return (
        <View style={styles.container}>
          <ActivityIndicator size="small" color="white" style={{ padding: 5 }} />
        </View>
      );
    }
    return (
      <View style={styles.container}>
        <TouchableOpacity onPress={this.onExpandChange}>
          <View style={styles.mainItem}>
            <Text style={styles.textStyle}>{messages.length > 0 ? I18n.t('message_title') : I18n.t('no_messages_title')}</Text>
            {messages.length > 0
              && <TouchableIcon name={showDetail ? 'arrow-drop-up' : 'arrow-drop-down'} disabled color="white" />}
          </View>
        </TouchableOpacity>
        {showDetail && this.renderDetails()}
      </View>
    );
  }
}

const mapStateToProps = ({ network: { status: netStatus }, messages: { messages, fetching } }) => ({ netStatus, messages, fetching });

const mapDispatchToProps = (dispatch) => {
  const { loadMessages } = actions;

  return bindActionCreators(
    {
      loadMessages,
    },
    dispatch,
  );
};

MessageView.propTypes = {
  previouslySelectedDatabaseList: PropTypes.array.isRequired,
  netStatus: PropTypes.string,
  messages: PropTypes.array.isRequired,
  fetching: PropTypes.bool.isRequired,
  loadMessages: PropTypes.func.isRequired,
};

MessageView.defaultProps = {
  netStatus: 'unknown',
};

export default connect(mapStateToProps, mapDispatchToProps)(MessageView);

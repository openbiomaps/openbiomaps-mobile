import { StyleSheet } from 'react-native';
import { Colors } from '../../colors';

export const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    backgroundColor: Colors.messageBrown,
    borderBottomEndRadius: 10,
    borderBottomStartRadius: 10,
    padding: 5,
    minHeight: 45,
  },
  mainItem: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  textStyle: {
    color: 'white',
    fontSize: 14,
    textAlign: 'center',
  },
  detailItemContainer: {
    flex: 1,
    padding: 5,
    marginVertical: 5,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: 'white',
  },
});

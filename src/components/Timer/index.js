import React, { Component } from 'react';
import { Text } from 'react-native';
import * as Animatable from 'react-native-animatable';
import PropTypes from 'prop-types';
import styles from '../Notification/styles';

class Timer extends Component {
  // eslint-disable-next-line
  _listTimer;

  constructor(props) { 
    super(props);

    this.state = {
      time: null,
    };

    clearInterval(this._listTimer);
    this._listTimer = setInterval(this.calculateTime, 1000); 
  }

  componentDidMount = () => {
    this.calculateTime();
  }

  componentWillUnmount = () => {
    clearInterval(this._listTimer);
  }

  _generateTimeFormat = diff => `${diff.getUTCHours()}h : ${diff.getUTCMinutes()}m : ${diff.getUTCSeconds()}s`;

  calculateTime = () => {
    const { startedAt, showTimer } = this.props;

    const diff = new Date().getTime() - new Date(startedAt).getTime();
    const dateDiff = new Date(diff);
    if (!showTimer) {
      clearInterval(this._listTimer);
      this.setState({
        time: null,
      });
    } else {
      this.setState({
        time: this._generateTimeFormat(dateDiff),
      });
    }
  }

  render = () => {
    const { time } = this.state; 
    const { label, showTimer } = this.props;
    
    return (
      <Animatable.View
        style={styles.baseContainerStyle}
        animation="bounceIn"
        useNativeDriver
      >
        <Text style={styles.textStyle}>{`${label} ${showTimer ? `[${time}]` : ''}`}</Text>
      </Animatable.View>);
  }
}
   
Timer.propTypes = {
  startedAt: PropTypes.number,
  showTimer: PropTypes.bool,
  label: PropTypes.string,
};
  
Timer.defaultProps = {
  startedAt: null,
  showTimer: true,
  label: '',
};
    
export default Timer;

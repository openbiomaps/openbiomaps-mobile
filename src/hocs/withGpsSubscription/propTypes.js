import PropTypes from 'prop-types';

export const GpsPropTypes = PropTypes.shape({
  isObserving: PropTypes.bool.isRequired,
  position: PropTypes.shape({
    longitude: PropTypes.number.isRequired,
    latitude: PropTypes.number.isRequired,
    accuracy: PropTypes.number.isRequired,
  }),
  error: PropTypes.shape({
    code: PropTypes.number.isRequired,
    message: PropTypes.string.isRequired,
  }),
  subscribe: PropTypes.func.isRequired,
  unsubscribe: PropTypes.func.isRequired,
});

export const GpsDefaultPropTypes = {
  position: null,
  error: null,
};

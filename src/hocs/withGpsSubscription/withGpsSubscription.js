import React from 'react';

import { useGpsSubscription } from '../../utils/useGpsSubscription';

const withGpsSubscription = (WrappedComponent, getOptions) => {
  const WithGpsHOC = (props) => {
    const options = getOptions(props);

    const { 
      isObserving, 
      position, 
      error, 
      subscribe, 
      unsubscribe, 
    } = useGpsSubscription(options);

    return (
      <WrappedComponent
        // eslint-disable-next-line react/jsx-props-no-spreading
        {...props}
        gps={{ 
          isObserving,
          position,
          error,
          subscribe,
          unsubscribe,
        }}
      />
    );
  };

  return WithGpsHOC;
};

export default withGpsSubscription;

import { Component } from 'react';
import NetInfo from '@react-native-community/netinfo';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from './actions';
import { isAccessTokenExpired } from './utils';
import ClientLogger, { ClientLoggerUseCases } from './api/ClientLogger';

class NetworkHandler extends Component {
  constructor(props) {
    super(props);

    this.state = {
      type: null,
    };

    this.networkListener = null;
  }

  componentDidMount = () => {
    this.networkListener = NetInfo.addEventListener(this.onNetworkChange);
  }

  componentWillUnmount = () => {
    // Unsubscribe
    this.networkListener();
  }

  onNetworkChange = ({ type }) => {
    const {
      serverList, 
      changeNetworkStatus, 
      syncMeasurements, 
      syncTrackLogs,
      tokenRefresh,
    } = this.props;

    // Network listener triggers multiple times, hence the underlying logic to prevent that.
    this.setState((prevState) => {
      if (prevState.type !== type) {
        changeNetworkStatus(type);

        if (type !== 'none' && type !== 'unknown' && prevState.type) {
          serverList.forEach((server) => {
            const { user: { requestTime = null, expiresIn = null, refreshToken = null } = {} } = server;
            if (requestTime && expiresIn && refreshToken) {
              if (!isAccessTokenExpired({ requestTime, expiresIn })) {
                syncMeasurements(server.id);
                syncTrackLogs(server.id);
              } else {
                ClientLogger.log(ClientLoggerUseCases.NETWORK_HANDLER_REFRESH);
                tokenRefresh(server.id);
              }
            }
          });
        }
        
        return { type };
      }
      return { prevState };
    });
  };

  render() {
    return null;
  }
}

NetworkHandler.propTypes = {
  changeNetworkStatus: PropTypes.func.isRequired,
  syncMeasurements: PropTypes.func.isRequired,
  syncTrackLogs: PropTypes.func.isRequired,
  tokenRefresh: PropTypes.func.isRequired,
  serverList: PropTypes.arrayOf(PropTypes.object).isRequired,
};

const mapStateToProps = ({ servers: { data: serverList } }) => ({
  serverList,
});

const mapDispatchToProps = (dispatch) => {
  const { changeNetworkStatus, syncMeasurements, syncTrackLogs, tokenRefresh } = actions;

  return bindActionCreators(
    {
      changeNetworkStatus,
      syncMeasurements,
      syncTrackLogs,
      tokenRefresh,
    },
    dispatch,
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(NetworkHandler);

class StateFinder {
  constructor(servers) {
    this._value = servers;
  }

  get value() {
    return this._value;
  }

  getServer(serverId) {
    this._value = this._value.find(server => server.id === serverId);
    return this;
  }

  getDatabase(databaseId) {
    if (this._value.databases && this._value.databases.data) {
      this._value = this._value.databases.data.find(database => database.id === databaseId);
    }
    return this;
  }

  getObservation(observationId) {
    if (this._value.observations && this._value.observations.data) {
      this._value = this._value.observations.data.find(observation => observation.id === observationId);
    }
    return this;
  }
}

export default StateFinder;

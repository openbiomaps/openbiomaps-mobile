export default {
  username: 'Email',
  password: 'Password',
  login: 'Login',
  login_error: 'Error occurred during the authentication process',
  empty_fields: 'All fields are required',
  invalid_username_password: 'Invalid username or password',
  database_screen_title: 'Select project',
  observation_screen_title: 'Select form',
  settings_online_profile: 'Online profile settings',
  gps_timeout: 'Connecting to GPS…',
  gps_unavailable: 'GPS signal is not available.',
  gps_disabled_hint: 'Location permission is required in order to be able to use the application properly.',
  error: 'Error',
  position_input_hint: 'Where is the location of your observation?\n' +
    'Press long on the selected location.',
  form: 'Form',
  error_text_out_of_range: 'The text should be between {{min}} and {{max}} characters in length',
  error_number_out_of_range: 'The number should be between {{min}} and {{max}}',
  error_required: 'Missing required field',
  error_nan: 'The provided value is not a number.',
  bool_true: 'Yes',
  bool_false: 'No',
  task_screen_title: 'Task',
  questions_screen_title: 'Questions',
  game_observation: 'Game observation',
  player: 'player',
  score: 'score',
  toplist: 'toplist',
  game: 'game',
  quiz_error: 'One or more answers are incorrect.',
  measurements: 'Observations',
  measurement_nonvalid: 'Incorrect',
  measurement_syncing: 'Syncing',
  measurement_pending: 'Waiting for upload',
  measurement_uploaded: 'Uploaded',
  remove: 'remove',
  image_picker_title: 'Take a photo or choose one from the gallery.',
  cancel: 'Cancel',
  camera: 'Camera',
  gallery: 'Gallery',
  measurement_updated_message: 'Record updated.',
  latest_tab: 'Latest',
  offline_available_tab: 'Downloaded',
  all_tab: 'All',
  missing_form_data: 'Failed to load form data.',
  invalid_fields: 'Check the fields with red background!',
  empty_list: 'Data is not available.',
  needs_net: 'Network connection is required for loading the data.',
  image_load_failed: 'Failed to load image.',
  image_needs_net: 'Network connection is required for loading the image.',
  img_loading: 'Loading image…',
  measurement_under_editing: 'Editing',
  ok: 'OK',
  form_item: 'Form {{count}}',
  collect_data: 'Forms',
  collected_data: 'Recorded data',
  map: 'Map',
  settings: 'Settings',
  export: 'Export',
  servers: 'Servers',
  choose_server: 'Select server',
  no_previous_measurements: 'Previous records are not available',
  location_permission_denied: 'Location permission denied.',
  no_previous_observation_form: 'Form {{count}} is not available.',
  successful_form_creation: 'Successful data recording.',
  create_new_form: 'Would you like to record a new observation?',
  have_unsaved_changes: 'You have unsaved changes.',
  do_you_want_to_exit: 'Do you really want to exit?',
  auto_backup: 'Automatic backup',
  restore_backup: 'Restoring backup',
  backup_confirm_title: 'Are you sure about restoring data to a previous version?',
  restore: 'Restore',
  no_backup: 'Previous backup version is not available. Would you like to search for backups in the filesystem manually?',
  invalid_refresh_token: 'You are not logged in.',
  uncertain_location: 'Inaccurate position',
  no_gps: 'GPS signal is not available',
  change_project: 'Change project',
  change_server: 'Change server',
  fixed_position: 'Fixed position',
  synced: 'Synced',
  unsynced: 'Unsynced',
  no_synced_data: 'No synced data.',
  no_unsynced_data: 'No unsynced data.',
  date: 'date',
  no_field_data: 'There is not any recorded data!',
  delete_dialog_title: 'Are you sure about deleting this observation?',
  delete_dialog_message: 'Data already uploaded to the server will still be available there.',
  no_connection: 'Network connection is not available.',
  sync_requires_net: 'Network is needed for data synchronization.',
  backup: 'Backup',
  sync: 'Synchronization',
  auto_sync: 'Automatic synchronization',
  form_is_not_available: 'Form is not available on the server!',
  about: 'About',
  version: 'Version',
  list_creation_in_progress: 'Session recording is in progress',
  general: 'General',
  language: 'Language',
  sync_with_wifi: 'Syncing data automatically with wifi connection.',
  map_engine: 'Map engine',
  do_you_want_to_save_the_list: 'Do you really want to save the session?',
  no_data_recorded_in_the_list: "No data was recorded during the observation. Didn't observe anything during the monitoring or don't want to save the session?",
  record_empty_observation: "Didn't observe anything",
  do_not_save_the_list: "Don't want to save the session",
  lot_of_unsynced_measurements: 'You have a lot of unsynced observations. Please sync them as soon as possible!',
  has_measurement_older_than_one_week: 'You have unsynced observations older than one week. Please sync them as soon as possible!',
  fiel_common_fields: 'Fill in common session fields.',
  successful_list_creation: 'Successful session recording.',
  logout: 'Logout',
  logout_dialog_title: 'Are you sure you want to log out?',
  logout_dialog_message: 'You have unsynced data on the server.',
  logout_success: 'Logout success',
  logout_success_from: 'You have successfully logged out from the following server: {{server}}',
  null_record: 'null record',
  create_null_record: 'Create null record',
  create_null_record_instruction: 'In this case please create a null record. Write "null" into the required text fields and "0" into the required number fields, furthermore, provide appropriate data into the other fields where it is necessary then save the observation.',
  not_enough_memory: 'There is not enough memory to perform the operation. Synchronize your measurements to free up memory!',
  critical_memory_usage: 'Not enough memory available, please sync your measurements!',
  internet_connection: 'Connected to the internet',
  time_spent: 'Elapsed time',
  delete_all: 'Delete all',
  no_or_weak_network: 'You are offline, or your connection is weak. Try to sync with good signal later.',
  measurement_sync_started: 'Synchronization started',
  server_communication: 'Communicating with the server, checking conditions…',
  new_login_alert: 'Due to a new login that does not match the previous one, which is \n' +
    '{{name}}\n' +
    ', the database will be re-initialized. Previous data will be available from the backup.',
  default_alert_title: 'Alert',
  external_permission_denied: 'External location permission denied',
  external_permission_denied_backup_creation: "External location permission denied - backups won't be saved to the external directory",
  possible_crash_backup_offer: 'It is possible that due to an unexpected crash your data has been lost. Do you want to restore them? If you are not sure that data is missing, you can restore them manually in the Settings menu.',
  syncing_with_num_of_left: 'Syncing: {{num}} left',
  airplane_gps_error: "The gps signal can't be reached and you are in airplane mode. Would you like to turn it off?",
  no_measurements: 'No data recorded',
  close: 'Close',
  pin_status_save: "Saving pin's status",
  pin_save_option: 'Always save',
  pin_dont_save_option: 'Reinitialize before entering form',
  pin_save_but_delete_after_sync: 'Save states, but reinitialize after synchronization',
  shortcut_to_home_screen_title: 'Create shortcut on Home screen.',
  shortcut_to_home_screen_message: 'If you create a shortcut to this form, you will be able to open it fast and it will be available offline.',
  create_shortcut: 'create',
  no_shortcut_observations: 'No available shortcuts',
  create_shortcut_suggestion: 'You can create a shortcut at the form chooser screen.',
  delete_shortcut_title: 'Delete shortcut',
  delete_shortcut_message: 'Are you sure you want to delete this shortcut? The form will be available at the form chooser screen.',
  error_title_export: 'Export failed',
  error_text_export: 'The export file not been created :(',
  error_title_file_writing_permission: 'File writing not granted :(',
  error_text_file_writing_permission: 'Permission is needed for write the export file.',
  ask_me_later: 'Ask me later',
  ext_writing_permission: 'External writing permission',
  obm: 'OBM',
  export_finished: 'Export finished',
  start_export: 'Start export',
  export_path: 'Export path',
  message_title: 'You have unread messages',
  personal_messages: 'personal',
  comment_notifications: 'comments',
  system_messages: 'system',
  only_email: 'only email',
  jump_to_latest_observation: 'Jump to the latest form: {{observationName}}',
  tracklog: 'Tracklog',
  tracklog_update_time: 'Tracklog update time (s)',
  tracklog_time_description: 'Setting the update time to 0 stops the tracklog recording',
  tracklog_update_distance: 'Tracklog distance filter (m)',
  tracklog_distance_description: 'If other than 0, tracklog will update only if the device moved at least the amount set here.',
  not_integer: 'The number is not integer',
  copying_session_element: 'You are copying a session element',
  editing_session_element: 'You are editing a session element',
  wrong_file_uri: 'A file or image you have attached is deleted or its location may have changed. Try to remove and reattach before uploading.',
  missing_file: 'A file is missing :(',
  missing: 'is missing :(',
  error_file_copy: "Some of the files are not copied, because they don't exist!",
  session_only_blocker_message: 'This is a form that can be used only in session mode. To start the session press the button below.',
  form_creation_in_progress: 'Data recording…',
  list_time_remaining_alert: 'You have {{timeRemaining}} minutes left to finish the session of form: {{observationName}}!',
  list_time_expired_alert: 'You must finish the session creation of form: {{observationName}}!',
  copy: 'Copy',
  clipboard_message: 'Copy to clipboard successfully!',
  position_update: 'The update time of the GPS positions in the forms (s)',
  position_update_title: 'Position update interval',
  performance_alert_title: 'Performance alert',
  frequent_position_update_alert_message: 'This update interval is really fast, and can cause performance issues such as higher memory and battery usage.',
  setting_distance_filter_alert_message: 'Setting a distance to filter the coordinates will overwrite the update time',
  map_settings_modal_title: 'You can set how your data is shown here',
  tracklog_visibility_setting_title: 'Visibility of tracklogs',
  date_based_map_filter_title: 'Filter data by date',
  date_based_map_filter_start: 'Start of interval',
  date_based_map_filter_end: 'End of interval',
  date_based_map_filter_shift_button: 'shift the interval considering current time',
  tracklog_recording_in_progress: 'recording in progress',
  tracklog_not_recording: 'not recording',
  no_messages_title: "You don't have new messages",
  error_title_max_file: 'Upload error',
  error_text_max_file: 'Too large file to upload',
  tracklog_switched_on: 'Tracklog recording in progress',
  tracklog_switched_off: 'Tracklog recording turned off',
  recorded_tracklogs: 'Recorded tracklogs',
  tracklogs: 'Tracklogs',
  delete_tracklog_dialog_title: 'Are you sure about deleting this tracklog?',
  delete_all_synced_dialog_title: 'Are you sure about deleting all synced data?',
  create_backup: 'Create backup',
  successful_backup_creation: 'Successful backup creation!',
  possible_crash_external_backup_offer: 'We have found an external backup of the app. Do you want to load it? You can restore backups manually later in the Settings menu.',
  choose_backup_from_filesystem: 'Choose backup from the filesystem…',
  search_backup_manually: 'Search backup manually',
  invalid_backup_file: 'The file you picked is invalid. Make sure it is a real backup made by the application',
  no_database_assigned_to_tracklog_while_sync: 'None of the tracklogs below has a project to upload to',
  no_database_assigned_to_tracklog: 'No project assigned, not uploadable',
  showing_only_one_tracklog: 'Showing only the selected tracklog with name:',
  export_tracklog_alert_title: 'Exporting a tracklog',
  export_tracklog_alert_message: 'Are you sure about exporting this tracklog? The file "{{trackLogName}}.gpx" will be available at {{fileUri}}',
  successful_tracklog_creation: 'Successful tracklog export!',
  push_to_turn_on: 'push here to turn it on',
  push_to_turn_off: 'push here to turn it off',
  play_services_not_available: 'Google play service is not installed or has an older version',
  location_settings_not_appropriate: 'Location settings are not satisfied. Check whether you have turned the GPS off or Airplane mode on',
  weak_gps: 'The gps signal is low. Try to turn the gps off and on, or set the battery optimization for this app off.',
  measurements_visibility_setting_title: 'Visibility of observations',
  late_response_trial_call: 'The server responded slowly, it is not safe to upload data. Try again later or contact with the server administrator.',
  no_data_to_sync: 'There are no uploadable data!',
  wrong_content_uri: "Could not open the file. Try to choose this file through the storage root directories, instead of a shortcut like 'Downloads'.",
  lost_communication_invalid_refresh_token: 'The communication is lost with the server. You can still use the downloaded forms, but consider logging out and in to load more data.',
  unexpected_component_error: 'An unexpected error occurred in this component. Please contact the administrator.',
  tracklog_export: 'Tracklog export',
  position_input_hint_type: 'You are recording a {{type}}.',
  position_input_hint_point_press: 'Press long on the selected location!',
  position_input_hint_array_press: 'Press long to add a new point to the {{type}}',
  position_input_array_mode_hint: "If you'd like to set it manually, choose below to edit the existing data or make a new one.",
  position_input_mode_use_existing: 'edit',
  position_input_mode_make_new: 'restart',
  delete_last_element: 'Delete last element',
  choose_edit_mode_first: 'Choose edit mode first!',
  error_polygon_length: 'Polygon must have at least 3 points!',
  error_line_length: 'Line must have at least 2 points',
  tracklog_length: 'The length of this tracklog is {{length}}',
  session_tracklog_empty: "The recorded session tracklog doesn\\'t contain points!",
  session_tracklog_recorded: 'Session tracklog recorded successfully, length: {{length}}',
  error_title_filename: 'Filename error',
  error_text_filename: 'The filename contains prohibited character.',
  background_location_permission_title: 'Location collecting in the background',
  background_location_permission_message: 'This App uses location data to allow collecting biotic data. Allowing access to location data is necessary for the app to work correctly, even if it runs in the background, or is not currently in use.\n' +
    'On the Settings page, select "Allow all the time" to ensure the GPS function works smoothly.',
  session_tracklog_alert_title: 'Session tracklog recording',
  session_tracklog_recording_started: 'Tracklog recording started!',
  session_tracklog_optional_description: 'Turn the recording on please! This helps us measure the distance covered, a very important information in the process of data interpretation.',
  session_tracklog_record: 'Start recording',
  session_tracklog_dont_record: "Don\\'t record",
  global_tracklog_interval_null_error: "You set the tracklog\\'s update time to 0. This does not allow the global tracklog recording.",
  sync_tracklogs_manually_hint: 'If you would like to sync tracklogs, you can do it in the tracklog list.',
  camera_permission_denied: 'Camera permission denied.',
  external_folder: '{{type}} folder',
  select_external_folder_description: 'You have to mark the folder to create and save {{type}} file.',
  select: 'Select',
  storage_settings: 'Storage',
  delete_unused_files: 'Delete unused files attached before.',
  files_deleted_with_size: '{{size}} disk space has been freed',
  wkt_type: 'Data type: {{wktType}}',
  wkt_array_length: '{{wktType}} length: {{length}}',
  current_tracklog: 'Current tracklog',
  current_tracklog_length: 'The length of the current global tracklog: {{length}}',
  current_tracklog_databases: 'The tracklog will be uploaded to the following databases:',
  current_tracklog_no_database: 'No database is assigned to the tracklog. To do so, open a form related to that database.',
  current_tracklog_servers: 'The tracklog will be uploaded to the following servers:',
  current_tracklog_no_server: 'No server is assigned to the tracklog. To do so, open a form related to that server.',
  view_on_map: 'view on map',
  point: 'Point',
  line: 'Line',
  polygon: 'Polygon',
  tracklog_global_in_progress: 'Tracklog recording in progress',
  tracklog_session_in_progress: 'Session tracklog recording in progress',
  syncing_tracklogs_with_num_of_left: 'Syncing tracklogs: {{num}} left',
  root_dir_selected: "You can't set the root directory of your phone for security reasons",
  external_dir_warning: 'Warning! The selected directory will be read and written by OpenBioMaps. Do not choose any directory which contains non OBM files!',
  distance_filter: 'Distance filter (m)',
  turn_off_gps_alert_message: 'Currently the following components are using GPS',
  turn_off_gps_alert_title: 'Turn off GPS',
  location_permission_never_ask_again: 'Location permission revoked by user.',
  location_tracking_channel_name: 'Location Tracking Channel',
  location_tracking_channel_description: 'Tracks location of user',
  tracking_location_updates: 'Tracking location updates',
  pernament_sample_plots_visibility: 'visibility of permanent sample plots',
  periodical_notification_message: 'Record an observation!',
  iosBackupFolderPlace: 'Files > On my iPhone > OpenBioMaps >',
  choose_backup_from_filesystem_positive: 'File browsing',
  choose_backup_from_filesystem_message: 'Backup file browsing from Filesystem',
  choose_backup_from_filesystem_title: 'Choose Backup From Filesystem',
  gps: 'GPS',
  auto_backup_description: 'Write the data collected in the application to the backup directory specified during installation, each time a new observation is recorded.',
  create_backup_description: 'Make occasional backups of current data to the backup directory specified during installation.',
  delete_unused_files_description: 'It cleans up any stuck attachments (photos) not related to the data.',
  distance_filter_description: 'It will not update the GPS position if you move less than the specified distance.',
  notification_sound_form_save: 'Sound signal on successful recording data'
}
import { useCallback, useEffect, useMemo } from 'react';

import { useDispatch } from 'react-redux';

import { GPS_OBSERVER } from './Constants';
import { useShallowEqualSelector } from './utils/useShallowEqualSelector';
import { getSelectedDatabase, getSelectedServer, isServerOfDatabaseInSelectedDatabases } from './utils';
import { useTrackLogPositionUpdate } from './utils/useTrackLogPositionUpdate';
import { 
  addPointToGlobalTrackLog,
  deleteCurrentTrackLog,
  editCurrentTrackLog,
  initCurrentTrackLog,
  saveCurrentGlobalTrackLog,
  stopTrackLogRecording, 
  setGpsPosition
} from './actions';

const DEFAULT_GLOBAL_TRACKLOG = {
  trackLogId: null,
  trackLogArray: [],
  selectedDatabases: [],
  startTime: null,
  endTime: null,
};

const GlobalTrackLogHandler = () => {
  const dispatch = useDispatch();

  const isRecording = useShallowEqualSelector((state) => state.trackLog.recordingInProgress);
  const currentTrackLog = useShallowEqualSelector((state) => state.trackLog.currentTrackLog);
  const position = useShallowEqualSelector((state) => state.gps.position);
  const observers = useShallowEqualSelector((state) => state.gps.observers);
  const servers = useShallowEqualSelector((state) => state.servers.data);

  const hasCurrentTracklog = useMemo(() => !!currentTrackLog, [currentTrackLog]);

  const hasRecordedTrackPoints = useMemo(() => {
    if (!currentTrackLog) return false;
    return currentTrackLog.trackLogArray.length;
  }, [currentTrackLog]);

  const isObservingGps = useMemo(() => observers.includes(GPS_OBSERVER.TRACKLOG), [observers]);

  const validPosition = useMemo(() => {
    if (!isObservingGps || !Object.values(position).every((item) => item)) return null;

    if (position.accuracy >= 100) return null;

    return position;
  }, [isObservingGps, position]);
  
  const shouldUpdatePosition = useCallback(() => {
    if (!isRecording || !hasCurrentTracklog) return false;

    return true;
  }, [isRecording, hasCurrentTracklog])

  const selectedDatabaseData = useMemo(() => {
    const selectedServer = getSelectedServer(servers);
    const selectedDatabase = getSelectedDatabase(selectedServer);

    if (!Object.values(selectedDatabase).length) {
      return null;
    }

    return ({
      serverName: selectedServer.name,
      serverId: selectedServer.id,
      databaseName: selectedDatabase.name,
      databaseId: selectedDatabase.id,
      projectUrl: selectedDatabase.projectUrl,
    });
  }, [servers]);

  const hasSelectedDatabases = useMemo(() => {
    if (!hasCurrentTracklog) return false;
    return !!currentTrackLog.selectedDatabases.length;
  }, [hasCurrentTracklog, currentTrackLog]);
  
  const hasNewSelectedDatabaseData = useMemo(() => {
    if (!hasSelectedDatabases || !selectedDatabaseData) return false;
    return !isServerOfDatabaseInSelectedDatabases(currentTrackLog.selectedDatabases, selectedDatabaseData);
  }, [hasSelectedDatabases, currentTrackLog, selectedDatabaseData]);

  const updatedPosition = {
    ...validPosition,
    timestamp: Date.now()
  }
  useTrackLogPositionUpdate(setGpsPosition, updatedPosition, shouldUpdatePosition())
  useTrackLogPositionUpdate(addPointToGlobalTrackLog, validPosition, shouldUpdatePosition());

  useEffect(() => {
    if (isRecording && !hasCurrentTracklog) {
      dispatch(initCurrentTrackLog({ 
        ...DEFAULT_GLOBAL_TRACKLOG,
        startTime: Date.now(),
        selectedDatabases: selectedDatabaseData ? [selectedDatabaseData] : [],
      }));
    }

    if (!isRecording && hasCurrentTracklog) {
      if (hasRecordedTrackPoints) {
        dispatch(saveCurrentGlobalTrackLog());
      } else {
        dispatch(deleteCurrentTrackLog());
      }
    }
  }, [isRecording, hasCurrentTracklog, hasRecordedTrackPoints, selectedDatabaseData, dispatch]);

  useEffect(() => {
    if (isRecording && hasNewSelectedDatabaseData) {
      dispatch(editCurrentTrackLog({
        selectedDatabases: [...currentTrackLog.selectedDatabases, selectedDatabaseData],
      }));
    }
  }, [isRecording, hasNewSelectedDatabaseData, currentTrackLog, selectedDatabaseData, dispatch]);

  useEffect(() => {
    if (isRecording && hasCurrentTracklog && !isObservingGps) {
      dispatch(stopTrackLogRecording());
    }
  }, [isRecording, hasCurrentTracklog, isObservingGps, dispatch]);

  return null;
};

export default GlobalTrackLogHandler;

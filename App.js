import React, { Fragment } from 'react';
import { ActivityIndicator, SafeAreaView, StatusBar } from 'react-native';

import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/es/integration/react';
import * as Sentry from '@sentry/react-native';

import { store, persistor } from './src/config/setupStore';
import RootNavigator from './src/RootNavigator';
import MemoryHandler from './src/MemoryHandler';
import NetworkHandler from './src/NetworkHandler';
import ListTimerAlertHandler from './src/ListTimerAlertHandler';
import GlobalTrackLogHandler from './src/GlobalTrackLogHandler';
import SessionTrackLogHandler from './src/SessionTrackLogHandler';
import GpsHandler from './src/GpsHandler';
import { Colors } from './src/colors';
import NavigationHandler from './src/NavigationHandler';
import { bootstrappingStore } from './src/utils/bootstrappingStore';
import AirplaneModeHandler from './src/AirplaneModeHandler';

Sentry.init({ 
  dsn: 'https://18e9dd476cf440789e319457ff0f4e3a@o1084726.ingest.sentry.io/6094710', 
});

const App = () => (
  <Provider store={store}>
    <PersistGate persistor={persistor}>
      {(bootstrapped) => {
        if (bootstrapped) {
          bootstrappingStore();
          return (
            <SafeAreaView style={{ flex: 1, backgroundColor: Colors.mantisGreen }}>
              <>
                <StatusBar backgroundColor={Colors.mantisGreen} barStyle="light-content" />
                <RootNavigator ref={NavigationHandler.setTopLevelNavigator} />
                <MemoryHandler />
                <NetworkHandler />
                <ListTimerAlertHandler />
                <GlobalTrackLogHandler />
                <SessionTrackLogHandler />
                <GpsHandler />
                <AirplaneModeHandler />
              </>
            </SafeAreaView>
          );
        }
        return <ActivityIndicator />;
      }}
    </PersistGate>
  </Provider>
);

export default Sentry.wrap(App);

#! /usr/bin/env node

/* .yaml fájlokból kiírt js object fájlokat készít
 *
 * */

// npm install node-read-yaml

var files= require('fs').readdirSync('.');
files = files.filter(a => a.match(/yaml$/) )

const readSync = require("node-read-yaml").sync;
 
files.forEach(function(filename) {

    let output_js = filename.replace(/yaml$/,"js");

    try {

        const doc = readSync(filename);

        var util = require('util');
        require('fs').writeFileSync(
            output_js,
            'export default ',
            function (err) {
                if (err) {
                    console.error('Crap happens');
                }
            }
        );
        
        require('fs').appendFileSync(
            output_js,
            util.inspect(doc, false, 2, false),
            'utf-8',
            function (err) {
                if (err) {
                    console.error('Crap happens');
                }
            }
        );

    } catch (err) {
        
        console.log(err);

    }

});
